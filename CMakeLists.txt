#############################################################################
# This file is part of DistAIX
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


cmake_minimum_required(VERSION 3.0)
project(distaix)

option(WITH_VILLAS "Build with VILLASnode support in agents" ON )
option(WITH_DB "Build with database support" ON )

#aux_source_directory(./src SRC_LIST)
file(GLOB_RECURSE SRC_LIST ./src/*.cpp)

#find MPI library
find_package(MPI REQUIRED)

#find boost libraries
set(Boost_NO_BOOST_CMAKE ON)
find_package(Boost REQUIRED)
#add_definitions(-DBOOST_ALLOW_DEPRECATED_HEADERS)

#save previous compiler to be used by library in libs folder
set(OLD_CMAKE_CXX_COMPILER ${CMAKE_CXX_COMPILER})

#set compiler and compiler flags
set(CMAKE_CXX_COMPILER ${MPI_CXX_COMPILER})
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -O0 -std=c++17 -Wall")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g -O0 -std=c++17 -Wall")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/bin")

#add feature infos
include(FeatureSummary)
include(FindPkgConfig)
find_package(PkgConfig REQUIRED)
add_feature_info(VILLAS   WITH_VILLAS   "Build with VILLASnode support in agents")
add_feature_info(DATABASE WITH_DB     "Build with database support")
feature_summary(WHAT ALL VAR FEATURES)
message(STATUS "${FEATURES}")

#add libraries as sub dir (currently for fbscomp library)
add_subdirectory(libs)

if(WITH_DB)
    find_package(Protobuf 3.3.2 REQUIRED)
    add_subdirectory(proto)
    #add protobuf .cc files to source list of distaix
    list(APPEND SRC_LIST ${PROTO_SRC})
endif()

set(LIBRARIES_DISTAIX
        ${Boost_LIBRARIES}
        repast_hpc
        fbscomp
)

set(INCLUDE_DIRS_DISTAIX
        ./include
        ${MPI_CXX_INCLUDE_PATH}
        ${Boost_INCLUDE_DIRS}
        libs
        libs/fbs-components/include
)

if(WITH_DB)
    #add a definition to use in the code
    add_definitions(-DUSE_DB)
    list(APPEND LIBRARIES_DISTAIX dbconnector ${Protobuf_LIBRARIES} ${Protobuf_PROTOC_LIBRARIES})
    list(APPEND INCLUDE_DIRS_DISTAIX ${Protobuf_INCLUDE_DIRS} proto)
endif()

if(WITH_VILLAS)
    #add definition to use in the code
    add_definitions(-DWITH_VILLAS)
    if(NANOMSG_FOUND)
        list(APPEND LIBRARIES_DISTAIX villas ${NANOMSG_LIBRARIES})
        link_directories(${NANOMSG_LIBRARY_DIRS})
    else()
        list(APPEND LIBRARIES_DISTAIX villas)
    endif()
    list(APPEND INCLUDE_DIRS_DISTAIX  libs/villasnode/include)

endif()

add_executable(distaix ${SRC_LIST})

target_include_directories(distaix SYSTEM PUBLIC ${INCLUDE_DIRS_DISTAIX})

target_link_libraries( distaix PUBLIC ${LIBRARIES_DISTAIX})

#############################################################################
#
# This file is part of DistAIX
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

#!/bin/bash

WITH_DB=false
WITH_VILLAS=false
HELP=false

for i in "$@"
do
case $i in
    -db)
    WITH_DB=true
	;;
	-villas)
	WITH_VILLAS=true
	;;
	-help)
	HELP=true
	;;
	*)
	echo "Unkown option"
	exit
	;;
esac
done

if [ "$HELP" = true ]
then
    echo ""
    echo "INFO: Build and install script for DistAIX. If compile is successful, binary is placed in ../bin/ folder."
    echo "USAGE: Options are:"
    echo "-db:\t compile with database"
    echo "-villas:\t compile with villasnode library"
    echo "-help:\t print this message and exit"
    echo ""
else
    echo Executing Pre Build commands ...
    if [ "$WITH_DB" = true ] && [ "$WITH_VILLAS" = true ]
    then
        cmake -DWITH_DB=1 -DWITH_VILLAS=1 ..
    elif [ "$WITH_DB" = true ] && [ "$WITH_VILLAS" = false ]
    then
        cmake -DWITH_DB=1 -DWITH_VILLAS=0 ..
    elif [ "$WITH_DB" = false ] && [ "$WITH_VILLAS" = true ]
    then
        cmake -DWITH_DB=0 -DWITH_VILLAS=1 ..
    elif [ "$WITH_DB" = false ] && [ "$WITH_VILLAS" = false ]
    then
        cmake -DWITH_DB=0 -DWITH_VILLAS=0 ..
    fi
    echo Done

    echo Executing Build commands ...
    make -j
    echo Done
fi

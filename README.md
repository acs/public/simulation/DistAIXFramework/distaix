#  <img src="doc/DistAIX.png" width=100 /> DistAIX <br/> Distributed Agent-based Simulation of Complex Power Systems
[![pipeline status](https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/distaix/badges/master/pipeline.svg)](https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/distaix/commits/master)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

DistAIX is a simulator for cyber-physical power systems that makes use of high performance computing techniques to scale up the simulation.
An agent-based modeling and simulation approach is applied to model the behavior of the electrical system as well as distributed control and decision-making processes.
Communication between participants of the system (agents) is also modeled and simulated.

DistAIX uses the framework [RepastHPC](https://github.com/Repast/repast.hpc) as basis for scalable agent-based modeling and simulation based on the Message Passing Interface (MPI).
It relies on [Cassandra](http://cassandra.apache.org/), [PostgreSQL](https://www.postgresql.org/) and [Google Protocol Buffers](https://developers.google.com/protocol-buffers/) for a scalable management of simulation results.

## The following documentation is available for DistAIX:
- [Repository structure](doc/repo.md)
- [System preparation](doc/preparation.md)
- [Installation guide](doc/install.md)
- [Configuration parameters](doc/config.md)
- [Scenario files](doc/scenarios.md)
- [Execution guide](doc/exec.md)


## Publications

We kindly ask all academic publications employing components of DistAIX to cite at least one of the following papers:

- S. Kolen, T. Isermann, S. Dähling and A. Monti, "Swarm behavior for distribution grid control," 2017 IEEE PES Innovative Smart Grid Technologies Conference Europe (ISGT-Europe), Torino, 2017, pp. 1-6, [DOI: 10.1109/ISGTEurope.2017.8260160](https://doi.org/10.1109/ISGTEurope.2017.8260160)
- S. Kolen, S. Dähling, T. Isermann, and A. Monti, “Enabling the Analysis of Emergent Behavior in Future Electrical Distribution Systems Using Agent-Based Modeling and Simulation,” Complexity, vol. 2018, Article ID 3469325, 16 pages, 2018, [DOI: 10.1155/2018/3469325](https://doi.org/10.1155/2018/3469325)
- S. Dähling, S. Kolen, and A. Monti, "Swarm-based automation of electrical power distribution and transmission system support", IET Cyber-Physical Systems: Theory and Applications, Volume: 3, Issue: 4, pp. 212-223, 2018, [DOI: 10.1049/iet-cps.2018.5001](https://doi.org/10.1049/iet-cps.2018.5001)
- S. Happ, S. Dähling, and A. Monti, "Scalable assessment method for agent-based control in cyber-physical distribution grids", IET Cyber-Physical Systems: Theory and Applications, Volume: 5, Issue: 3, pp. 283-291, 2020, [DOI: 10.1049/iet-cps.2019.0096](https://doi.org/10.1049/iet-cps.2019.0096)

## Copyright

2022, Institute for Automation of Complex Power Systems, EONERC  

## License

This project is released under the terms of the [GPL version 3](COPYING.md).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

# Further software related to DistAIX

## DistAIX Scenario Generator

The DistAIX Scenario Generator is a Python tool to generate scenarios in the format of the DistAIX simulator.
The tool is capable of creating new electrical grids / scenarios based on existing grids as well as from scratch.
Custom topologies (networks of Slack/Trafo/Nodes) can be created and assembled with different types of consumers and producers (Load/CHP/EV/Wind...).

The source code and documentation can be obtained here:
[https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/scenariogenerator](https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/scenariogenerator)

## DistAIXweb

DistAIXweb is a web application that enables browsing, visualizing, and downloading of DistAIX simulation results stored in a database.
It consists of a frontend and a backend part.
The backend provides a REST-style interface with methods to view the metadata and get the results, which is used by the frontend to offer an easy way to access simulation results.

The source code and documentation can be obtained here: 
[https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/distaixweb](https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/distaixweb)

# Contact
[![EONERC ACS Logo](doc/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Felix Wege](mailto:felix.wege@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)  










/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/
#include "edges/cable.h"

Cable::Cable(
        Agent *source,
        Agent *target,
        bool _is_non_local,
        std::string _id,
        double _step_size,
        int _model_type,
        std::string &_subtype,
        struct data_props _d_props,
        float _r,
        float _x,
        float _b,
        float _g,
        float length,
        float _long_term_rate,
        float _emergency_rate
): Edge<Agent>(source, target, _is_non_local, _id, _step_size), model_type(_model_type), subtype(_subtype) {

    data_props temp_d_props = _d_props;
    bool logging = ((!is_non_local && !no_losses) || (is_non_local && !no_losses && RepastEdge::source()->id_.id() > RepastEdge::target()->id_.id()));
    if(!logging){
        //deactivate all logging if this edge is non local and not used for computation in FBS
        temp_d_props.log.logging = false;
        temp_d_props.result.csv_results = false;
        temp_d_props.result.db_results = false;
    }

    IO = new EdgeIO(id, temp_d_props);

    t_start = 0.0;
    t_step = step_size;
    t_next = t_start;

    //path_log_file_edge = "runlog/edges/cable_" + id + ".log";
    //path_result_file_edge = "results/edges/cable_" + id + ".csv";

    IO->init_logging();

    number_of_iterations = 0;
    set_cable_type(_r, _x, _b,_g,  length, _long_term_rate, _emergency_rate);

}

Cable::~Cable() {
    IO->finish_io();
}


void Cable::set_cable_type(float _r, float _x, float _b, float _g, float _length, float _long_term_rate, float _emergency_rate) {


    IO->log_info("Creating cable of subtype " + subtype + "; Length: " + std::to_string(_length));

    model_data.length = _length; // in km
    model_data.Sr = _long_term_rate;
    model_data.Sr_emergency = _emergency_rate;


    if(subtype == "1" || subtype == "NO_LOSSES"){
        no_losses = true;
    } else {
        no_losses = false;
    }

    model_data.R = _r * model_data.length; // _r is per km
    model_data.X = _x * model_data.length; // _x is per km
    model_data.B = _b * pow(10, -6) * model_data.system_omega * model_data.length; // _b is per km
    model_data.G = _g * model_data.length; // _g is per km

    IO->log_info("Cable params:  no_losses=" + std::to_string(no_losses) +
                 " | R = " + std::to_string(model_data.R) +
                 " | X = " + std::to_string(model_data.X) +
                 " | B = " +std::to_string(model_data.B) +
                 " | G = " + std::to_string(model_data.G) +
                 " | length = " + std::to_string(_length) +
                 " | Sr = " + std::to_string(_long_term_rate) +
                 " | Sr_emergency = " + std::to_string(_emergency_rate));

    line_model = new Transmission(model_type, step_size, 0, _long_term_rate, model_data.R, model_data.X, model_data.G, model_data.B);
}


void Cable::calculate(double _voltage_input_real, double _voltage_input_imag, double _current_input_real, double _current_input_imag){
    // calculate the losses over this cable

    //set inputs
    model_data.v1_im = _voltage_input_imag;
    model_data.v1_re = _voltage_input_real;
    model_data.i1_im = _current_input_imag;
    model_data.i1_re = _current_input_real;

    if(no_losses){
        //inputs = outputs
        model_data.v2_im = model_data.v1_im;
        model_data.v2_re = model_data.v1_re;
        model_data.i2_im = model_data.i1_im;
        model_data.i2_re = model_data.i1_re;
    }
    else{


        IO->log_info("Cable " + id + ": calculating cable for voltage: (" + std::to_string(model_data.v1_re) +
        ","  + std::to_string(model_data.v1_im) + ") and current (" + std::to_string(model_data.i1_re) +
        "," + std::to_string(model_data.i1_im) + ")");

        line_model->solve(model_data.v1_re, model_data.v1_im, model_data.i1_re, model_data.i1_im,
                          model_data.v2_re, model_data.v2_im, model_data.i2_re, model_data.i2_im);

        IO->log_info("Cable " + id + ": calculated voltage: (" + std::to_string(model_data.v2_re) +
                     ","  + std::to_string(model_data.v2_im) + ") and current (" + std::to_string(model_data.i2_re) +
                     "," + std::to_string(model_data.i2_im) + ")");
    }

    number_of_iterations++;

}

void Cable::step(Time_measurement *_tm_csv, Time_measurement * _tm_db, Time_measurement * _tm_db_serialize, Time_measurement * _tm_db_add){
    //proceed one time step

    line_model->post_processing();
    /* update measurement variables */
    model_data.V1 = sqrt(3.0) * sqrt(model_data.v1_re * model_data.v1_re + model_data.v1_im * model_data.v1_im);
    model_data.V2 = sqrt(3.0) * sqrt(model_data.v2_re * model_data.v2_re + model_data.v2_im * model_data.v2_im);
    model_data.I1 = sqrt(model_data.i1_re * model_data.i1_re + model_data.i1_im * model_data.i1_im);
    model_data.I2 = sqrt(model_data.i2_re * model_data.i2_re + model_data.i2_im * model_data.i2_im);
    model_data.S1 = model_data.V1 * model_data.I1;
    model_data.S2 = model_data.V2 * model_data.I2;
    model_data.Srel = sqrt(3.0) * model_data.S2 / model_data.Sr * 100;
    model_data.Srel_emergency = sqrt(3.0) * model_data.S2 / model_data.Sr_emergency * 100;
    model_data.Slosses = model_data.S1 - model_data.S2;
    if (model_data.Slosses < 0) {
        model_data.Slosses = -model_data.Slosses;
    }

    IO->save_result(&model_data, first_step, _tm_csv, _tm_db, _tm_db_serialize, _tm_db_add);

    if(first_step) {
        first_step = false;
    }

    IO->log_info("Step: " + std::to_string(t_next) + "s");

    number_of_iterations = 0;
    t_next += t_step;
    IO->update_t(t_next);
}

std::complex<double> Cable::get_voltage_output()
{
    return std::complex<double>(model_data.v2_re, model_data.v2_im);
}

std::complex<double> Cable::get_leakage_current()
{
    if (no_losses) {
        std::complex<double> i_l(0,0);
        return i_l;
    } else {
        double i_re, i_im;
        line_model->get_leackage_current(i_re, i_im);
        std::complex<double> i_l(i_re, i_im);
        return i_l;
    }
}





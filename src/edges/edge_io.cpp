/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "edges/edge_io.h"
#include "model/config.h"
#include "agents/agent_datastructs.h"

#ifdef USE_DB
#include "DBException.h"
#include "cable.pb.h"
#endif

EdgeIO::EdgeIO(std::string &_id, struct data_props _d_props) : IO_object(_d_props), id(_id) {


}

void EdgeIO::init_logging() {
    init_logfile();
    init_resultfile();
    update_t(0.0);

    //if required, set up DB Saving
    if(d_props.result.db_results){
        set_result_types();
    }
}

void EdgeIO::set_result_file_header() {

    if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
        result_stream << "sim time[s],i1.re [A],i1.im [A],i2.re [A],i2.im [A],Slosses [W],Srel" << std::endl;
    }
    else{
        result_stream << "sim time[s],Slosses [W],Srel" << std::endl;
    }
}


void EdgeIO::set_result_types() {
#ifdef USE_DB
    // set up DB Saving if required
    if(d_props.result.db_results){
        try{
            CableResult proto_res;
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(0)->name(), "A", "i2_im");
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(1)->name(), "A", "i2_re");
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(2)->name(), "A", "i1_re");
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(3)->name(), "A", "i1_im");
            }
            d_props.result.dbconn->addResultType(proto_res.descriptor()->field(4)->name(), "W", "slosses");
            d_props.result.dbconn->addResultType(proto_res.descriptor()->field(5)->name(), "", "srel");
            d_props.result.dbconn->addResultType(proto_res.descriptor()->field(6)->name(), "-", "used");
            //meta
            d_props.result.dbconn->addResultType("length","km","length of cable");
            d_props.result.dbconn->addResultType("B","","line susceptance");
            d_props.result.dbconn->addResultType("C","","line capacitance");
            d_props.result.dbconn->addResultType("G","","line conductance");
            d_props.result.dbconn->addResultType("L","","line inductance");
            d_props.result.dbconn->addResultType("R","","line resistance");
            d_props.result.dbconn->addResultType("X","","line reactance");
            d_props.result.dbconn->addResultType("long_term_rate","VA","long term rate of line");
            d_props.result.dbconn->addResultType("emergency_rate","VA","emergency rate of line");

        }catch(DBException &e){
            std::cout << "[DBEXCEPTION ON CABLE "<<id<<"] "<< e.what()<<std::endl;
        }
    }

#endif
}


void EdgeIO::save_meta(void *_model_data) {
#ifdef USE_DB
    try {
        if (d_props.result.db_results) {
            auto *model_data = (PiLine_data *) _model_data;

            d_props.result.dbconn->addAgentMetaEntry("length",id,TYPE_CABLE_INT,model_data->length);
            d_props.result.dbconn->addAgentMetaEntry("B",id,TYPE_CABLE_INT,model_data->B);
            d_props.result.dbconn->addAgentMetaEntry("C",id,TYPE_CABLE_INT,model_data->C);
            d_props.result.dbconn->addAgentMetaEntry("G",id,TYPE_CABLE_INT,model_data->G);
            d_props.result.dbconn->addAgentMetaEntry("L",id,TYPE_CABLE_INT,model_data->L);
            d_props.result.dbconn->addAgentMetaEntry("R",id,TYPE_CABLE_INT,model_data->R);
            d_props.result.dbconn->addAgentMetaEntry("X",id,TYPE_CABLE_INT,model_data->X);
            d_props.result.dbconn->addAgentMetaEntry("long_term_rate", id, TYPE_CABLE_INT, model_data->Sr);
            d_props.result.dbconn->addAgentMetaEntry("emergency_rate", id, TYPE_CABLE_INT, model_data->Sr_emergency);
        }
    }
    catch(const DBException &ex){
        std::cerr  << "WARNING: Agent " << id << " caught DBException in AgentIO:" << ex.what() << std::endl;
        std::cout  << "WARNING: Agent " << id << " caught DBException in AgentIO:" << ex.what() << std::endl;
    }
#endif
}

void EdgeIO::save_result(void *_model_data, bool first_step, Time_measurement *_tm_csv, Time_measurement * _tm_db, Time_measurement * _tm_db_serialize, Time_measurement * _tm_db_add) {


    _tm_csv->start();
    save_result_csv(_model_data);
    flush_result();
    _tm_csv->stop();

    _tm_db->start();
    save_result_db(_model_data, first_step, _tm_db_serialize, _tm_db_add);
    _tm_db->stop();

}

void EdgeIO::save_result_csv(void *_model_data) {
    if(d_props.result.csv_results) {
        auto *model_data = (PiLine_data *) _model_data;

        result_stream << t_next;
        if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
            result_stream << "," << model_data->i1_re << ","
                        << model_data->i1_im << ","
                        << model_data->i2_re << ","
                        << model_data->i2_im;
        }
        result_stream<< ","
                   << model_data->Slosses << ","
                   << model_data->Srel << std::endl;
    }

}

void EdgeIO::save_result_db(void *_model_data, bool first_step, Time_measurement * _tm_db_serialize, Time_measurement * _tm_db_add) {
#ifdef USE_DB

    CableResult proto_res;
    auto *model_data = (PiLine_data *) _model_data;
    if(d_props.result.db_results){
        _tm_db_serialize->start();
        int at = TYPE_CABLE_INT;

        if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
            proto_res.set_i2_im(model_data->i2_im);
            proto_res.set_i2_re(model_data->i2_re);
            proto_res.set_i1_re(model_data->i1_re);
            proto_res.set_i1_im(model_data->i1_im);
        }
        proto_res.set_slosses(model_data->Slosses);
        proto_res.set_srel(model_data->Srel);
        proto_res.set_used(true);

        int size = proto_res.ByteSize();
        void *buffer = nullptr;
        if(size >0) {
            buffer = malloc(size);
            proto_res.SerializeToArray(buffer, size);
        }
        _tm_db_serialize->stop();
        if(buffer != nullptr) {
            _tm_db_add->start();
            d_props.result.dbconn->addResultSerialized(id, at, t_next, buffer, size, first_step);
            //free buffer
            free(buffer);
            _tm_db_add->stop();
        }

        if(first_step){
            save_meta(&model_data);
        }
    }

#endif
}
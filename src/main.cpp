/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/model.h"
#include <boost/exception/all.hpp>

int main(int argc, char** argv){

	#ifdef DEBUG_GDB
          int i = 0;
          char hostname[256];
          gethostname(hostname, sizeof(hostname));
          printf("PID %d on %s ready for attach\n", getpid(), hostname);
          fflush(stdout);
          while (0 == i) sleep(5);
        #endif

	try {

		std::string configFile = argv[1]; // The name of the configuration file is Arg 1
		std::string propsFile = argv[2]; // The name of the properties file is Arg 2

		boost::mpi::environment env(argc, argv);
		boost::mpi::communicator world;

		repast::RepastProcess::init(configFile);

		//check if IFHT model is selected
		repast::Properties* props = new repast::Properties(propsFile, argc, argv, &world);
		std::string scenario = props->getProperty("dir.scenario");

		Model *model = new Model(propsFile, argc, argv, &world);
		repast::ScheduleRunner &runner = repast::RepastProcess::instance()->getScheduleRunner();

		model->init();
		model->initSchedule(runner);

		runner.run();

		delete model;

		repast::RepastProcess::instance()->done();

	}
	catch (const boost::exception & err){
		std::cerr << "Main : boost exception caught. Abort. \n" << boost::diagnostic_information(err) << std::endl;
		MPI_Abort(MPI_COMM_WORLD, -1);
	}
	catch(const std::exception &err){
		std::cerr << "Main : std::exception caught. Abort. \n" << err.what() << std::endl;
		MPI_Abort(MPI_COMM_WORLD, -1);
	}

	
}

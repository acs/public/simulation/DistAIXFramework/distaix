/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include <vector>
#include <list>
#include "model/model_creator.h"
#include "model/model.h"

/*! \brief Constructor of Model_creator class
 * */
Model_creator::Model_creator(boost::multi_array<int, 2> *_scenario_file_components_content,
         boost::multi_array<std::string, 1> *_scenario_file_components_subtypes,
         boost::multi_array<double, 2> *_scenario_file_components_attributes,
         boost::multi_array<int, 2> *_scenario_file_el_grid_content,
         int _world_size,
         std::vector<int> &_transformers, std::string &_distribution_method,
         std::vector<Agent *> *_agents_scheduling,
         int _rank, repast::SharedContext<Agent> *_context, ModelIO *_IO, Model *_model)
{

    distribution_method = _distribution_method;
    agents_scheduling = _agents_scheduling;
    transformers = _transformers;
    rank = _rank;
    context = _context;
    IO=_IO;
    world_size = _world_size;
    model = _model;

    scenario_file_components_content = _scenario_file_components_content;
    scenario_file_components_subtypes = _scenario_file_components_subtypes;
    scenario_file_components_attributes = _scenario_file_components_attributes;
    scenario_file_el_grid_content = _scenario_file_el_grid_content;

    current_workitem = 0;
    agent_count = 0;
    number_of_nodes = 0;
    number_of_components = 0;

    //split the MPI communicator into groups that can each allocate a shared memory region:
    MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, rank, MPI_INFO_NULL, &mc_comm);
    //determine new worldsize and rank of this process for the new communicator
    MPI_Comm_rank(mc_comm, &mc_rank);
    MPI_Comm_size(mc_comm, &mc_worldsize);
    //print debug info
    //IO->log_info("mc_rank " + std::to_string(mc_rank) + ": after MPI_Comm_split: mc_rank = "
    //                + std::to_string(mc_rank) + " mc_worldsize = " + std::to_string(mc_worldsize));

    //integer division intended
    edges_per_rank = scenario_file_el_grid_content->shape()[1] / mc_worldsize;

}

/*! \brief Destructor of Model_creator class frees all dynamically allocated memory
 * */
Model_creator::~Model_creator(){
    
    free(node_depth);
    free(hop_depth);
    free(agent_workitem_relation);
    
    MPI_Win_free(&shared_mem_window_adjacent_nodes);
    MPI_Win_free(&shared_mem_window_scheduled_nodes);
    
    MPI_Comm_free(&mc_comm);

    for(unsigned int i = 0; i < number_of_workitems; ++i) {
        child_workitems[i].clear();
    }
    

}

/*! \brief Allocate MPI Shared Memory for adjacent nodes and scheduled nodes arrays
 * */
void Model_creator::allocate_shared_memory() {
    MPI_Aint size_adjacent_nodes;
    MPI_Aint size_scheduled_nodes;
   
    if(mc_rank == 0) {
        size_adjacent_nodes = (long long int) number_of_nodes * (long long int) sizeof(int);
        size_scheduled_nodes = (long long int) number_of_nodes * (long long int) sizeof(unsigned int);
        
        int ret = 0;
        char retstring[200];
        int retstringlength;
        ret = MPI_Win_allocate_shared(size_adjacent_nodes, sizeof(int), MPI_INFO_NULL,
                                    mc_comm, &adjacent_nodes_base_pointer, &shared_mem_window_adjacent_nodes);
        MPI_Error_string(ret,retstring,&retstringlength);
        ret = MPI_Win_allocate_shared(size_scheduled_nodes, sizeof(unsigned int), MPI_INFO_NULL,
                                      mc_comm, &scheduled_nodes_base_pointer, &shared_mem_window_scheduled_nodes);
        MPI_Error_string(ret,retstring,&retstringlength);
    }
    else {
        int disp_unit_adjacent_nodes;
        int disp_unit_scheduled_nodes;
        int ret = 0;
        char retstring[200];
        int retstringlength;

        ret = MPI_Win_allocate_shared(0, sizeof(int), MPI_INFO_NULL,
                                    mc_comm, &adjacent_nodes_base_pointer, &shared_mem_window_adjacent_nodes);
        MPI_Error_string(ret,retstring,&retstringlength);
        ret = MPI_Win_allocate_shared(0, sizeof(int), MPI_INFO_NULL,
                                     mc_comm, &scheduled_nodes_base_pointer, &shared_mem_window_scheduled_nodes);
        MPI_Error_string(ret,retstring,&retstringlength);
        ret = MPI_Win_shared_query(shared_mem_window_adjacent_nodes, 0, &size_adjacent_nodes, &disp_unit_adjacent_nodes,
                                    &adjacent_nodes_base_pointer);
        MPI_Error_string(ret,retstring,&retstringlength);
        ret = MPI_Win_shared_query(shared_mem_window_scheduled_nodes, 0, &size_scheduled_nodes, &disp_unit_scheduled_nodes,
                                   &scheduled_nodes_base_pointer);
        MPI_Error_string(ret,retstring,&retstringlength);
    }
    number_of_adjacent_nodes = (int*) adjacent_nodes_base_pointer;
    node_agents_scheduled = (unsigned int*) scheduled_nodes_base_pointer;
    MPI_Barrier(mc_comm);

    //init scheduled nodes
    if(mc_rank == 0){
        std::fill_n(node_agents_scheduled, number_of_nodes,0);
    }
    MPI_Barrier(mc_comm);

}

/*! \brief determine the adjacent nodes of each node by parsing the grid topology input file
 * */
void Model_creator::determine_adjacent_nodes() {
    unsigned long N = scenario_file_el_grid_content->shape()[1];

    for(unsigned int i = 0; i<N; i++){
        int id1 = scenario_file_el_grid_content->data()[i+N*0]; //(id1_index);
        int id2 = scenario_file_el_grid_content->data()[i+N*1]; //(id2_index);

        // if both ids are in the node-namespace [1,number_of_nodes], a node-to-node connection is found
        if(id1 <= (int) number_of_nodes && id2 <= (int) number_of_nodes) {

            //add nodes to adjacent nodes of other node
            //IO->log_info("Node " + std::to_string(id1) + " adj " + std::to_string(id2));
            adjacent_nodes[id1 - 1].push_back(id2);
            adjacent_nodes[id2 - 1].push_back(id1);
        }
    }


}

/*! \brief Determines number of adjacent nodes for each node based on edge counting
*
* This Function iterates over all edges in the scenario files counting how many node-to-node edges were found.
* The number of those edges determines the number of adjacent nodes for each node.
* */
void Model_creator::determine_number_of_adjacent_nodes(){

    //std::vector<int> number_of_adjacent_nodes_local(number_of_nodes, 0);
    auto * number_of_adjacent_nodes_local = (int*) malloc(number_of_nodes * sizeof(int));
    //std::fill_n(number_of_adjacent_nodes_local, number_of_nodes,0);
    for(unsigned int k = 0; k<number_of_nodes; k++){
        number_of_adjacent_nodes_local[k] = 0;
    }

    if(mc_rank == 0) {
        std::fill(number_of_adjacent_nodes, number_of_adjacent_nodes + number_of_nodes, 0);
    }
    MPI_Barrier(mc_comm);
    unsigned long N = scenario_file_el_grid_content->shape()[1];
    IO->log_info("edges_per_rank = " + std::to_string(edges_per_rank) + "; number of edges = "
                                                                        + std::to_string(N));
    IO->log_info("number_of_nodes = " + std::to_string(number_of_nodes));


    for(unsigned int i = (unsigned int) mc_rank * edges_per_rank; i <
            ((unsigned int) mc_rank + 1) * edges_per_rank; i++){
        //boost::array<boost::multi_array<int, 2>::index, 2> id1_index = {{0, i}};
        //boost::array<boost::multi_array<int, 2>::index, 2> id2_index = {{1, i}};

        int id1 = scenario_file_el_grid_content->data()[i+N*0]; //(id1_index);
        int id2 = scenario_file_el_grid_content->data()[i+N*1]; //(id2_index);

        // if both ids are in the node-namespace [1,number_of_nodes], a node-to-node connection is found
        if(id1 <= (int) number_of_nodes && id2 <= (int) number_of_nodes) {

            // only nodes (still) not scheduled should be regarded
            bool id1_exists = false;
            bool id2_exists = false;

            if(node_agents_scheduled[id1-1] == 0){
                id1_exists = true;
            }
            if(node_agents_scheduled[id2-1] == 0){
                id2_exists = true;
            }

            /*for(unsigned long k = node_agents.size(); k > 0; k--) {
                if(node_agents[k-1] == id1) {
                    id1_exists = true;
                }
                if(node_agents[k-1] == id2) {
                    id2_exists = true;
                }
            }*/
            // if both nodes should be regarded, increment respective entry
            if(id1_exists && id2_exists) {
                number_of_adjacent_nodes_local[id1-1]++;
                number_of_adjacent_nodes_local[id2-1]++;
            }
        }

    }
    if(mc_rank == mc_worldsize - 1) {
        unsigned long M = scenario_file_el_grid_content->shape()[1];
        for(unsigned int i = mc_worldsize * edges_per_rank; i < M; i++){
            //boost::array<boost::multi_array<int, 2>::index, 2> id1_index = {{0, i}};
            //boost::array<boost::multi_array<int, 2>::index, 2> id2_index = {{1, i}};

            int id1 = scenario_file_el_grid_content->data()[i+M*0]; //(id1_index);
            int id2 = scenario_file_el_grid_content->data()[i+M*1]; //(id2_index);

            // if both ids are in the node-namespace [1,number_of_nodes], a node-to-node connection is found
            if(id1 <= (int) number_of_nodes && id2 <= (int) number_of_nodes) {

                // only nodes (still) not scheduled should be regarded
                bool id1_exists = false;
                bool id2_exists = false;


                if(node_agents_scheduled[id1-1] == 0){
                    id1_exists = true;
                }
                if(node_agents_scheduled[id2-1] == 0){
                    id2_exists = true;
                }

                // if both nodes should be regarded, increment respective entry
                if(id1_exists && id2_exists) {
                    number_of_adjacent_nodes_local[id1-1]++;
                    number_of_adjacent_nodes_local[id2-1]++;
                }
            }
        }
    }
    // Wait until all edges were computed...
    MPI_Barrier(mc_comm);

    // MPI REDUCE
    MPI_Reduce (number_of_adjacent_nodes_local , number_of_adjacent_nodes , number_of_nodes ,
            MPI_INT , MPI_SUM ,
            0 , mc_comm );

    MPI_Barrier(mc_comm);
    free(number_of_adjacent_nodes_local);
}

/*! \brief Determine nodes with no children (leafs) based on scenario file(s)
 *
 * This function searches for nodes without children based on the grid scenario files (csv format).
 * This is done by iterating over all edges, while counting the number of edges that connect two
 * nodes. A dynamic vector, with the size equal to the total number of nodes in the electrical grid,
 * is used to save the current number of adjacent nodes.If those values are equal to 1, we found a
 * leaf. Found leafs are stored in another vector, which has to be passed as an argument, before
 * triggering a logstream output.
 * */
void Model_creator::determine_leaf_nodes() {

    leaf_nodes.clear();
   
    // search for all nodes with only ONE adjacent node

    if(number_of_nodes_scheduled() < number_of_nodes-1){
        // j = 1; excludes slack...
        for(unsigned int j = 1; j < number_of_nodes; j++) {
            if (number_of_adjacent_nodes[j] == 1) {
                leaf_nodes.push_back(j + 1); // j+1 to get node ID
                //IO->log_info("Node " + std::to_string(j + 1) + " is leaf...");
            }
        }
    }
    else{
        //all nodes but slack scheduled
        //push slack node to leaf nodes
        leaf_nodes.push_back(SLACK_ID);

        //IO->log_info("Node " + std::to_string(SLACK_ID) + " is leaf...");
    }
    //IO->log_info("Leaf_nodes determination terminated");
}

/*! \brief Determine nodes which create forking points based on scenario file(s)
 *
 * This function searches for nodes without any components attached to it, based on the grid
 * scenario files (csv format). This is done by iterating over all edges, while counting the number
 * of edges that connect this node with any components. A dynamic vector, with the size equal to the
 * total number of nodes in the electrical grid, is used to save the current number of attached
 * components. If a node is connected to one preceding and at least 2 succeeding nodes, a fork node
 * is found. Found fork nodes are stored in another vector, which has to be passed as an argument,
 * before triggering a logstream output.
 * */
void Model_creator::determine_fork_nodes() {

    fork_nodes.clear();

    // Since slack has no incoming edges, 2 adjacent nodes are sufficient for it to be a forknode
    if(number_of_adjacent_nodes[0]==2){
        fork_nodes.push_back(1);

        //IO->log_info("Node " + std::to_string(1) + " is fork node...");
    }

    // find every node, that is connected to three or more other nodes
    for(unsigned int j = 0; j < number_of_nodes; j++) {
        if(number_of_adjacent_nodes[j]>=3) {
            fork_nodes.push_back(j+1);

            //IO->log_info("Node " + std::to_string(j+1) + " is fork node...");
        }
    }
}

/*! \brief Modification of the depth_search function that includes additional processing of
 *          the grid to enable hop distance calculations
 *  \param v node id from which depth first search is started
 *  \param previous_depth distance of the previous node to the root
 *  \param previous_hop_depth hop distance of the previous node to the root (lossless cable = no hop)
 *  \param previous_workitem ID of previous workitem
 *  \param lossless_cable true if cable between previous node and v is lossless, false otherwise
 *  \param seen_forknodes Fork nodes that have been passed on the way from the root to v
 *  \param seen_workitems Workitems that have been passed on the way to the workitem of v
 *  \param visited_nodes boolean array that is true for each node ID that was already visited with this method
 *
 * In addition to the previously stored information, an agent_workitem_relation field as well as
 * all seen forknodes on the path between the slack and the regarded agent are stored.
 */
void Model_creator::depth_search_hop(int v, unsigned int previous_depth,
             unsigned int previous_hop_depth, int previous_workitem, bool lossless_cable,
             std::vector<int> seen_forknodes, std::vector<int> seen_workitems, bool visited_nodes[]){

    bool previous_cable_lossless = lossless_cable;
    bool next_cable_lossless = false;

    //boost::array<boost::multi_array<int, 2>::index, 2> type_index = {{1, v-1}};
    unsigned long N = scenario_file_components_content->shape()[1];
    int agent_type = scenario_file_components_content->data()[(v-1)+N*1]; //(type_index);

    if(agent_type == 3 || agent_type == 4) { // TYPE_TRANSFORMER_INT = 3; TYPE_SLACK_INT = 4;
        previous_cable_lossless = true;
        next_cable_lossless = true;
    }
    
    // mark node as visited 
    visited_nodes[v-1] = true;
    // set agent_workitem_relation
    agent_workitem_relation[v-1] = current_workitem;
   
    //IO->log_info("Node: " + std::to_string(v));
    // store all seen forknodes in the path_forknodes vector of the regarded agent
    for(auto seen_fork : seen_forknodes) {
         //IO->log_info("path_forknodes:" + std::to_string(seen_fork));
         path_forknodes[v-1].push_back(seen_fork);
    }


    // store all seen workitems in the path_workitems vector of the regarded agent
    for(auto seen_workitem :  seen_workitems) {
        //IO->log_info("path_workitems:" + std::to_string(seen_workitem));
        path_workitems[v-1].push_back(seen_workitem);
    }
    // push it into the current workitem
    workitems[current_workitem].push_back(v);
    unsigned int depth;
    unsigned int depth_hop;

    // hop_depth does not change if the cable was lossless
    if(previous_cable_lossless){
        depth = previous_depth + 1;
        node_depth[v-1] = depth;

        depth_hop = previous_hop_depth;
        hop_depth[v-1] = depth_hop;
    }
    else {
      depth = previous_depth + 1;
      node_depth[v-1] = depth;

      depth_hop = previous_hop_depth + 1;
      hop_depth[v-1] = depth_hop;
    }

    // Add slack with depth 0
    if(v == SLACK_ID) {
      depth = previous_depth;
      node_depth[0] = 0;

      depth_hop = previous_hop_depth;
      hop_depth[0] = 0;
    }

    //IO->log_info("Node " + std::to_string(v) + " added to workitem " +
    //             std::to_string(current_workitem) +
    //             " with depth: " + std::to_string(node_depth[v-1]) +
    //             " and hop_depth: " + std::to_string(hop_depth[v-1]));

    if(previous_workitem != current_workitem) {
        // "Child workitem" found...
        child_workitems[previous_workitem].push_back(current_workitem);
    }

    previous_workitem = current_workitem;
    // Check if we found either a leaf or a forknode
    // following nodes have to be pushed in a new workitem...
    for(auto leaf : leaf_nodes) {
        if(v == leaf) {  // if node is leaf, increment workitem
            // when the workitem was changes, add the previous one as a "seen workitem"
            seen_workitems.push_back(current_workitem);
            current_workitem++;
        }
    }
    for(auto fork : fork_nodes) {
	    if (v == fork) { //if node is fork, increment workitem
            // when the workitem was changes, add the previous one as a "seen workitem"
            seen_workitems.push_back(current_workitem);
	        current_workitem++;
            // when a fork was visited, add it as a "seen forknode"
            seen_forknodes.push_back(v);

	    }
    }

    // recursive call of depth_search for each adjacent node
    for(auto node : adjacent_nodes[v-1]) {
        if(!visited_nodes[(node)-1]){
    	    depth_search_hop(node, depth, depth_hop, previous_workitem, next_cable_lossless,
                             seen_forknodes, seen_workitems, visited_nodes);
        }
    }

}

/*! \brief Function that initiates workitem determination, based on a recursive depth_search
 *
 *  This function initializes and initiates the determination of workitems, based on the grid
 *  scenario files (csv format). Workitems are defined as sequential node chains, without any
 *  forking points, that cannot be parallelized without creating overhead, due to the serial
 *  dependency of the forward-backward sweep calculation.
 * */
void Model_creator::determine_workitems()
{
    auto *visited_nodes = new bool [number_of_nodes];

    path_forknodes = new std::vector<int> [number_of_nodes];
    path_workitems = new std::vector<int> [number_of_nodes];

    std::vector<int> seen_forknodes;
    std::vector<int> seen_workitems;
    // Mark all nodes as not visited
    for(unsigned int i = 0; i < number_of_nodes; i++)
    {
        visited_nodes[i] = false;
    }

    // Call recursive depth_serach for the Slack as starting node (root)
    depth_search_hop(SLACK_ID, 0, 0, 0, false, seen_forknodes, seen_workitems, visited_nodes);

    // Adds independent nodes (no adjacent nodes) into a seperate vector to treat them accordingly
    // !! This seems not used for the current simulation-grids because of missing independent nodes,
    // !! but seems necessary to expand the algorithm
    // !! to treat all treelike sturctures in a proper way.
    // !! Correct interaction with the other functions and/or the consequences of deleting this
    // !! code has to be further observed...

    seen_forknodes.clear();
    seen_workitems.clear();
}

/*! \brief Function to determine hop distances for all node agents in the grid.
 * \param id1 first ID from which the hop distance shall be determined
 * \param id2 second ID to which the hop distance shall be determined
 *
 * For each pair of two node agent ids, one of the following cases applies:
 * 1) Both ids are part of the same workitem --> hop distance equals the difference in the nodes depths
 * 2) The ids are in different workitems, but share a "common forknode" on the path between the slack
 *    and the regarded agent
 *    --> hop distance equals the sum of the distances from the common forknode to the regarded agents
 * 3) The ids are in different workitems, but one of the agents is in the same workitem as the slack
 *    --> no fork node seen
 *    --> no common forknode
 *    --> hop distance again equals the difference in the nodes depths
 * 4) If no case applies, both ids seem to be in separated nets --> no meaningful hop distance available
 */
unsigned int Model_creator::hop_distance(int id1, int id2){

    //IO->log_info("Calling hop_distance for " + std::to_string(id1) + "," + std::to_string(id2));

    bool hop_dist_found = false;

    unsigned int return_hop_dist = 0;
    int high_id;
    int low_id;

    if(id1 > id2) {
        high_id = id1;
        low_id = id2;
    }
    else {
        high_id = id2;
        low_id = id1;
    }

    // agents are in the same workitem
    if(agent_workitem_relation[id1-1] == agent_workitem_relation[id2-1]){
        //IO->log_info("Same Workitem...");
        // Use absolute value to enable unordered passing of the ids
        int hop = hop_depth[id1-1] - hop_depth[id2-1];
        return_hop_dist = (unsigned int) abs(hop);
        hop_dist_found = true;
    }
    // check if one of the workitems is a childworkitem of the other workitem
    if(!hop_dist_found && !child_workitems[agent_workitem_relation[id1-1]].empty()){
        // workitem of id1 has children...
        for (auto workitem : child_workitems[agent_workitem_relation[id1-1]]){
            if(workitem == agent_workitem_relation[id2-1]){
                // Workitem of id2 is child of workitem of id1...
                //IO->log_info("Workitem of id2 is child of workitem of id1...");
                int hop = hop_depth[id1-1] - hop_depth[id2-1];
                return_hop_dist = (unsigned int) abs(hop);
                hop_dist_found = true;
            }
        }
    }
    if(!hop_dist_found && !child_workitems[agent_workitem_relation[id2-1]].empty()){
        // workitem of id2 has children...
        for (auto workitem : child_workitems[agent_workitem_relation[id2-1]]){
            if(workitem == agent_workitem_relation[id1-1]){
                // Workitem of id1 is child of workitem of id2...
                //IO->log_info("Workitem of id1 is child of workitem of id2...");
                int hop = hop_depth[id1-1] - hop_depth[id2-1];
                return_hop_dist = (unsigned int) abs(hop);
                hop_dist_found = true;
            }
        }
    }
    
    // agents share a common fork node
    if(!hop_dist_found && !path_forknodes[id1-1].empty() && !path_forknodes[id2-1].empty()){
        auto it1 = path_forknodes[id1-1].end();
        auto it2 = path_forknodes[id2-1].end();
        it1--;
        it2--;
        bool done = false;
        while(!(it1 == path_forknodes[id1-1].begin() && it2 == path_forknodes[id2-1].begin())){
            if(*it1 == *it2){
                //IO->log_info("Common fork...");
                return_hop_dist =  hop_depth[id1-1] + hop_depth[id2-1] - 2*hop_depth[*it1-1];
                done = true;
                hop_dist_found = true;
                break;
            }
            else if(*it1 > *it2){
                --it1;
            }
            else if(*it2 > *it1){
                --it2;
            }
        }
        // If first element is the "common" forknode
        if(*it1 == *it2 && !done){
            //IO->log_info("Common fork...");
            return_hop_dist =  hop_depth[id1-1] + hop_depth[id2-1] - 2*hop_depth[*it1-1];
            hop_dist_found = true;
        }
    }

    // There is straight path between those nodes
    if(!hop_dist_found && !path_workitems[high_id-1].empty()) {
        for(auto item : path_workitems[high_id-1]){
            if (item == agent_workitem_relation[low_id-1]) {
                int hop = hop_depth[id1-1] - hop_depth[id2-1];
                //IO->log_info("Straight path...");
                return_hop_dist = (unsigned int) abs(hop);
                hop_dist_found = true;
                break;
            }
        }
    }

    if(!hop_dist_found) {
        // ERROR: nodes may be in seperated nets
        IO->log_info(
                std::to_string(id1) + "->" + std::to_string(id2) + " :: Case: ERROR! - Nodes may be in seperated nets");
        std::cerr << "ERROR in hop distance calculation: nodes may be in seperated grids. Abort." << std::endl;
        do_exit(-20);
    }
    return return_hop_dist;

}

/*! \brief exit the simulation (called in error case)
 * \param code Integer giving the the code of the error that happened
 *
 * Send an MPI Abort signal to all MPI processes.
 * */
void Model_creator::do_exit(int code) {
    std::cout << "Model Error. Aborting!" << std::endl;
    MPI_Abort(MPI_COMM_WORLD, code);

}

/*! \brief Function that reads the agents properties from the scenario files and
 *          passes them to Model::agent_creator()
 *  \param node_id The ID of the node at which the component is created
 * */
void Model_creator::create_node_agent(int node_id) {
    unsigned long N = scenario_file_components_content->shape()[1];
    //boost::array<boost::multi_array<int, 2>::index, 2> id_index = {{0, node_id}};
    //boost::array<boost::multi_array<int, 2>::index, 2> type_index = {{1, node_id}};
    //boost::array<boost::multi_array<std::string, 1>::index, 1> subtype_index = {{node_id}};
    int agent_id = scenario_file_components_content->data()[node_id+N*0]; //(id_index);
    int agent_type = scenario_file_components_content->data()[node_id+N*1]; //(type_index);
    std::string subtype = scenario_file_components_subtypes->data()[node_id]; //(subtype_index);
    double param[COMPONENTS_MAX_ATTRIBUTE_COLUMNS];
    for(unsigned int w = 0; w<COMPONENTS_MAX_ATTRIBUTE_COLUMNS; w++){
        //boost::array<boost::multi_array<double, 2>::index, 2> attributes_index = {{w,node_id}};
        param[w]  = scenario_file_components_attributes->data()[node_id+N*w]; //(attributes_index);
    }
    repast::AgentId id(agent_id, rank, agent_type);
    bool ict_connectivity = true; // ict connectivity is true for Slack and Transformer agent
    if(agent_type == TYPE_NODE_INT){
        ict_connectivity = param[2]; // only Node agent get ict connectivity from components.csv
    }
    model->agent_creator(id, subtype, agent_id, param, 0, 0, param[0], ict_connectivity);
    agents_scheduling->push_back(context->getAgent(id));
    agent_count++;
}

/*! \brief Distribute node agents evenly among processes
 *  \param _agent_rank_relation [out] array that saves for each agent in which rank it is created
 *
 * Distributes nodes evenly of all ranks. Rest agents are always created in the highest rank!
 * This function is called if the option "distribution_method = evenly" is set in the model.props file.
 * */
void Model_creator::evenly_distribution(int * _agent_rank_relation){
    
    unsigned int node_agents_per_rank = number_of_nodes / world_size;
    unsigned int rest_node_agents =  number_of_nodes % world_size;
    
    IO->log_info("Evenly distribution initiated... \nnodes_per_rank  = "
                 + std::to_string(node_agents_per_rank) + "\nrest_node_agents = "
                 + std::to_string(rest_node_agents));
      /*Create node agents*/

    unsigned int n_own = node_agents_per_rank;
    //the highest rank gets the rest node agents
    if (rank == world_size -1)
        n_own += rest_node_agents;

    IO->log_info("I create " + std::to_string(n_own) + " node agents");

    highest_filled_rank = (unsigned int) world_size - 1;
    /*Distribute agents evenly over all processes*/
    for (unsigned int i = 0; i < number_of_nodes; i++) {

        if (i >= rank * node_agents_per_rank && i < (rank + 1) * node_agents_per_rank) {
            create_node_agent(i);
        } else if (i >= (rank + 1) * node_agents_per_rank && rank == world_size - 1) {
            create_node_agent(i);
        }
        if(mc_rank == 0) {
            // determine rank of this node
            if ((i / node_agents_per_rank) < (unsigned int) world_size) {
                // no rest node agent
                _agent_rank_relation[i] = i / node_agents_per_rank;
            } else { // rest node agents are created in highest rank...
                _agent_rank_relation[i] = world_size - 1;
            }
        }
    }
    // for proper use, agent_scheduling has to be filled from the leafs to the root...
    reverse(agents_scheduling->begin(), agents_scheduling->end());

}

/*! \brief Distribute workitems to processes based on their interconnections and depth in the topology
 *  \param _agent_rank_relation [out] array that saves for each agent in which rank it is created
 *
 * TODO: improve documentation of this method
 * This function is called if the option "distribution_method = workitem" is set in the model.props file.
 * */
void Model_creator::workitem_distribution(int *_agent_rank_relation){

    IO->log_info("Workitem_children_feeder_based_balanced distribution initiated...");


    // workitem children feeder based distribution algorithm here...

    int max_depth;

    int deepest_node_id = 0;
    unsigned int deepest_leaf_position = 0;
    auto *scheduled_nodes_ranks = new int [world_size];
    std::fill(scheduled_nodes_ranks, scheduled_nodes_ranks + world_size, 0);
    auto *rank_handled = new bool [world_size];
    bool first_run = true;

    // Initialize workitem_rank_relation
    auto workitem_rank_relation = (int*) malloc(number_of_workitems * sizeof(int));
    for(unsigned int i = 0; i < number_of_workitems; ++i) {
        workitem_rank_relation[i] = -1;
    }

    int number_of_utilized_processes = 0;

    int least_filled_rank = 0; //signed because workitem_rank_relation is singed
    highest_filled_rank = 0;

    bool finished = false;

    //while(!node_agents.empty()) {
    while(!finished) {
        // as long as there are undistributed agents left in node_agents[] ...

        std::fill(rank_handled, rank_handled + world_size, false); // mark all ranks as "unhandled"

        unsigned long number_of_schedulable_workitems;

        //IO->log_info("Determining leaf nodes.");

        determine_leaf_nodes();

        MPI_Barrier(mc_comm);

        IO->start_time_measurement(IO->tm_distribute_agents);
        if(first_run) {
            recommended_number_of_processes = (int) leaf_nodes.size();
            if(recommended_number_of_processes > world_size) {
                number_of_utilized_processes = world_size;
            }
            else{
                number_of_utilized_processes = recommended_number_of_processes;
            }
            //if(rank == 0){
                //std::cout << "Recommended number of processes: " << recommended_number_of_processes << std::endl;
                //std::cout << "Number of workitems: " << number_of_workitems << std::endl;
            //}
        }

        // at max. the number of leafs can be scheduled during one run
        if (leaf_nodes.size() < (unsigned int) world_size) {
            number_of_schedulable_workitems = leaf_nodes.size();
        } else {
            number_of_schedulable_workitems = (unsigned long) world_size;
        }

        // creates agents in as many ranks as there can possibly scheduled
        for (unsigned int j = 0; j < number_of_schedulable_workitems; j++) {

            MPI_Barrier(mc_comm);

            max_depth = -1; // needs to be negative in order to find Slacks with depth 0 as well...

            // find deepest_node...
            for (unsigned int i = 0; i < leaf_nodes.size(); i++) {

                if ((int) node_depth[leaf_nodes[i] - 1] > max_depth) {
                    max_depth = node_depth[leaf_nodes[i] - 1];
                    deepest_leaf_position = i;
                    deepest_node_id = leaf_nodes[i];
                }
            }

            //if (first_run && rank == 0) {
            //    std::cout << "max_depth = " << max_depth << std::endl;
            //}

            // reset first_run in all ranks, not only in rank 0
            // otherwise number_of_utilized processes will be recomputed in each while run
            if(first_run){
                first_run = false;
            }


            for (unsigned int i = 0; i < number_of_workitems; i++) {
                if (workitems[i].back() == deepest_node_id) {
                    // leafs are always the last item of a workitem
                    // If workitem has child workitems...
                    if(!child_workitems[i].empty()){
                        //IO->log_info("Workitem: " + std::to_string(i) + " has children...");

                        // Determine trafo of the deepest node
                        // (The trafo indicates the voltage level the node is located in)
                        std::string trafo_deepest_node =
                                scenario_file_components_subtypes->data()[deepest_node_id];
                        std::string trafo_workitem;
                        
                        least_filled_rank = workitem_rank_relation[child_workitems[i].front()];
                        // Bool value to check if another workitem on the same voltage was found
                        bool workitem_on_same_voltage_level = false;

                        for (auto child_workitem : child_workitems[i]) {
                        
                            /*IO->log_info("Rank " +
                                         std::to_string(workitem_rank_relation[child_workitem]) +
                                         " containing Workitem: " + std::to_string(child_workitem) +
                                         "has " +
                                         std::to_string(scheduled_nodes_ranks[workitem_rank_relation[child_workitem]]) +
                                         " nodes scheduled...");*/
                            
                            int first_node_id = *workitems[child_workitem].begin();
                            trafo_workitem = scenario_file_components_subtypes->data()[first_node_id-1];
                            //IO->log_info("Node: " + std::to_string(first_node_id));
                            //IO->log_info(trafo_deepest_node + " = " + trafo_workitem + "?");

                            // For every found child workitem, check if they share the same trafo id
                            if(trafo_deepest_node == trafo_workitem)
                            {   
                                //IO->log_info("Found workitem on same voltage level...");
                                if(!workitem_on_same_voltage_level)
                                {   
                                    // If no workitem on the same voltage level was found before,
                                    // set bool value and take the corresponding process as least_filled_rank
                                    workitem_on_same_voltage_level = true;
                                    least_filled_rank = workitem_rank_relation[child_workitem];
                                }
                                // If another workitem on the same voltage level was already found,
                                // check which of the processes is less filled
                                else if(scheduled_nodes_ranks[workitem_rank_relation[child_workitem]]
                                        <= scheduled_nodes_ranks[least_filled_rank]) {
                                    least_filled_rank = workitem_rank_relation[child_workitem];
                                }
                            }

                            if(!workitem_on_same_voltage_level &&
                                    (scheduled_nodes_ranks[workitem_rank_relation[child_workitem]]
                               <= scheduled_nodes_ranks[least_filled_rank])) {
                                least_filled_rank = workitem_rank_relation[child_workitem];
                            }
                        }
                    }
                    // If no child workitems are available, simply seach for the least filled process
                    else {
                         // find least_filled_rank, that wasn't used during this iteration...
                        for (int rank_counter = 0; rank_counter <
                                number_of_utilized_processes; rank_counter++) {

                            if (rank_counter > highest_filled_rank) {
                                highest_filled_rank = rank_counter;
                            }

                            if (!rank_handled[rank_counter] &&
                                    (scheduled_nodes_ranks[rank_counter] <
                                     scheduled_nodes_ranks[least_filled_rank])) {
                                least_filled_rank = rank_counter;

                            } else if(!rank_handled[rank_counter] &&
                                    (scheduled_nodes_ranks[rank_counter] ==
                                    scheduled_nodes_ranks[least_filled_rank])){
                                //if ranks have the same amount of agents,
                                // the rank with the smaller ID is taken as the least filled rank
                                if(rank_counter < least_filled_rank) {
                                    least_filled_rank = rank_counter;
                                }
                            }
                        }
                    }

                    //IO->log_info("LEAST FILLED: " + std::to_string(least_filled_rank) +
                    //             " with nodes: " + std::to_string(scheduled_nodes_ranks[least_filled_rank]));
                    workitem_rank_relation[i] = least_filled_rank;
                    rank_handled[least_filled_rank] = true; // mark rank as "handled"

                    // Iterate backwards over workitem to schedule nodes in the correct order!!
                    for (auto rit = workitems[i].rbegin(); rit != workitems[i].rend(); ++rit) {
                        int current_node_id = *rit;

                        if(mc_rank == 0){
                            _agent_rank_relation[current_node_id - 1] = least_filled_rank;
                            //mark node as scheduled
                            node_agents_scheduled[current_node_id - 1] = 1;

                            //remove this node from adjacency
                            for(auto adj_node : adjacent_nodes[current_node_id -1]){
                                if(number_of_adjacent_nodes[adj_node-1] >0){
                                    number_of_adjacent_nodes[adj_node-1]--;
                                }
                            }
                            //also this node has no adjacency anymore as it is already scheduled
                            number_of_adjacent_nodes[current_node_id-1] = 0;
                        }

                        scheduled_nodes_ranks[least_filled_rank]++;

                        //IO->log_info("Node: " + std::to_string(current_node_id) + " -> " +
                        //            std::to_string(least_filled_rank));

                        if(rank == least_filled_rank){
                            create_node_agent(current_node_id -1);
                        }

                    }
                    // delete processed leaf node from leaf_nodes, so its not processed twice
                    leaf_nodes.erase(leaf_nodes.begin() + deepest_leaf_position);
                }
            }
        }

        MPI_Barrier(mc_comm);
        //broadcast finished status from acs_rank 0 to all others
        finished = all_nodes_scheduled();
        MPI_Bcast(&finished, 1, MPI_C_BOOL, 0, mc_comm);
    } // while...

    free(workitem_rank_relation);

    IO->stop_time_measurement(IO->tm_distribute_agents);
    IO->log_info("distributing the nodes took me " +
                 std::to_string(IO->get_time_measurement(IO->tm_distribute_agents) / 1000000.0 ) + " ms");

}

/*! \brief Return the number of created agents
 *  \return number of created agents as saved in member agent_count
 * */
unsigned int Model_creator::get_agent_count() {
    return agent_count;
}

/*! \brief Return the number of nodes
 *  \return number of nodes
 * */
unsigned int Model_creator::get_number_of_nodes() {
    return number_of_nodes;
}

/*! \brief Return the ID of the rank with the highest ID that as created agents
 *  \return the ID of the rank with the highest ID that as created agents
 * */
int Model_creator::get_highest_filled_rank() {
    return highest_filled_rank;
}

/*! \brief Perform preprocessing steps for the agents distribution
 * */
void Model_creator::preprocessing() {
    //unsigned int agent_count = 0; //counting number of created agents

    //std::vector<int> node_agents; //storing not scheduled nodes

    //if(rank==0){
    //    std::cout << "---> Preprocessing...";
    //}

    unsigned long N = scenario_file_components_content->shape()[1];
    for(unsigned int i = 0; i<N; i++){
        //boost::array<boost::multi_array<int, 2>::index, 2> id_index = {{0, i}};
        //boost::array<boost::multi_array<int, 2>::index, 2> type_index = {{1, i}};
        int agent_id = scenario_file_components_content->data()[N*0+i];// (id_index);
        int type = scenario_file_components_content->data()[N*1+i];//(type_index);
        if((type == TYPE_NODE_INT) || (type == TYPE_TRANSFORMER_INT) || (type == TYPE_SLACK_INT)){
            number_of_nodes++;
            node_agents.push_back(agent_id);
            //add one empty list of adjacent nodes per node
            adjacent_nodes.emplace_back(std::list<int>());
        }
        else{
            number_of_components++;
        }

        if(type == TYPE_TRANSFORMER_INT){
            transformers.push_back(agent_id);
        }
    }

    if(distribution_method == "evenly_distributed" &&
       (unsigned int) world_size > number_of_nodes && rank == 0){
        std::cout << "ERROR: Scenario has fewer nodes than number"
                     " of processes in the simulation." << std::endl;
        std::cout << "Either choose different scenario or reduce"
                     " number of processes." << std::endl << "Abort." <<  std::endl;
        do_exit(-100);
    }

    //if(rank==0){
    //    std::cout << "done." << std::endl;
    //}

}

/*! \brief Determine if all nodes are scheduled
 * \return true if all nodes are scheduled, false otherwise
 * */
bool Model_creator::all_nodes_scheduled() {

    unsigned int sum = 0;
    for(unsigned int i = 0; i<number_of_nodes;i++){
        sum += node_agents_scheduled[i];
    }

    if(sum == number_of_nodes){
        return true;
    }else{
        return false;
    }

}

/*! \brief Determine the number of scheduled nodes
 * \return number of scheduled nodes
 * */
unsigned int Model_creator::number_of_nodes_scheduled() {
    unsigned int sum = 0;
    for(unsigned int i = 0; i<number_of_nodes;i++){
        sum += node_agents_scheduled[i];
    }
    return sum;
}

/*! \brief Check if parsed topology is plausible
 *  \return 1 = topology plausible, 0 = warnings for topology, -1 = error (simulation should exit)
 * */
int Model_creator::topology_plausibility_check(){
    int plausibility = 1; // 1 = plausible, 0 = warning, -1 = error

    // Check if node ids along the branches of the topology tree
    // are in increasing order
    int node_id = 1;
    for(auto& node : adjacent_nodes){
        // Count number of adjacent nodes with smaller ids
        unsigned int nodes_with_smaller_id = 0;
        for(auto& adjacent_node : node){
            if (adjacent_node < node_id){
                ++nodes_with_smaller_id;
            }
        }
        // Warn user if for a certain node, more than one node with smaller ids was found
        if (nodes_with_smaller_id > 1){
            if (world_size > 1){
                plausibility = -1; // This topology cannot be simulated with more than one process
            }
            else {
                if (plausibility != -1) {
                    plausibility = 0; // Do not overwrite critical plausibility violation
                }
            }

            if (rank == 0){
                std::cout << "############################### WARNING ###############################" << std::endl
                          << "## Node " << std::to_string(node_id)
                          << " has more than one adjacent node with a smaller id!" << std::endl
                          << "## This topology cannot be simulated with number of processes n>1" << std::endl
                          << "## Please check the scenario files!" << std::endl
                          << "#######################################################################" << std::endl;
            }
        }
        ++node_id;
    }
    return plausibility;
}


/*! \brief This method distributed agents to processes and creates the agents that belong to a process
 * \param _agent_rank_relation [out] array that saves for each agent in which rank it is created
 * \param components_at_nodes [out] vector that saves for each node which components are connected to it
 * \param ict_connectivity_at_nodes [out] vector that saves ICT connectivity for each node
 * */
void Model_creator::create_model(int *_agent_rank_relation,
                     std::vector<std::list<std::tuple<int, int, int>>> &components_at_nodes,
                     std::vector<bool> &ict_connectivity_at_nodes) {

    //_agent_rank_relation is a shared memory field in model class; it is only written by mc_rank 0
    //the other processes help to parallelize the computation of adjacent nodes
    double ms_distribute_agents_time = 0.0;
    gettimeofday(&distribute_agents_time_start, nullptr);

    /*initialize length of components_at_nodes and ict_connectivity_of_nodes vectors*/
    for(unsigned int i=0; i<number_of_nodes; i++){
        components_at_nodes.emplace_back(std::list<std::tuple<int,int,int>>());
        ict_connectivity_at_nodes.emplace_back(true); // all connectivity true by default
    }

    //allocate shared memories used for distribution of agents!
    allocate_shared_memory();

    //determine list of adjacent nodes for each node
    determine_adjacent_nodes();

    //determine number of adjacent nodes
    determine_number_of_adjacent_nodes();

    //determine initial leaf nodes of the grid topology
    determine_leaf_nodes();

    MPI_Barrier(MPI_COMM_WORLD);
    //determine fork nodes of the topology
    determine_fork_nodes();


    if(rank==0){
        std::cout << "---> Determine workitems" << std::endl;;
    }

    number_of_workitems = leaf_nodes.size() + fork_nodes.size();
    //int node_depth[number_of_nodes];// = {0};
    node_depth = (unsigned int*) malloc(number_of_nodes * sizeof(unsigned int));
    hop_depth = (unsigned int*) malloc(number_of_nodes * sizeof(unsigned int));
    workitems = new std::list<int>[number_of_workitems];
    agent_workitem_relation = (int*) malloc(number_of_nodes * sizeof(int));
    child_workitems = new std::vector<int>[number_of_workitems];

    determine_workitems();

    /*for(unsigned int i = 0; i < number_of_workitems; ++i) {
        IO->log_info("Workitem: " + std::to_string(i) + " : Children: ");
        for(auto workitem: child_workitems[i]){
            IO->log_info(std::to_string(workitem));
        }
    }*/

    // Check plausibility of parsed topology before distribution agents among ranks
    if(rank==0){
        std::cout << "---> Check plausibility of topology" << std::endl;;
    }
    // 1 = plausible, 0 = warning, -1 = error
    int plausibility = topology_plausibility_check();
    if(rank==0){
        if(plausibility == -1){
            std::cout << "     !!! ERROR in plausibility check !!!" << std::endl;
            do_exit(-1);
        } else if (plausibility == 0){
            std::cout << "     Warnings occurred in plausibility check!" << std::endl;
        }
    }

    if(distribution_method == "workitem") {
        if(rank==0){
            std::cout << "---> Start workitem distribution"<< std::endl;;
        }
        workitem_distribution(_agent_rank_relation);
        if(rank==0){
            std::cout << "     Number of leaf nodes: " << recommended_number_of_processes << std::endl;
            std::cout << "     Number of workitems: " << number_of_workitems << std::endl;
        }
    }
    else if (distribution_method == "evenly") {
        if(rank==0){
            std::cout << "---> Start evenly distribution" << std::endl;;
        }
        evenly_distribution(_agent_rank_relation);
    }
    else {
        IO->log_info("\"" + distribution_method + "\" is not a valid distribution method!");
    }

    gettimeofday(&distribute_agents_time_end, nullptr);
    ms_distribute_agents_time += (distribute_agents_time_end.tv_sec-
            distribute_agents_time_start.tv_sec)*1000.0 +
            (distribute_agents_time_end.tv_usec-distribute_agents_time_start.tv_usec) / 1000.0;

    IO->log_info("Creating agent distribution took: " + std::to_string(ms_distribute_agents_time));

    if(rank==0){
        std::cout << "---> Create component agents" << std::endl;;
    }

    /*distribute component agents*/
    unsigned long N = scenario_file_el_grid_content->shape()[1];
    unsigned long M = scenario_file_components_content->shape()[1];
    for(unsigned int u = 0; u < scenario_file_el_grid_content->shape()[1]; u++){
        //boost::array<boost::multi_array<int, 2>::index, 2> id1_index = {{0, u}};
        //boost::array<boost::multi_array<int, 2>::index, 2> id2_index = {{1, u}};

        int id1 = scenario_file_el_grid_content->data()[u+N*0]; //(id1_index);
        int id2 = scenario_file_el_grid_content->data()[u+N*1]; //(id2_index);

        //boost::array<boost::multi_array<int, 2>::index, 2> type1_index = {{1, id1-1}};
        //boost::array<boost::multi_array<int, 2>::index, 2> type2_index = {{1, id2-1}};

        int type1 = scenario_file_components_content->data()[(id1-1)+M*1]; // (type1_index);
        int type2 = scenario_file_components_content->data()[(id2-1)+M*1]; //(type2_index);

        //boost::array<boost::multi_array<int, 2>::index, 2> id11_index = {{2, id1-1}};
        //boost::array<boost::multi_array<int, 2>::index, 2> id21_index = {{2, id2-1}};

        int profile_id11 = scenario_file_components_content->data()[(id1-1)+M*2]; //(id11_index);
        int profile_id21 = scenario_file_components_content->data()[(id2-1)+M*2]; //(id21_index);

        //boost::array<boost::multi_array<int, 2>::index, 2> id12_index = {{3, id1-1}};
        //boost::array<boost::multi_array<int, 2>::index, 2> id22_index = {{3, id2-1}};

        int profile_id12 = scenario_file_components_content->data()[(id1-1)+M*3]; //(id12_index);
        int profile_id22 = scenario_file_components_content->data()[(id2-1)+M*3]; //(id22_index);

        //boost::array<boost::multi_array<std::string, 1>::index, 1> subtype1_index = {{id1-1}};
        //boost::array<boost::multi_array<std::string, 1>::index, 1> subtype2_index = {{id2-1}};

        std::string subtype1 = scenario_file_components_subtypes->data()[id1-1]; //(subtype1_index);
        std::string subtype2 = scenario_file_components_subtypes->data()[id2-1]; //(subtype2_index);

        double param1[COMPONENTS_MAX_ATTRIBUTE_COLUMNS];
        double param2[COMPONENTS_MAX_ATTRIBUTE_COLUMNS];
        //get additional parameters for components
        for(unsigned int w = 0; w<COMPONENTS_MAX_ATTRIBUTE_COLUMNS;w++){
            //boost::array<boost::multi_array<double, 2>::index, 2> param1_index = {{w,id1-1}};
            //boost::array<boost::multi_array<double, 2>::index, 2> param2_index = {{w,id2-1}};
            param1[w] = scenario_file_components_attributes->data()[(id1-1)+M*w]; //(param1_index);
            param2[w] = scenario_file_components_attributes->data()[(id2-1)+M*w]; //(param2_index);

        }

        if(type1 == TYPE_NODE_INT){

            double Vnom = param1[0];
            bool ict_connected = (bool) param1[2];
            ict_connectivity_at_nodes[id1-1] = ict_connected;

            if(type2 == TYPE_BATTERY_INT || type2 == TYPE_LOAD_INT || type2 == TYPE_PV_INT ||
               type2 == TYPE_CHP_INT || type2 == TYPE_EV_INT || type2 == TYPE_COMPENSATOR_INT ||
               type2 == TYPE_WEC_INT || type2 == TYPE_HP_INT || type2 == TYPE_BIOFUEL_INT){
                repast::AgentId agent1_id(id1, rank, type1 );
                if(context->contains(agent1_id)){
                    //if node agent of this component is contained in this process,
                    // create component agent here
                    repast::AgentId new_component_agent(id2, rank, type2);
                    model->agent_creator(new_component_agent, subtype2, id1, param2,
                                         profile_id21, profile_id22, Vnom, ict_connected);
                    agent_count++;
                }
                //boost::array<boost::multi_array<std::string, 1>::index, 1> trafo_id_index = {{id1-1}};
                int trafo_id = std::stoi(scenario_file_components_subtypes->data()[id1-1]); // (trafo_id_index));
                components_at_nodes[id1-1].emplace_back(std::tuple<int,int,int>(id2,type2,trafo_id));
            }
        }
        else if(type2 == TYPE_NODE_INT){
            double Vnom = param2[0];
            bool ict_connected = (bool) param2[2];
            ict_connectivity_at_nodes[id2-1] = ict_connected;

            if(type1 == TYPE_BATTERY_INT || type1 == TYPE_LOAD_INT || type1 == TYPE_PV_INT ||
               type1 == TYPE_CHP_INT  || type1 == TYPE_EV_INT || type1 == TYPE_COMPENSATOR_INT ||
               type1 == TYPE_WEC_INT || type1 == TYPE_HP_INT || type2 == TYPE_BIOFUEL_INT){
                repast::AgentId agent2_id(id2, rank, type2);
                if(context->contains(agent2_id)){
                    //if node agent of this component is contained in this process,
                    // create component agent here
                    repast::AgentId new_component_agent(id1, rank, type1);
                    model->agent_creator(new_component_agent, subtype1, id2, param1,
                                         profile_id11, profile_id12, Vnom, ict_connected);
                    agent_count++;
                }
                //boost::array<boost::multi_array<std::string, 1>::index, 1> trafo_id_index = {{id2-1}};
                int trafo_id = std::stoi(scenario_file_components_subtypes->data()[id2-1]); //(trafo_id_index));
                components_at_nodes[id2-1].emplace_back(std::tuple<int,int,int>(id1,type1,trafo_id));
            }
        }
    }

    // clear() all vectors and arrays that are not needed anymore
    // (all except for the ones required by hop_distance(...) method which is called by DF-Agents)
    adjacent_nodes.clear();
    transformers.clear();
    leaf_nodes.clear();
    fork_nodes.clear();
    node_agents.clear();
}

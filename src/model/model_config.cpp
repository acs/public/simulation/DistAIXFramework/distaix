/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/model.h"
#include "villas_interface/villas_interface.h"

/*! \brief Read all properties of the model from the provided model.props file
 * and save them to member variables
 */
void Model::read_model_props(){
    /*read from model configuration file*/
    stop_at = repast::strToUInt(props->getProperty("stop.at"));
    step_size = repast::strToDouble(props->getProperty("step.size"));
    use_commdata = (bool) repast::strToInt(props->getProperty("use.commdata"));
    interpolation_type = props->getProperty("profile.interpolation_type");
    model_type = repast::strToInt(props->getProperty("model.type"));
    distribution_method = props->getProperty("distribution_method");
    scenario_dir = props->getProperty("dir.scenario");
    profile_dir = props->getProperty("dir.profiles");
    realtime = (bool) repast::strToUInt(props->getProperty("realtime"));

    std::string scenario_temp;
    std::stringstream ss;
    scenario_temp = props->getProperty("file.scenario.components");
    ss << scenario_dir << scenario_temp;
    scenario_file_components = ss.str();
    ss.str("");
    scenario_temp = props->getProperty("file.scenario.elgrid");
    ss << scenario_dir << scenario_temp;
    scenario_file_el_grid = ss.str();

    /*determine log level of DB*/
    loglevel = props->getProperty("results.loglevel");

    /*determine which agents should be logged*/
    std::string temp = props->getProperty("log.ids");
    if (temp != "none") {
        std::stringstream idstream(temp);
        std::string id_log;
        while(std::getline(idstream, id_log, ',')){
            agent_logging.push_back(std::stoi(id_log));
        }
    }
    mr_logging = (bool) repast::strToUInt(props->getProperty("log.mr"));
    df_logging = (bool) repast::strToUInt(props->getProperty("log.df"));
    log_agents = (bool) repast::strToUInt(props->getProperty("log.agents"));
    log_ranks = (bool) repast::strToUInt(props->getProperty("log.ranks"));
    log_cables = (bool) repast::strToUInt(props->getProperty("log.cables"));

    /*determine for which agents result csv files should be saved*/
    temp = props->getProperty("results.ids");
    if (temp != "none") {
        std::stringstream idstream(temp);
        std::string id_result;
        while(std::getline(idstream, id_result, ',')){
            agent_results.push_back(std::stoi(id_result));
        }
    }
    results_folder = props->getProperty("results.folder");
    mr_results= (bool) repast::strToUInt(props->getProperty("results.mr"));
    df_results = (bool) repast::strToUInt(props->getProperty("results.df"));
    results_csv_agents = (bool) repast::strToUInt(props->getProperty("results.csv.agents"));
    results_csv_ranks = (bool) repast::strToUInt(props->getProperty("results.csv.ranks"));
    results_csv_cables = (bool) repast::strToUInt(props->getProperty("results.csv.cables"));
    results_db_agents = (bool) repast::strToUInt(props->getProperty("results.db.agents"));
    results_db_ranks = (bool) repast::strToUInt(props->getProperty("results.db.ranks"));
    results_db_cables = (bool) repast::strToUInt(props->getProperty("results.db.cables"));


    /* determine control method for agents */
    behavior_type = repast::strToInt(props->getProperty("ctrl.type"));

    /* determine logging properties */
    d_props.log.logging = log_ranks;
    log_folder = props->getProperty("log.folder");
    d_props.log.path_log_file = log_folder + "/rank" + std::to_string(rank) + ".log";
    d_props.result.csv_results = results_csv_ranks;
    d_props.result.path_result_file = results_folder + "/rank" + std::to_string(rank) + ".csv";
    d_props.result.db_results = results_db_ranks;
    d_props.result.db_enabled = results_db_ranks || results_db_agents || results_db_cables;
    d_props.result.loglevel = loglevel;


    // read latency file if applicable
    if(use_commdata) {
        ss.str("");
        scenario_temp = props->getProperty("file.scenario.latency");
        ss << scenario_dir << scenario_temp;
        scenario_file_latency = ss.str();
        boost::filesystem::path p(scenario_file_latency);
        if (!boost::filesystem::exists(p)) {
            if(rank==0) {

                std::cout << "WARNING: YOU HAVE CHOSEN OPTION use.commdata=1" << std::endl
                          << "BUT THE LATENCY FILE DOES NOT EXIST IN THE SCENARIO DIRECTORY." << std::endl
                          << "RUNNING WITH use.commdata=0 INSTEAD! CHECK YOUR model.props FILE!!!" << std::endl;
            }
            use_commdata = false;
        }
    }
}

/*! \brief Read all villas configuration parameters from the model.props file
 * depending on the selected behavior
 */
void Model::read_villas_config() {

    //create villas config with disabled villas node (used in all agents that never contain a villas node)
    villas_config_node_disabled = new villas_node_config();
    villas_config_node_disabled->with_node = false;
    villas_config_node_disabled->type_name = "";
    villas_config_node_disabled->format_name = "";
    villas_config_node_disabled->loglevel = "";
    villas_config_agents = villas_config_node_disabled; //set the villas node to disabled per default

    if (behavior_type == CTRL_MQTT_TEST ||
        behavior_type == CTRL_MQTT_PINGPONG ||
        behavior_type == CTRL_MQTT_HIGHLOAD ||
        behavior_type == CTRL_ENSURE ||
        behavior_type == CTRL_DPSIM_COSIM_REF ||
        behavior_type == CTRL_DPSIM_COSIM_SWARMGRIDX) {

        // we are using a behavior that needs villas node(s)

        // set all config params for villas interface of model that are independent of node type
        villas_config_model = new villas_node_config();
        villas_config_model->type_name = props->getProperty("villas.nodetype");
        villas_config_model->format_name = props->getProperty("villas.format");
        villas_config_model->loglevel = props->getProperty("villas.loglevel");
        villas_config_model->with_node = false;
        villas_config_model->stop_at = stop_at;

        // villas node configs for agents (independent of node type)
        villas_config_agents = new villas_node_config();
        villas_config_agents->type_name = villas_config_model->type_name;
        villas_config_agents->format_name = villas_config_model->format_name;
        villas_config_agents->loglevel = villas_config_model->loglevel;
        villas_config_agents->with_node = true;
        villas_config_agents->stop_at = villas_config_model->stop_at;

        //parse the properties of the node type configuration for the agents
        if (villas_config_model->type_name == "mqtt" ) {

            IO->log_info("Reading VILLAS config for MQTT");
            villas_config_agents->type_config.mqtt_conf = new mqtt_data();
            villas_config_agents->type_config.mqtt_conf->broker = props->getProperty(
                    "villas.mqtt.broker");
            villas_config_agents->type_config.mqtt_conf->port = repast::strToInt(
                    props->getProperty("villas.mqtt.port"));
            villas_config_agents->type_config.mqtt_conf->qos = repast::strToInt(
                    props->getProperty("villas.mqtt.qos"));
            villas_config_agents->type_config.mqtt_conf->retain = repast::strToInt(
                    props->getProperty("villas.mqtt.retain"));
            villas_config_agents->type_config.mqtt_conf->keepalive = repast::strToInt(
                    props->getProperty("villas.mqtt.keepalive"));
            villas_config_agents->type_config.mqtt_conf->subscribe = props->getProperty(
                    "villas.mqtt.subscribe");
            villas_config_agents->type_config.mqtt_conf->publish = props->getProperty(
                    "villas.mqtt.publish");
            // MQTT ssl params
            villas_config_agents->type_config.mqtt_conf->ssl_enabled = repast::strToInt(
                    props->getProperty("villas.mqtt.ssl_enabled"));
            villas_config_agents->type_config.mqtt_conf->ssl_insecure = repast::strToInt(
                    props->getProperty("villas.mqtt.ssl_insecure"));
            villas_config_agents->type_config.mqtt_conf->ssl_cert_reqs = repast::strToInt(
                    props->getProperty("villas.mqtt.ssl_cert_reqs"));
            villas_config_agents->type_config.mqtt_conf->ssl_cafile = props->getProperty(
                    "villas.mqtt.ssl_cafile");
            villas_config_agents->type_config.mqtt_conf->ssl_capath = props->getProperty(
                    "villas.mqtt.ssl_capath");
            villas_config_agents->type_config.mqtt_conf->ssl_certfile = props->getProperty(
                    "villas.mqtt.ssl_certfile");
            villas_config_agents->type_config.mqtt_conf->ssl_keyfile = props->getProperty(
                    "villas.mqtt.ssl_keyfile");
            villas_config_agents->type_config.mqtt_conf->ssl_ciphers = props->getProperty(
                    "villas.mqtt.ssl_ciphers");
            villas_config_agents->type_config.mqtt_conf->ssl_tls_version = props->getProperty(
                    "villas.mqtt.ssl_tls_version");

        } else if (villas_config_model->type_name == "nanomsg") {

            IO->log_info("Reading VILLAS config for nanomsg");
            villas_config_agents->type_config.nanomsg_conf = new nanomsg_data();
            villas_config_agents->type_config.nanomsg_conf->in_endpoints.push_back(props->getProperty(
                    "villas.nanomsg.in_endpoints"));
            villas_config_agents->type_config.nanomsg_conf->out_endpoints.push_back(props->getProperty(
                    "villas.nanomsg.out_endpoints"));
        }

        // start the node type in this process
        std::vector<Meta_infos> empty_meta(0);
        interface = new Villas_interface(villas_config_model, IO,
                                         "rank_" + std::to_string(rank), empty_meta);
        IO->log_info("Starting villas node type");
        interface->start_node_type();

    }
}

/*! \brief Read all profile files and save the profiles in the process
 */
void Model::read_profiles() {
    if(rank == 0) {
        std::cout << " * Read profile data..." << std::endl;
    }
    /*read all profiles*/
    component_profiles.init(interpolation_type);
    IO->read_profiles(&component_profiles);
    if(rank == 0) {
        std::cout << "Done." << std::endl;
    }
}


void Model::read_scenario_files(){

    //read scenario files
    if (!IO->read_components_file((char*) scenario_file_components.c_str(), scenario_file_components_content, scenario_file_components_subtypes, scenario_file_components_attributes)){
        std::cout << "Rank " << rank << ": Failure in reading component scenario file! Abort" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, -1);
    }

    if (!IO->read_elgrid_file((char*) scenario_file_el_grid.c_str(), scenario_file_el_grid_content, scenario_file_el_grid_subtypes, scenario_file_edge_attributes)){
        std::cout << "Rank " << rank << ": Failure in reading electrical grid scenario file! Abort" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, -1);
    }


    if(rank == 0){

        const unsigned long number_agents = scenario_file_components_content.shape()[1];
        if(number_agents > MAX_CSV_AGENTS && (results_csv_agents ||results_csv_cables))
            std::cout << "WARNING: Simulation may crash because of too many agents or cables save results to csv files" << std::endl;


        if((log_agents || log_cables) && number_agents > MAX_CSV_AGENTS){
            std::cout << "WARNING: Simulation may crash because of too many agents or cables are logged" << std::endl;
        }

        if(log_ranks && MAX_STEPS_LOGGING && number_agents > MAX_CSV_AGENTS){
            std::cout << "WARNING: Simulation may crash because of too much logging to ranks' log files" << std::endl;
        }
    }

}
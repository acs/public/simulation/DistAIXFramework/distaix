/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/model.h"

/*! \brief Connect agents electrically according to content of scenario files
 * */
void Model::init_elec_network() {

    if(rank == 0){
        std::cout << " * Initializing electrical network in all MPI processes..." << std::endl;
    }

    //save all connections to be made to non-local agents
    std::vector<std::pair<repast::AgentId, repast::AgentId>> connections_missing;
    repast::AgentRequest req_el_connections(rank);

    if(rank == 0) {
        std::cout << "---> creating electrical connections of local agents" << std::endl;
    }

    /*initialize length of connected_nodes*/
    for(unsigned int i=0; i<number_of_nodes; i++){
        connected_nodes.emplace_back(std::list<std::tuple<int,int,int>>());
    }

    /*connect component agents to node agents*/
    for(unsigned int i = 0; i< scenario_file_el_grid_content.shape()[1]; i++) {
        boost::array<boost::multi_array<int, 2>::index, 2> id1_index = {{0, i}};
        boost::array<boost::multi_array<int, 2>::index, 2> id2_index = {{1, i}};
        boost::array<boost::multi_array<std::string, 1>::index, 1> subtype_index = {{i}};
        boost::array<boost::multi_array<float,2>::index,2> length_index = {{0,i}};
        boost::array<boost::multi_array<float,2>::index,2> r_index = {{1,i}};
        boost::array<boost::multi_array<float,2>::index,2> x_index = {{2,i}};
        boost::array<boost::multi_array<float,2>::index,2> b_index = {{3,i}};
        boost::array<boost::multi_array<float,2>::index,2> g_index = {{4,i}};
        boost::array<boost::multi_array<float,2>::index,2> long_term_rate_index = {{5,i}};
        boost::array<boost::multi_array<float,2>::index,2> emergency_rate_index = {{6,i}};
        int id1 = scenario_file_el_grid_content(id1_index);
        int id2 = scenario_file_el_grid_content(id2_index);
        std::string subtype = scenario_file_el_grid_subtypes(subtype_index);
        float length =  scenario_file_edge_attributes(length_index);
        float r_value = scenario_file_edge_attributes(r_index);
        float x_value = scenario_file_edge_attributes(x_index);
        float b_value = scenario_file_edge_attributes(b_index);
        float g_value = scenario_file_edge_attributes(g_index);
        float long_term_rate = scenario_file_edge_attributes(long_term_rate_index);
        float emergency_rate = scenario_file_edge_attributes(emergency_rate_index);

        boost::array<boost::multi_array<int, 2>::index, 2> type1_index = {{1, id1 - 1}};
        boost::array<boost::multi_array<int, 2>::index, 2> type2_index = {{1, id2 - 1}};

        int type1 = scenario_file_components_content(type1_index);
        int type2 = scenario_file_components_content(type2_index);

        repast::AgentId agent1_id(id1, rank, type1);
        repast::AgentId agent2_id(id2, rank, type2);

        boost::array<boost::multi_array<std::string, 1>::index, 1> trafo_id1_index = {{id1-1}};
        boost::array<boost::multi_array<std::string, 1>::index, 1> trafo_id2_index = {{id2-1}};

        std::string trafo_id1 = scenario_file_components_subtypes(trafo_id1_index);
        std::string trafo_id2 = scenario_file_components_subtypes(trafo_id2_index);

        //save connected nodes, trafos and slack
        if((type1 == TYPE_NODE_INT) && (type2 == TYPE_NODE_INT)){

            connected_nodes[id1-1].emplace_back(std::tuple<int,int,int>(id2, TYPE_NODE_INT, stoi(trafo_id2)));
            connected_nodes[id2-1].emplace_back(std::tuple<int,int,int>(id1, TYPE_NODE_INT, stoi(trafo_id1)));

        }else if((type1 == TYPE_NODE_INT) && (type2 == TYPE_TRANSFORMER_INT)){
            if(trafo_id1 != std::to_string(id2)){ //id1 is not in subgrid of trafo id2
                connected_nodes[id1-1].emplace_back(std::tuple<int,int,int>(id2, TYPE_TRANSFORMER_INT, stoi(trafo_id1)));
            }else{ //id1 is in subgrid of trafo id2: trafo id is irrelevant in this case (set to -1)
                connected_nodes[id1-1].emplace_back(std::tuple<int,int,int>(id2, TYPE_TRANSFORMER_INT, -1));
            }
            connected_nodes[id2-1].emplace_back(std::tuple<int,int,int>(id1, TYPE_NODE_INT, stoi(trafo_id1)));

        }else if((type1 == TYPE_TRANSFORMER_INT) && (type2 == TYPE_NODE_INT)){
            if(trafo_id2 != std::to_string(id1)){ //id2 is not in subgrid of trafo id1
                connected_nodes[id2-1].emplace_back(std::tuple<int,int,int>(id1, TYPE_TRANSFORMER_INT, stoi(trafo_id2)));
            }else{ //id2 is in subgrid of trafo id1: trafo id is irrelevant in this case (set to -1)
                connected_nodes[id2-1].emplace_back(std::tuple<int,int,int>(id1, TYPE_TRANSFORMER_INT, -1));
            }
            connected_nodes[id1-1].emplace_back(std::tuple<int,int,int>(id2, TYPE_NODE_INT, stoi(trafo_id2)));

        }else if((type1 == TYPE_NODE_INT) && (type2 == TYPE_SLACK_INT)){
            connected_nodes[id1-1].emplace_back(std::tuple<int,int,int>(id2, TYPE_SLACK_INT, SLACK_ID));
            connected_nodes[id2-1].emplace_back(std::tuple<int,int,int>(id1, TYPE_NODE_INT, SLACK_ID));

        }else if((type1 == TYPE_SLACK_INT) && (type2 == TYPE_NODE_INT)){
            connected_nodes[id1-1].emplace_back(std::tuple<int,int,int>(id2, TYPE_NODE_INT, SLACK_ID));
            connected_nodes[id2-1].emplace_back(std::tuple<int,int,int>(id1, TYPE_SLACK_INT, SLACK_ID));

        }else if((type1 == TYPE_SLACK_INT) && (type2 == TYPE_TRANSFORMER_INT)){
            connected_nodes[id1-1].emplace_back(std::tuple<int,int,int>(id2, TYPE_TRANSFORMER_INT, SLACK_ID));
            connected_nodes[id2-1].emplace_back(std::tuple<int,int,int>(id1, TYPE_SLACK_INT, SLACK_ID));

        }else if((type1 == TYPE_TRANSFORMER_INT) && (type2 == TYPE_SLACK_INT)){
            connected_nodes[id1-1].emplace_back(std::tuple<int,int,int>(id2, TYPE_SLACK_INT, SLACK_ID));
            connected_nodes[id2-1].emplace_back(std::tuple<int,int,int>(id1, TYPE_TRANSFORMER_INT, SLACK_ID));
        }


        /*save all trafos in components_at_nodes vector*/
        if(type1 == TYPE_NODE_INT && type2== TYPE_TRANSFORMER_INT && trafo_id1 != std::to_string(id2) && components_at_nodes[id2-1].empty()){
            //id1 is node at higher voltage side of trafo: add entry with trafo id of this node for the trafo id2
            if(std::stoi(trafo_id1) > 0){
                components_at_nodes[id2-1].emplace_back(std::tuple<int,int,int>(id2, TYPE_TRANSFORMER_INT, std::stoi(trafo_id1)));
            } else {
                components_at_nodes[id2-1].emplace_back(std::tuple<int,int,int>(id2, TYPE_TRANSFORMER_INT,SLACK_ID));
            }

        } else if (type1 == TYPE_TRANSFORMER_INT && type2 == TYPE_NODE_INT && trafo_id2 != std::to_string(id1) && components_at_nodes[id1-1].empty()){
            //id2 is node at higher voltage side of trafo: add entry with trafo id of this node for the trafo id1
            if(std::stoi(trafo_id2) > 0){
                components_at_nodes[id1-1].emplace_back(std::tuple<int,int,int>(id1, TYPE_TRANSFORMER_INT,std::stoi(trafo_id2)));
            } else {
                //
                components_at_nodes[id1-1].emplace_back(std::tuple<int,int,int>(id1, TYPE_TRANSFORMER_INT,SLACK_ID));
            }

        } else if (type1 == TYPE_SLACK_INT && type2 == TYPE_TRANSFORMER_INT && components_at_nodes[id2-1].empty()){
            //if slack and trafo are connected directly, trafo gets trafoid = SLACK_ID
            components_at_nodes[id2-1].emplace_back(std::tuple<int,int,int>(id2, TYPE_TRANSFORMER_INT,SLACK_ID));

        } else if (type1 == TYPE_TRANSFORMER_INT && type2 == TYPE_SLACK_INT && components_at_nodes[id1-1].empty()){
            //if slack and trafo are connected directly, trafo gets trafoid = SLACK_ID
            components_at_nodes[id1-1].emplace_back(std::tuple<int,int,int>(id1, TYPE_TRANSFORMER_INT,SLACK_ID));
        }

        //add entry for slack with trafo id 1
        if ((type1 == TYPE_SLACK_INT || type2 == TYPE_SLACK_INT) && components_at_nodes[SLACK_ID-1].empty() ){
            components_at_nodes[SLACK_ID-1].emplace_back(std::tuple<int, int, int>(SLACK_ID, TYPE_SLACK_INT, SLACK_ID));
        }


        if (context.contains(agent1_id)) {
            if (context.contains(agent2_id)) {
                //both agents in context: connect:
                Agent *agent1 = context.getAgent(agent1_id);
                Agent *agent2 = context.getAgent(agent2_id);
                if (agent_network_elec->findEdge(agent1, agent2) == nullptr) {
                    std::string cable_name = std::to_string(agent1->getId().id()) + "_" + std::to_string(agent2->getId().id());
                    struct data_props cable_d_props;
                    cable_d_props.log.logging = log_cables;
                    cable_d_props.log.path_log_file = log_folder +"/cables/" + cable_name + ".log";
                    cable_d_props.result.csv_results = results_csv_cables;
                    cable_d_props.result.db_results = results_db_cables;
                    cable_d_props.result.db_enabled = results_db_ranks || results_db_agents || results_db_cables;
                    cable_d_props.result.loglevel = loglevel;
                    cable_d_props.result.dbconn = d_props.result.dbconn;
                    cable_d_props.result.path_result_file = results_folder +"/cables/" + cable_name + ".csv";
                    boost::shared_ptr<Cable> new_cable(new Cable(agent1, agent2, false, cable_name, step_size, model_type, subtype, cable_d_props, r_value,x_value,b_value, g_value, length, long_term_rate, emergency_rate));
                    cables.push_back(new_cable);
                    agent_network_elec->addEdge(new_cable);
                    IO->log_info("added electrical edge between agents: " + IO->id2str(agent1->getId()) + " and " + IO->id2str(agent2->getId()) + " subtype=" + subtype);
                }

            } else {
                //only agent1 is in context: request agent2
                int agent2_rank = 0;

                /*determine ID of agent 2*/
                agent2_rank = agent_rank_relation[id2 - 1];

                repast::AgentId agent2(id2, agent2_rank, type2);

                if (!req_el_connections.contains(agent2)) {
                    req_el_connections.addRequest(agent2);
                }

                std::pair<repast::AgentId, repast::AgentId> connection;
                connection.first = agent1_id;
                connection.second = agent2;
                connections_missing.push_back(connection);

            }
        } else if (context.contains(agent2_id)) {
            //only agent2 is in this context: request agent1
            /*determine ID of agent 1*/
            int agent1_rank = 0;

            agent1_rank = agent_rank_relation[id1 - 1];

            repast::AgentId agent1(id1, agent1_rank, type1);

            if (!req_el_connections.contains(agent1)) {
                req_el_connections.addRequest(agent1);
            }

            std::pair<repast::AgentId, repast::AgentId> connection;
            connection.first = agent2_id;
            connection.second = agent1;
            connections_missing.push_back(connection);
        }

    }

    if(rank == 0) {
        std::cout << "---> requesting non local agents" << std::endl;
    }
    // request all missing non-local agents (call this function only once for all agents that need to be requested)
    IO->log_info("agent el connections request for nodes: " + IO->req2str(req_el_connections));
    repast::RepastProcess::instance()->requestAgents<Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(
            context, req_el_connections, *provider, *receiver, *receiver, SET_EL_CONNECTIONS);

    if(rank == 0) {
        std::cout << "---> creating electrical connections to non-local agents" << std::endl;
    }

    for(auto j : connections_missing){
        Agent * agent1 = context.getAgent(j.first);
        Agent * agent2 = context.getAgent(j.second);

        if (agent_network_elec->findEdge(agent1, agent2) == nullptr)
        {
            // if connection does not exist yet

            float r_value = .0;
            float x_value = .0;
            float b_value = .0;
            float g_value = .0;
            float length = .0;
            float long_term_rate = .0;
            float emergency_rate = .0;
            //get no_losses flag
            std::string subtype = "1";
            for(unsigned int u = 0; u<scenario_file_el_grid_content.shape()[1]; u++){
                boost::array<boost::multi_array<int, 2>::index, 2> id1_index = {{0, u}};
                boost::array<boost::multi_array<int, 2>::index, 2> id2_index = {{1, u}};
                boost::array<boost::multi_array<std::string, 1>::index, 1> subtype_index = {{u}};
                boost::array<boost::multi_array<float,2>::index,2> length_index = {{0,u}};
                boost::array<boost::multi_array<float,2>::index,2> r_index = {{1,u}};
                boost::array<boost::multi_array<float,2>::index,2> x_index = {{2,u}};
                boost::array<boost::multi_array<float,2>::index,2> b_index = {{3,u}};
                boost::array<boost::multi_array<float,2>::index,2> g_index = {{4,u}};
                boost::array<boost::multi_array<float,2>::index,2> long_term_rate_index = {{5,u}};
                boost::array<boost::multi_array<float,2>::index,2> emergency_rate_index = {{6,u}};

                int id1 = scenario_file_el_grid_content(id1_index);
                int id2 = scenario_file_el_grid_content(id2_index);


                if(id1 == agent1->getId().id()){
                    if(id2 == agent2->getId().id()){
                        subtype = scenario_file_el_grid_subtypes(subtype_index);
                        length = scenario_file_edge_attributes(length_index);
                        r_value = scenario_file_edge_attributes(r_index);
                        x_value = scenario_file_edge_attributes(x_index);
                        b_value = scenario_file_edge_attributes(b_index);
                        g_value = scenario_file_edge_attributes(g_index);
                        long_term_rate = scenario_file_edge_attributes(long_term_rate_index);
                        emergency_rate = scenario_file_edge_attributes(emergency_rate_index);
                        break;
                    }
                }
                else if(id1 == agent2->getId().id()){
                    if(id2 == agent1->getId().id()){
                        subtype = scenario_file_el_grid_subtypes(subtype_index);
                        length = scenario_file_edge_attributes(length_index);
                        r_value = scenario_file_edge_attributes(r_index);
                        x_value = scenario_file_edge_attributes(x_index);
                        b_value = scenario_file_edge_attributes(b_index);
                        g_value = scenario_file_edge_attributes(g_index);
                        long_term_rate = scenario_file_edge_attributes(long_term_rate_index);
                        emergency_rate = scenario_file_edge_attributes(emergency_rate_index);
                        break;

                    }
                }

            }
            std::string cable_name = std::to_string(agent1->getId().id()) + "_" + std::to_string(agent2->getId().id());
            struct data_props cable_d_props;
            cable_d_props.log.logging = log_cables;
            cable_d_props.log.path_log_file = log_folder +"/cables/" + cable_name + ".log";
            cable_d_props.result.csv_results = results_csv_cables;
            cable_d_props.result.db_results = results_db_cables;
            cable_d_props.result.db_enabled = results_db_ranks || results_db_agents || results_db_cables;
            cable_d_props.result.loglevel = loglevel;
            cable_d_props.result.dbconn = d_props.result.dbconn;
            cable_d_props.result.path_result_file = results_folder +"/cables/" + cable_name + ".csv";
            boost::shared_ptr<Cable> new_cable(new Cable(agent1, agent2, true, cable_name,step_size, model_type, subtype, cable_d_props, r_value, x_value, b_value, g_value, length, long_term_rate, emergency_rate));
            cables.push_back(new_cable);
            agent_network_elec->addEdge(new_cable);
            IO->log_info("added electrical edge (node agents) between agents: " + IO->id2str(j.first) + " and " + IO->id2str(j.second) + " subtype=" + subtype);
        }
    }

    MPI_Barrier(MPI_COMM_WORLD);

    if(rank == 0){
        std::cout << "Done." << std::endl;
    }

}

/*! \brief initialize the communication network
 * */
void Model::init_comm_network(){

    if(rank == 0){
        std::cout << " * Initializing communication network in all MPI processes... " << std::endl;
    }

    IO->start_time_measurement(IO->tm_init_MR);
    if(behavior_type == CTRL_NON_INTELLIGENT){
        //Agent communication network is not required in non-intelligent case
        if(rank == 0) {
            std::cout << "---> not needed because non-intelligent case is simulated!" << std::endl;
        }
        return;
    }

    //if(rank == 0) {
    //    std::cout << "---> creating communication edges to MR Agents" << std::endl;
    //}

    if(!agents_scheduling.empty()) {
        if (context.getAgent(repast::AgentId(TYPE_MR_INT, rank, TYPE_MR_INT, rank)) ==
            nullptr) {
            std::cerr << "Rank " << rank << ": MR is not in context. Abort." << std::endl;
            do_exit(-33);
        }

        for(auto i : local_agents){
            /*create edge to Message Router agent for every local agent in this process*/
            if(i->getId().agentType() != TYPE_MR_INT){
                std::string edge_name = "MR" + std::to_string(rank) + "_" + std::to_string(i->getId().id());
                boost::shared_ptr<Edge<Agent>> new_edge(new Edge<Agent>(MR_agent, i, false, edge_name, step_size));
                if(!agent_network_MR->findEdge(MR_agent, i)) {
                    MR_edges.push_back(new_edge);
                    agent_network_MR->addEdge(new_edge);
                    IO->log_info("added communication edge between agents: " + IO->id2str(MR_agent->getId())
                                 + " and " + IO->id2str(i->getId()));
                }
            }
        }



        if (context.getAgent(repast::AgentId(TYPE_DF_INT,rank,TYPE_DF_INT, rank)) == nullptr){
            std::cerr << "Rank " << rank << ": DF is not in context. Abort." << std::endl;
            do_exit(-33);
        }

    }

    //Initialize Shared Network of Message Router agents
    /*request a copy of all other MR agents in other ranks*/
    repast::AgentRequest req_MR(rank);

    if (!agents_scheduling.empty()) {
        for (int i = 0; i < world_size; i++) {
            if (i != rank) {
                repast::AgentId other_MR(TYPE_MR_INT, i, TYPE_MR_INT, i);
                req_MR.addRequest(other_MR);
            }
        }
    }

    MPI_Barrier(MPI_COMM_WORLD);

    // request all missing MR agents
    IO->log_info("MR agent request: " + IO->req2str(req_MR));
    repast::RepastProcess::instance()->requestAgents<Agent, AgentPackage, AgentPackageProvider, AgentPackageReceiver>(
            context, req_MR, *provider, *receiver, *receiver, SET_MESSAGE_ROUTERS);
    IO->log_info("finished MR agent request");

    if (!agents_scheduling.empty()) {
        /*create edge to MR agents of other ranks*/
        for (int i = 0; i <= highest_filled_rank; i++) {
            if (i != rank) {
                Agent *otherMR = context.getAgent(
                        repast::AgentId(TYPE_MR_INT, i, TYPE_MR_INT, i));
                if (otherMR != nullptr) {
                    IO->log_info("Other MR: " + IO->id2str(otherMR->getId()));
                    std::string edge_name =
                            "MR" + std::to_string(rank) + "_MR" + std::to_string(i);
                    boost::shared_ptr<Edge<Agent>> new_edge(
                            new Edge<Agent>(MR_agent, otherMR, true, edge_name, step_size));
                    MR_edges.push_back(new_edge);
                    if (!agent_network_MR->findEdge(MR_agent, otherMR)) {
                        agent_network_MR->addEdge(new_edge);
                        IO->log_info("added communication edge between agents: " +
                                     IO->id2str(MR_agent->getId())
                                     + " and " + IO->id2str(otherMR->getId()));
                    }
                } else {
                    IO->log_info("Other MR not in context");
                }
            }
        }
    }


    IO->stop_time_measurement(IO->tm_init_MR);

    MPI_Barrier(MPI_COMM_WORLD);
    if(rank == 0){
        std::cout << "Done." << std::endl;
    }

}


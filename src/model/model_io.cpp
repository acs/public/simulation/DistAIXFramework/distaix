/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include <cstdio>
#include <boost/exception/diagnostic_information.hpp>
#include <repast_hpc/Utilities.h>
#include <repast_hpc/RepastProcess.h>

#ifdef USE_DB
    #include "DBException.h"
#endif

#include "model/model_io.h"
#include "model/config.h"




ModelIO::ModelIO(repast::Properties* _props,  int _rank, int _world_size, std::string _profile_dir, struct data_props  _d_props): IO_object(_d_props),
props(_props), rank(_rank), world_size(_world_size), profile_dir(_profile_dir)
{
    // start time measurement
    tm = new Time_measurement("simulation-time", rank);
    tm->start();

    tm_sync_el	= new Time_measurement("synctime_el",rank);
    tm_sync_mr	= new Time_measurement("synctime_mr",rank);
    tm_step	= new Time_measurement("steptime",rank);
    tm_tick	= new Time_measurement("ticktime",rank);
    tm_rt_tick	= new Time_measurement("realtimeticktime",rank);
    tm_fb_sweep = new Time_measurement("gridcalculationtime",rank);
    tm_msg_proc_internal = new Time_measurement("msgproctime_internal",rank);
    tm_msg_proc_external = new Time_measurement("msgproctime_external",rank);
    tm_behaviors = new Time_measurement("behaviors",rank);
    tm_calculate_components = new Time_measurement("calculatecomponents",rank);
    tm_backward_sweep = new Time_measurement("backward_sweep",rank);
    tm_forward_sweep = new Time_measurement("forward_sweep",rank);
    tm_init = new Time_measurement("init",rank);
    tm_distribute_agents = new Time_measurement("distribute_agents",rank);
    tm_init_MR = new Time_measurement("init_MR",rank);
    tm_comm_partners_init = new Time_measurement("comm_partners_init",rank);


    tm_load = new Time_measurement("load_behavior", rank);
    tm_pv = new Time_measurement("pv_behavior", rank);
    tm_ev = new Time_measurement("ev_behavior", rank);
    tm_hp = new Time_measurement("hp_behavior", rank);
    tm_chp = new Time_measurement("chp_behavior", rank);
    tm_wec = new Time_measurement("wec_behavior", rank);
    tm_bio = new Time_measurement("bio_behavior", rank);
    tm_bat = new Time_measurement("battery_behavior", rank);
    tm_comp = new Time_measurement("compensator_behavior", rank);
    tm_substation = new Time_measurement("substation_behavior", rank);
    tm_slack = new Time_measurement("slack_behavior", rank);
    tm_df = new Time_measurement("df_behavior", rank);

    tm_bcast_FBS = new Time_measurement("bcast_fbs", rank);
    tm_bcast_forward_sweep = new Time_measurement("bcast_forward_sweep", rank);
    tm_bcast_backward_sweep = new Time_measurement("bcast_backward_sweep", rank);

    tm_csv = new Time_measurement("results_csv", rank);
    tm_db = new Time_measurement("results_db", rank);
    tm_db_serialize = new Time_measurement("results_db_serialize", rank);
    tm_db_add = new Time_measurement("results_db_add", rank);
}

/*! \brief Initialize message and result logging on rank level
 *
 * Message logging output goes to runlog/rankX.log.
 * Result logging output goes to results/rankX.csv.
 * X is the number of the rank.
 * */
void ModelIO::init_logging() {

    if(rank==0 ) {
        boost::filesystem::path destination(props->getProperty("log.folder"));
        boost::filesystem::path destination_agents(props->getProperty("log.folder") + "/agents/");
        boost::filesystem::path destination_behavior(props->getProperty("log.folder") + "/behavior/");
        boost::filesystem::path destination_edges(props->getProperty("log.folder") + "/cables/");
        boost::filesystem::path destination_results(props->getProperty("results.folder"));
        boost::filesystem::path destination_results_agents(props->getProperty("results.folder") + "/agents/");
        boost::filesystem::path destination_results_edges(props->getProperty("results.folder") + "/cables/");

        /* create new folders in rank 0*/
        boost::filesystem::create_directories(destination);
        boost::filesystem::create_directory(destination_agents);
        boost::filesystem::create_directory(destination_behavior);
        boost::filesystem::create_directory(destination_edges);

        //result folders are created in any case
        boost::filesystem::create_directories(destination_results);
        boost::filesystem::create_directory(destination_results_agents);
        boost::filesystem::create_directory(destination_results_edges);

    }
    //all ranks have to wait until folder is created
    MPI_Barrier(MPI_COMM_WORLD);

    init_logfile();
    init_resultfile();
    update_t(0.0);
}


/*! \brief Initialize database if activated
 * */
void ModelIO::init_db() {
#ifdef USE_DB
    if(d_props.result.db_enabled){
        try {

            d_props.result.dbconn = new DBConnector(props->getProperty("db.host"),
                                     props->getProperty("db.user"),
                                     props->getProperty("db.pass"),
                                     repast::strToInt(props->getProperty("db.port")),
                                     props->getProperty("db.dbname"));

            // initialize database and table (once!) and connect all threads to the database
            //

            if (rank == 0) {
                d_props.result.dbconn->initializePostgreSQLDB();
            }

            MPI_Barrier(MPI_COMM_WORLD);

            d_props.result.dbconn->setupPostgreSQLDB();
            d_props.result.dbconn->initPostgreSQLCopy();

            // create a global simulation ID
            if (rank == 0) {
                simId = d_props.result.dbconn->newSim();
            }

            //distribute global simulation ID around processes
            MPI_Bcast(&simId, 1, MPI_INT, 0, MPI_COMM_WORLD);
            d_props.result.dbconn->setSimId(simId);

            int iothreads = repast::strToInt(props->getProperty("cassandra.iothreads"));
            d_props.result.dbconn->setCassandraConfig(props->getProperty("cassandra.hosts"),"swarmgrid", iothreads);
            d_props.result.dbconn->createCassandraSession();

            if(rank == 0){
                //create keyspace and table
                d_props.result.dbconn->setupCassandra();
            }
            MPI_Barrier(MPI_COMM_WORLD);

            d_props.result.dbconn->prepareCassandraQuery();

            //initial typedeclarations
            if (rank == 0) {
                // register timeMeasurements
                d_props.result.dbconn->addTimeMeasurementType("synctime_el", "Time for syncing el connections of Agents in us");
                d_props.result.dbconn->addTimeMeasurementType("synctime_mr", "Time for syncing MR Agents in us");

                d_props.result.dbconn->addTimeMeasurementType("steptime", "Time for step in us");
                d_props.result.dbconn->addTimeMeasurementType("ticktime","duration of a simulation tick in us");
                d_props.result.dbconn->addTimeMeasurementType("realtimeticktime","duration of a realtime simulation tick in us");

                d_props.result.dbconn->addTimeMeasurementType("msgproctime_internal", "Time for deliverin process internal messages in us");
                d_props.result.dbconn->addTimeMeasurementType("msgproctime_external", "Time for delivering process external messages in us");
                d_props.result.dbconn->addTimeMeasurementType("behaviors", "Time for agent behavior computation in us");

                d_props.result.dbconn->addTimeMeasurementType("gridcalculationtime", "Time for calculating grid in us");
                d_props.result.dbconn->addTimeMeasurementType("calculatecomponents","Time for calculating components in us");
                d_props.result.dbconn->addTimeMeasurementType("backward_sweep","Time for backward sweeping in us");
                d_props.result.dbconn->addTimeMeasurementType("forward_sweep","Time for forward sweeping in us");

                d_props.result.dbconn->addTimeMeasurementType("load_behavior","Time for load behavior in us");
                d_props.result.dbconn->addTimeMeasurementType("pv_behavior","Time for PV behavior in us");
                d_props.result.dbconn->addTimeMeasurementType("ev_behavior","Time for EV behavior in us");
                d_props.result.dbconn->addTimeMeasurementType("hp_behavior","Time for HP behavior in us");
                d_props.result.dbconn->addTimeMeasurementType("chp_behavior","Time for CHP behavior in us");
                d_props.result.dbconn->addTimeMeasurementType("wec_behavior","Time for WEC behavior in us");
                d_props.result.dbconn->addTimeMeasurementType("bio_behavior","Time for biofuel behavior in us");
                d_props.result.dbconn->addTimeMeasurementType("battery_behavior","Time for battery behavior in us");
                d_props.result.dbconn->addTimeMeasurementType("compensator_behavior","Time for compensator behavior in us");
                d_props.result.dbconn->addTimeMeasurementType("substation_behavior","Time for substation behavior in us");
                d_props.result.dbconn->addTimeMeasurementType("slack_behavior","Time for slack behavior in us");
                d_props.result.dbconn->addTimeMeasurementType("df_behavior","Time for DF behavior in us");

                d_props.result.dbconn->addTimeMeasurementType("bcast_fbs","Time for MPI broadcast for FBS convergence in us");
                d_props.result.dbconn->addTimeMeasurementType("bcast_forward_sweep","Time for MPI broadcast for forward sweep completion in us");
                d_props.result.dbconn->addTimeMeasurementType("bcast_backward_sweep","Time for MPI broadcast for backward sweep completion in us");

                d_props.result.dbconn->addTimeMeasurementType("results_csv","Time for saving results to csv in us");
                d_props.result.dbconn->addTimeMeasurementType("results_db","Time for saving results to db in us");
                d_props.result.dbconn->addTimeMeasurementType("results_db_serialize","Time for serializing results before saving to DB in us");
                d_props.result.dbconn->addTimeMeasurementType("results_db_add","Time for adding results to DB in us");

                // initialize metadata
                addMetaEntry(rank, "simulationmeta", "stop.at", props->getProperty("stop.at"));
                addMetaEntry(rank, "simulationmeta", "step.size", props->getProperty("step.size"));
                addMetaEntry(rank, "simulationmeta", "dir.scenario", props->getProperty("dir.scenario"));
                addMetaEntry(rank, "simulationmeta", "dir.profiles", props->getProperty("dir.profiles"));
                addMetaEntry(rank, "simulationmeta", "file.scenario.components", props->getProperty("file.scenario.components"));
                addMetaEntry(rank, "simulationmeta", "file.scenario.elgrid", props->getProperty("file.scenario.elgrid"));
                addMetaEntry(rank, "simulationmeta", "interpolation_type", props->getProperty("profile.interpolation_type"));
                addMetaEntry(rank, "simulationmeta", "ctrl.type", props->getProperty("ctrl.type"));
                addMetaEntry(rank, "simulationmeta", "use.commdata", props->getProperty("use.commdata"));
                addMetaEntry(rank, "simulationmeta", "model.type", props->getProperty("model.type"));
                addMetaEntry(rank, "simulationmeta", "realtime", props->getProperty("realtime"));
                addMetaEntry(rank, "simulationmeta", "distribution_method", props->getProperty("distribution_method"));
                addMetaEntry(rank, "simulationmeta", "results.loglevel", props->getProperty("results.loglevel"));
                addMetaEntry(rank, "simulationmeta", "results.db.ranks", props->getProperty("results.db.ranks"));
                addMetaEntry(rank, "simulationmeta", "results.db.agents", props->getProperty("results.db.agents"));
                addMetaEntry(rank, "simulationmeta", "results.db.cables", props->getProperty("results.db.cables"));
                addMetaEntry(rank, "simulationmeta", "results.ids", props->getProperty("results.ids"));
                addMetaEntry(rank, "simulationmeta", "results.df", props->getProperty("results.df"));
                addMetaEntry(rank, "simulationmeta", "results.mr", props->getProperty("results.mr"));

            }



            // catch DB Exceptions
        } catch (const DBException &e) {
            std::cout << "[DBEXCEPTION ON RANK " << rank << "] Error: " << e.what() << std::endl;
            std::cerr << "[DBEXCEPTION ON RANK " << rank << "] Error: " << e.what() << std::endl;
        }
    }
    else{
        d_props.result.dbconn = nullptr; //will always be nullptr in non-DB mode
    }
#endif
}

/*! \brief Finish all IO activities depending on which are activated
 * */
void ModelIO::finish_io() {
#ifdef USE_DB
    if(d_props.result.db_enabled){
        try{
            if(rank == 0) {
                d_props.result.dbconn->finishSimulation();
            }

            d_props.result.dbconn->endCopy();

            if(rank == 0){
                std::cout << "Waiting for inserts into DB...";
            }
            delete d_props.result.dbconn;
            if(rank == 0){
                std::cout << " Done" << std::endl;
            }
        }catch(const DBException &e){
            std::cout << "[DBEXCEPTION ON RANK " << rank << "] Error: " << e.what() << std::endl;
            std::cerr << "[DBEXCEPTION ON RANK " << rank << "] Error: " << e.what() << std::endl;
        }
    }
#endif

    IO_object::finish_io();
}


/*! \brief Finish all time measurements
 *  \param stop_at [in] number of time steps to be calculated in the simulation
 * */
void ModelIO::finish_time_measurements(unsigned long &stop_at){
    tm->stop();

    /* if there's a timemeasurement instance and a dbconn instance, stop the measurement and send it to the database */
    if(d_props.result.db_results){
        this->addMetaEntry(rank, "simulationmeta", "simulation-time in us", std::to_string(tm->get_duration()));
    }

    delete tm;
    delete tm_sync_el;
    delete tm_sync_mr;
    delete tm_tick;
    delete tm_rt_tick;
    delete tm_step;
    delete tm_fb_sweep;
    delete tm_msg_proc_internal;
    delete tm_msg_proc_external;
    delete tm_behaviors;
    delete tm_calculate_components;
    delete tm_backward_sweep;
    delete tm_forward_sweep;
    delete tm_init;
    delete tm_distribute_agents;
    delete tm_init_MR;
    delete tm_comm_partners_init;

    delete tm_load;
    delete tm_pv;
    delete tm_ev;
    delete tm_hp;
    delete tm_chp;
    delete tm_wec;
    delete tm_bio;
    delete tm_bat;
    delete tm_comp;
    delete tm_substation;
    delete tm_slack;
    delete tm_df;

    delete tm_bcast_FBS;
    delete tm_bcast_forward_sweep;
    delete tm_bcast_backward_sweep;

    delete tm_csv;
    delete tm_db;
    delete tm_db_serialize;
    delete tm_db_add;

}


/*! \brief Add a meta entry to the database
 *  \param _rank [in] rank for which the meta entry shall be added
 *  \param _metatype [in] type of meta info
 *  \param _key [in] key of meta info
 *  \param _value [in] of meta info
 * */
void ModelIO::addMetaEntry(int _rank, std::string _metatype, std::string _key, std::string _value) {
#ifdef USE_DB
    if(d_props.result.db_enabled) {
        try {
            d_props.result.dbconn->addSimMetaEntry(_rank, _metatype, _key, _value);
        }
        catch (const DBException &ex){
            std::cout << "[DBEXCEPTION ON RANK " << rank << "] Error: " << ex.what() << std::endl;
            std::cerr << "[DBEXCEPTION ON RANK " << rank << "] Error: " << ex.what() << std::endl;
        }
    }
#endif
}


/*! \brief Log time measurements on rank level for current simulation tick
 * */
void ModelIO::log_results_for_tick() {
    log_result_csv( std::to_string((int) repast::RepastProcess::instance()->getScheduleRunner().currentTick()) +
                        "," + std::to_string(tm_tick->get_duration() / 1000000.0) + //us to s
                        "," + std::to_string(tm_sync_el->get_duration() / 1000000.0) + //us to s
                        "," + std::to_string(tm_sync_mr->get_duration() / 1000000.0) + //us to s
                        "," + std::to_string(tm_step->get_duration() / 1000000.0) +
                        "," + std::to_string(tm_fb_sweep->get_duration() / 1000000.0) +
                        "," + std::to_string(tm_calculate_components->get_duration() / 1000000.0) +
                        "," + std::to_string(tm_forward_sweep->get_duration() / 1000000.0) +
                        "," + std::to_string(tm_backward_sweep->get_duration() / 1000000.0) +
                        "," + std::to_string(tm_msg_proc_internal->get_duration() / 1000000.0) +
                        "," + std::to_string(tm_msg_proc_external->get_duration() / 1000000.0) +
                        "," + std::to_string(tm_behaviors->get_duration() / 1000000.0) +
                        "," + std::to_string(number_of_fbs_sweeps) +
                        "," + std::to_string(tm_rt_tick->get_duration() / 1000000.0) +
                        //"," + std::to_string(tm_load->get_duration() / 1000000.0) +
                        //"," + std::to_string(tm_pv->get_duration() / 1000000.0) +
                        //"," + std::to_string(tm_ev->get_duration() / 1000000.0) +
                        //"," + std::to_string(tm_hp->get_duration() / 1000000.0) +
                        //"," + std::to_string(tm_chp->get_duration() / 1000000.0) +
                        //"," + std::to_string(tm_wec->get_duration() / 1000000.0) +
                        //"," + std::to_string(tm_bio->get_duration() / 1000000.0) +
                        //"," + std::to_string(tm_bat->get_duration() / 1000000.0) +
                        //"," + std::to_string(tm_comp->get_duration() / 1000000.0) +
                        //"," + std::to_string(tm_substation->get_duration() / 1000000.0) +
                        //"," + std::to_string(tm_slack->get_duration() / 1000000.0) +
                        //"," + std::to_string(tm_df->get_duration() / 1000000.0)+
                        //"," + std::to_string(tm_bcast_FBS->get_duration() / 1000000.0)+
                        //"," + std::to_string(tm_bcast_forward_sweep->get_duration() / 1000000.0)+
                        //"," + std::to_string(tm_bcast_backward_sweep->get_duration() / 1000000.0)+
                        //"," + std::to_string(forward_loop_counter)+
                        //"," + std::to_string(backward_loop_counter)
                        "," + std::to_string(tm_csv->get_duration() / 1000000.0)+
                        "," + std::to_string(tm_db->get_duration() / 1000000.0)+
                        "," + std::to_string(tm_db_serialize->get_duration() / 1000000.0)+
                        "," + std::to_string(tm_db_add->get_duration() / 1000000.0)

    );


#ifdef USE_DB
    if(d_props.result.db_results){
        auto tick = repast::RepastProcess::instance()->getScheduleRunner().currentTick();
        try {

            //TODO: DO NOT SAVE TIME MEASUREMENTS OF RANKS TO POSTGRESQL DB BUT TO CASSANDRA DB TO AVOID FLUSHING POSTGRES DURING SIMULATION

            d_props.result.dbconn->addTimeMeasurement(tm_sync_el->get_time_measurement_type(), tm_sync_el->getId(), tm_sync_el->get_duration(),
                                       tick);
            d_props.result.dbconn->addTimeMeasurement(tm_sync_mr->get_time_measurement_type(), tm_sync_mr->getId(), tm_sync_mr->get_duration(),
                                                      tick);
            d_props.result.dbconn->addTimeMeasurement(tm_tick->get_time_measurement_type(), tm_tick->getId(), tm_tick->get_duration(),
                                       tick);
            d_props.result.dbconn->addTimeMeasurement(tm_step->get_time_measurement_type(), tm_step->getId(), tm_step->get_duration(),
                                       tick);
            d_props.result.dbconn->addTimeMeasurement(tm_fb_sweep->get_time_measurement_type(), tm_fb_sweep->getId(),
                                       tm_fb_sweep->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_calculate_components->get_time_measurement_type(),
                                       tm_calculate_components->getId(),
                                       tm_calculate_components->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_forward_sweep->get_time_measurement_type(), tm_forward_sweep->getId(),
                                       tm_forward_sweep->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_backward_sweep->get_time_measurement_type(), tm_backward_sweep->getId(),
                                       tm_backward_sweep->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_msg_proc_internal->get_time_measurement_type(), tm_msg_proc_internal->getId(),
                                       tm_msg_proc_internal->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_msg_proc_external->get_time_measurement_type(), tm_msg_proc_external->getId(),
                                                      tm_msg_proc_external->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_behaviors->get_time_measurement_type(), tm_behaviors->getId(),
                                                      tm_behaviors->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_rt_tick->get_time_measurement_type(), tm_rt_tick->getId(),
                                       tm_rt_tick->get_duration(), tick);

            // Agent behavior time measurements
            /*
            d_props.result.dbconn->addTimeMeasurement(tm_load->get_time_measurement_type(), tm_load->getId(), tm_load->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_pv->get_time_measurement_type(), tm_pv->getId(), tm_pv->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_ev->get_time_measurement_type(), tm_ev->getId(), tm_ev->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_hp->get_time_measurement_type(), tm_hp->getId(), tm_hp->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_chp->get_time_measurement_type(), tm_chp->getId(), tm_chp->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_wec->get_time_measurement_type(), tm_wec->getId(), tm_wec->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_bio->get_time_measurement_type(), tm_bio->getId(), tm_bio->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_bat->get_time_measurement_type(), tm_bat->getId(), tm_bat->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_comp->get_time_measurement_type(), tm_comp->getId(), tm_comp->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_substation->get_time_measurement_type(), tm_substation->getId(), tm_substation->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_slack->get_time_measurement_type(), tm_slack->getId(), tm_slack->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_df->get_time_measurement_type(), tm_df->getId(), tm_df->get_duration(), tick);
            */

            // MPI broadcast time measurements
            /*
            d_props.result.dbconn->addTimeMeasurement(tm_bcast_FBS->get_time_measurement_type(), tm_bcast_FBS->getId(), tm_bcast_FBS->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_bcast_forward_sweep->get_time_measurement_type(), tm_bcast_forward_sweep->getId(), tm_bcast_forward_sweep->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_bcast_backward_sweep->get_time_measurement_type(), tm_bcast_backward_sweep->getId(), tm_bcast_backward_sweep->get_duration(), tick);
            */

            d_props.result.dbconn->addTimeMeasurement(tm_csv->get_time_measurement_type(), tm_csv->getId(), tm_csv->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_db->get_time_measurement_type(), tm_db->getId(), tm_db->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_db_serialize->get_time_measurement_type(), tm_db_serialize->getId(), tm_db_serialize->get_duration(), tick);
            d_props.result.dbconn->addTimeMeasurement(tm_db_add->get_time_measurement_type(), tm_db_add->getId(), tm_db_add->get_duration(), tick);
        }
        catch (const DBException &ex){
            std::cout << "[DBEXCEPTION ON RANK " << rank << "] Error: " << ex.what() << std::endl;
            std::cerr << "[DBEXCEPTION ON RANK " << rank << "] Error: " << ex.what() << std::endl;
        }
        if(tick == 1){
            //send time measurements of init phase as meta info to db
            if(d_props.result.db_results) {
                this->addMetaEntry(rank, "simulationmeta", "time_measurement_init in us",
                                   std::to_string(tm_init->get_duration()));
                this->addMetaEntry(rank, "simulationmeta", "time_measurement_distribute_agents in us",
                                   std::to_string(tm_distribute_agents->get_duration()));
                this->addMetaEntry(rank, "simulationmeta", "time_measurement_init_MR in us",
                                   std::to_string(tm_init_MR->get_duration()));
                this->addMetaEntry(rank, "simulationmeta", "time_measurement_comm_partners_init in us",
                                   std::to_string(tm_comm_partners_init->get_duration()));
	        this->flush_db();
            }
        }
    }
#endif
}

void ModelIO::reset_time_measurements() {
    //reset all time measurements for the next tick
    tm_sync_el->reset();
    tm_sync_mr->reset();
    tm_tick->reset();
    tm_rt_tick->reset();
    tm_step->reset();
    tm_fb_sweep->reset();
    tm_calculate_components->reset();
    tm_forward_sweep->reset();
    tm_backward_sweep->reset();
    tm_msg_proc_internal->reset();
    tm_msg_proc_external->reset();
    tm_behaviors->reset();

    tm_load->reset();
    tm_pv->reset();
    tm_ev->reset();
    tm_hp->reset();
    tm_chp->reset();
    tm_wec->reset();
    tm_bio->reset();
    tm_bat->reset();
    tm_comp->reset();
    tm_substation->reset();
    tm_slack->reset();
    tm_df->reset();

    tm_bcast_FBS->reset();
    tm_bcast_forward_sweep->reset();
    tm_bcast_backward_sweep->reset();

    tm_csv->reset();
    tm_db->reset();
    tm_db_serialize->reset();
    tm_db_add->reset();

}

void ModelIO::set_result_file_header() {
    result_stream <<
    "tick,tick time [s]," <<
    "sync time el [s],"<<
    "sync time MR [s],"<<
    "step time [s]," <<
    "forward backward sweep time [s],"<<
    "calculate components [s],"<<
    "forward sweep [s],"<<
    "backward sweep [s],"<<
    "msg proc time internal [s],"<<
    "msg proc time external [s],"<<
    "behavior calculation time [s],"<<
    "number of FBS sweeps,"<<
    "real time tick time [s]," <<
    //"Load behavior [s]," <<
    //"PV behavior [s]," <<
    //"EV behavior [s]," <<
    //"HP behavior [s]," <<
    //"CHP behavior [s]," <<
    //"WEC behavior [s]," <<
    //"Biofuel behavior [s]," <<
    //"Battery behavior [s]," <<
    //"Compensator behavior [s]," <<
    //"Substation behavior [s]," <<
    //"Slack behavior [s]," <<
    //"DF behavior [s]," <<
    //"MPI bcast FBS [s]," <<
    //"MPI bcast forward sweep [s]," <<
    //"MPI bcast backward sweep [s]," <<
    //"Forward loop counter," <<
    //"Backward loop counter" <<
    "CSV saving," <<
    "DB saving," <<
    "DB serializing," <<
    "DB adding" <<
     std::endl;


}

/*! \brief Log one line in csv file
 *  \param result_line [in] line to write to file
 * */
void ModelIO::log_result_csv(std::string result_line) {
    if(d_props.result.csv_results){
        result_stream << result_line << std::endl;
    }
}

/*! \brief Flush database if it is in use
 * */
void ModelIO::flush_db() {
#ifdef USE_DB
    if(d_props.result.db_enabled) {
        try {
            d_props.result.dbconn->flush();
        }
        catch (const DBException &ex){
            std::cout << "[DBEXCEPTION ON RANK " << rank << "] Error: " << ex.what() << std::endl;
            std::cerr << "[DBEXCEPTION ON RANK " << rank << "] Error: " << ex.what() << std::endl;
        }
    }
#endif
}


/*! \brief Print configuration of the model. To be called at the beginning of the simulation run.
 * */
void ModelIO::print_model_configuration() {

    std::cout << "######################################################################################" << std::endl;
    std::cout << "##################################### MODEL CONFIG ###################################" << std::endl;
    std::cout << "######################################################################################" << std::endl;
    std::cout << "Number of processes\t\t=\t"  << world_size << std::endl;
    std::cout << "Scenario folder:\t\t=\t"  << props->getProperty("dir.scenario") << std::endl;
    std::cout << "Components file:\t\t=\t" << props->getProperty("file.scenario.components") << std::endl;
    std::cout << "El. grid file\t\t\t=\t" << props->getProperty("file.scenario.elgrid") << std::endl;
    std::cout << "Profile folder:\t\t\t=\t" << profile_dir << std::endl;
    std::cout << "Use latency matrix:\t\t=\t" << props->getProperty("use.commdata") << std::endl;
    if(props->getProperty("use.commdata") == "1"){
        std::cout << "Latency matrix: \t\t\t=\t" << props->getProperty("file.scenario.latency") << std::endl;
    }
    std::cout << "Interpolation:\t\t\t=\t" << props->getProperty("profile.interpolation_type") << std::endl;
    std::cout << "Number of simulation steps:\t=\t" << props->getProperty("stop.at") << std::endl;
    std::cout << "Size of simulation step in s:\t=\t" << props->getProperty("step.size") << std::endl;
    std::cout << "Agent distribution method:\t=\t" << props->getProperty("distribution_method") << std::endl;
    std::cout << "Behavior type\t\t\t=\t" << props->getProperty("ctrl.type") << std::endl;
    std::cout << "Model type\t\t\t=\t" << props->getProperty("model.type") << std::endl;
    std::cout << "Real time\t\t\t=\t" << props->getProperty("realtime") << std::endl;

    std::cout << "############################# RESULT SAVING CONFIG ###################################" << std::endl;
    std::cout << "Result folder:\t\t\t=\t" << props->getProperty("results.folder") << std::endl;
    std::cout << "Loglevel:\t\t\t=\t" << props->getProperty("results.loglevel") << std::endl;
    std::cout << "Rank reuslts csv:\t\t=\t" << props->getProperty("results.csv.ranks") << std::endl;
    std::cout << "Rank reuslts DB:\t\t=\t" << props->getProperty("results.db.ranks") << std::endl;
    std::cout << "Agent results csv:\t\t=\t" << props->getProperty("results.csv.agents") << std::endl;
    std::cout << "Agent results DB:\t\t=\t" << props->getProperty("results.db.agents") << std::endl;
    if((props->getProperty("results.csv.agents") == "1" ||
            props->getProperty("results.db.agents") == "1" )
            && (props->getProperty("results.ids") != "0")) {
        std::cout << "Agents to save:\t\t\t=\t" << props->getProperty("results.ids") << std::endl;
    }
    std::cout << "Cable results csv:\t\t=\t" << props->getProperty("results.csv.cables") << std::endl;
    std::cout << "Cable results DB:\t\t=\t" << props->getProperty("results.db.cables") << std::endl;
    std::cout << "Save DF agent:\t\t\t=\t" << props->getProperty("results.df") << std::endl;
    std::cout << "Save MR agent:\t\t\t=\t" << props->getProperty("results.mr") << std::endl;

    std::cout << "################################ LOGGING CONFIG ######################################" << std::endl;
    std::cout << "Log folder:\t\t\t=\t" << props->getProperty("log.folder") << std::endl;
    std::cout << "Rank logging:\t\t\t=\t" << props->getProperty("log.ranks") << std::endl;
    std::cout << "Agent logging:\t\t\t=\t" << props->getProperty("log.agents") << std::endl;
    if((props->getProperty("log.agents") == "1") && (props->getProperty("log.agents") != "0")){
        std::cout << "Agents to log:\t\t\t=\t" << props->getProperty("log.ids") << std::endl;
    }
    std::cout << "Cable logging:\t\t\t=\t" << props->getProperty("log.cables") << std::endl;
    std::cout << "Log DF agent:\t\t\t=\t" << props->getProperty("log.df") << std::endl;
    std::cout << "Log MR agent:\t\t\t=\t" << props->getProperty("log.mr") << std::endl;

    if(d_props.result.db_enabled){
        std::cout << "############################# DATABASE CONFIG ####################################" << std::endl;
        std::cout << "db.host\t\t\t\t=\t" << props->getProperty("db.host") << std::endl;
        std::cout << "db.port\t\t\t\t=\t" << props->getProperty("db.port") << std::endl;
        std::cout << "db.user\t\t\t\t=\t" << props->getProperty("db.user") << std::endl;
        std::cout << "db.pass\t\t\t\t=\t" << props->getProperty("db.pass") << std::endl;
        std::cout << "db.dbname\t\t\t=\t" << props->getProperty("db.dbname") << std::endl;
    }

    if(props->getProperty("ctrl.type") == "2" || props->getProperty("ctrl.type") == "4"){
        std::cout << "############################## VILLAS CONFIG #####################################" << std::endl;
        std::cout << "villas.format\t\t\t=\t" << props->getProperty("villas.format") << std::endl;

        std::cout << "villas.mqtt.broker\t\t=\t" << props->getProperty("villas.mqtt.broker") << std::endl;
        std::cout << "villas.mqtt.port\t\t=\t" << props->getProperty("villas.mqtt.port") << std::endl;
        std::cout << "villas.mqtt.qos\t\t\t=\t" << props->getProperty("villas.mqtt.qos") << std::endl;
        std::cout << "villas.mqtt.retain\t\t=\t" << props->getProperty("villas.mqtt.retain") << std::endl;
        std::cout << "villas.mqtt.ssl_enabled\t\t=\t" << props->getProperty("villas.mqtt.ssl_enabled") << std::endl;
        std::cout << "villas.mqtt.keepalive\t\t=\t" << props->getProperty("villas.mqtt.keepalive") << std::endl;
        std::cout << "villas.mqtt.user\t\t=\t" << props->getProperty("villas.mqtt.user") << std::endl;
        std::cout << "villas.mqtt.password\t\t=\t" << props->getProperty("villas.mqtt.password") << std::endl;

    } else if (props->getProperty("ctrl.type") == "3"){
        std::cout << "############################## VILLAS CONFIG #####################################" << std::endl;
        std::cout << "villas.format\t\t\t=\t" << props->getProperty("villas.format") << std::endl;
        // TODO add nanomsg specific config here
    }

    std::cout << "######################################################################################" << std::endl;

    std::cout.flush();

}

/*! \brief Read a profile file
 *  \param path [in] Path to csv file
 *  \param values [out] Array where the content of the profile is stored
 *  \return false on error, true on success
 * */
bool ModelIO::read_profile_file(char* path, double* &t, std::vector<double*> &values,
    std::vector<std::string> &headers, unsigned int &num_values)
{
    std::vector<std::string> line_vector;
    int n_columns = 0;
    int n_rows = 0;

    /* profile file does contain header but it is needed here */
    this->read_file_mpi(path, line_vector, n_columns, n_rows, MPI_COMM_WORLD);
    num_values = n_rows-1;

    /* allocate memory */
    for (unsigned int i = 0; i < (unsigned int) n_columns; i++) {
        double *v = new double[num_values];
        if (i == 0) {
            /* time */
            t = v;
        } else {
            /* profile values */
            values.push_back(v);
        }
    }

    unsigned int row = 0, col = 0;

    //MPI_Barrier(MPI_COMM_WORLD);

    try {
        for ( auto & one_line : line_vector ) {
            col = 0;
            std::stringstream linestream(one_line);
            std::string token;

            if (row == 0) {
                /* first line contains header */
                while(std::getline(linestream, token, ',')){
                    if (col>0) {
                        headers.push_back(token);
                    }
                    col++;
                }
            } else {
                /* other lines contains data */
                while(std::getline(linestream, token, ',')){
                    if (col == 0) {
                        /* time */
                        t[row-1] = std::stod(token);
                    } else {
                        /* values */
                        values[col-1][row-1] = std::stod(token);
                    }
                    col++;
                }
            }
            row++;
        }
        return true;

    }
    catch(std::invalid_argument &err){
        std::cerr << "Rank " << rank << ": Error (invalid argument) converting data from profile file " << path  << " to boost multi_array\nWhat:" << *(err.what()) << std::endl;
        return false;
    }
    catch(boost::exception & err){
        std::cerr << "Rank " << rank << ": Error (boost) converting data from profile file " << path << "to boost multi_array\nWhat:\n" <<  boost::diagnostic_information(err) << std::endl;
        return false;
    }
}

/*! \brief Read a components.csv scenario file
 *  \param path [in] Path to csv file
 *  \param values [out] Array where the int content of the CSV scenario file shall be stored
 *  \param subtypes [out] Array where the subtypes are stored
 *  \param components_attributes [out] Array where the float attributes of the components are stored
 *  \return false on error, true on success
 * */
bool ModelIO::read_components_file(char* path, boost::multi_array<int, 2> &values, boost::multi_array<std::string, 1> &subtypes, boost::multi_array<double, 2> &components_attributes){

    std::vector<std::string> line_vector;
    int n_columns = 0;
    int n_rows = 0;

    this->read_file_mpi(path, line_vector, n_columns, n_rows, MPI_COMM_WORLD);

    //resize values
    boost::multi_array<int, 2>::extent_gen extents;
    boost::multi_array<std::string,1>::extent_gen extents_subtype;
    boost::multi_array<double,2>::extent_gen extents_attributes;
    values.resize(extents[4][n_rows]);
    subtypes.resize(extents_subtype[n_rows]);
    components_attributes.resize(extents_attributes[COMPONENTS_MAX_ATTRIBUTE_COLUMNS][n_rows]);
    std::fill_n(values.data(), values.num_elements(), 0); // fill list of agents with zeros
    std::fill_n(subtypes.data(), subtypes.num_elements(), ""); // fill list of subtypes with empty strings
    std::fill_n(components_attributes.data(), components_attributes.num_elements(), 0);


    int row = 0, col = 0;

    MPI_Barrier(MPI_COMM_WORLD);

    try {

        for ( auto & one_line : line_vector ) {

            col = 0;
            std::stringstream linesteam(one_line);
            std::string token;
            int attributes = 0;
            while(std::getline(linesteam, token, ',')){
                if (col == 0) {//first column contains ID (int)
                    boost::array<boost::multi_array<int, 2>::index, 2> index = {{col, row}};
                    values(index) = std::stoi(token);
                }
                else if (col == 1) {// second to left column contains component type string
                    boost::array<boost::multi_array<int, 2>::index, 2> index = {{col, row}};
                    values(index) = map_component_type(token);
                }
                else if (col == 2){ //third column contains subtype string
                    boost::array<boost::multi_array<std::string, 1>::index, 1> index = {{row}};
                    subtypes(index) = token;
                }
                else if (col == 3 || col == 4){//fourth and fifth columns contain profile information
                    boost::array<boost::multi_array<int, 2>::index, 2> index = {{col-1, row}};
                    values(index) = std::stoi(token);
                }
                else if(col<=COMPONENTS_MAX_COLUMNS){//other columns contain additional components information
                    boost::array<boost::multi_array<float, 2>::index,2> index = {{col-5,row}};
                    components_attributes(index) = std::stof(token);
                    attributes++;
                }
                else{
                    std::cerr << "ERROR in rank " << rank << ": Scenario file " << path << " contains too many columns" << std::endl;
                    return false;
                }
                col++;
            }

            boost::array<boost::multi_array<int, 2>::index, 2> index_type = {{1, row}};
            if(values(index_type) == TYPE_NODE_INT && attributes < 3 ){
                // For NODE components
                boost::array<boost::multi_array<float, 2>::index,2> index_ict_conn = {{2,row}};
                // if ICT connectivity of node is not specified: assume node is connected by default
                // this ensures backward compatibility
                components_attributes(index_ict_conn) = 1;

            }

            row++;

        }
        return true;
    }
    catch(std::invalid_argument &err){
        std::cerr << "Rank " << rank << ": Error (invalid argument) converting data from file " << path  << " to boost multi_array\nWhat:" << *(err.what()) << std::endl;
        return false;
    }
    catch(boost::exception & err){
        std::cerr << "Rank " << rank << ": Error (boost) converting data from file " << path << "to boost multi_array\nWhat:\n" <<  boost::diagnostic_information(err) << std::endl;
        return false;
    }
}

/*! \brief Read el_grid.csv file in parallel using MPI
 *  \param path [in] Path to csv file
 *  \param values [out] Array where the int content of the CSV scenario file shall be stored
 *  \param subtypes [out] Array where the subtypes are stored
 *  \param edge_attributes [out] Array where the float attributes of the cables are stored
 *  \return false on error, true on success
 * */
bool ModelIO::read_elgrid_file(char* path, boost::multi_array<int, 2> &values, boost::multi_array<std::string, 1> &subtypes, boost::multi_array<float, 2> &edge_attributes){

    std::vector<std::string> line_vector;
    int n_columns = 0;
    int n_rows = 0;

    this->read_file_mpi(path, line_vector, n_columns, n_rows, MPI_COMM_WORLD);

    //resize values
    boost::multi_array<int, 2>::extent_gen extents;
    boost::multi_array<float,2>::extent_gen extents_edges;
    boost::multi_array<std::string, 1>::extent_gen extents_subtypes;
    values.resize(extents[2][n_rows]);
    edge_attributes.resize(extents_edges[ELGRID_MAX_ATTRIBUTE_COLUMNS][n_rows]);
    subtypes.resize(extents_subtypes[n_rows]);
    std::fill_n(values.data(), values.num_elements(), 0); // fill list of agents with zeros
    std::fill_n(edge_attributes.data(), edge_attributes.num_elements(), 0); // fill list of subtypes with empty strings
    std::fill_n(subtypes.data(), subtypes.num_elements(), "");

    int row = 0, col = 0;

    MPI_Barrier(MPI_COMM_WORLD);

    try {


        for ( auto & one_line : line_vector ) {
            col = 0;
            std::stringstream linesteam(one_line);
            std::string token;

            while(std::getline(linesteam, token, ',')){
                boost::array<boost::multi_array<int, 2>::index, 2> index = {{col, row }};
                if(col<=1){ //first two columns contain node IDs
                    values(index) = std::stoi(token);
                } else if(col==2){ // third column contains subtype string
					 boost::array<boost::multi_array<std::string, 1>::index, 1> index = {{row}};
                     subtypes(index) = token;
				} else if(col<=ELGRID_MAX_COLUMNS){ //other columns contain line attributes
                    boost::array<boost::multi_array<float, 2>::index,2> index = {{col-3,row}};
                    edge_attributes(index) = std::stof(token);
                } else{
                    std::cerr << "ERROR in rank " << rank << ": Scenario file " << path << " contains too many columns" << std::endl;
                    return false;
                }
                col++;
            }
            row++;

        }
        return true;

    }
    catch(std::invalid_argument &err){
        std::cerr << "Rank " << rank << ": Error (invalid argument) converting data from file " << path  << " to boost multi_array\nWhat:" << *(err.what()) << std::endl;
        return false;
    }
    catch(boost::exception & err){
        std::cerr << "Rank " << rank << ": Error (boost) converting data from file " << path << "to boost multi_array\nWhat:\n" <<  boost::diagnostic_information(err) << std::endl;
        return false;
    }

}

/*! \brief Read a commdata.csv file in parallel using MPI
 *  \param path [in] Path to csv file
 *  \param values [out] Array where the content of the comm data file is stored
 *  \return false on error, true on success
 * */
bool ModelIO::read_comm_data_file(char* path, double * values){

    std::vector<std::string> line_vector;
    int n_columns = 0;
    int n_rows = 0;

    this->read_file_mpi(path, line_vector, n_columns, n_rows, MPI_COMM_WORLD);

    int row = 0, col = 0;

    MPI_Barrier(MPI_COMM_WORLD);

    try {

        for ( auto & one_line : line_vector ) {

            col = 0;
            std::stringstream linesteam(one_line);
            std::string token;

            while(std::getline(linesteam, token, ',')){
                values[row*n_columns + col] = std::stod(token);
                col++;
            }
            row++;
        }
        return true;
    }
    catch(std::invalid_argument &err){
        std::cerr << "Rank " << rank << ": Error (invalid argument) converting data from file " << path  << " to boost multi_array\nWhat:" << *(err.what()) << std::endl;
        return false;
    }
    catch(boost::exception & err){
        std::cerr << "Rank " << rank << ": Error (boost) converting data from file " << path << "to boost multi_array\nWhat:\n" <<  boost::diagnostic_information(err) << std::endl;
        return false;
    }
}


/*! \brief Map a component type string to its integer representation
 *  \param [in] type Agent type to be mapped
 *  \return type of agent in integer representation
 * */
int ModelIO::map_component_type(std::string &type) {
    if(type == TYPE_LOAD_STRING){
        return TYPE_LOAD_INT;
    }
    else if (type == TYPE_PV_STRING){
        return TYPE_PV_INT;
    }
    else if (type == TYPE_BIOFUEL_STRING){
        return TYPE_BIOFUEL_INT;
    }
    else if(type == TYPE_BATTERY_STRING){
        return TYPE_BATTERY_INT;
    }
    else if(type == TYPE_TRANSFORMER_STRING){
        return TYPE_TRANSFORMER_INT;
    }
    else if(type == TYPE_SLACK_STRING){
        return TYPE_SLACK_INT;
    }
    else if(type == TYPE_NODE_STRING){
        return TYPE_NODE_INT;
    }
    else if(type == TYPE_CHP_STRING){
        return TYPE_CHP_INT;
    }
    else if(type == TYPE_HP_STRING){
        return TYPE_HP_INT;
    }
    else if(type == TYPE_GASBOILER_STRING){
        return TYPE_GASBOILER_INT;
    }
    else if(type == TYPE_BIOMASS_STRING){
        return TYPE_BIOMASS_INT;
    }
    else if(type == TYPE_HEATINGROD_STRING){
        return TYPE_HEATINGROD_INT;
    }
    else if(type == TYPE_ELECTRICKILN_STRING){
        return TYPE_ELECTRICKILN_INT;
    }
    else if(type == TYPE_GASKILN_STRING){
        return TYPE_GASICKILN_INT;
    }
    else if(type == TYPE_EV_STRING){
        return TYPE_EV_INT;
    }
    else if(type == TYPE_COMPENSATOR_STRING){
        return TYPE_COMPENSATOR_INT;
    }
    else if(type == TYPE_WEC_STRING){
        return TYPE_WEC_INT;
    }
    else{
        // unknown type
        std::cerr << "Rank " << rank << ": detected an unknown component type in map_component_type(): " << type << " This may cause trouble!" << std::endl;
        return TYPE_UNKNOWN_INT;
    }
}

/*! \brief Start a time measurement
 *  \param tm [in, out] time measurement to be started
 * */
void ModelIO::start_time_measurement(Time_measurement* tm){
    tm->start();
}

/*! \brief Stop a time measurement
*   \param tm [in, out] time measurement to be stopped
* */
void ModelIO::stop_time_measurement(Time_measurement* tm){
    tm->stop();
}

/*! \brief Stop a time measurement
*   \param tm [in, out] time measurement to be stopped
* */
long long int ModelIO::get_time_measurement(Time_measurement* tm){
    return tm->get_duration();
}

void ModelIO::reset_number_of_fbs_sweeps() {
    number_of_fbs_sweeps = 0;
}

void ModelIO::reset_loop_counters() {
    backward_loop_counter = 0;
    forward_loop_counter = 0;
}

void ModelIO::increment_number_of_fbs_sweeps() {
    number_of_fbs_sweeps++;
}

void ModelIO::add_to_forward_loop_counter(unsigned int n) {
    forward_loop_counter += n;
}

void ModelIO::add_to_backward_loop_counter(unsigned int n) {
    backward_loop_counter += n;
}

unsigned int ModelIO::get_number_of_fbs_sweeps() {
    return number_of_fbs_sweeps;
}

/*! \brief Read all input profiles and save them
 * */
void ModelIO::read_profiles(Profile *comp_profiles)
{
    double *t;
    std::vector<double*> values;
    std::vector<std::string> headers;
    unsigned int num_values;

    /* loads */
    std::string path = profile_dir + FILE_PROFILE_LOAD;
    read_profile_file((char*) path.c_str(), t, values, headers, num_values);
    int P_counter = 0;
    int Q_counter = 0;
    for (unsigned int i = 0; i < values.size(); i++) {

        if(headers[i].find('Q') != string::npos) {
            // header of column must contain "Q" to be treated as Q profile
            comp_profiles->add_profile(PROFILE_LOAD_Q, t, values[i], num_values, headers[i]);
            Q_counter++;
        }
        else{
            // in case header of the column does not contain "Q", profile is treated as P profile
            comp_profiles->add_profile(PROFILE_LOAD_P, t, values[i], num_values, headers[i]);
            P_counter++;
        }

    }
    log_info("read " + std::to_string(P_counter) + " P load profiles and " + std::to_string(Q_counter) + " Q load profiles");
    values.clear();
    headers.clear();

    /* pv */
    path = profile_dir + FILE_PROFILE_PV;
    read_profile_file((char*) path.c_str(), t, values, headers, num_values);
    for (unsigned int i = 0; i < values.size(); i++) {
        comp_profiles->add_profile(PROFILE_PV_P, t, values[i], num_values, headers[i]);
    }
    log_info("read " + std::to_string(values.size()) + " pv profiles");
    values.clear();
    headers.clear();

    /* wec */
    path = profile_dir + FILE_PROFILE_WEC;
    read_profile_file((char*) path.c_str(), t, values, headers, num_values);
    for (unsigned int i = 0; i < values.size(); i++) {
        comp_profiles->add_profile(PROFILE_WEC_P, t, values[i], num_values, headers[i]);
    }
    log_info("read " + std::to_string(values.size()) + " wec profiles");
    values.clear();
    headers.clear();

    /* ev */
    path = profile_dir + FILE_PROFILE_EV;
    read_profile_file((char*) path.c_str(), t, values, headers, num_values);
    int conn_counter = 0;
    P_counter = 0;
    for (unsigned int i = 0; i < values.size(); i++) {
        if(headers[i].find("conn") != string::npos){ // only column headers containing "conn" are treated as connection profiles
            comp_profiles->add_profile(PROFILE_EV_CONN, t, values[i], num_values, headers[i]);
            conn_counter++;
        }
        else{
            //if 'conn' is not contained in header, profile is treated as P profile
            comp_profiles->add_profile(PROFILE_EV_P, t, values[i], num_values, headers[i]);
            P_counter++;
        }
    }
    log_info("read " + std::to_string(conn_counter) + " connection ev profiles and " + std::to_string(P_counter) + " P ev profiles");
    values.clear();
    headers.clear();

    /* thermal */
    path = profile_dir + FILE_PROFILE_TH;
    read_profile_file((char*) path.c_str(), t, values, headers, num_values);
    for (unsigned int i = 0; i < values.size(); i++) {
        comp_profiles->add_profile(PROFILE_TH, t, values[i], num_values, headers[i]);
    }
    log_info("read " + std::to_string(values.size()) + " thermal profiles");
    values.clear();
    headers.clear();

    /*biofuel*/
    path = profile_dir + FILE_PROFILE_BIO;
    read_profile_file((char *) path.c_str(), t, values, headers, num_values);
    for (unsigned int i = 0; i < values.size(); i++) {
        comp_profiles->add_profile(PROFILE_BIO_P, t, values[i], num_values, headers[i]);
    }
    log_info("read " + std::to_string(values.size()) + " biofuel profiles");
    values.clear();
    headers.clear();


}


/*! \brief Read the content of a csv file using MPI functions
 *  \param path [in] path to csv file
 *  \param line_vector [out] vector storing the lines of the file
 *  \param n_columns [out] number of columns of the file
 *  \param n_rows [out] number of rows of the file
 *  \param communicator [in] MPI communicator for file reading
 * */
void ModelIO::read_file_mpi(char* path, std::vector<std::string> &line_vector, int &n_columns, int &n_rows, const MPI_Comm &communicator) {

    char* buffer;
    std::string file_content;
    MPI_Offset size = 0;
    MPI_Status status;
    MPI_File file;

    int ret = 0;
    char retstring[500];
    int retstringlength = 0;

    ret = MPI_File_open(MPI_COMM_SELF, path, MPI_MODE_RDONLY, MPI_INFO_NULL, &file);
    if (ret != MPI_SUCCESS) {
        MPI_Error_string(ret, retstring, &retstringlength);
        std::cerr << "ERROR during MPI_FILE_OPEN for file " << path << " on rank " << rank << ": " << retstring << std::endl;
        MPI_Abort(MPI_COMM_WORLD, -22);
    }

    ret = MPI_File_get_size(file, &size); //file.Get_size();
    if (ret != MPI_SUCCESS){
        MPI_Error_string(ret, retstring, &retstringlength);
        std::cerr << "ERROR during MPI_FILE_GET_SIZE for file " << path << " on rank " << rank << ": " << retstring << std::endl;
        MPI_Abort(MPI_COMM_WORLD, -22);
    }

    log_info("opened file " + std::string(path) + " of size " + std::to_string(size) + " Bytes");

    buffer = new char[size];
    ret = MPI_File_read(file, buffer, size, MPI_CHAR, &status);
    if (ret != MPI_SUCCESS){
        MPI_Error_string(ret, retstring, &retstringlength);
        std::cerr << "ERROR during MPI_FILE_READ for file " << path << " on rank " << rank << ": " << retstring << std::endl;
        MPI_Abort(MPI_COMM_WORLD, -22);
    }

    MPI_File_close(&file);
    if (ret != MPI_SUCCESS){
        MPI_Error_string(ret, retstring, &retstringlength);
        std::cerr << "ERROR during MPI_FILE_CLOSE for file " << path << " on rank " << rank << ": " << retstring << std::endl;
        MPI_Abort(MPI_COMM_WORLD, -22);
    }

    file_content = "";
    for(int char_element=0; char_element<size; char_element++){
        file_content += buffer[char_element];
    }

    std::stringstream file_stream(file_content);
    std::string line;
    file_stream.clear();

    /* read file line by line */
    n_columns = (unsigned int) 0;
    while ( !file_stream.eof() ) {
        std::getline( file_stream, line, '\n' );
        if(!line.empty() && line.rfind('#',0) != 0) { //ignore empty lines and comments
            line_vector.push_back(line);
            n_columns = (int) std::max((unsigned int) std::count( line.begin(), line.end(), ',') + 1, (unsigned int) n_columns);
            n_rows++;
        }
    }

    log_info("rows:" + std::to_string(n_rows) + " max columns: " + std::to_string(n_columns));
}

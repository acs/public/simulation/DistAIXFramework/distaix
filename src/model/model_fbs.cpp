/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/model.h"

/*! \brief Perform the forward-backward-sweep algorithm for the current tick
 * */
void Model::forward_backward_sweep() {
    IO->log_info("####### now calculating electrical interactions #######");

    if(agents_scheduling.empty()){
        return;
    }

    IO->start_time_measurement(IO->tm_fb_sweep);

    // synchronize electrical connections
    // required to sync (=reset) next_action_expected and convergence flags of non-local agents
    synchronize_el_connections();

    repast::ScheduleRunner &runner = repast::RepastProcess::instance()->getScheduleRunner();
    IO->reset_number_of_fbs_sweeps();
    IO->reset_loop_counters();

    bool forward_backward_sweep_finished = false;     // set to true if process has finished FBS algorithm,
    bool contains_slack = context.contains(repast::AgentId(SLACK_ID,rank,TYPE_SLACK_INT, rank));

    while (!forward_backward_sweep_finished) {
        IO->log_info("####### number of loops: " + std::to_string(IO->get_number_of_fbs_sweeps()+1) + "#######");
        if(!agents_scheduling.empty()) {
            IO->log_info("################ calculate components");
            // calculation of all components
            this->calculate_components();

            IO->log_info("################ backward sweep");
            // backward sweep: calculation of currents
            this->do_backward_sweep();

            IO->log_info("################ forward sweep");
            // forward sweep: calculation of voltages
            this->do_forward_sweep();

            //check if finished
            if (contains_slack) {
                Agent *slack = context.getAgent(repast::AgentId(SLACK_ID, rank, TYPE_SLACK_INT, rank));

                if (slack->get_convergence()) {
                    IO->log_info("\tat tick " + std::to_string(runner.currentTick()) + " in loop: " +
                                 std::to_string(IO->get_number_of_fbs_sweeps() + 1) +
                                 ": FBS convergence");
                    forward_backward_sweep_finished = true;
                }
            } // check finished

            if(world_size > 1) {
                // Broadcast status
                MPI_Bcast(&forward_backward_sweep_finished, 1, MPI_C_BOOL, agent_rank_relation[SLACK_ID-1], filled_ranks);
            }
        }

        IO->increment_number_of_fbs_sweeps();
        IO->flush_log();
    } //while

    IO->stop_time_measurement(IO->tm_fb_sweep);
}

/*! \brief Calculate the electrical model of all component agents of this process
 * */
void Model::calculate_components() {
    IO->start_time_measurement(IO->tm_calculate_components);
    for(auto i : component_agents){
        i->calculate();
    }
    IO->stop_time_measurement(IO->tm_calculate_components);
}

/*! \brief Perform a backward sweep
 * */
void Model::do_backward_sweep() {

    IO->start_time_measurement(IO->tm_backward_sweep);

    IO->log_info("now in backward sweep");
    int counter= 0;
    uint have_backward_sweeped = 0;
    std::vector<bool> state(agents_scheduling.size(), false);

    bool backward_sweep_finished = false;             //set to true if process has finished a backward sweep
    bool contains_slack = context.contains(repast::AgentId(SLACK_ID, rank, TYPE_SLACK_INT, rank));

    while(!backward_sweep_finished) {

        if(have_backward_sweeped <  agents_scheduling.size()){
            //perform backward sweep for all node agents if not all node agents have finished backward sweeping
            int index = 0;
            for(auto i : agents_scheduling){ // iterate forward
                if(state[index] == false){
                    state[index] = i->do_backward_sweep(agent_network_elec);

                    if(state[index]){
                        have_backward_sweeped++;
                    }
                    IO->log_info("ID " + std::to_string(i->getId().id()) + " state=" + std::to_string(state[index]));
                }
                index++;
            }
        }
        counter++;

        //check if finished
        if (contains_slack) {
            Agent *slack = context.getAgent(repast::AgentId(SLACK_ID, rank, TYPE_SLACK_INT, rank));

            if ((slack->get_next_action_expected() == FORWARD_SWEEP)) {
                backward_sweep_finished = true;
            }
        } // check finished

        if(world_size > 1) {
            // Broadcast status
            MPI_Bcast(&backward_sweep_finished, 1, MPI_C_BOOL, agent_rank_relation[SLACK_ID-1], filled_ranks);
        }

        IO->stop_time_measurement(IO->tm_backward_sweep);
        synchronize_el_connections();
        IO->start_time_measurement(IO->tm_backward_sweep);

        IO->flush_log();

    } // while
    IO->add_to_backward_loop_counter(counter);
    IO->log_info("backward sweep took " + std::to_string(counter) + " iterations.");
    IO->stop_time_measurement(IO->tm_backward_sweep);
}

/*! \brief Perform a forward sweep
 * */
void Model::do_forward_sweep() {

    IO->start_time_measurement(IO->tm_forward_sweep);

    IO->log_info("now in forward sweep");
    int counter = 0;
    bool started_convergence_check = false;
    uint checked_convergence = 0;
    std::vector<uint> state(agents_scheduling.size(), DONE_NOTHING);

    bool forward_sweep_finished = false; // set to true if process has finished a forward sweep
    bool contains_slack = context.contains(repast::AgentId(SLACK_ID, rank, TYPE_SLACK_INT, rank));

    while(!forward_sweep_finished){
        if(checked_convergence < agents_scheduling.size()){
            if(!started_convergence_check) {
                //perform forward sweep for all node agents
                for (unsigned long i = agents_scheduling.size(); i > 0; i--) { // iterate backwards...
                    if(state[i-1] == DONE_NOTHING || distribution_method == "evenly" ){
                        state[i-1] = agents_scheduling[i-1]->do_forward_sweep(agent_network_elec);
                        IO->log_info("FW ID " + std::to_string(agents_scheduling[i-1]->getId().id()) + " state=" + std::to_string(state[i-1]));
                        if (state[i-1] == DONE_CONVERGENCE_CHECK){
                            started_convergence_check = true;
                            checked_convergence++;
                        } else{
                            started_convergence_check = false;
                        }
                    }
                }
            } else{
                //perform convergence check for all node agents
                int index = 0;
                for (auto i : agents_scheduling) { // iterate forward...
                    if(state[index] <= DONE_FORWARD_SWEEP || distribution_method == "evenly" ){
                        state[index] = i->do_forward_sweep(agent_network_elec);
                        IO->log_info("Conv. ID " + std::to_string(i->getId().id()) + " state="+ std::to_string(state[index]));
                        if(state[index] == DONE_CONVERGENCE_CHECK){
                            checked_convergence++;
                        }
                    }
                    index++;
                }
            }
        }

        counter++;

        //check if finished
        if(contains_slack){
            Agent * slack = context.getAgent(repast::AgentId(SLACK_ID,rank,TYPE_SLACK_INT, rank));
            if((slack->get_next_action_expected() == BACKWARD_SWEEP)){
                forward_sweep_finished = true;
            }
        } // finished check

        if(world_size > 1) {
            // Broadcast status
            MPI_Bcast(&forward_sweep_finished, 1, MPI_C_BOOL, agent_rank_relation[SLACK_ID-1], filled_ranks);
        }

        IO->stop_time_measurement(IO->tm_forward_sweep);
        synchronize_el_connections();
        IO->start_time_measurement(IO->tm_forward_sweep);

        IO->flush_log();
    } // while
    IO->add_to_forward_loop_counter(counter);
    IO->log_info("forward sweep took " + std::to_string(counter) + " iterations.");
    IO->stop_time_measurement(IO->tm_forward_sweep);
}

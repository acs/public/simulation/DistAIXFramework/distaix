/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/model.h"
#include "agents/prosumer_agent.h"
#include "agents/node_agent.h"
#include "agents/transformer_agent.h"
#include "agents/slack_agent.h"

void Model::create_MR_DF_agents(){
    IO->log_info("Creating DF agent");
    /*create one directory facilitator agent per rank*/
    repast::AgentId df_id(TYPE_DF_INT,rank, TYPE_DF_INT, rank);
    std::string st="";
    struct data_props d_props_DF;
    d_props_DF.log.logging = df_logging && log_agents;
    d_props_DF.log.path_log_file = log_folder + "/agents/agent_DF" + std::to_string(rank) + ".log";
    d_props_DF.result.csv_results = df_results && results_csv_agents;
    d_props_DF.result.db_results = df_results && results_db_agents;
    d_props_DF.result.path_result_file = results_folder + "/agents/agent_DF" + std::to_string(rank) + ".csv";
    d_props_DF.result.loglevel = loglevel;
    d_props_DF.result.db_enabled = results_db_agents || results_db_ranks || results_db_cables;
    d_props_DF.result.dbconn = d_props.result.dbconn;
    DF_agent = new Directoryfacilitator_agent(df_id, true, step_size, behavior_type, model_type,st, d_props_DF, villas_config_node_disabled);
    context.addAgent(DF_agent);

    IO->log_info("Creating MR agent");
    /*create one message router agent per rank*/
    repast::AgentId mr_id(TYPE_MR_INT,rank, TYPE_MR_INT, rank);
    struct data_props d_props_MR;
    d_props_MR.log.logging = mr_logging && log_agents;
    d_props_MR.log.path_log_file = log_folder + "/agents/agent_MR" + std::to_string(rank) + ".log";
    d_props_MR.result.csv_results = mr_results && results_csv_agents;
    d_props_MR.result.db_results = mr_results && results_db_agents;
    d_props_MR.result.path_result_file = results_folder + "/agents/agent_MR" + std::to_string(rank) + ".csv";
    d_props_MR.result.loglevel = loglevel;
    d_props_MR.result.db_enabled = results_db_agents || results_db_ranks || results_db_cables;
    d_props_MR.result.dbconn = d_props.result.dbconn;
    MR_agent = new Messagerouter_agent(mr_id, true, step_size, behavior_type, model_type, st, d_props_MR, villas_config_node_disabled, use_commdata);
    context.addAgent(MR_agent);
}

/*! \brief Create agents based on scenario file(s)
 *
 * This function creates agents according to a scenario file (csv format).
 * One agent is created for every slack, node, transformer and every component in the grid.
 * Slack, node and transformer agents are distributed to processes according to the distribution method
 * chosen by the user. They are created first. Component agents are always created in the process
 * where their corresponding node agent is located.
 * */
void Model::create_agents() {
    if(rank == 0){
        std::cout << " * Creating agents in MPI processes..." << std::endl;
    }

    //if(rank==0){
    //    std::cout << "---> Allocate shared memory for agent_rank_relation";
    //}

    IO->start_time_measurement(IO->tm_distribute_agents);

    // Create object of Model_creator class
    creator = new Model_creator(&scenario_file_components_content,
                                &scenario_file_components_subtypes,
                                &scenario_file_components_attributes,
                                &scenario_file_el_grid_content,
                                world_size, transformers,
                                distribution_method, &agents_scheduling, rank, &context, IO, this);

    /*determine number of nodes etc.*/
    creator->preprocessing();
    number_of_nodes = creator->get_number_of_nodes();

    //if(rank==0){
    //    std::cout << "---> Allocate shared memory for agent_rank_relation";
    //}

    //split the MPI communicator into groups that can each allocate a shared memory region:
    MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, rank, MPI_INFO_NULL, &agent_rank_relation_comm);
    //determine new worldsize and rank of this process for the new communicator
    MPI_Comm_rank(agent_rank_relation_comm, &agent_rank_relation_rank);
    MPI_Comm_size(agent_rank_relation_comm, &agent_rank_relation_worldsize);

    MPI_Aint size_agent_rank_relation;

    if(agent_rank_relation_rank == 0) {
        size_agent_rank_relation = (long long int) number_of_nodes * (long long int) sizeof(int);

        int ret = 0;
        char retstring[200];
        int retstringlength;
        ret = MPI_Win_allocate_shared(size_agent_rank_relation, sizeof(int), MPI_INFO_NULL,
                                      agent_rank_relation_comm, &agent_rank_relation_base_pointer, &shared_mem_window_agent_rank_relation);
        MPI_Error_string(ret,retstring,&retstringlength);
        //IO->log_info("agent_rank_relation_rank " + std::to_string(agent_rank_relation_rank) + ":  allocate_shared returned: " + retstring);

    }
    else {
        int disp_unit_agent_rank_relation;
        int ret = 0;
        char retstring[200];
        int retstringlength;

        ret = MPI_Win_allocate_shared(0, sizeof(int), MPI_INFO_NULL,
                                      agent_rank_relation_comm, &agent_rank_relation_base_pointer, &shared_mem_window_agent_rank_relation);
        MPI_Error_string(ret,retstring,&retstringlength);
        IO->log_info("agent_rank_relation_rank " + std::to_string(agent_rank_relation_rank) + ":  allocate_shared returned: " +retstring);

        ret = MPI_Win_shared_query(shared_mem_window_agent_rank_relation, 0, &size_agent_rank_relation, &disp_unit_agent_rank_relation,
                                   &agent_rank_relation_base_pointer);
        MPI_Error_string(ret,retstring,&retstringlength);
        //IO->log_info("agent_rank_relation_rank " +  std::to_string(agent_rank_relation_rank) + ":  shared_query returned: " + retstring);


    }
    agent_rank_relation = (int*) agent_rank_relation_base_pointer;
    MPI_Barrier(agent_rank_relation_comm);

    /*create scenario*/
    creator->create_model(agent_rank_relation, components_at_nodes, ict_connectivity_of_nodes);

    // Receive the number of agents as well as the highest filled rank
    unsigned int agent_count = creator->get_agent_count();
    highest_filled_rank = creator->get_highest_filled_rank();

    IO->log_info("Highest filled rank: " + std::to_string(highest_filled_rank));

    // create MPI communicator for FBS method:
    int filled = 0;
    if (rank <= highest_filled_rank){
        filled = 1;
    }
    MPI_Comm_split(MPI_COMM_WORLD, filled, rank, &filled_ranks);


    MPI_Barrier(MPI_COMM_WORLD);

    //Set hop matrix in DF agent if required
    if(behavior_type != CTRL_NON_INTELLIGENT) {
        //if(rank == 0){
        //    std::cout << "---> Set creator in DF-Agent";
        //}
        //Set pointer to creator in DF Agent, it is used to determine hop distances on demand
        DF_agent->set_model_creator(creator);

    } else{
        //if(rank == 0){
        //    std::cout << "---> Creator in DF not needed because non-intelligent case is simulated." << std::endl;
        //}
    }

    // [DB] Add agentcount
    IO->addMetaEntry(rank, "agents_on_rank", "agents", std::to_string(agent_count));

    IO->log_info("I have created " + std::to_string(agent_count) + " agents.");

    /*generate vectors for local_agents, components, and node_agents*/
    this->agent_selections();

    if(rank == 0){
        std::cout << "Done." << std::endl;
    }

    IO->stop_time_measurement(IO->tm_distribute_agents);
}

/*! \brief initialize the agent behaviors
 * */
void Model::init_agent_behaviors() {

    if(rank == 0) {
        std::cout << " * Initializing agent behaviors... " << std::endl;
    }
    IO->start_time_measurement(IO->tm_comm_partners_init);

    /*set internal agents and neighborhood swarm of each component agent*/
    for(auto i : local_agents){
        if(i->getId().agentType() != TYPE_NODE_INT && i->getId().agentType() != TYPE_MR_INT) {
            //for all agents who have a behavior, initialize the agent behavior

            IO->log_info("Init behavior of agent: " + IO->id2str(i->getId()));
            IO->log_info("  Agent Type: " + std::to_string(i->getId().agentType()));
            IO->log_info("  Behavior Type: " + std::to_string(i->behavior_type));
            int ret = i->init_behavior(&components_at_nodes, &connected_nodes,
                             &scenario_file_components_content,
                             &scenario_file_components_subtypes, &scenario_file_el_grid_content);

            if (ret){
                // something went wrong during behavior init
                do_exit(55);
            }
        }
        else if(i->getId().agentType() == TYPE_MR_INT){
            //initialize count matrix in MR Agent (memory allocated only if use_commdata = true)
            MR_agent->init_count_matrix(number_of_nodes);
            //set components_at_nodes vector in message router agent
            MR_agent->set_components_at_nodes(components_at_nodes);
            //set input matrices (latency)
            this->set_MR_input();
        }

        /* each agent determines electrically adjacent agents and saves them locally*/
        i->save_adjacent_agents(agent_network_elec);

        /* set pointers to time measurements*/
        i->set_tm(IO->tm_csv, IO->tm_db, IO->tm_db_serialize, IO->tm_db_add);

    }

    //wait here for fixed amount of time to be sure that all VILLAS connections are ready to be used
    if (behavior_type == CTRL_MQTT_TEST ||
        behavior_type == CTRL_ENSURE ||
        behavior_type == CTRL_MQTT_PINGPONG ||
        behavior_type == CTRL_MQTT_HIGHLOAD ||
        behavior_type == CTRL_DPSIM_COSIM_REF ||
        behavior_type == CTRL_DPSIM_COSIM_SWARMGRIDX){

        struct timespec t;
        t.tv_sec=1;
        t.tv_nsec=0;
        if(rank == 0){
            std::cout << " +++++++ Waiting for " << t.tv_sec << " sec and " << t.tv_nsec <<
                      " ns to make sure all VILLASnode connections are ready +++++++" << std::endl;
        }
        nanosleep(&t, nullptr);
    }


    IO->stop_time_measurement(IO->tm_comm_partners_init);

    if(rank == 0) {
        std::cout << "Done." << std::endl;
    }

    //set components at nodes in DF agent
    DF_agent->set_components_at_nodes(components_at_nodes);
    // set ICT connectivity of nodes in DF agent
    DF_agent->set_ict_connectivity_of_nodes(ict_connectivity_of_nodes);

}

/*! \brief Create agents of correct class based on their type
 *  \param id RepastHPC Agent ID of the agent to be created
 *  \param _subtype Subtype of the agent to be created
 *  \param node_id ID of the node to which the agent is connected
 *  \param param Pointer to array that contains parameters of the agent
 *  \param profile_id1 ID used to identify profile for the agent
 *  \param profile_id2 ID used to identify profile for the agent
 *  \param Vnom nominal voltage of the agent in Volt
 *
 * This function creates agents of a specific class based on the agent type.
 * Agent types are defined in the config.h file.
 * */
void Model::agent_creator(repast::AgentId &id, std::string &_subtype, int &node_id,
                          double * param, int profile_id1, int profile_id2, double Vnom, bool _ict_connectivity) {

    struct data_props d_props_agent;
    d_props_agent.log.path_log_file = log_folder + "/agents/agent_" + std::to_string(id.id()) + ".log";
    d_props_agent.log.logging = log_agents;
    d_props_agent.result.path_result_file = results_folder + "/agents/agent_" + std::to_string(id.id()) + ".csv";
    d_props_agent.result.dbconn = d_props.result.dbconn;
    d_props_agent.result.loglevel = loglevel;
    d_props_agent.result.db_enabled = results_db_ranks || results_db_agents || results_db_cables;
    d_props_agent.result.csv_results = results_csv_agents;
    d_props_agent.result.db_results = results_db_agents;
    if((results_csv_agents || results_db_agents) && agent_results[0] != 0) {
        //if not all agent results shall be saved
        if (std::find(agent_results.begin(), agent_results.end(), id.id()) != agent_results.end()) {
            //if agent id is in the list, save results for this agent
            d_props_agent.result.csv_results = results_csv_agents;
            d_props_agent.result.db_results = results_db_agents;
        }
        else{
            //if agent id is not in the list, do not save results for this agent
            d_props_agent.result.csv_results = false;
            d_props_agent.result.db_results = false;
        }
    }

    if(log_agents && agent_logging[0] != 0){
        //if not all agent logfiles shall be created
        if (std::find(agent_logging.begin(), agent_logging.end(), id.id()) != agent_logging.end()) {
            //if agent id is in the list, save log file for this agent
            d_props_agent.log.logging = true;
        }
        else{
            //if agent id is not in the list, do not save log file for this agent
            d_props_agent.log.logging = false;
        }

    }

    IO->log_info("Creating Agent (id, rank, type) (" + std::to_string(id.id()) +
                 ", " + std::to_string(rank) + ", " +
                 std::to_string(id.agentType()) + ") of subtype " + _subtype +
                 " at node " + std::to_string(node_id));

    if (id.agentType() == TYPE_SLACK_INT) {
        if(behavior_type == CTRL_DPSIM_COSIM_REF ||behavior_type == CTRL_DPSIM_COSIM_SWARMGRIDX){
            if(param[0] == 0.0 && param[1] == 0.0){
                std::cout << "#########################################################################################" << std::endl;
                std::cout << "WARNING: Co-simulation behavior was chosen, but Slack was initialized with V = Complex(0,0)" << std::endl;
                std::cout << "#########################################################################################" << std::endl;
            }
            auto * agent = new Slack_agent(id, true, step_size, std::stod(_subtype), param[0], param[1],
                                           behavior_type, model_type, _subtype, d_props_agent,
                                           villas_config_agents, _ict_connectivity, realtime);
            context.addAgent(agent);
        }
        else {
            auto * agent = new Slack_agent(id, true, step_size, std::stod(_subtype),
                                           behavior_type, model_type, _subtype, d_props_agent,
                                           villas_config_agents, _ict_connectivity);
            context.addAgent(agent);
        }
    } else if (id.agentType() == TYPE_TRANSFORMER_INT) {
        auto *agent = new Transformer_agent(id, true, step_size, node_id,
                                            behavior_type, model_type, _subtype, d_props_agent,
                                            villas_config_agents,
                                            param[0], param[1],
                                            param[2], param[3], param[4],
                                            (int) param[5], (int) param[6], _ict_connectivity);
        context.addAgent(agent);
    } else if (id.agentType() == TYPE_NODE_INT) {
        auto *agent = new Node_agent(id, true, step_size,  param[0],
                                     behavior_type, model_type, _subtype, d_props_agent,
                                     villas_config_agents,
                                     (int) param[1], _ict_connectivity);
        context.addAgent(agent);
    } else {
        Prosumer_data *prosumer_data = new Prosumer_data;
        prosumer_data->type = id.agentType();
        prosumer_data->subtype = _subtype;
        prosumer_data->node_id = node_id;
        prosumer_data->Vnom = Vnom;
        prosumer_data->profile_scale = param[0];
        prosumer_data->S_r = param[1];
        prosumer_data->profile1_id = profile_id1;
        prosumer_data->profile2_id = profile_id2;
        prosumer_data->ict_connected = _ict_connectivity;


        if (id.agentType() == TYPE_LOAD_INT) {
            prosumer_data->pf = param[2];
        } else if (id.agentType() == TYPE_EV_INT) {
            prosumer_data->P_nom = param[2]; //P_nom is used as P_max_in
            prosumer_data->pf_min = param[3];
            prosumer_data->C_el = param[4];
            prosumer_data->SOC_el = param[5]; //]((id.id()%12)*0.05 + 0.2);
        } else if (id.agentType() == TYPE_COMPENSATOR_INT) {
            prosumer_data->N = param[2];
        } else if (id.agentType() == TYPE_PV_INT || id.agentType() == TYPE_WEC_INT || id.agentType() == TYPE_BIOFUEL_INT) {
            prosumer_data->pf_min = param[2];
            prosumer_data->P_nom = prosumer_data->S_r*prosumer_data->pf_min;
        } else if (id.agentType() == TYPE_CHP_INT || id.agentType() == TYPE_HP_INT) {
            prosumer_data->pf_min = param[2];
            prosumer_data->P_nom = param[3];
            prosumer_data->f_el = param[4];
            prosumer_data->C_th = param[5];
            prosumer_data->SOC_th = param[6];
            prosumer_data->E_th = prosumer_data->SOC_th * prosumer_data->C_th;
            prosumer_data->f_el_sec = param[7];
            prosumer_data->P_sec = param[8];
            prosumer_data->profile_scale_ww = prosumer_data->profile_scale;
        } else if (id.agentType() == TYPE_BATTERY_INT) {
            prosumer_data->P_nom = param[2];
            prosumer_data->pf_min = param[3];
            prosumer_data->C_el = param[4];
            prosumer_data->SOC_el = param[5]; //((id.id()%12)*0.05 + 0.2);
            prosumer_data->E_el = prosumer_data->SOC_el * prosumer_data->C_el;
        } else {
            std::cerr << "Rank " << rank << ": unknown agent type : "  << id.agentType()
                      << "  Cannot create this type of agent! Abort! " << std::endl;
            do_exit(666);
        }

        auto *agent = new Prosumer_agent(id, true, step_size, behavior_type, model_type,
                                         &component_profiles, interpolation_type, d_props_agent, villas_config_agents, prosumer_data);
        context.addAgent(agent);
    }
}

/*! \brief initialize the member variables (vectors) local_agents, components, and node_agents
 * */
void Model::agent_selections() {
    local_agents.clear();
    node_agents.clear();
    component_agents.clear();
    context.selectAgents(repast::SharedContext<Agent>::filterLocalFlag::LOCAL, local_agents, false);

    for(auto i : local_agents){
        Agent * a = i;
        int type = a->getId().agentType();
        switch(type){
            case TYPE_NODE_INT:
                node_agents.push_back(a);
                break;
            case TYPE_TRANSFORMER_INT:
                node_agents.push_back(a);
                break;
            case TYPE_SLACK_INT:
                node_agents.push_back(a);
                break;
            case TYPE_PV_INT:
                component_agents.push_back(a);
                break;
            case TYPE_BIOFUEL_INT:
                component_agents.push_back(a);
                break;
            case TYPE_LOAD_INT:
                component_agents.push_back(a);
                break;
            case TYPE_EV_INT:
                component_agents.push_back(a);
                break;
            case TYPE_BATTERY_INT:
                component_agents.push_back(a);
                break;
            case TYPE_CHP_INT:
                component_agents.push_back(a);
                break;
            case TYPE_HP_INT:
                component_agents.push_back(a);
                break;
            case TYPE_COMPENSATOR_INT:
                component_agents.push_back(a);
                break;
            case TYPE_WEC_INT:
                component_agents.push_back(a);
                break;
            case TYPE_DF_INT:
                break;
            case TYPE_MR_INT:
                break;
            default:
                std::cerr << "Unknown agent type " << type << " in agent selection function detected. Abort." << std::endl;
                do_exit(-3000);
                break;
        }
        IO->log_info(IO->id2str(a->getId()) + "added to local_agents");

    }
}

/*! \brief Set the input of the Message Router (MR) agent of this process if latency matrix
 * shall be used instead of default communication delay
 * */
void Model::set_MR_input(){


    /*Set the input comm data for MR agent in case use_commdata is true*/
    if(use_commdata) {

        //split the MPI communicator into groups that can each allocate a shared memory region:
        MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, rank, MPI_INFO_NULL, &MR_comm);
        //determine new worldsize and rank of this process for the new communicator
        MPI_Comm_rank(MR_comm, &MR_rank);
        MPI_Comm_size(MR_comm, &MR_worldsize);
        //print debug info
        IO->log_info("after MPI_Comm_split: MR_rank = " + std::to_string(MR_rank) + " MR_worldsize = " +
                     std::to_string(MR_worldsize));

        MPI_Aint size_latency;

        if (MR_rank == 0) {
            size_latency =
                    (long long int) number_of_nodes * (long long int) number_of_nodes * (long long int) sizeof(double);
            int ret = 0;
            char retstring[200];
            int retstringlength;
            ret = MPI_Win_allocate_shared(size_latency, sizeof(double), MPI_INFO_NULL,
                                          MR_comm, &latency_base_pointer, &shared_mem_window_latency);
            MPI_Error_string(ret, retstring, &retstringlength);
            IO->log_info("MR_rank " + std::to_string(MR_rank) + ":  allocate_shared returned: " + retstring);

        } else {
            int disp_unit_latency;
            //int disp_unit_per;

            int ret;
            char retstring[200];
            int retstringlength;

            IO->log_info("MR_rank " + std::to_string(MR_rank) + ": allocating shared memory windows of size 0 and 0");
            ret = MPI_Win_allocate_shared(0, sizeof(double), MPI_INFO_NULL,
                                          MR_comm, &latency_base_pointer, &shared_mem_window_latency);
            MPI_Error_string(ret, retstring, &retstringlength);
            IO->log_info("MR_rank " + std::to_string(MR_rank) + ":  allocate_shared returned: " + retstring);

            IO->log_info("MR_rank " + std::to_string(MR_rank) + ": query shared memory windows");

            ret = MPI_Win_shared_query(shared_mem_window_latency, 0, &size_latency, &disp_unit_latency,
                                       &latency_base_pointer);
            MPI_Error_string(ret, retstring, &retstringlength);
            IO->log_info("MR_rank " + std::to_string(MR_rank) + ":  shared_query returned: " + retstring);

        }

        MPI_Barrier(MR_comm);

        //fields for latency and per matrixes
        latency_matrix = (double *) latency_base_pointer;

        //read latency and PER files
        IO->log_info("MR_rank " + std::to_string(MR_rank) + ":  read latency file");
        IO->read_comm_data_file((char *) scenario_file_latency.c_str(), latency_matrix);

        //set the pointer of the matrix inside MR agent
        MR_agent->set_latency(latency_matrix);
        IO->log_info("MR_rank " + std::to_string(MR_rank) + ":  set latency pointer finished");
    }

}
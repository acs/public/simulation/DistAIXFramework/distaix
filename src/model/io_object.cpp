/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/io_object.h"

IO_object::IO_object(struct data_props _d_props) : d_props(_d_props) {
}

/*! \brief Initialize the stream for the log file
 * */
void IO_object::init_logfile() {
    if (d_props.log.logging) {
        try {
            log_stream.clear();
        }
        catch(std::ofstream::failure &err){
            std::cerr << "IO_object::init_logfile(): " << d_props.log.path_log_file << ": clear: " << err.what() << std::endl;
            MPI_Abort(MPI_COMM_WORLD, -2);
        }

        try{
            log_stream.open(d_props.log.path_log_file);
            if(!log_stream.fail()) {
                log_stream << "############ LOG FILE " << d_props.log.path_log_file << " ############" << std::endl;
            }
        }
        catch(std::ofstream::failure &err){
            std::cerr << "IO_object::init_logfile(): " << d_props.log.path_log_file << ": open/ write: " << err.what() << std::endl;
            log_stream.close();
            MPI_Abort(MPI_COMM_WORLD, -3);
        }
    }
}

/*! \brief Initialize the stream for the result file
 * */
void IO_object::init_resultfile() {
    if(d_props.result.csv_results){

        try {
            result_stream.clear();
        }
        catch(std::ofstream::failure &err){
            std::cerr << "IO_object::init_resultfile(): " << d_props.result.path_result_file << ": clear: " << err.what() << std::endl;
            MPI_Abort(MPI_COMM_WORLD, -2);
        }

        try{
            result_stream.open(d_props.result.path_result_file);
            if(!result_stream.fail()) {
                set_result_file_header();
            }
        }
        catch(std::ofstream::failure &err){
            std::cerr << "IO_object::init_resultfile(): " << d_props.result.path_result_file << ": open/ write: " << err.what() << std::endl;
            result_stream.close();
            MPI_Abort(MPI_COMM_WORLD, -3);
        }
    }
}

void IO_object::set_result_file_header() {
    //dummy
}

/*! \brief Add a string to the log file
 * \param log [in] String to add to the agent's log file
 * */
void IO_object::log_info(std::string log) {
    if (d_props.log.logging) {
        log_stream << log << std::endl;
    }
}

/*! \brief update the next time step t_next
 * \param _t_next [in] new value for the next time step
 * */
void IO_object::update_t(double _t_next) {
    t_next = _t_next;
    log_info("---------------------------------------- Step: " + std::to_string(t_next) +
             "s ----------------------------------------");
}

/*! \brief update the paths to result and log files
 * \param _path_log [in]    new path to log files
 * \param _path_result [in] new path to result files
 * */
void IO_object::update_file_paths(std::string &_path_log, std::string &_path_result) {
    d_props.result.path_result_file = _path_result;
    d_props.log.path_log_file = _path_log;
}

/*! \brief flush the log stream
 * */
void IO_object::flush_log() {
    if (d_props.log.logging) {
        log_stream.flush();
    }
}

/*! \brief flush the result stream
 * */
void IO_object::flush_result() {
    if(d_props.result.csv_results) {
        result_stream.flush();
    }
}

/*! \brief finish all IO operations by closing all file streams
 * */
void IO_object::finish_io() {

    if (d_props.log.logging) {
        log_stream.close();
    }
    if(d_props.result.csv_results) {
        result_stream.close();
    }
}

/*! \brief Transform a RepastHPC agent ID to string
 *  \param [in] _id Agent ID to be transformed to string
 *  \return String of agent ID
 * */
std::string IO_object::id2str(repast::AgentId &_id) {
    std::stringstream ret;
    ret << _id;
    return ret.str();
}

/*! \brief Transform a RepastHPC agent request to string
 *  \param [in] _req Agent request to be transformed to string
 *  \return String of agent request
 * */
std::string IO_object::req2str(repast::AgentRequest &_req) {
    std::stringstream ret;
    ret << _req;
    return ret.str();
}
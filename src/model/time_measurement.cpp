/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "model/time_measurement.h"

/*!
*   \brief create a new Time_measurement
*   \param _measurementType Name for the time measurement
*   \param _id              an ID of the time measurement
* */
Time_measurement::Time_measurement(std::string _measurementType, int _id)
                                    :measurementType(_measurementType), id(_id){
    duration = 0;
}


/*!
*   \brief start a timemeasurement
* */
void Time_measurement::start(){
    start_t = std::chrono::high_resolution_clock::now();
}

/*!
*   \brief stop a timemeasurement and add the difference between start and stop to the duration
* */
void Time_measurement::stop(){
    stop_t = std::chrono::high_resolution_clock::now();
    long long diff = std::chrono::duration_cast<std::chrono::microseconds>(stop_t.time_since_epoch()).count() - 
    std::chrono::duration_cast<std::chrono::microseconds>(start_t.time_since_epoch()).count();
    duration += diff;
}

/*!
*   \brief reset duration to zero
* */
void Time_measurement::reset(){
  duration = 0;
  //reset
  start_t = std::chrono::high_resolution_clock::time_point();
  stop_t = std::chrono::high_resolution_clock::time_point();
}

/*!
*   \brief get the duration
* */
long long Time_measurement::get_duration(){
  return duration;
}

/*!
*   \brief get the timeMeasurementType
* */
std::string Time_measurement::get_time_measurement_type(){
  return measurementType;
}
/*!
*   \brief get the ID
* */
int Time_measurement::getId(){
  return id;
}

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/


#include <sstream>
#include <list>
#include <boost/mpi.hpp>
#include <boost/exception/all.hpp>
#include "model/config.h"
#include "model/model.h"
#include "villas_interface/villas_interface.h"


BOOST_CLASS_EXPORT_GUID(repast::SpecializedProjectionInfoPacket<Edge_content<Agent> >, "SpecializedProjectionInfoPacket_EDGE");


/*! \brief Constructor of Model class
 *  \param propsFile path to model.props file
 *  \param argc parameter handed over from main
 *  \param argv parameter handed over from main
 *  \param comm MPI communicator
 * */
Model::Model(std::string &propsFile, int argc, char** argv, boost::mpi::communicator* comm): context(comm) {



    props = new repast::Properties(propsFile, argc, argv, comm);
    rank = repast::RepastProcess::instance()->rank();
    world_size = repast::RepastProcess::instance()->worldSize();

#ifndef USE_DB
    if(rank==0){
        std::cout << "********************** IMPORTANT NOTICE ******************" << std::endl;
        std::cout << "** YOU ARE RUNNING THE 'NO DATABASE' VERSION OF DISTAIX **" << std::endl;
        std::cout << "** THAT MEANS: NO DATABASE FEATURES ARE AVAILABLE ********" << std::endl;
        std::cout << "** COMPILE WITH 'WITH_DB' SET TO 1  TO USE DB ************" << std::endl;
        std::cout << "**********************************************************" << std::endl;
    }
#endif

    read_model_props();


    IO = new ModelIO(props,rank,world_size,profile_dir, d_props);

    if (rank == 0){
        IO->print_model_configuration();
    }

    // initialize database if needed
    IO->init_db();
    //save database connector of model IO (dbconn) in data_props of model for further usage
    d_props.result.dbconn = IO->d_props.result.dbconn;

    // initialize logging:
    IO->init_logging();

	provider = new AgentPackageProvider(&context);
	receiver = new AgentPackageReceiver(&context);

    agent_network_elec = new repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> >("electrical_network", false, &edgeContentManager);
	agent_network_MR = new repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> >("communication_network", false, &edgeContentManager);
	context.addProjection(agent_network_elec);
    context.addProjection(agent_network_MR);

    //read scenario files
    read_scenario_files();

    //initialize counter variables for FBS
    IO->reset_loop_counters();

    // read villas configuration parameters from model.props file
    read_villas_config();

    // Create Message Router and Directory Facilitator agents
    create_MR_DF_agents();

}

/*! \brief Destructor of Model class
 * Deleting all pointers to dynamically allocated objects.
 * */
Model::~Model(){

    IO->finish_time_measurements(stop_at);

    if(behavior_type == CTRL_MQTT_TEST
    || behavior_type == CTRL_MQTT_PINGPONG
    || behavior_type == CTRL_MQTT_HIGHLOAD
    || behavior_type == CTRL_ENSURE
    || behavior_type == CTRL_DPSIM_COSIM_REF
    || behavior_type == CTRL_DPSIM_COSIM_SWARMGRIDX){
        //disconnect villas_interface of all all agents
        for(auto a : local_agents){
            IO->log_info("Disconnecting agent " + IO->id2str(a->getId()));
            int ret = a->villas_interface_disconnect();
            if (ret) {
                IO->log_info("Error upon disconnecting agent " + IO->id2str(a->getId()) + ": " + std::to_string(ret));
            }
        }
        struct timespec t;
        t.tv_sec = 0;
        t.tv_nsec = 1000000; // 1 ms for all disconnections to complete
        nanosleep(&t, nullptr);

        //Stop the villas node type before deleting IO
        interface->stop_node_type();
        delete villas_config_agents;
        delete villas_config_node_disabled;
        delete interface;

    }

    delete creator;
    delete props;
    delete provider;
    delete receiver;

    if(rank == 0)
      delete progress_p;

    if(behavior_type != CTRL_NON_INTELLIGENT && use_commdata) {
        //free shared memory variables only if not in non-intelligent case
        MPI_Win_free(&shared_mem_window_latency);
//        //MPI_Win_free(&shared_mem_window_per);
        MPI_Comm_free(&MR_comm);

    }
    MPI_Win_free(&shared_mem_window_agent_rank_relation);
    MPI_Comm_free(&agent_rank_relation_comm);

    IO->finish_io();
    delete IO;
}

/*#################### METHODS USED DURING INITIALIZATION OF THE MODEL ##########################*/

/*! \brief Initialize the Model
 *
 * The following steps are performed
 * - create all agents that belong to this proces)
 * - create electrical network between agents
 * - create communication network between MR agents
 * - initialize agent behaviors
 * - initialize progress bar (rank 0 only)
 * */
void Model::init() {

    if(rank == 0){
        std::cout << "### Initializing model in all MPI processes..." << std::endl;
    }

    IO->start_time_measurement(IO->tm_init);
    /* read profile data*/
    read_profiles();
	
    // If we should ever go back to MPI shared memory for saving profiles
    // we need to uncomment the following barrier to make sure that all processes
    // finished reading their share of the profiles to have all profiles available
    // before starting with agent creation
    //MPI_Barrier(MPI_COMM_WORLD);

    /* distribute agents to processes and create them*/
    this->create_agents();

    // Barrier here because agents need to be created in all processes
    // before starting to connect them (eventually across processes)
    MPI_Barrier(MPI_COMM_WORLD);


    /* create electrical connections between agents*/
    this->init_elec_network();

    /* create communication connections between all MR agents in the model*/
    this->init_comm_network();

    /* initialize the agent behaviors*/
    this->init_agent_behaviors();

    MPI_Barrier(MPI_COMM_WORLD);

    IO->stop_time_measurement(IO->tm_init);

    /*Initialize progess bar in rank 0*/
    if(rank == 0)
        progress_p = new boost::progress_display( stop_at );

    /*Write some time measurement results of initialization*/
    IO->log_info("############## INIT FINISHED - TIME MEASUREMENTS ###################");
    IO->log_info("init() took me " + std::to_string(IO->get_time_measurement(IO->tm_init) / 1000000.0 ) + " ms");
    IO->log_info("comm init: MR agent took me " + std::to_string(IO->get_time_measurement(IO->tm_init_MR) / 1000000.0) + " ms");
    IO->log_info("comm init: initial comm partners took me " + std::to_string(IO->get_time_measurement(IO->tm_comm_partners_init) / 1000000.0) + " ms");
    IO->log_info("####################################################################");

}

/*! \brief Initialize the schedule of the model that is executed by the MPI process
 *
 *  Schedules all methods to be called be the MPI process during simulation to a specific simulation tick.
 *  Single as well as periodic events can be scheduled.
 *  The stop tick of the simulation is also specified in this function.
 * */
void Model::initSchedule(repast::ScheduleRunner& runner){

    //start measurement of exact tick duration (stop is called in Model::step(...) method)
    //runner.scheduleEvent(0.4, 1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::start_tick_time_measurement)));

    //calculate communication interactions and agent behavior
    runner.scheduleEvent(0.6,  1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::process_agent_messages)));

    //synchronize electrical connections (required to sync next_action expected and convergence flags of non-local agents)
    //runner.scheduleEvent(0.7,  1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::synchronize_el_connections)));

    //calculate electrical interactions
    runner.scheduleEvent(0.8,  1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::forward_backward_sweep)));

    //advance one simulation step
    runner.scheduleEvent(1,    1, repast::Schedule::FunctorPtr(new repast::MethodFunctor<Model> (this, &Model::step)));

    runner.scheduleStop(stop_at);

}


/*#################### METHODS USED DURING EXECUTION OF THE MODEL ##########################*/

/*void Model::start_tick_time_measurement() {
    IO->start_time_measurement(IO->tm_rt_tick);
    IO->start_time_measurement(IO->tm_tick);

}*/

/*! \brief Synchronize copies of non-local agents with their origins using RepastHPC methods
 * \param agent_set Name of the set of non-local agents to be updated
 * */
void Model::synchronize(const std::string agent_set) {

    if (agent_set == SET_EL_CONNECTIONS){
        IO->start_time_measurement(IO->tm_sync_el);
    } else {
        IO->start_time_measurement(IO->tm_sync_mr);
    }

    try {
        repast::RepastProcess::instance()->synchronizeAgentStates<AgentPackage, AgentPackageProvider, AgentPackageReceiver>(
                *provider, *receiver, agent_set);
    } catch (boost::exception &err) {
        std::cerr << "Rank " << rank << " : boost exception in model::synchronization(). Abort. \n"
                  << boost::diagnostic_information(err) << std::endl;
        MPI_Abort(MPI_COMM_WORLD, -1);

    } catch (std::exception &err) {
        std::cerr << "Rank " << rank
                  << " : std::exception caught in model::synchronization(). Abort. \n"
                  << err.what() << std::endl;
        MPI_Abort(MPI_COMM_WORLD, -1);
    }

    if (agent_set == SET_EL_CONNECTIONS){
        IO->stop_time_measurement(IO->tm_sync_el);
    } else {
        IO->stop_time_measurement(IO->tm_sync_mr);
    }


}

void Model::synchronize_el_connections(){
    this->synchronize(SET_EL_CONNECTIONS);
}

/*! \brief Advances a simulation step for every local agent and cable
 * */
void Model::step() {
    IO->start_time_measurement(IO->tm_step);

    repast::ScheduleRunner &runner = repast::RepastProcess::instance()->getScheduleRunner();
    double tick = runner.currentTick();
    IO->log_info("At Tick " + std::to_string(tick));

    /* Advance a step for every local agent */
    for(auto i : local_agents){
        IO->log_info("at tick " + std::to_string(runner.currentTick()) + " step for agent: " + IO->id2str(i->getId()));
        i->step();
    }
    /* Advance a step for every local cable */
    for(auto &i  : cables){
        IO->log_info("at tick " + std::to_string(runner.currentTick()) + " step for cable: " + i->getId());
        i->step(IO->tm_csv, IO->tm_db, IO->tm_db_serialize, IO->tm_db_add);
    }

    /* Flush the log file of this process */
    IO->flush_log();

    /* Advance the progress bar */
    if (rank == 0)
        ++( *progress_p );

    /* flush database every 100 steps */
    //if((int) std::round(runner.currentTick()) % 100 == 0){
    //   	IO->flush_db();
    //}

    // During co-simulation, the rank including the slack-agent can decide to terminate
    // the simulation early, i.e. before all time steps are simulated.
    // In order to terminate the whole simulation properly, the schedulers of all ranks have to
    // synchronized
    if (behavior_type == CTRL_DPSIM_COSIM_REF || behavior_type == CTRL_DPSIM_COSIM_SWARMGRIDX){
        synchronize_schedulers();
    }

    IO->stop_time_measurement(IO->tm_tick);

    if(realtime){
        long long int time_elapsed_us = IO->tm_tick->get_duration();
        long long int time_to_wait_us = (long long int) (step_size * 1000000.0) - time_elapsed_us;
        if(time_to_wait_us > 0){
            //if this time step did not last long enough for realtime
            //wait until real time is over
            struct timespec t;
            t.tv_sec = time_to_wait_us / 1000000; //integer result intended!
            t.tv_nsec = time_to_wait_us*1000 - t.tv_sec * 1000000; //rest is nano seconds
            if(t.tv_nsec > 999999999){
                t.tv_nsec = 999999999;
            }
            nanosleep(&t, nullptr);
        }
        else if(time_to_wait_us < 0) {
            //if the computation of this time step took longer than real time
            // provide a warning to the user
            IO->log_info("REALTIME WARNING: unable to compute tick " + std::to_string(tick) +
                         " in time step size " + std::to_string(step_size) + " s");
            std::cerr << "REALTIME WARNING: unable to compute tick " << tick <<
                         " in time step size " << step_size << " s" << std::endl;
        }
    }
    IO->stop_time_measurement(IO->tm_step);
    IO->stop_time_measurement(IO->tm_rt_tick);
    /* log time measurements of this process for this tick */
    IO->log_results_for_tick(); //this logs the time measurement results
    IO->reset_time_measurements(); //This resets all time measurements

}

/*! \brief Process all agent messages
 *
 * This function processes all agent messages at every agent.
 * As a result, the agents determine their control behavior
 * */
void Model::process_agent_messages() {
    //start tick time measurements (stopped in step method of the same tick)
    IO->start_time_measurement(IO->tm_rt_tick);
    IO->start_time_measurement(IO->tm_tick);


    IO->start_time_measurement(IO->tm_msg_proc_internal);
    IO->log_info("####### now calculating communication interactions #######");

    /*route messages of last step*/
    MR_agent->route_outgoing_agent_messages_own(agent_network_MR);
    IO->stop_time_measurement(IO->tm_msg_proc_internal);

    //Send messages between processes' Message Routers via MPI
    this->synchronize(SET_MESSAGE_ROUTERS);

    IO->start_time_measurement(IO->tm_msg_proc_external);
    MR_agent->route_outgoing_agent_messages_other(agent_network_MR);

    IO->stop_time_measurement(IO->tm_msg_proc_external);
    /*make agents process messages*/

    IO->start_time_measurement(IO->tm_behaviors);
    IO->log_info("####### local agents process incoming messages #######");
    for(auto i : local_agents){
        IO->log_info("###### for agent " + IO->id2str(i->getId()));
        
        if(i->getId().agentType() == TYPE_LOAD_INT){
            IO->start_time_measurement(IO->tm_load);
        } else if (i->getId().agentType() == TYPE_PV_INT) {
            IO->start_time_measurement(IO->tm_pv);
        } else if (i->getId().agentType() == TYPE_EV_INT) {
            IO->start_time_measurement(IO->tm_ev);
        } else if (i->getId().agentType() == TYPE_HP_INT) {
            IO->start_time_measurement(IO->tm_hp);
        } else if (i->getId().agentType() == TYPE_CHP_INT) {
            IO->start_time_measurement(IO->tm_chp);
        } else if (i->getId().agentType() == TYPE_WEC_INT) {
            IO->start_time_measurement(IO->tm_wec);
        } else if (i->getId().agentType() == TYPE_BIOFUEL_INT) {
            IO->start_time_measurement(IO->tm_bio);
        } else if (i->getId().agentType() == TYPE_BATTERY_INT) {
            IO->start_time_measurement(IO->tm_bat);
        } else if (i->getId().agentType() == TYPE_COMPENSATOR_INT) {
            IO->start_time_measurement(IO->tm_comp);
        } else if (i->getId().agentType() == TYPE_TRANSFORMER_INT) {
            IO->start_time_measurement(IO->tm_substation);
        } else if (i->getId().agentType() == TYPE_SLACK_INT) {
            IO->start_time_measurement(IO->tm_slack);
        } else if (i->getId().agentType() == TYPE_DF_INT) {
            IO->start_time_measurement(IO->tm_df);
        }
        
        i->process_incoming_messages();
        
        if(i->getId().agentType() == TYPE_LOAD_INT){
            IO->stop_time_measurement(IO->tm_load);
        } else if (i->getId().agentType() == TYPE_PV_INT) {
            IO->stop_time_measurement(IO->tm_pv);
        } else if (i->getId().agentType() == TYPE_EV_INT) {
            IO->stop_time_measurement(IO->tm_ev);
        } else if (i->getId().agentType() == TYPE_HP_INT) {
            IO->stop_time_measurement(IO->tm_hp);
        } else if (i->getId().agentType() == TYPE_CHP_INT) {
            IO->stop_time_measurement(IO->tm_chp);
        } else if (i->getId().agentType() == TYPE_WEC_INT) {
            IO->stop_time_measurement(IO->tm_wec);
        } else if (i->getId().agentType() == TYPE_BIOFUEL_INT) {
            IO->stop_time_measurement(IO->tm_bio);
        } else if (i->getId().agentType() == TYPE_BATTERY_INT) {
            IO->stop_time_measurement(IO->tm_bat);
        } else if (i->getId().agentType() == TYPE_COMPENSATOR_INT) {
            IO->stop_time_measurement(IO->tm_comp);
        } else if (i->getId().agentType() == TYPE_TRANSFORMER_INT) {
            IO->stop_time_measurement(IO->tm_substation);
        } else if (i->getId().agentType() == TYPE_SLACK_INT) {
            IO->stop_time_measurement(IO->tm_slack);
        } else if (i->getId().agentType() == TYPE_DF_INT) {
            IO->stop_time_measurement(IO->tm_df);
        }
    }
    IO->stop_time_measurement(IO->tm_behaviors);
    IO->log_info("####### calculating communication interactions finished #######");

}

/*! \brief synchronizes stop_at of all schedulers to that of rank0
 *
 * The function sets the stop_at variable of the scheduler to the value
 * defined in villas_config_agents and synchronizes all schedulers
 * to that of rank0
 *
 * */
void Model::synchronize_schedulers(){
    double curr_stop_at = villas_config_agents->stop_at;
    if(world_size > 1){
        IO->log_info("Synchronizing Scheduler");
        MPI_Bcast(&curr_stop_at, 1, MPI_DOUBLE, agent_rank_relation[0], MPI_COMM_WORLD);
	if(curr_stop_at != stop_at){
            IO->log_info("New value for stop_at received = " + std::to_string(curr_stop_at));
            stop_at = curr_stop_at;
            repast::RepastProcess::instance()->getScheduleRunner().scheduleStop(stop_at);
        }
    }


}

/*! \brief exit the simulation (called in error case)
 * \param code Integer giving the the code of the error that happend
 *
 * Send an MPI Abort signal to all MPI processes.
 * */
void Model::do_exit(int code) {
    std::cout << "Model Error. Aborting!" << std::endl;
    MPI_Abort(MPI_COMM_WORLD, code);

}

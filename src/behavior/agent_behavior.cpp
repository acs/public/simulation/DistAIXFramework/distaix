/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/agent_behavior.h"
#include "villas_interface/villas_interface.h"

/*! \brief Constructor used for creation of agent behavior
 *  \param _id [in]             RepastHPC agent id
 *  \param _type [in]           component type of the agent
 *  \param _subtype [in]        component subtype of the agent
 *  \param step_size [in]       size of a simulation step in seconds
 *  \param logging [in]         indicates if agents logs locally
 * */
Agent_behavior::Agent_behavior(int _id, int _type, std::string _subtype, double& step_size,
        struct data_props _d_props) : id(_id), type(_type), subtype(_subtype), t_step(step_size) {
    t_start = 0.0;
    t_next = t_start;
    msg_sent = 0;
    msg_received = 0;
    outgoing_messages.clear();
    incoming_messages.clear();
    villas_interface = nullptr;

    struct data_props d_props = _d_props;
    //change name of result subfolder
    std::string replace = "/agents/";
    std::string replace_with ="/behavior/";
    size_t pos = d_props.log.path_log_file.find(replace);
    d_props.log.path_log_file = d_props.log.path_log_file.replace(pos, replace.length(), replace_with);
    IO = new BehaviorIO(_id, d_props);
}

/*!
 * \brief Destroy the Agent_behavior::Agent_behavior object
 */
Agent_behavior::~Agent_behavior() {
}

/*! \brief update time (to be called after every time step)
 *  \param _t_next [in] next time step
 * */
void Agent_behavior::update_t(double _t_next) {
    t_next = _t_next;
    msg_sent = 0;
    msg_received = 0;
    IO->update_t(t_next);
    IO->flush_log();
}

/*! \brief inserts message into outgoing queue
 *  \param msg [in] message to be sent
 * */
void Agent_behavior::send_message(Agent_message &msg) {
    outgoing_messages.push_back(msg);
    msg_sent++;
    IO->log_info("\tSend message: " + msg.get_message_output());
}

/*! \brief insert message into incoming queue
 *  \param msg [in] msg to be receibed
 * */
void Agent_behavior::receive_message(Agent_message& msg) {
    /*insert message into incoming queue sorted by sender id*/
    if(incoming_messages.empty()){
        incoming_messages.push_back(msg);
    } else {
        bool inserted = false;
        for(auto it = incoming_messages.begin(); it!= incoming_messages.end(); it++){
            if(it->sender > msg.sender){

                incoming_messages.insert(it, msg);
                inserted = true;
                break;

            }
        }
        if(!inserted){
            incoming_messages.push_back(msg);
        }
    }

    msg_received++;
}

/*! \brief get next message in outgoing queue and delete it
 *  \return next message in outgoing queue
 * */
Agent_message Agent_behavior::get_next_outgoing_message() {
    Agent_message next_msg = outgoing_messages.front();
    outgoing_messages.pop_front();
    return next_msg;
}

/*! \brief check if outgoing queue is empty
 *  \return indicates if outgoing message queue is empty
 * */
bool Agent_behavior::outgoing_is_empty() {
    return outgoing_messages.empty();
}

/*! \brief get complete outgoing queue
 *  \return outgoing message queue
 * */
std::list<Agent_message> Agent_behavior::get_outgoing_queue() {
    return outgoing_messages;
}

/*! \brief delete all messages in outgoing queue
 * */
void Agent_behavior::clear_outgoing() {
    outgoing_messages.clear();
}

/*! \brief get number od messages that have been sent
 *  \return number of sent messages
 * */
unsigned int Agent_behavior::get_msg_sent() {
    return msg_sent;
}

/*! \brief get number of messages that have been received
 *  \return number of received messages
 * */
unsigned int Agent_behavior::get_msg_received() {
    return msg_received;
}


int Agent_behavior::init_villas_interface(villas_node_config *_villas_config, std::vector<Meta_infos> &meta) {
    //init and start villas node interface
    IO->log_info("Setting up villas interface");
    try{
        villas_interface = new Villas_interface(_villas_config, IO, "agent_" + std::to_string(id), meta);
        //villas_interface->init();
        villas_interface->start();
    } catch (std::runtime_error &ex)  {
        IO->log_info("Caught exception: " + std::string(ex.what()));
        return -1;
    }
    return 0;
}


int Agent_behavior::destroy_villas_interface() {
    if(villas_interface !=nullptr) {
        //init and start villas node interface
        IO->log_info("Destroying villas interface");
        try {
            villas_interface->stop();
            villas_interface->destroy();
        } catch (std::runtime_error &ex) {
            IO->log_info("Caught exception: " + std::string(ex.what()));
            return -1;
        }
        delete villas_interface;
    }
    return 0;
}
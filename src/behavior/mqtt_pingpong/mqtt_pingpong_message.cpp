/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/mqtt_pingpong/mqtt_pingpong_message.h"
#include <sstream>

std::vector<Meta_infos> Mqtt_pingpong_msg::mqtt_pingpong_msg_meta = std::vector<Meta_infos>();
bool Mqtt_pingpong_msg::meta_initialized = false;

Mqtt_pingpong_msg::Mqtt_pingpong_msg() : Villas_message() {

    performative = nullptr;
    agent_id = nullptr;
    P_ctrl = nullptr;
    Q_ctrl = nullptr;
    n_ctrl = nullptr;
    v_meas = nullptr;

    init_message(0,0,0,0.0, 0,0.0);

}

Mqtt_pingpong_msg::Mqtt_pingpong_msg(Villas_message &msg) : Villas_message(){
    data = msg.data;
    length = msg.length;

}


Mqtt_pingpong_msg::Mqtt_pingpong_msg(int _performative, int _agent_id) {
    performative = nullptr;
    agent_id = nullptr;
    P_ctrl = nullptr;
    Q_ctrl = nullptr;
    n_ctrl = nullptr;
    v_meas = nullptr;

    init_message(_performative,_agent_id,0,0.0, 0, 0.0);
}

void Mqtt_pingpong_msg::set_pointers() {

    //set the pointer to the elements in data field
    //if you change the order or data types of data elements you have to adapt the indices here
    performative = &(data[0].i);
    agent_id = &(data[1].i);
    P_ctrl = &(data[2].f);
    Q_ctrl = &(data[3].f);
    n_ctrl = &(data[4].i);
    v_meas = &(data[5].f);
}

void Mqtt_pingpong_msg::init_message(int _performative, int _agent_id, double _P_ctrl, double _Q_ctrl, int _n_ctrl, double _v_meas) {

    //add all data and meta elements of this message type
    Data_element perf_data;
    perf_data.i = _performative;
    add_element(perf_data, VILLAS_DATA_TYPE_INT64);

    Data_element sender_data;
    sender_data.i = _agent_id;
    add_element(sender_data, VILLAS_DATA_TYPE_INT64);

    Data_element P_ctrl_data;
    P_ctrl_data.f = _P_ctrl;
    add_element(P_ctrl_data, VILLAS_DATA_TYPE_DOUBLE);

    Data_element Q_ctrl_data;
    Q_ctrl_data.f = _Q_ctrl;
    add_element(Q_ctrl_data, VILLAS_DATA_TYPE_DOUBLE);

    Data_element n_ctrl_data;
    n_ctrl_data.i = _n_ctrl;
    add_element(n_ctrl_data, VILLAS_DATA_TYPE_INT64);

    Data_element v_meas_data;
    v_meas_data.f = _v_meas;
    add_element(v_meas_data, VILLAS_DATA_TYPE_DOUBLE);

    //set the pointers of the message to the corresponding fields in data
    set_pointers();
}


void Mqtt_pingpong_msg::init_meta() {
    // returns immediately if static meta information has already been initialized for this message type
    //initialize meta info only once
    if(!Mqtt_pingpong_msg::meta_initialized) {
        Mqtt_pingpong_msg::meta_initialized = true;
        Mqtt_pingpong_msg::mqtt_pingpong_msg_meta.clear();

        Meta_infos perf_meta;
        perf_meta.name = "performative";
        perf_meta.unit = "peformative of this message";
        perf_meta.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_pingpong_msg::mqtt_pingpong_msg_meta.push_back(perf_meta);

        Meta_infos sender_meta;
        sender_meta.name = "agent_id";
        sender_meta.unit = "id of agent involved in this message";
        sender_meta.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_pingpong_msg::mqtt_pingpong_msg_meta.push_back(sender_meta);

        Meta_infos P_ctrl_meta;
        P_ctrl_meta.name = "P_ctrl";
        P_ctrl_meta.unit =  "W";
        P_ctrl_meta.type = VILLAS_DATA_TYPE_DOUBLE;
        Mqtt_pingpong_msg::mqtt_pingpong_msg_meta.push_back(P_ctrl_meta);

        Meta_infos Q_ctrl_meta;
        Q_ctrl_meta.name = "Q_ctrl";
        Q_ctrl_meta.unit = "var";
        Q_ctrl_meta.type = VILLAS_DATA_TYPE_DOUBLE;
        Mqtt_pingpong_msg::mqtt_pingpong_msg_meta.push_back(Q_ctrl_meta);

        Meta_infos n_ctrl_meta;
        n_ctrl_meta.name = "n_ctrl";
        n_ctrl_meta.unit = "tap pos";
        n_ctrl_meta.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_pingpong_msg::mqtt_pingpong_msg_meta.push_back(n_ctrl_meta);

        Meta_infos v_meas_meta;
        v_meas_meta.name = "v_meas";
        v_meas_meta.unit = "V";
        v_meas_meta.type = VILLAS_DATA_TYPE_DOUBLE;
        Mqtt_pingpong_msg::mqtt_pingpong_msg_meta.push_back(v_meas_meta);

    }

}

/*!
*   \brief Writes the content of a message to a string for debugging
*   \return Message content as string
* */
std::string Mqtt_pingpong_msg::get_message_output()
{
    std::stringstream temp;
    std::string output;

    temp    << "Time: " << time_sec
            << " Agent ID: " << *agent_id
            << " Performative: " << *performative
            << " P_ctrl: " << *P_ctrl
            << " Q_ctrl: " << *Q_ctrl
            << " n_ctrl: " << *n_ctrl
            << " v_meas: " << *v_meas;

    output = temp.str();
    return output;
}


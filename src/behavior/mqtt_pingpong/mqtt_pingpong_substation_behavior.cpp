/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/mqtt_pingpong/mqtt_pingpong_substation_behavior.h"
#include "villas_interface/villas_interface.h"

/*! \brief Constructor
 *  \param _id [in]                 RepastHPC agent id
 *  \param _type [in]               component type of the agent
 *  \param _subtype [in]            component subtype of the agent
 *  \param step_size [in]           size of a simulation step in seconds
 *  \param logging [in]             indicates if agents logs locally
 *  \param _substation_data [in]    pointer to substation specific datastruct
 * */
Mqtt_pingpong_substation_behavior::Mqtt_pingpong_substation_behavior(int _id, int _type, int _rank, std::string _subtype,
                                                           double& step_size, struct data_props _d_props, villas_node_config * _villas_config,
                                                           Substation_data * _substation_data)
        : Mqtt_pingpong_behavior(_id, _type, _rank, _subtype, step_size, _d_props, _villas_config),
          substation_data(_substation_data)
{
    voltage_measurements.clear();
}

/*! \brief Substation reference behavior execution
 * */
void Mqtt_pingpong_substation_behavior::execute_agent_behavior()
{

    if(t_next == t_start){
        handshake();
    }

    IO->log_info("###### Process incoming messages");
    process_incoming_messages();
    IO->log_info("###### Apply control values");
    apply_control_values();
}

void Mqtt_pingpong_substation_behavior::process_mqtt_pingpong_msg(Mqtt_pingpong_msg &msg)
{
    // ensure test behavior receives a ping from the cloud platform and replies with a pong
    double v = sqrt(substation_data->v2_re * substation_data->v2_re +
                    substation_data->v2_im * substation_data->v2_im) / (substation_data->Vnom2 / sqrt(3));
    Mqtt_pingpong_msg new_msg;
    int counter = (int) *msg.n_ctrl;
    counter ++;
    new_msg.time_sec = t_next;
    *(new_msg.agent_id) = id;
    *(new_msg.performative) = *msg.performative;
    *(new_msg.P_ctrl) = *msg.P_ctrl;
    *(new_msg.Q_ctrl) = *msg.Q_ctrl;
    *(new_msg.n_ctrl) = counter;
    *(new_msg.v_meas) = v;
    IO->log_info("\tSend message: " + new_msg.get_message_output());
    send_villas_msg(new_msg);
}


/*! \brief applies control value for tap changer according to secondary node voltage
 * */
void Mqtt_pingpong_substation_behavior::apply_control_values()
{
    /* adjust tap position according to voltage of secondary node */
    double v = sqrt(substation_data->v2_re * substation_data->v2_re +
                    substation_data->v2_im * substation_data->v2_im) / (substation_data->Vnom2 / sqrt(3));
    // Define reasonable minimal interval for oltc switching, now 60 sek
    if (t_next >= substation_data->t_last_action + 60) {
        if (fabs(v) > 0.001) {
            if (v < 1) {
                /* voltage is smaller than 1 pu -> decrease tap position */
                double delta = ((1 - v) * 100) / (substation_data->range / substation_data->N);
                /* threshold for tap changing in order to avoid oscillations */
                if (delta > 0.7) {
                    substation_data->n_ctrl = substation_data->n - round(delta);
                }
                if (substation_data->n_ctrl < -substation_data->N) {
                    substation_data->n_ctrl = -substation_data->N;
                }
            } else {
                /* voltage is greater than 1 pu -> increase tap position */
                double delta = ((v - 1) * 100) / (substation_data->range / substation_data->N);
                /* threshold for tap changing in order to avoid oscillations */
                if (delta > 0.7) {
                    substation_data->n_ctrl = substation_data->n + round(delta);
                }
                if (substation_data->n_ctrl > substation_data->N) {
                    substation_data->n_ctrl = substation_data->N;
                }
            }

        }
        if (substation_data->n_ctrl != substation_data->n) {
            substation_data->t_last_action = t_next;
        }
    }

    IO->log_info("\tApplying control values\n\t\tn_ctrl: " +
                 std::to_string(substation_data->n_ctrl));
}

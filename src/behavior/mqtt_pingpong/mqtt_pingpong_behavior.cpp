/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/mqtt_pingpong/mqtt_pingpong_behavior.h"
#include "villas_interface/villas_interface.h"
#include "model/time_measurement.h"

/*! \brief  constructor
 *  \param  _id         unique ID of agent
 *  \param  _type       agent type as defined in config.h
 *  \param  _subtype    subtype as defined in config.h
 *  \param  step_size   simulation step size
 *  \param  logging     indicates if agents logs locally
 * */
Mqtt_pingpong_behavior::Mqtt_pingpong_behavior(int _id, int _type, int _rank, std::string _subtype, double& step_size,
                                     struct data_props _d_props, villas_node_config * _villas_config)
        : Agent_behavior(_id, _type, _subtype, step_size, _d_props)
{

    vnconfig = _villas_config;

    if(_type == TYPE_DF_INT) {
        std::string path = "runlog/behavior/agent_DF" + std::to_string(_rank) + ".log";
        std::string path_empty = "";
        IO->update_file_paths(path, path_empty);
    }

    IO->init_logging();


}

Mqtt_pingpong_behavior::~Mqtt_pingpong_behavior(){
    //destroy_villas_interface is called in agent method
    IO->finish_io();
}

/*! \brief process all messages that have been received since the last time step and call protocol
 * specific process function
 * */
void Mqtt_pingpong_behavior::process_incoming_messages()
{
    std::list<Villas_message> incoming_villas_messages;

    villas_interface->receive_messages(incoming_villas_messages);

    while (!incoming_villas_messages.empty()) {
        Mqtt_pingpong_msg msg = (Mqtt_pingpong_msg) incoming_villas_messages.front();
        //set pointers of data to members
        msg.set_pointers();
        IO->log_info("\tReceived message: " + msg.get_message_output());

        this->process_mqtt_pingpong_msg(msg);
        incoming_villas_messages.pop_front();
    }
}


/*! \brief Initialize the agent behavior
 * \param components_ad_nodes [in] vector saving a list of connceted components for each node
 * \param connected_nodes [in] vector saving a list of connected nodes for each node
 * \param components_file [in] data contained in scenario components input file (integers)
 * \param subtypes_file [in] subtypes contained in scenario components input file (strings)
 * \param el_grid_file [in] electrical connections contained in el. grid input file (integers)
 * */
int Mqtt_pingpong_behavior::initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                                              std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                                              boost::multi_array<int, 2> *components_file,
                                                              boost::multi_array<std::string, 1> *subtypes_file,
                                                              boost::multi_array<int, 2> *el_grid_file) {


    //initialize meta information for all message types used in this behavior
    Mqtt_pingpong_msg::init_meta();

    //prosumers publish sensor values and subscribe to control values
    vnconfig->type_config.mqtt_conf->subscribe = "agent_"+std::to_string(id)+"_ctrl";
    vnconfig->type_config.mqtt_conf->publish = "agent_"+std::to_string(id)+"_sen";
    vnconfig->type_config.mqtt_conf->retain = 0;
    vnconfig->type_config.mqtt_conf->keepalive = 1000; //set to high value to avoid keepalive messages

    //Initialize villas interface (important: after init of message meta info!)
    int ret = init_villas_interface(vnconfig, Mqtt_pingpong_msg::mqtt_pingpong_msg_meta);
    return ret;

}

/*! \brief Perform handshake with representation of this agent in the Ensure Cloud Platform
 * */
void Mqtt_pingpong_behavior::handshake() {

    Time_measurement *timer = new Time_measurement("handshake timer agent "+std::to_string(id), id);
    timer->reset();

    //prepare and send handshake message
    Mqtt_pingpong_msg handshake_msg;
    handshake_msg.time_sec = t_next;
    *(handshake_msg.agent_id) = id;
    *(handshake_msg.performative) = FIPA_PERF_INFORM;
    *(handshake_msg.P_ctrl) = 0.0;
    *(handshake_msg.Q_ctrl) = 0.0;
    *(handshake_msg.n_ctrl) = 0;
    *(handshake_msg.v_meas) = 0.0;
    IO->log_info("\tSend handshake ping message: " + handshake_msg.get_message_output());
    send_villas_msg(handshake_msg);

    /// TODO: uncomment from here to use handshake mechanism

    // start timer
    timer->start();

    // stay in method until response is received or timeout occurred
    double timeout = 10; //timeout in seconds
    std::list<Villas_message> incoming_villas_messages;
    while(true){

        //receive villas messages (only one message should be received for this topic)
        incoming_villas_messages.clear();
        villas_interface->receive_messages(incoming_villas_messages);

        if(!incoming_villas_messages.empty()){
            break;
        }

        timer->stop();
        double duration_us = timer->get_duration();
        if(duration_us > timeout * 1000000){
            //timeout
            IO->log_info("\tTimeout appeared during handshake, aborting simulation");
            MPI_Abort(MPI_COMM_WORLD, -6);
        }
        else{
            timer->start();
        }
    }

    // check content of handshake response
    if(incoming_villas_messages.size()>1){
        // if more than one response: error
        IO->log_info("\tError: Received more than one handshake response (" +
                     std::to_string(incoming_villas_messages.size()) + "), aborting simulation");

        MPI_Abort(MPI_COMM_WORLD, -7);
    }
    else {
        Mqtt_pingpong_msg msg = (Mqtt_pingpong_msg) incoming_villas_messages.front();
        msg.set_pointers();
        IO->log_info("\tReceived handshake response: " + msg.get_message_output());
        if( *msg.performative == FIPA_PERF_INFORM &&
            *msg.agent_id == id &&
            *msg.P_ctrl == 0 &&
            *msg.Q_ctrl == 0 &&
            *msg.n_ctrl == 0 &&
            *msg.v_meas == 0)
        {
            IO->log_info("\tHandshake with cloud platform agent completed successfully!");
        }
        else {
            IO->log_info("\tError: Content of handshake response is not correct, aborting simulation");
            MPI_Abort(MPI_COMM_WORLD, -8);
        }
    }

    // TODO uncomment until the end of the method to disable self ping pong
    /* 
    //prepare and send ping message
    Mqtt_pingpong_msg ping_msg;
    ping_msg.time_sec = t_next;
    *(ping_msg.agent_id) = id;
    *(ping_msg.performative) = FIPA_PERF_INFORM;
    *(ping_msg.P_ctrl) = 111.1;
    *(ping_msg.Q_ctrl) = 222.2;
    *(ping_msg.n_ctrl) = 0;
    *(ping_msg.v_meas) = 999.9;
    IO->log_info("\tSend message: " + ping_msg.get_message_output());
    send_villas_msg(ping_msg);
    */

}

/*!
 * \brief sending function for messages
 * \param msg message to be sent
 */
void Mqtt_pingpong_behavior::send_villas_msg(Villas_message &msg)
{
    villas_interface->send_message(msg);
}


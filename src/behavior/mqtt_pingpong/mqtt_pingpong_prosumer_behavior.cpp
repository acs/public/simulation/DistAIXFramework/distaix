/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/mqtt_pingpong/mqtt_pingpong_prosumer_behavior.h"
#include "villas_interface/villas_interface.h"

/*! \brief Constructor
 *  \param _id [in]             RepastHPC agent id
 *  \param _type [in]           component type of the agent
 *  \param _subtype [in]        component subtype of the agent
 *  \param step_size [in]       size of a simulation step in seconds
 *  \param logging [in]         indicates if agents logs locally
 *  \param _prosumer_data [in]  pointer to prosumer specific datastruct
 * */
Mqtt_pingpong_prosumer_behavior::Mqtt_pingpong_prosumer_behavior(int _id, int _type, int _rank, std::string _subtype,
                                                       double& step_size, struct data_props _d_props, villas_node_config * _villas_config,
                                                       Prosumer_data * _prosumer_data)
        : Mqtt_pingpong_behavior(_id, _type, _rank, _subtype, step_size, _d_props, _villas_config),
          prosumer_data(_prosumer_data)
{}


/*! \brief adjusts control values according to simple rules not requiring communication
 * */
void Mqtt_pingpong_prosumer_behavior::execute_agent_behavior()
{

    /*Upon first execution: perform handshake with ENSURE Flexibility platform*/
    if (t_next == t_start){
        handshake();
    }

    IO->log_info("###### Process incoming messages");
    process_incoming_messages();
    //IO->log_info("###### Publish voltage measurement");
    //publish_voltage_measurement();
    IO->log_info("###### Determine operation range");
    determine_operation_range();
    IO->log_info("###### Apply control values");
    apply_control_values();
}

void Mqtt_pingpong_prosumer_behavior::process_mqtt_pingpong_msg(Mqtt_pingpong_msg &msg)
{
    // ensure test behavior receives a ping from the cloud platform and replies with a pong
    std::complex<double> meas;
    meas.real(prosumer_data->v_re / (prosumer_data->Vnom / sqrt(3)));
    meas.imag(prosumer_data->v_im / (prosumer_data->Vnom / sqrt(3)));
    int counter = (int) (*msg.n_ctrl);
    counter++;
    Mqtt_pingpong_msg new_msg;
    new_msg.time_sec = t_next;
    *(new_msg.agent_id) = id;
    *(new_msg.performative) = *msg.performative;
    *(new_msg.P_ctrl) = *msg.P_ctrl;
    *(new_msg.Q_ctrl) = *msg.Q_ctrl;
    *(new_msg.n_ctrl) = counter;
    *(new_msg.v_meas) = std::abs(meas);
    IO->log_info("\tSend message: " + new_msg.get_message_output());
    send_villas_msg(new_msg);

}

/*! \brief determine the limits and optimal operation
 * */
void Mqtt_pingpong_prosumer_behavior::determine_operation_range()
{
    /* active power range */

    if (type == TYPE_BATTERY_INT) {
        /* determine limits according to SOC and maximum power output/input */
        prosumer_data->P_max = (1 - prosumer_data->SOC_el) * prosumer_data->C_el * 3600 / t_step;
        if (prosumer_data->P_max > prosumer_data->P_nom) {
            prosumer_data->P_max = prosumer_data->P_nom;
        }
        prosumer_data->P_min = -prosumer_data->SOC_el * prosumer_data->C_el * 3600 / t_step;
        if (prosumer_data->P_min < -prosumer_data->P_nom) {
            prosumer_data->P_min = -prosumer_data->P_nom;
        }
        prosumer_data->P_optimal = 0;
    }
    else if (type == TYPE_LOAD_INT) {
        /* loads are not flexible -> set all values to power demand */
        prosumer_data->P_max = prosumer_data->P_dem;
        prosumer_data->P_optimal = prosumer_data->P_dem;
        prosumer_data->P_min = prosumer_data->P_dem;
    }
    else if (type == TYPE_EV_INT) {
        /* determine minimum charging power */
        if (prosumer_data->profile1_id != -1) { // flexible EV
            if (prosumer_data->t_connected) {
                /* determine minimum power such that EV is charged when it disconnects */
                double E_lack = (1 - prosumer_data->SOC_el) * prosumer_data->C_el;
                prosumer_data->P_min = E_lack * 3600 / prosumer_data->t_connected;
                if (prosumer_data->P_min > prosumer_data->P_nom) {
                    prosumer_data->P_min = prosumer_data->P_nom;
                }
                /* determine maximum such that EV is not overcharged and maximum charging power is
                 * not exceeded */
                prosumer_data->P_max = E_lack * 3600 / t_step;
                if (prosumer_data->P_max > prosumer_data->P_nom) {
                    prosumer_data->P_max = prosumer_data->P_nom;
                }
                prosumer_data->P_optimal = prosumer_data->P_max;
            }
            else {
                prosumer_data->P_min = 0;
                prosumer_data->P_optimal = 0;
                prosumer_data->P_max = 0;
            }
        }
        else { // non flexible EV
            /* EV is fixed load */
            prosumer_data->P_max = prosumer_data->P_dem;
            prosumer_data->P_optimal = prosumer_data->P_dem;
            prosumer_data->P_min = prosumer_data->P_dem;
        }
    }
    else if (type == TYPE_HP_INT) {
        /* calculate minimum required power to cover thermal demand and maximum power according to
         * specifications and thermal storage capacity */
        prosumer_data->P_max = prosumer_data->P_nom;
        prosumer_data->P_min = 0;

        /* check if secondary heater is required to cover thermal demand */
        if (prosumer_data->P_th_dem > (prosumer_data->P_nom / prosumer_data->f_el +
                                       (prosumer_data->E_th * 3600 / t_step))) {
            /* HP has to run at P_nom -> no flexibility */
            prosumer_data->P_min = prosumer_data->P_nom;
            prosumer_data->P_max = prosumer_data->P_nom;
            prosumer_data->P_optimal = prosumer_data->P_nom;
        }
        else {
            /* HP has flexibility, secondary heater not needed */
            if ((prosumer_data->P_nom / prosumer_data->f_el - prosumer_data->P_th_dem)*t_step > 3600 *
                                                                                                (prosumer_data->C_th - prosumer_data->E_th)) {
                /* if there is space in the storage for what can additionally be thermally generated
                 * by the HP Pmax is thermal demand coverage + rest that fits in thermal storage */
                prosumer_data->P_max = (prosumer_data->P_th_dem + 3600 *
                                                                  (prosumer_data->C_th - prosumer_data->E_th) / t_step) * prosumer_data->f_el;
            }
            if (prosumer_data->P_th_dem * t_step > prosumer_data->E_th * 3600) {
                /* thermal demand is larger than stored thermal energy */
                prosumer_data->P_min = (prosumer_data->P_th_dem - prosumer_data->E_th * 3600 / t_step) *
                                       prosumer_data->f_el;
            }
            if (prosumer_data->P_min > prosumer_data->P_max) {
                prosumer_data->P_min = prosumer_data->P_max;
            }
            prosumer_data->P_optimal = prosumer_data->P_min;
        }

    }
    else if (type == TYPE_CHP_INT) {
        /* calculate minimum required power to cover thermal demand and maximum power according to
         * specifications and thermal storage capacity */
        prosumer_data->P_min = -prosumer_data->P_nom;
        prosumer_data->P_max = 0;

        /* check if secondary heater is required to cover thermal demand */
        if (prosumer_data->P_th_dem > (prosumer_data->P_nom / prosumer_data->f_el +
                                       (prosumer_data->E_th * 3600 / t_step))) {
            /* CHP has to run at P_nom -> no flexibility */
            prosumer_data->P_min = -prosumer_data->P_nom;
            prosumer_data->P_max = -prosumer_data->P_nom;
            prosumer_data->P_optimal = -prosumer_data->P_nom;
        }
        else {
            /* CHP has flexibility, secondary heater not needed */
            if ((prosumer_data->P_nom / prosumer_data->f_el - prosumer_data->P_th_dem)*t_step > 3600 *
                                                                                                (prosumer_data->C_th - prosumer_data->E_th)) {
                /* if there is space in the storage for what can additionally be thermally generated
                 * by the CHP Pmin is thermal demand coverage + rest that fits in thermal storage */
                prosumer_data->P_min = -(prosumer_data->P_th_dem + 3600 *
                                                                   (prosumer_data->C_th - prosumer_data->E_th) / t_step) * prosumer_data->f_el;
            }
            if (prosumer_data->P_th_dem * t_step > prosumer_data->E_th * 3600) {
                /* thermal demand is larger than stored thermal energy */
                prosumer_data->P_max = -(prosumer_data->P_th_dem - prosumer_data->E_th * 3600 / t_step)*
                                       prosumer_data->f_el;
            }
            if (prosumer_data->P_min > prosumer_data->P_max) {
                prosumer_data->P_min = prosumer_data->P_max;
            }
            prosumer_data->P_optimal = prosumer_data->P_max;
        }
    }
    else if (type == TYPE_PV_INT) {
        /* no flexibility */
        prosumer_data->P_max = -prosumer_data->P_gen;
        prosumer_data->P_optimal = -prosumer_data->P_gen;
        prosumer_data->P_min = -prosumer_data->P_gen;
    }
    else if (type == TYPE_WEC_INT) {
        /* no flexibility */
        prosumer_data->P_max = -prosumer_data->P_gen;
        prosumer_data->P_optimal = -prosumer_data->P_gen;
        prosumer_data->P_min = -prosumer_data->P_gen;
    }
    else if (type == TYPE_BIOFUEL_INT) {
        prosumer_data->P_max = 0;
        prosumer_data->P_optimal = -prosumer_data->P_gen;
        prosumer_data->P_min = -prosumer_data->P_gen;
    }
    else if (type == TYPE_COMPENSATOR_INT) {
        prosumer_data->P_max = 0;
        prosumer_data->P_optimal = 0;
        prosumer_data->P_min = 0;
    }

    /* reactive power range */

    if (type == TYPE_BATTERY_INT || type == TYPE_EV_INT) {
        prosumer_data->Q_max = get_q_available(prosumer_data->P_optimal);
        prosumer_data->Q_min = -prosumer_data->Q_max;
        prosumer_data->Q_optimal = 0;
    }
    else if (type == TYPE_PV_INT) {
        prosumer_data->Q_max = get_q_available(prosumer_data->P_optimal);
        prosumer_data->Q_min = -prosumer_data->Q_max;
        /*auto * prosumer_data = (PV_data *)model_data;
        double p_rel = fabs(prosumer_data->P_optimal) / prosumer_data->P_nom;
        if (p_rel < 0.5) {
            prosumer_data->Q_optimal = 0;
        }
        else {
            if (prosumer_data->P_nom < 3600) {
                prosumer_data->Q_optimal = 0;
            }
            else if (prosumer_data->P_nom < 13800) {
                double pf = 1 - (p_rel - 0.5) * 0.1;
                prosumer_data->Q_optimal = fabs(prosumer_data->P_ctrl) / pf * sin(acos(pf));
            }
            else {
                double pf = 1 - (p_rel - 0.5) * 0.1;
                prosumer_data->Q_optimal = fabs(prosumer_data->P_ctrl) / pf * sin(acos(pf));
            }
        }*/
        prosumer_data->Q_optimal = 0;
    }
    else if (type == TYPE_WEC_INT) {
        prosumer_data->Q_max = get_q_available(prosumer_data->P_optimal);
        prosumer_data->Q_min = -prosumer_data->Q_max;
        double p_rel = fabs(prosumer_data->P_optimal) / prosumer_data->P_nom;
        if (p_rel < 0.5) {
            prosumer_data->Q_optimal = 0;
        }
        else {
            if (prosumer_data->P_nom < 3600) {
                prosumer_data->Q_optimal = 0;
            }
            else if (prosumer_data->P_nom < 13800) {
                double pf = 1 - (p_rel - 0.5) * 0.1;
                prosumer_data->Q_optimal = fabs(prosumer_data->P_ctrl) / pf * sin(acos(pf));
            }
            else {
                double pf = 1 - (p_rel - 0.5) * 0.1;
                prosumer_data->Q_optimal = fabs(prosumer_data->P_ctrl) / pf * sin(acos(pf));
            }
        }
    }
    else if (type == TYPE_CHP_INT) {
        prosumer_data->Q_max = get_q_available(prosumer_data->P_optimal);
        prosumer_data->Q_min = -prosumer_data->Q_max;
        double p_rel = fabs(prosumer_data->P_optimal) / prosumer_data->P_nom;
        if (p_rel < 0.5) {
            prosumer_data->Q_optimal = 0;
        }
        else {
            if (prosumer_data->P_nom < 3600) {
                prosumer_data->Q_optimal = 0;
            }
            else if (prosumer_data->P_nom < 13800) {
                double pf = 1 - (p_rel - 0.5) * 0.1;
                prosumer_data->Q_optimal = fabs(prosumer_data->P_ctrl) / pf * sin(acos(pf));
            }
            else {
                double pf = 1 - (p_rel - 0.5) * 0.1;
                prosumer_data->Q_optimal = fabs(prosumer_data->P_ctrl) / pf * sin(acos(pf));
            }
        }
    }
    else if (type == TYPE_BIOFUEL_INT) {
        prosumer_data->Q_max = get_q_available(prosumer_data->P_optimal);
        prosumer_data->Q_min = -prosumer_data->Q_max;
        double p_rel = fabs(prosumer_data->P_optimal) / prosumer_data->P_nom;
        if (p_rel < 0.5) {
            prosumer_data->Q_optimal = 0;
        }
        else {
            if (prosumer_data->P_nom < 3600) {
                prosumer_data->Q_optimal = 0;
            }
            else if (prosumer_data->P_nom < 13800) {
                double pf = 1 - (p_rel - 0.5) * 0.1;
                prosumer_data->Q_optimal = fabs(prosumer_data->P_ctrl) / pf * sin(acos(pf));
            }
            else {
                double pf = 1 - (p_rel - 0.5) * 0.1;
                prosumer_data->Q_optimal = fabs(prosumer_data->P_ctrl) / pf * sin(acos(pf));
            }
        }
    }
    else if (type == TYPE_LOAD_INT) {
        /* no flexibility */
        prosumer_data->Q_max = prosumer_data->Q_dem;
        prosumer_data->Q_optimal = prosumer_data->Q_dem;
        prosumer_data->Q_min = prosumer_data->Q_dem;
    }
    else if (type == TYPE_HP_INT) {
        // TODO check reactive power behavior of HP
        prosumer_data->Q_max = 0;
        prosumer_data->Q_optimal = 0;
        prosumer_data->Q_min = 0;

    }
    else if (type == TYPE_COMPENSATOR_INT) {
        prosumer_data->Q_max = 0;
        prosumer_data->Q_optimal = 0;
        prosumer_data->Q_min = -prosumer_data->Q_r;
    }

    IO->log_info("\t\tP_min: " + std::to_string(prosumer_data->P_min) + ", P_optimal: " +
                 std::to_string(prosumer_data->P_optimal) + ", P_max: " +
                 std::to_string(prosumer_data->P_max));
    IO->log_info("\t\tQ_min: " + std::to_string(prosumer_data->Q_min) + ", Q_optimal: " +
                 std::to_string(prosumer_data->Q_optimal) + ", Q_max: " +
                 std::to_string(prosumer_data->Q_max));
}


void Mqtt_pingpong_prosumer_behavior::publish_voltage_measurement()
{
    if (t_next - ((int)(t_next / 5)) * 5 < t_step) { //every 5 seconds
        std::complex<double> meas;
        meas.real(prosumer_data->v_re / (prosumer_data->Vnom / sqrt(3)));
        meas.imag(prosumer_data->v_im / (prosumer_data->Vnom / sqrt(3)));
        Mqtt_pingpong_msg new_msg;
        new_msg.time_sec = t_next;
        *(new_msg.agent_id) = id;
        *(new_msg.performative) = FIPA_PERF_INFORM;
        *(new_msg.P_ctrl) = 0.0;
        *(new_msg.Q_ctrl) = 0.0;
        *(new_msg.n_ctrl) = 0;
        *(new_msg.v_meas) = std::abs(meas);
        IO->log_info("\tSend message: " + new_msg.get_message_output());
        send_villas_msg(new_msg);
    }
}


/*! \brief applies control values according to agent's optimal values
 * */
void Mqtt_pingpong_prosumer_behavior::apply_control_values()
{
    prosumer_data->P_ctrl = prosumer_data->P_optimal;
    prosumer_data->Q_ctrl = prosumer_data->Q_optimal;
    if (type == TYPE_COMPENSATOR_INT) {
        prosumer_data->n_ctrl = 0;
    }

    IO->log_info("\tApplying control values\n\t\tP_ctrl: " + std::to_string(prosumer_data->P_ctrl) +
                 "W Q_ctrl: " + std::to_string(prosumer_data->Q_ctrl) + "var");
}


/*! \brief  calculates the maximum available reactive power regarding the limits of converter and
 *          rated power
 *  \param p [in]   produced real power
 *  \return         available reactive power
 * */
double Mqtt_pingpong_prosumer_behavior::get_q_available(double p)
{
    double q1, q2;
    double q_available;

    q1 = sqrt(prosumer_data->S_r * prosumer_data->S_r - p * p);
    q2 = fabs(p * tan(acos(prosumer_data->pf_min)));
    if (q1 < q2)
        q_available = q1;
    else
        q_available = q2;

    return q_available;
}

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include <sstream>

#include "behavior/ensure/ensure_knowledge.h"


Ensure_knowledge::Ensure_knowledge() : id(0), IO(nullptr)
{
}

/*! \brief initialization of Knowledge class
 *  \param _id agent ID of owner agent
 *  \param _io pointer to agent's IO object
 *  \param know_comm pointer to agent's communication knowledge
 *  \param step_size simulation step size in s
 * */
void Ensure_knowledge::init(int &_id, BehaviorIO* _io)
{
    id = _id;
    IO = _io;
    SOC_meas_last = 0;
    P_meas_last = 0;
    Q_meas_last = 0;
    Pth_meas_last = 0;
    connected_last = false;
    t_connected_last = 0;
    v_meas_last = 0;
    v2_meas_last = 0;
    P_ctrl_last = 0;
    Q_ctrl_last = 0;
    n_ctrl_last = 0;
    t_last_sent = 0;
    t_last_recv = 0;
    return;
}

/*! \brief setter for soc measurement
 *  \param soc SOC measurement
 * */
void Ensure_knowledge::set_SOC_meas(double soc)
{
    SOC_meas_last = soc;
    return;
}

/*! \brief setter for PQ measurement
 *  \param p P measurement
 *  \param q Q measurement
 * */
void Ensure_knowledge::set_PQ_meas(double p, double q)
{
    P_meas_last = p;
    Q_meas_last = q;
    return;
}

/*! \brief setter for Pth measurement
 *  \param pth Pth measurement
 * */
void Ensure_knowledge::set_Pth_meas(double pth)
{
    Pth_meas_last = pth;
    return;
}

/*! \brief setter for connection status
 *  \param conn connection status
 * */
void Ensure_knowledge::set_conn(bool conn)
{
    connected_last = conn;
    return;
}

/*! \brief setter for connection time
 *  \param t_conn connection time
 * */
void Ensure_knowledge::set_t_conn(double t_conn)
{
    t_connected_last = t_conn;
    return;
}

/*! \brief setter for voltage measurement
 *  \param v_meas voltage measurement
 * */
void Ensure_knowledge::set_v_meas(double v_meas)
{
    v_meas_last = v_meas;
    return;
}

/*! \brief setter for second voltage measurement
 *  \param v_meas voltage measurement
 * */
void Ensure_knowledge::set_v2_meas(double v_meas)
{
    v2_meas_last = v_meas;
    return;
}

/*! \brief setter for PQ set point
 *  \param p P set point
 *  \param q Q set point
 * */
void Ensure_knowledge::set_PQ_ctrl(double p, double q)
{
    P_ctrl_last = p;
    Q_ctrl_last = q;
    return;
}

/*! \brief setter for n set point
 *  \param n n set point
 * */
void Ensure_knowledge::set_n_ctrl(int n)
{
    n_ctrl_last = n;
    return;
}

/*! \brief setter for t last sent
 *  \param t time of last sending
 * */
void Ensure_knowledge::set_t_sent(double t)
{
    t_last_sent = t;
    return;
}

/*! \brief setter for t last received
 *  \param t time of last receiving
 * */
void Ensure_knowledge::set_t_recv(double t)
{
    t_last_recv = t;
    return;
}

/*! \brief getter for soc measurement
 *  \return soc measurement
 * */
double Ensure_knowledge::get_SOC_meas()
{
    return SOC_meas_last;
}

/*! \brief getter for PQ measurement
 *  \param p [out] P measurement
 *  \param q [out] Q measurement
 * */
void Ensure_knowledge::get_PQ_meas(double &p, double &q)
{
    p = P_meas_last;
    q = Q_meas_last;
    return;
}

/*! \brief getter for Pth measurement
 *  \return Pth measurement
 * */
double Ensure_knowledge::get_Pth_meas()
{
    return Pth_meas_last;
}

/*! \brief getter for connection status
 *  \return connection status
 * */
bool Ensure_knowledge::get_conn()
{
    return connected_last;
}

/*! \brief getter for connection time
 *  \return connection time
 * */
double Ensure_knowledge::get_t_conn()
{
    return t_connected_last;
}

/*! \brief getter for voltage measurement
 *  \return voltage measurement
 * */
double Ensure_knowledge::get_v_meas()
{
    return v_meas_last;
}

/*! \brief getter for second voltage measurement
 *  \return voltage measurement
 * */
double Ensure_knowledge::get_v2_meas()
{
    return v2_meas_last;
}

/*! \brief getter for PQ set point
 *  \param p [out] P set point
 *  \param q [out] Q set point
 * */
void Ensure_knowledge::get_PQ_ctrl(double &p, double &q)
{
    p = P_ctrl_last;
    q = Q_ctrl_last;
    return;
}

/*! \brief getter for n set point
 *  \return n set point
 * */
int Ensure_knowledge::get_n_ctrl()
{
    return n_ctrl_last;
}

/*! \brief getter for t last sent
 *  \return time of last sending
 * */
double Ensure_knowledge::get_t_sent()
{
    return t_last_sent;
}

/*! \brief getter for t last received
 *  \return time of last receiving
 * */
double Ensure_knowledge::get_t_recv()
{
    return t_last_recv;
}


/*! \brief logging of knowledge
 * */
void Ensure_knowledge::log_sensor_knowledge()
{
    std::stringstream temp;
    std::string output;

    temp << "\tKnowledge: " << std::endl;

    temp << "\t\tLast transmitted measurement values at " << t_last_sent << ": " << std::endl;
    temp << "\t\t\tSOC: " << SOC_meas_last << std::endl;
    temp << "\t\t\tP: " << P_meas_last << "W, Q: "<< Q_meas_last << "var" << std::endl;
    temp << "\t\t\tPth: " << Pth_meas_last << "W" << std::endl;
    temp << "\t\t\tconnected: " << connected_last << std::endl;
    temp << "\t\t\tt connected: " << t_connected_last << std::endl;
    temp << "\t\t\tVoltage: " << v_meas_last << "V" << std::endl;
    temp << "\t\t\tVoltage2: " << v2_meas_last << "V" << std::endl;

    temp << "\t\tLast received set points at " << t_last_recv << ": " << std::endl;
    temp << "\t\t\tP: " << P_ctrl_last << "W, Q: "<< Q_ctrl_last << "var" << std::endl;
    temp << "\t\t\tn: " << n_ctrl_last << std::endl;

    output = temp.str();
    IO->log_info(output);
    return;
}

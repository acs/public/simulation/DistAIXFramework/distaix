/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/ensure/ensure_message.h"
#include <sstream>

std::vector<Meta_infos> Ensure_msg::ensure_msg_meta = std::vector<Meta_infos>();
bool Ensure_msg::meta_initialized = false;

Ensure_msg::Ensure_msg() : Villas_message() {

    performative = nullptr;
    agent_id = nullptr;
    P = nullptr;
    Q = nullptr;
    n = nullptr;
    v = nullptr;
    v2 = nullptr;
    SOC = nullptr;
    connected = nullptr;
    t_connected = nullptr;

    init_message(0, 0, 0, 0.0, 0, 0.0, 0.0, 0.0, false, 0);

}

Ensure_msg::Ensure_msg(Villas_message &msg) : Villas_message(){
    data = msg.data;
    length = msg.length;

}


Ensure_msg::Ensure_msg(int _performative, int _agent_id) {
    performative = nullptr;
    agent_id = nullptr;
    P = nullptr;
    Q = nullptr;
    n = nullptr;
    v = nullptr;
    v2 = nullptr;
    SOC = nullptr;
    connected = nullptr;
    t_connected = nullptr;

    init_message(_performative, _agent_id, 0, 0.0, 0, 0.0, 0.0, 0.0, false, 0);
}

void Ensure_msg::set_pointers() {

    //set the pointer to the elements in data field
    //if you change the order or data types of data elements you have to adapt the indices here
    performative = &(data[0].i);
    agent_id = &(data[1].i);
    P = &(data[2].f);
    Q = &(data[3].f);
    n = &(data[4].i);
    v = &(data[5].f);
    v2 = &(data[6].f);
    SOC = &(data[7].f);
    connected = &(data[8].b);
    t_connected = &(data[9].i);
}

void Ensure_msg::init_message(int _performative, int _agent_id, double _P, double _Q, int _n, double _v, double _v2, double _SOC, bool _conn, int _t_conn) {

    //add all data and meta elements of this message type
    Data_element perf_data;
    perf_data.i = _performative;
    add_element(perf_data, VILLAS_DATA_TYPE_INT64);

    Data_element sender_data;
    sender_data.i = _agent_id;
    add_element(sender_data, VILLAS_DATA_TYPE_INT64);

    Data_element P_data;
    P_data.f = _P;
    add_element(P_data, VILLAS_DATA_TYPE_DOUBLE);

    Data_element Q_data;
    Q_data.f = _Q;
    add_element(Q_data, VILLAS_DATA_TYPE_DOUBLE);

    Data_element n_data;
    n_data.i = _n;
    add_element(n_data, VILLAS_DATA_TYPE_INT64);

    Data_element v_data;
    v_data.f = _v;
    add_element(v_data, VILLAS_DATA_TYPE_DOUBLE);

    Data_element v2_data;
    v2_data.f = _v2;
    add_element(v2_data, VILLAS_DATA_TYPE_DOUBLE);

    Data_element soc_data;
    soc_data.f = _SOC;
    add_element(soc_data, VILLAS_DATA_TYPE_DOUBLE);

    Data_element conn_data;
    conn_data.b = _conn;
    add_element(conn_data, VILLAS_DATA_TYPE_BOOLEAN);

    Data_element t_conn_data;
    t_conn_data.i = _t_conn;
    add_element(t_conn_data, VILLAS_DATA_TYPE_INT64);

    //set the pointers of the message to the corresponding fields in data
    set_pointers();
}


void Ensure_msg::init_meta() {
    // returns immediately if static meta information has already been initialized for this message type
    //initialize meta info only once
    if(!Ensure_msg::meta_initialized) {
        Ensure_msg::meta_initialized = true;
        Ensure_msg::ensure_msg_meta.clear();

        Meta_infos perf_meta;
        perf_meta.name = "performative";
        perf_meta.unit = "peformative of this message";
        perf_meta.type = VILLAS_DATA_TYPE_INT64;
        Ensure_msg::ensure_msg_meta.push_back(perf_meta);

        Meta_infos sender_meta;
        sender_meta.name = "agent_id";
        sender_meta.unit = "id of agent involved in this message";
        sender_meta.type = VILLAS_DATA_TYPE_INT64;
        Ensure_msg::ensure_msg_meta.push_back(sender_meta);

        Meta_infos P_meta;
        P_meta.name = "P";
        P_meta.unit =  "W";
        P_meta.type = VILLAS_DATA_TYPE_DOUBLE;
        Ensure_msg::ensure_msg_meta.push_back(P_meta);

        Meta_infos Q_meta;
        Q_meta.name = "Q";
        Q_meta.unit = "var";
        Q_meta.type = VILLAS_DATA_TYPE_DOUBLE;
        Ensure_msg::ensure_msg_meta.push_back(Q_meta);

        Meta_infos n_meta;
        n_meta.name = "n";
        n_meta.unit = "tap pos";
        n_meta.type = VILLAS_DATA_TYPE_INT64;
        Ensure_msg::ensure_msg_meta.push_back(n_meta);

        Meta_infos v_meta;
        v_meta.name = "v";
        v_meta.unit = "V";
        v_meta.type = VILLAS_DATA_TYPE_DOUBLE;
        Ensure_msg::ensure_msg_meta.push_back(v_meta);

        Meta_infos v2_meta;
        v2_meta.name = "v2";
        v2_meta.unit = "V";
        v2_meta.type = VILLAS_DATA_TYPE_DOUBLE;
        Ensure_msg::ensure_msg_meta.push_back(v2_meta);

        Meta_infos soc_meta;
        soc_meta.name = "SOC";
        soc_meta.unit = "state of charge";
        soc_meta.type = VILLAS_DATA_TYPE_DOUBLE;
        Ensure_msg::ensure_msg_meta.push_back(soc_meta);

        Meta_infos conn_meta;
        conn_meta.name = "connected";
        conn_meta.unit = "boolean";
        conn_meta.type = VILLAS_DATA_TYPE_BOOLEAN;
        Ensure_msg::ensure_msg_meta.push_back(conn_meta);

        Meta_infos t_conn_meta;
        t_conn_meta.name = "t_connected";
        t_conn_meta.unit = "s";
        t_conn_meta.type = VILLAS_DATA_TYPE_INT64;
        Ensure_msg::ensure_msg_meta.push_back(t_conn_meta);
    }

}

/*!
*   \brief Writes the content of a message to a string for debugging
*   \return Message content as string
* */
std::string Ensure_msg::get_message_output()
{
    std::stringstream temp;
    std::string output;

    temp    << "Time: " << time_sec
            << " Agent ID: " << *agent_id
            << " Performative: " << *performative
            << " P: " << *P
            << " Q: " << *Q
            << " n: " << *n
            << " v: " << *v
            << " v2: " << *v2
            << " SOC: " << *SOC
            << " connected: " << *connected
            << " t_connected: " << *t_connected;

    output = temp.str();
    return output;
}


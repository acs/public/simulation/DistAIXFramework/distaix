/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/ensure/ensure_slack_behavior.h"
#include "villas_interface/villas_interface.h"

/*! \brief Constructor
 *  \param _id [in]             RepastHPC agent id
 *  \param _type [in]           component type of the agent
 *  \param _subtype [in]        component subtype of the agent
 *  \param step_size [in]       size of a simulation step in seconds
 *  \param logging [in]         indicates if agents logs locally
 *  \param _prosumer_data [in]  pointer to prosumer specific datastruct
 * */
Ensure_slack_behavior::Ensure_slack_behavior(int _id, int _type, int _rank, std::string _subtype,
                                                       double& step_size, struct data_props _d_props, villas_node_config * _villas_config,
                                                       Slack_data * _slack_data)
        : Ensure_behavior(_id, _type, _rank, _subtype, step_size, _d_props, _villas_config),
          slack_data(_slack_data)
{
    knowledge.init(_id, IO);
}


/*! \brief adjusts control values according to simple rules not requiring communication
 * */
void Ensure_slack_behavior::execute_agent_behavior()
{
    /*Upon first execution: perform handshake with ENSURE Flexibility platform*/
    if (t_next == t_start){
        handshake();
        knowledge.set_t_sent(id%10); // offset for sending MQTT message
    }

    IO->log_info("###### Process incoming messages");
    process_incoming_messages();
    //IO->log_info("###### Publish voltage measurement");
    //publish_voltage_measurement();
    send_measurement_message();
    knowledge.log_sensor_knowledge();
}

void Ensure_slack_behavior::process_ensure_msg(Ensure_msg &msg)
{
    knowledge.set_t_recv(t_next);
    // no ensure msg should be received
    return;
}

void Ensure_slack_behavior::send_measurement_message()
{
    if (t_next - knowledge.get_t_sent() > 10) {
        Ensure_msg new_msg;
        new_msg.time_sec = t_next;
        *(new_msg.agent_id) = id;
        *(new_msg.performative) = FIPA_PERF_INFORM;
        *(new_msg.P) = slack_data->P;
        *(new_msg.Q) = slack_data->Q;

        IO->log_info("\tSend message: " + new_msg.get_message_output());
        send_villas_msg(new_msg);
        knowledge.set_t_sent(t_next);
    }
    return;
}

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/ref_intcomm/refic_substation_behavior.h"

/*! \brief Constructor used for creation of DF reference behavior
 *  \param _id [in]                 RepastHPC agent id
 *  \param _type [in]               component type of the agent
 *  \param _subtype [in]            component subtype of the agent
 *  \param step_size [in]           size of a simulation step in seconds
 *  \param logging [in]             indicates if agents logs locally
 *  \param _substation_data [in]    pointer to substation specific datastruct
 * */
Refic_substation_behavior::Refic_substation_behavior(int _id, int _type, std::string _subtype,
    double& step_size, struct data_props _d_props, Substation_data * _substation_data)
    : Agent_behavior(_id, _type, _subtype, step_size, _d_props),
    substation_data(_substation_data)
{
    IO->init_logging();
}

/*!
 * \brief Destroy the object (close log file)
 */
Refic_substation_behavior::~Refic_substation_behavior()
{
    IO->finish_io();
}

/*! \brief Initialize the agent behavior
 * \param components_ad_nodes [in] vector saving a list of connceted components for each node
 * \param connected_nodes [in] vector saving a list of connected nodes for each node
 * \param components_file [in] data contained in scenario components input file (integers)
 * \param subtypes_file [in] subtypes contained in scenario components input file (strings)
 * \param el_grid_file [in] electrical connections contained in el. grid input file (integers)
 * */
int Refic_substation_behavior::initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                                        std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                                        boost::multi_array<int, 2> *components_file,
                                                        boost::multi_array<std::string, 1> *subtypes_file,
                                                        boost::multi_array<int, 2> *el_grid_file) {
    return 0;
}

/*! \brief Substation reference behavior execution
 * */
void Refic_substation_behavior::execute_agent_behavior()
{
    apply_control_values();
}


/*! \brief applies control value for tap changer according to secondary node voltage
 * */
void Refic_substation_behavior::apply_control_values()
{
    /* adjust tap position according to voltage of secondary node */
    double v = sqrt(substation_data->v2_re * substation_data->v2_re +
        substation_data->v2_im * substation_data->v2_im) / (substation_data->Vnom2 / sqrt(3));
    // Define reasonable minimal interval for oltc switching, now 60 sek
    if (t_next >= substation_data->t_last_action + 60) {
        if (fabs(v) > 0.001) {
            if (v < 1) {
                /* voltage is smaller than 1 pu -> decrease tap position */
                double delta = ((1 - v) * 100) / (substation_data->range / substation_data->N);
                /* threshold for tap changing in order to avoid oscillations */
                if (delta > 0.7) {
                    substation_data->n_ctrl = substation_data->n - round(delta);
                }
                if (substation_data->n_ctrl < -substation_data->N) {
                    substation_data->n_ctrl = -substation_data->N;
                }
            } else {
                /* voltage is greater than 1 pu -> increase tap position */
                double delta = ((v - 1) * 100) / (substation_data->range / substation_data->N);
                /* threshold for tap changing in order to avoid oscillations */
                if (delta > 0.7) {
                    substation_data->n_ctrl = substation_data->n + round(delta);
                }
                if (substation_data->n_ctrl > substation_data->N) {
                    substation_data->n_ctrl = substation_data->N;
                }
            }

        }
        if (substation_data->n_ctrl != substation_data->n) {
            substation_data->t_last_action = t_next;
        }
    }

    IO->log_info("\tApplying control values\n\t\tn_ctrl: " +
        std::to_string(substation_data->n_ctrl));
}

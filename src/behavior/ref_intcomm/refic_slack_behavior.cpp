/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/ref_intcomm/refic_slack_behavior.h"

/*! \brief Constructor used for creation of DF reference behavior
 *  \param _id [in]             RepastHPC agent id
 *  \param _type [in]           component type of the agent
 *  \param _subtype [in]        component subtype of the agent
 *  \param step_size [in]       size of a simulation step in seconds
 *  \param logging [in]         indicates if agents logs locally
 *  \param _slack_data [in]     pointer to slack specific datastruct
 * */
Refic_slack_behavior::Refic_slack_behavior(int _id, int _type, std::string _subtype, double& step_size,
        struct data_props _d_props)
    : Agent_behavior(_id, _type, _subtype, step_size, _d_props)
{
    IO->init_logging();
}

/*!
 * \brief Destroy the object (close log file)
 */
Refic_slack_behavior::~Refic_slack_behavior()
{
    IO->finish_io();
}

/*! \brief Initialize the agent behavior
 * \param components_ad_nodes [in] vector saving a list of connceted components for each node
 * \param connected_nodes [in] vector saving a list of connected nodes for each node
 * \param components_file [in] data contained in scenario components input file (integers)
 * \param subtypes_file [in] subtypes contained in scenario components input file (strings)
 * \param el_grid_file [in] electrical connections contained in el. grid input file (integers)
 * */
int Refic_slack_behavior::initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                                   std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                                   boost::multi_array<int, 2> *components_file,
                                                   boost::multi_array<std::string, 1> *subtypes_file,
                                                   boost::multi_array<int, 2> *el_grid_file) {
    return 0;
}

/*! \brief Slack agent has nothing to do in reference behavior
 * */
void Refic_slack_behavior::execute_agent_behavior()
{
}

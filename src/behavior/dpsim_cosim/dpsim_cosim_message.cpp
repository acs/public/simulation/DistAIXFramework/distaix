/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/dpsim_cosim/dpsim_cosim_message.h"
#include <sstream>

std::vector<Meta_infos> Dpsim_cosim_msg::dpsim_cosim_message_meta = std::vector<Meta_infos>();
bool Dpsim_cosim_msg::meta_initialized = false;

Dpsim_cosim_msg::Dpsim_cosim_msg() : Villas_message() {
    value = nullptr;
    init_message(std::complex<float>(0.0,0.0));

}

Dpsim_cosim_msg::Dpsim_cosim_msg(Villas_message &msg) : Villas_message() {
    data = msg.data;
    length = msg.length;

}

void Dpsim_cosim_msg::set_pointers() {
    value = &(data[0].z);
}

void Dpsim_cosim_msg::init_message(std::complex<float> _value) {

    // add all data and meta elements of this message type
    Data_element value_data;
    value_data.z = _value;
    add_element(value_data, VILLAS_DATA_TYPE_COMPLEX);

    // set the pointers of the message to the corresponding fields in data
    set_pointers();

}

void Dpsim_cosim_msg::init_meta() {
    // returns immediately if static meta information has already been initialized for this message type
    // initialize meta info only once
    if(!Dpsim_cosim_msg::meta_initialized) {
        Dpsim_cosim_msg::meta_initialized = true;
        Dpsim_cosim_msg::dpsim_cosim_message_meta.clear();

        //Data values
        Meta_infos value_meta;
        value_meta.name = "value_complex";
        value_meta.unit = "value_complex of this message";
        value_meta.type = VILLAS_DATA_TYPE_COMPLEX;
        Dpsim_cosim_msg::dpsim_cosim_message_meta.push_back(value_meta);
    
    }
}

/*!
*   \brief Writes the content of a message to a string for debugging
*   \return Message content as string
* */

std::string Dpsim_cosim_msg::get_message_output() {
    std::stringstream temp;
    std::string output;

    temp    << "Time: " << time_sec << std::endl
            << " Value_Re: " << value->real() << std::endl
            << " Value_Im: " << value->imag();
    
    output = temp.str();
    return output;
}
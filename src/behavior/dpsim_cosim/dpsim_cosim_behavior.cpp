/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/dpsim_cosim/dpsim_cosim_behavior.h"
#include "villas_interface/villas_interface.h"

/*! \brief  constructor
 *  \param  _id         unique ID of agent
 *  \param  _type       agent type as defined in config.h
 *  \param  _subtype    subtype as defined in config.h
 *  \param  step_size   simulation step size
 *  \param  logging     indicates if agents logs locally
 * */

Dpsim_cosim_behavior::Dpsim_cosim_behavior(int _id, int _type, int _rank, std::string _subtype, double& step_size,
        struct data_props _d_props, villas_node_config * _villas_config)
    : Agent_behavior(_id, _type, _subtype, step_size, _d_props)
{
    vnconfig = _villas_config;
    IO->init_logging();
}

Dpsim_cosim_behavior::~Dpsim_cosim_behavior() {

    IO->log_info("Number of sent messages: " + std::to_string(numOfSentMessages));
    IO->log_info("Number of received messages: " + std::to_string(numOfReceivedMessages));
    //destroy_villas_interface is called in agent method
    IO->finish_io();
}

/*! \brief process all messages that have been received since the last time step and call protocol
 * specific process function
 * */

void Dpsim_cosim_behavior::process_incoming_messages() {
    std::list<Villas_message> incoming_villas_messages;

    villas_interface->receive_messages(incoming_villas_messages);

    while (!incoming_villas_messages.empty()) {
        Dpsim_cosim_msg dpsim_cosim_msg = (Dpsim_cosim_msg) incoming_villas_messages.front();
        // set pointers of data to members
        dpsim_cosim_msg.set_pointers();
        IO->log_info("\tReceived message: " + dpsim_cosim_msg.get_message_output());

        this->process_dpsim_cosim_msg(dpsim_cosim_msg);
        incoming_villas_messages.pop_front();
    }
}

/*!
 * \brief sending function for nanomsg messages
 * \param msg message to be sent
 */
void Dpsim_cosim_behavior::send_villas_msg(Villas_message &msg) {
    villas_interface->send_message(msg);
}

void Dpsim_cosim_behavior::create_endpoints(villas_node_config * _vnconfig, std::string agent_id) {
    
    std::list<std::string> communication_patterns;
    // Copy name of agent and remove "agent_"
    std::list<std::string> *_in_endpoints = &_vnconfig->type_config.nanomsg_conf->in_endpoints;
    std::list<std::string> *_out_endpoints = &_vnconfig->type_config.nanomsg_conf->out_endpoints;

    IO->log_info("In_endpoints:");
    for(auto i : *_in_endpoints)
        IO->log_info(i);

    IO->log_info("Out_endpoints:");
    for(auto i : *_out_endpoints)
        IO->log_info(i);


}

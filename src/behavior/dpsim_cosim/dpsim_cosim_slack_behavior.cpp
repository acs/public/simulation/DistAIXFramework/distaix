/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/dpsim_cosim/dpsim_cosim_slack_behavior.h"
#include "villas_interface/villas_interface.h"
#include <repast_hpc/RepastProcess.h>
#include <chrono>

/*! \brief Constructor used for creation of DF reference behavior
 *  \param _id [in]             RepastHPC agent id
 *  \param _type [in]           component type of the agent
 *  \param _subtype [in]        component subtype of the agent
 *  \param step_size [in]       size of a simulation step in seconds
 *  \param logging [in]         indicates if agents logs locally
 * */

Dpsim_cosim_slack_behavior::Dpsim_cosim_slack_behavior(int _id, int _type, int _rank, std::string _subtype, double& _step_size,
        struct data_props _d_props, villas_node_config * _villas_config, bool _realtime, bool _sync)
    : Dpsim_cosim_behavior(_id, _type, _rank, _subtype, step_size, _d_props, _villas_config)
{
    // this->sender = _id;
    // this->step_size = step_size;
    // this->realtime = _realtime;
    sender = _id;
    step_size = _step_size;
    realtime = _realtime;
    sync = _sync;

}


void Dpsim_cosim_slack_behavior::synchronize_with_remote(){

    bool remove_msg_overhead = false;

    IO->log_info("###### Start synchronization with remote...");
    Dpsim_cosim_msg out_msg, in_msg;
    std::list<Villas_message> incoming_villas_messages;

    IO->log_info("Waiting for answer...");
    busy_receive_msg(&incoming_villas_messages);
    send_msg(out_msg);

    // process incoming message
    if (!incoming_villas_messages.empty()) {
        if(incoming_villas_messages.size() > 1){
            IO->log_info("WARNING: More than one sync message received... ("
                + std::to_string(incoming_villas_messages.size()) + ")");
        }
        in_msg = (Dpsim_cosim_msg) incoming_villas_messages.front();
        // set pointer of data to members
        in_msg.set_pointers();

        incoming_villas_messages.pop_front();

        IO->log_info("Answer received...");
        IO->log_info("\tSync.in_re = " + std::to_string(in_msg.value->real()));
        IO->log_info("\tSync.in_im = " + std::to_string(in_msg.value->imag()));
    }
    else {
        IO->log_info("ERROR IN SYNCHRONIZE_WITH_REMOTE! BUSY WAITING RETURNED BUT NO RESPONSE COULD BE READ");
    }

    

    if (remove_msg_overhead) {
        IO->log_info("Additional removal of second synchronization message of dpsim triggered...");
        IO->log_info("Be careful with this option, as the removal of the CORRECT message cannot be guaranteed...");

        IO->log_info("Waiting for answer...");
        IO->log_info("Wait 5secs before reading socket again...");
        sleep(5);
        busy_receive_msg(&incoming_villas_messages);

        // process incoming message
        if (incoming_villas_messages.size() >= 1) {
            if(incoming_villas_messages.size() > 1){
                IO->log_info("WARNING: More than one sync message received... ("
                    + std::to_string(incoming_villas_messages.size()) + ")");
            }
            in_msg = (Dpsim_cosim_msg) incoming_villas_messages.front();
            // set pointer of data to members
            in_msg.set_pointers();

            incoming_villas_messages.pop_front();

            IO->log_info("Answer received...");
            IO->log_info("\tSync2.in_re = " + std::to_string(in_msg.value->real()));
            IO->log_info("\tSync2.in_im = " + std::to_string(in_msg.value->imag()));
        }
    }

}

/*! \brief Initialize the agent behavior
 * \param components_ad_nodes [in] vector saving a list of connceted components for each node
 * \param connected_nodes [in] vector saving a list of connected nodes for each node
 * \param components_file [in] data contained in scenario components input file (integers)
 * \param subtypes_file [in] subtypes contained in scenario components input file (strings)
 * \param el_grid_file [in] electrical connections contained in el. grid input file (integers)
 * */

int Dpsim_cosim_slack_behavior::initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                                   std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                                   boost::multi_array<int, 2> *components_file,
                                                   boost::multi_array<std::string, 1> *subtypes_file,
                                                   boost::multi_array<int, 2> *el_grid_file) {
    // initialize meta information for all message types used in this behavior
    Dpsim_cosim_msg::init_meta();
    if(vnconfig->type_name == "nanomsg"){
        IO->log_info("Create Endpoints...");
        create_endpoints(vnconfig, std::to_string(sender));
    }
    else if(vnconfig->type_name == "mqtt") {
        IO->log_info("Set connection parameters...");
        vnconfig->type_config.mqtt_conf->subscribe = "dpsim->dist";
        vnconfig->type_config.mqtt_conf->publish = "dist->dpsim";
    }
    else {
        IO->log_info("ERROR: '" + vnconfig->type_name + "' is no valid type for cosim behavior...");
    }

    // Initialize villas interface (important: after init of message meta info!)
    IO->log_info("Init VILLAS interface...");
    int ret = init_villas_interface(vnconfig, Dpsim_cosim_msg::dpsim_cosim_message_meta);
    if (ret) {
        return ret;
    }

    if(sync){
        synchronize_with_remote();
    }

    return 0;

    

}

/*! \brief Slack agent has nothing to do in reference behavior
 * */
void Dpsim_cosim_slack_behavior::execute_agent_behavior() {

}

void Dpsim_cosim_slack_behavior::report_to_superior_grid(Dpsim_cosim_msg message, std::list<Villas_message> *incoming_villas_messages) {

    IO->log_info("###### Execute dpsim_cosim behavior... ");
    send_msg(message);  

    if(realtime){
        IO->log_info("[REALTIME] Try to read new message...");
        receive_message(incoming_villas_messages);
    }
    else {
        IO->log_info("Wait until message arrives...");
        int ret = busy_receive_msg(incoming_villas_messages);
        if(ret == 1) {
            // early simulation stop signalized
            vnconfig->stop_at = repast::RepastProcess::instance()->getScheduleRunner().currentTick();
            std::cout << std::endl << "Cosimulation-timeout reached! Stopping simulation..." << std::endl;
        }
    }
}

void Dpsim_cosim_slack_behavior::send_msg(Dpsim_cosim_msg message) {
    numOfSentMessages++;

    IO->log_info("\tSend message: " + message.get_message_output());
    send_villas_msg(message);
}

// void Dpsim_cosim_slack_behavior::receive_msg() {
//     numOfReceivedMessages++;
//     IO->log_info("Reading message...");
//     std::list<Villas_message> incoming_villas_messages;
//     villas_interface->receive_messages(incoming_villas_messages);

//     while (!incoming_villas_messages.empty()) {
//         Dpsim_cosim_msg msg = (Dpsim_cosim_msg) incoming_villas_messages.front();
//         // set pointer of data to members
//         msg.set_pointers();
//         IO->log_info("\tReceived message: " + msg.get_message_output());

//         incoming_villas_messages.pop_front();
//     }
// }

void Dpsim_cosim_slack_behavior::receive_message(std::list<Villas_message> *incoming_villas_messages) {
    numOfReceivedMessages++;
    
    villas_interface->receive_messages(*incoming_villas_messages);
    if(!incoming_villas_messages->empty()) {
        IO->log_info("Message received..." + std::to_string(incoming_villas_messages->size()));
    }
}

int Dpsim_cosim_slack_behavior::busy_receive_msg(std::list<Villas_message> *incoming_villas_messages) {
    bool message_removal = true;
    numOfReceivedMessages++;
    // TODO: Do not cancel in first step!!...
    auto start = std::chrono::steady_clock::now();
    while(incoming_villas_messages->empty()) {
        // If timeout reached, return 1 to signalize early simulation stop
        if (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-start).count() >= timeout)
            return 1;

        villas_interface->receive_messages(*incoming_villas_messages);
    }
    if(!incoming_villas_messages->empty()) {
        IO->log_info("Message received...");
        
        if(incoming_villas_messages->size() > 1 && message_removal){
            IO->log_info("More than one message received!");

            auto it = incoming_villas_messages->begin();
            std::advance(it, incoming_villas_messages->size() - 1);
            incoming_villas_messages->erase(incoming_villas_messages->begin(), it); // Complexity linear with number of erased elements

            IO->log_info("Reduced down to: " + std::to_string(incoming_villas_messages->size()));
        }
    }

    // if(first_step)
    //     first_step = false;
    // auto stop = std::chrono::steady_clock::now();
    // IO->log_info("Time in busy_receive_msg() = " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(stop-start).count()));
    return 0;
}

void Dpsim_cosim_slack_behavior::process_dpsim_cosim_msg(Dpsim_cosim_msg &msg) {
    
}

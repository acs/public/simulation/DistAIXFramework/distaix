/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/mqtt_reference/mqtt_message.h"
#include <sstream>

std::vector<Meta_infos> Mqtt_ref_msg::mqtt_ref_msg_meta = std::vector<Meta_infos>();
bool Mqtt_ref_msg::meta_initialized = false;

Mqtt_ref_msg::Mqtt_ref_msg() : Villas_message() {

    performative = nullptr;
    sender = nullptr;
    receiver = nullptr;
    measurement_type = nullptr;
    measurement_value = nullptr;

    init_message(0,0,0,0,0.0);

}

Mqtt_ref_msg::Mqtt_ref_msg(Villas_message &msg) : Villas_message(){
    data = msg.data;
    length = msg.length;

}


Mqtt_ref_msg::Mqtt_ref_msg(int _performative, int _sender, int _receiver) {
    performative = nullptr;
    sender = nullptr;
    receiver = nullptr;
    measurement_type = nullptr;
    measurement_value = nullptr;

    init_message(_performative,_sender,_receiver,0,0.0);
}

void Mqtt_ref_msg::set_pointers() {

    //set the pointer to the elements in data field
    //if you change the order or data types of data elements you have to adapt the indices here
    performative = &(data[0].i);
    sender = &(data[1].i);
    receiver = &(data[2].i);
    measurement_type = &(data[3].i);
    measurement_value = &(data[4].f);
}

void Mqtt_ref_msg::init_message(int _performative, int _sender, int _receiver, int _measurement_type, double _measurement_value) {

    //add all data and meta elements of this message type
    Data_element perf_data;
    perf_data.i = _performative;
    add_element(perf_data, VILLAS_DATA_TYPE_INT64);

    Data_element sender_data;
    sender_data.i = _sender;
    add_element(sender_data, VILLAS_DATA_TYPE_INT64);

    Data_element receiver_data;
    receiver_data.i = _receiver;
    add_element(receiver_data, VILLAS_DATA_TYPE_INT64);

    Data_element measurement_type_data;
    measurement_type_data.i = _measurement_type;
    add_element(measurement_type_data, VILLAS_DATA_TYPE_INT64);

    Data_element measurement_value_data;
    measurement_value_data.f = _measurement_value;
    add_element(measurement_value_data, VILLAS_DATA_TYPE_DOUBLE);

    //set the pointers of the message to the corresponding fields in data
    set_pointers();
}


void Mqtt_ref_msg::init_meta() {
    // returns immediately if static meta information has already been initialized for this message type
    //initialize meta info only once
    if(!Mqtt_ref_msg::meta_initialized) {
        Mqtt_ref_msg::meta_initialized = true;
        Mqtt_ref_msg::mqtt_ref_msg_meta.clear();

        Meta_infos perf_meta;
        perf_meta.name = "performative";
        perf_meta.unit = "peformative of this message";
        perf_meta.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_ref_msg::mqtt_ref_msg_meta.push_back(perf_meta);

        Meta_infos sender_meta;
        sender_meta.name = "sender";
        sender_meta.unit = "sender of this message";
        sender_meta.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_ref_msg::mqtt_ref_msg_meta.push_back(sender_meta);

        Meta_infos receiver_meta;
        receiver_meta.name = "receiver";
        receiver_meta.unit = "receiver of this message";
        receiver_meta.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_ref_msg::mqtt_ref_msg_meta.push_back(receiver_meta);

        Meta_infos measurement_type_meta;
        measurement_type_meta.name = "measurement_type";
        measurement_type_meta.unit = "measurement_type of this message";
        measurement_type_meta.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_ref_msg::mqtt_ref_msg_meta.push_back(measurement_type_meta);

        Meta_infos measurement_value_meta;
        measurement_value_meta.name = "measurement_value";
        measurement_value_meta.unit = "measurement_value of this message";
        measurement_value_meta.type = VILLAS_DATA_TYPE_DOUBLE;
        Mqtt_ref_msg::mqtt_ref_msg_meta.push_back(measurement_value_meta);

    }

}

/*!
*   \brief Writes the content of a message to a string for debugging
*   \return Message content as string
* */
std::string Mqtt_ref_msg::get_message_output()
{
    std::stringstream temp;
    std::string output;

    temp    << "Time: " << time_sec
            << " Sender: " << *sender
            << " Receiver: " << *receiver
            << " Performative: " << *performative
            << " Type: " << *measurement_type
            << " Value: " << *measurement_value;

    output = temp.str();
    return output;
}
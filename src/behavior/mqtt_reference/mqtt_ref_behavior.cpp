/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/mqtt_reference/mqtt_ref_behavior.h"
#include "villas_interface/villas_interface.h"

/*! \brief  constructor
 *  \param  _id         unique ID of agent
 *  \param  _type       agent type as defined in config.h
 *  \param  _subtype    subtype as defined in config.h
 *  \param  step_size   simulation step size
 *  \param  logging     indicates if agents logs locally
 * */
Mqtt_ref_behavior::Mqtt_ref_behavior(int _id, int _type, int _rank, std::string _subtype, double& step_size,
        struct data_props _d_props, villas_node_config * _villas_config)
    : Agent_behavior(_id, _type, _subtype, step_size, _d_props)
{

    vnconfig = _villas_config;

    if(_type == TYPE_DF_INT) {
        std::string path = "runlog/behavior/agent_DF" + std::to_string(_rank) + ".log";
        std::string path_empty = "";
        IO->update_file_paths(path, path_empty);
    }

    IO->init_logging();
}

Mqtt_ref_behavior::~Mqtt_ref_behavior(){
    //destroy_villas_interface is called in agent method
    IO->finish_io();
}


/*! \brief Initialize the agent behavior
 * \param components_ad_nodes [in] vector saving a list of connceted components for each node
 * \param connected_nodes [in] vector saving a list of connected nodes for each node
 * \param components_file [in] data contained in scenario components input file (integers)
 * \param subtypes_file [in] subtypes contained in scenario components input file (strings)
 * \param el_grid_file [in] electrical connections contained in el. grid input file (integers)
 * */
int Mqtt_ref_behavior::initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                                             std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                                             boost::multi_array<int, 2> *components_file,
                                                             boost::multi_array<std::string, 1> *subtypes_file,
                                                             boost::multi_array<int, 2> *el_grid_file) {
    //set connection parameters
    set_connection_params(components_at_nodes);

    //initialize meta information for all message types used in this behavior
    Mqtt_ref_msg::init_meta();

    //Initialize villas interface (important: after init of message meta info!)
    int ret = init_villas_interface(vnconfig, Mqtt_ref_msg::mqtt_ref_msg_meta);
    return ret;
}



/*! \brief process all messages that have been received since the last time step and call protocol
 * specific process function
 * */
void Mqtt_ref_behavior::process_incoming_messages()
{
    std::list<Villas_message> incoming_villas_messages;

    villas_interface->receive_messages(incoming_villas_messages);

    while (!incoming_villas_messages.empty()) {
        Mqtt_ref_msg mqtt_msg = (Mqtt_ref_msg) incoming_villas_messages.front();
        //set pointers of data to members
        mqtt_msg.set_pointers();
        IO->log_info("\tReceived message: " + mqtt_msg.get_message_output());

        this->process_mqtt_ref_msg(mqtt_msg);
        incoming_villas_messages.pop_front();
    }
}


/*!
 * \brief sending function for mqtt messages
 * \param msg message to be sent
 */
void Mqtt_ref_behavior::send_villas_msg(Villas_message &msg)
{
    villas_interface->send_message(msg);
}


/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/mqtt_reference/mqtt_ref_substation_behavior.h"
#include "villas_interface/villas_interface.h"

/*! \brief Constructor
 *  \param _id [in]                 RepastHPC agent id
 *  \param _type [in]               component type of the agent
 *  \param _subtype [in]            component subtype of the agent
 *  \param step_size [in]           size of a simulation step in seconds
 *  \param logging [in]             indicates if agents logs locally
 *  \param _substation_data [in]    pointer to substation specific datastruct
 * */
Mqtt_ref_substation_behavior::Mqtt_ref_substation_behavior(int _id, int _type, int _rank, std::string _subtype,
    double& step_size, struct data_props _d_props, villas_node_config * _villas_config,
    Substation_data * _substation_data)
    : Mqtt_ref_behavior(_id, _type, _rank, _subtype, step_size, _d_props, _villas_config),
    substation_data(_substation_data)
{
    voltage_measurements.clear();
}

/*! \brief set connection parameters for this behavior
 * \param components_at_nodes provides overview on components connected to nodes
 * */
void Mqtt_ref_substation_behavior::set_connection_params(
        std::vector<std::list<std::tuple<int, int, int>>> *components_at_nodes) {
    //substations subscribe to voltage measurements and publish nothing
    vnconfig->type_config.mqtt_conf->subscribe = "voltage_measurements_trafo"+std::to_string(id);
    vnconfig->type_config.mqtt_conf->publish = "";
}

/*! \brief Substation reference behavior execution
 * */
void Mqtt_ref_substation_behavior::execute_agent_behavior()
{
    process_incoming_messages();
    apply_control_values();
}

void Mqtt_ref_substation_behavior::process_mqtt_ref_msg(Mqtt_ref_msg &msg)
{
    int agent_id = *msg.sender;
    double voltage = *msg.measurement_value;
    bool update_measurment = false;

    for (auto &i : voltage_measurements) {
        if (std::get<0>(i) == agent_id) {
            i = std::tuple<int, double,bool>(agent_id, voltage, true);
            update_measurment = true;
            break;
        }
    }
    if (!update_measurment) {
        voltage_measurements.push_back(std::tuple<int, double,bool>(agent_id, voltage, false));
    }
}


/*! \brief applies control value for tap changer according to secondary node voltage
 * */
void Mqtt_ref_substation_behavior::apply_control_values()
{
    double v_mean = 0.0;
    for (auto &i : voltage_measurements) {
        v_mean += std::get<1>(i);
    }
    if (voltage_measurements.size() > 0) {
        v_mean = v_mean / voltage_measurements.size();
    } else {
        v_mean = 1.0;
    }
    /* adjust tap position according to voltage of secondary node */
    //double v = sqrt(substation_data->v2_re * substation_data->v2_re +
    //    substation_data->v2_im * substation_data->v2_im) / (substation_data->Vnom2 / sqrt(3));
    IO->log_info("Mean voltage: " + std::to_string(v_mean));
    // Define reasonable minimal interval for oltc switching, now 60 sek
    if (t_next >= substation_data->t_last_action + 60) {
        if (fabs(v_mean) > 0.001) {
            if (v_mean < 1) {
                /* voltage is smaller than 1 pu -> decrease tap position */
                double delta = ((1 - v_mean) * 100) / (substation_data->range / substation_data->N);
                /* threshold for tap changing in order to avoid oscillations */
                if (delta > 0.7) {
                    substation_data->n_ctrl = substation_data->n - round(delta);
                }
                if (substation_data->n_ctrl < -substation_data->N) {
                    substation_data->n_ctrl = -substation_data->N;
                }
            } else {
                /* voltage is greater than 1 pu -> increase tap position */
                double delta = ((v_mean - 1) * 100) / (substation_data->range / substation_data->N);
                /* threshold for tap changing in order to avoid oscillations */
                if (delta > 0.7) {
                    substation_data->n_ctrl = substation_data->n + round(delta);
                }
                if (substation_data->n_ctrl > substation_data->N) {
                    substation_data->n_ctrl = substation_data->N;
                }
            }

        }
        if (substation_data->n_ctrl != substation_data->n) {
            substation_data->t_last_action = t_next;
        }
    }

    IO->log_info("\tApplying control values\n\t\tn_ctrl: " +
        std::to_string(substation_data->n_ctrl));
}

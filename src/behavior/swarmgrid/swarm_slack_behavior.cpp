/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/swarmgrid/swarm_slack_behavior.h"
#include <vector>


/*! \brief  constructor
 *  \param _id              unique ID of agent
 *  \param _type            agent type as defined in config.h
 *  \param _subtype         subtype as defined in config.h
 *  \param step_size        simulation step size
 *  \param logging          indicates if agents logs locally
 *  \param _slack_data      pointer to type specific data struct
 * */
Swarm_slack_behavior::Swarm_slack_behavior(int _id, int _type, std::string _subtype, 
    double& step_size, struct data_props _d_props, Slack_data * _slack_data)
    : Swarm_negotiator_behavior(_id, _type, _subtype, step_size, _d_props),
        slack_data(_slack_data)
{
}

/*!
 * \brief Destroy the object (close log file)
 */
Swarm_slack_behavior::~Swarm_slack_behavior()
{
    IO->finish_io();
}

/*! \brief Initialize the agent behavior
 *  \param components_ad_nodes [in] vector saving a list of connceted components for each node
 *  \param connected_nodes [in] vector saving a list of connected nodes for each node
 *  \param components_file [in] data contained in scenario components input file (integers)
 *  \param subtypes_file [in] subtypes contained in scenario components input file (strings)
 *  \param el_grid_file [in] electrical connections contained in el. grid input file (integers)
 * */
int Swarm_slack_behavior::initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                                     std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                                     boost::multi_array<int, 2> *components_file,
                                                     boost::multi_array<std::string, 1> *subtypes_file,
                                                     boost::multi_array<int, 2> *el_grid_file) {

    IO->log_info("### Determining initial communication partners ");
    /* adjacent nodes should be only transformers */
    std::list<std::tuple<int,int,int>> node_adjacent = connected_nodes->at(id-1);

    for (auto u : node_adjacent) {
        int node_id = std::get<0>(u);
        int node_type = std::get<1>(u);
        if (node_type == TYPE_TRANSFORMER_INT) {
            IO->log_info("Adding trafo " + std::to_string(node_id) + " to internal agents");
            knowledge_comm.add_internal(node_id);
        }
        else if (node_type == TYPE_NODE_INT) {
            /* in IFHT case there is one node between slack and trafo -> also check adjacent nodes of this node */
            std::list<std::tuple<int,int,int>> node_adjacent_to_node =
                    connected_nodes->at(node_id-1);
            for (auto n : node_adjacent_to_node) {
                int adj_node_id = std::get<0>(n);
                int adj_node_type = std::get<1>(n);
                if (adj_node_type == TYPE_TRANSFORMER_INT) {
                    IO->log_info("Adding trafo " + std::to_string(adj_node_id)
                                 + " to internal agents");
                    knowledge_comm.add_internal(adj_node_id);
                }
            }
        }
    }
    IO->log_info("### FINISHED Determining initial communication partners ");

    return 0;
}

/*! \brief processes messages, executes behavior rules, adjusts control values
 * */
void Swarm_slack_behavior::execute_agent_behavior()
{
    if (t_next == t_start) {
        IO->log_info("Initial actions");
        initial_actions();
        //slack_data->Q_target = true; // einkommentieren für bestimmtes Q target
        //slack_data->Q_optimal = 100000000; //einkommentieren für maximales Q
        //slack_data->Q_max = 0;
        //slack_data->Q_min = 0;
    }
    /*if (t_next < 7000) {
        slack_data->Q_min = 0;
        slack_data->Q_optimal = 2000000; //setzen von zielwert für Q
    }
    else if (t_next >= 7000 && t_next < 11000) {
        slack_data->Q_min = 0;
        slack_data->Q_optimal = 100000000;
    }
    else {
        slack_data->Q_min = 0;
        slack_data->Q_optimal = 1000000;
    }*/

    IO->log_info("Determine operation range");
    determine_operation_range();

    IO->log_info("Process incoming messages");
    process_incoming_messages();

    IO->log_info("Determine state");
    determine_state(NEGOTIATIONTYPE_P_INT);
    determine_state(NEGOTIATIONTYPE_Q_INT);

    IO->log_info("Cover power demand");
    if (knowledge_neg_internal_p.get_state() == BASETYPE_CONSUMER_INT) {
        cover_power_demand(&knowledge_neg_internal_p);
    }
    if (knowledge_neg_internal_q.get_state() == BASETYPE_CONSUMER_INT) {
        cover_power_demand(&knowledge_neg_internal_q);
    }

    IO->log_info("Release unused power demand");
    release_contracted_power(&knowledge_neg_internal_p, BASETYPE_CONSUMER_INT, 
        get_threshold(&knowledge_neg_internal_p));
    release_contracted_power(&knowledge_neg_internal_q, BASETYPE_CONSUMER_INT, 
        get_threshold(&knowledge_neg_internal_q));

    IO->log_info("Adapt contracts to generation");
    release_contracted_power(&knowledge_neg_internal_p, BASETYPE_PRODUCER_INT, 
        get_threshold(&knowledge_neg_internal_p));
    release_contracted_power(&knowledge_neg_internal_q, BASETYPE_PRODUCER_INT, 
        get_threshold(&knowledge_neg_internal_q));

    IO->log_info("Update services");
    update_services(&knowledge_neg_internal_p);
    update_services(&knowledge_neg_internal_q);

    IO->log_info("Determine power correction");
    determine_power_correction();

    knowledge_comm.log_communication_knowledge();
    knowledge_neg_internal_p.log_negotiation_knowledge();
    knowledge_neg_internal_q.log_negotiation_knowledge();
}


/*! \brief executes action in the first time step
 * */
void Swarm_slack_behavior::initial_actions()
{
    /* subscribe to all internal agents */
    std::vector<int> _agents;
    knowledge_comm.get_internal_agents(_agents);
    swarm_subscribe_msg new_msg(FIPA_PERF_SUBSCRIBE, id, 0);
    new_msg.dist = 0;
    for (auto i : _agents) {
        new_msg.service.svc_type = NEGOTIATIONTYPE_P_INT;
        new_msg.receiver = i;
        send_subscribe_msg(new_msg);
        new_msg.service.svc_type = NEGOTIATIONTYPE_Q_INT;
        send_subscribe_msg(new_msg);
    }
    struct service_ex _service;
    _service.svc_type = NEGOTIATIONTYPE_P_INT;
    _service.needed_max = 1000000000;
    _service.optional_max = 0;
    knowledge_neg_internal_p.new_producer_service(id, _service);
    _service.svc_type = NEGOTIATIONTYPE_Q_INT;
    knowledge_neg_internal_q.new_producer_service(id, _service);
}


/*! \brief determine the limits and optimal operation
*/
void Swarm_slack_behavior::determine_operation_range()
{
    IO->log_info("\t\tP_min: " + std::to_string(slack_data->P_min-slack_data->P_corr) +
        ", P_optimal: " + std::to_string(slack_data->P_optimal-slack_data->P_corr) + ", P_max: " +
        std::to_string(slack_data->P_max-slack_data->P_corr));
    IO->log_info("\t\tQ_min: " + std::to_string(slack_data->Q_min-slack_data->Q_corr) +
        ", Q_optimal: " + std::to_string(slack_data->Q_optimal-slack_data->Q_corr) + ", Q_max: " +
        std::to_string(slack_data->Q_max-slack_data->Q_corr));
}


/*! \brief checks whether state has to be changed from producer to consumer
 *  \param  neg_type    active or reactive power
 * */
void Swarm_slack_behavior::determine_state(int neg_type)
{
    int state;
    double power_min;
    double power_max;
    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        state = knowledge_neg_internal_p.get_state();
        power_min = slack_data->P_min;
        power_max = slack_data->P_max;
        if (slack_data->P_target) {
            power_min -= slack_data->P_corr;
            power_max -= slack_data->P_corr;
        }
    }
    else {
        state = knowledge_neg_internal_q.get_state();
        power_min = slack_data->Q_min;
        power_max = slack_data->Q_max;
        if (slack_data->Q_target) {
            power_min -= slack_data->Q_corr;
            power_max -= slack_data->Q_corr;
        }
    }
    struct contract_ex power_sum = get_power_sum(neg_type, GROUPTYPE_SUBSTATION_INT);
    struct contract_ex open_power_sum = get_open_power_sum(neg_type, GROUPTYPE_SUBSTATION_INT);

    /* slack becomes consumer if there are no producer contracts */
    if (state == BASETYPE_PRODUCER_INT) {
        if (fabs(power_sum.needed + power_sum.optional + open_power_sum.needed +
                open_power_sum.optional) < NEGOTIATION_THRESHOLD_FACTOR * slack_data->Vnom) {
            change_state(neg_type, BASETYPE_CONSUMER_INT);
            state = BASETYPE_CONSUMER_INT;
        }
    }
    if (state == BASETYPE_CONSUMER_INT) {
        /* agent cannot operate as consumser due to operation range */
        if (power_max < -NEGOTIATION_THRESHOLD_FACTOR * slack_data->Vnom) {
            change_state(neg_type, BASETYPE_PRODUCER_INT);
            state = BASETYPE_PRODUCER_INT;
        }
    }
    else {
        /* agent cannot operate as producer due to operation range */
        if (power_min > NEGOTIATION_THRESHOLD_FACTOR * slack_data->Vnom) {
            change_state(neg_type, BASETYPE_CONSUMER_INT);
            state = BASETYPE_CONSUMER_INT;
        }
    }

    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        IO->log_info("\t\tActive power state: " + std::to_string(state));
    }
    else {
        IO->log_info("\t\tReactive power state: " + std::to_string(state));
    }
}


/*! \brief get the missing power for specified group
 *  \param  know_neg    affected negotiation knowledge
 *  \return power lack
 * */
contract_ex Swarm_slack_behavior::get_power_lack(Negotiation_knowledge * know_neg)
{
    struct contract_ex power_sum = get_power_sum(know_neg->get_negotiation_type(),
        know_neg->get_group_type());
    struct contract_ex open_power_sum = get_open_power_sum(know_neg->get_negotiation_type(), 
        know_neg->get_group_type());
    std::vector<std::pair<int, struct service_ex>> internal_services;

    /* determine the amount of power that is missing */
    struct contract_ex power_lack;
    power_lack.neg_type = know_neg->get_negotiation_type();
    double power_min;
    double power_optimal;
    double power_max;
    if (power_lack.neg_type == NEGOTIATIONTYPE_P_INT) {
        power_min = slack_data->P_min;
        power_optimal = slack_data->P_optimal;
        power_max = slack_data->P_max;
        if (slack_data->P_target) {
            power_min -= slack_data->P_corr;
            power_optimal -= slack_data->P_corr;
            power_max -= slack_data->P_corr;
        }
    }
    else {
        power_min = slack_data->Q_min;
        power_optimal = slack_data->Q_optimal;
        power_max = slack_data->Q_max;
        if (slack_data->Q_target) {
            power_min -= slack_data->Q_corr;
            power_optimal -= slack_data->Q_corr;
            power_max -= slack_data->Q_corr;
        }
    }

    /* needed up to optimal */
    power_lack.needed = power_optimal -
        (power_sum.needed + open_power_sum.needed + power_sum.optional + open_power_sum.optional);
    power_lack.optional = 0;

    know_neg->get_older_producer_services(internal_services, t_next);
    for (unsigned int i = 0; i < internal_services.size(); i++) {
        /* if more producer minimum services are available take them as optional */
        if (!know_neg->open_contract_exists(internal_services[i].first, id) 
            && internal_services[i].second.minimum > NEGOTIATION_THRESHOLD_FACTOR*slack_data->Vnom) {
            power_lack.optional += internal_services[i].second.minimum;
        }
    }
    if (power_lack.needed > 0) {
        power_lack.optional -= power_lack.needed;
    }

    return power_lack;
}


/*! \brief  consumer power exceed
 *  \param  know_neg    affected negotiation knowlegde
 *  \return consumer power exceed
 * */
contract_ex Swarm_slack_behavior::get_consumer_power_exceed(Negotiation_knowledge * know_neg)
{
    struct contract_ex power_sum = get_power_sum(know_neg->get_negotiation_type(),
        know_neg->get_group_type());
    std::vector<std::pair<int, struct service_ex>> internal_services;

    /* determine the difference between contracted and needed power */
    struct contract_ex power_exceed;
    power_exceed.neg_type = know_neg->get_negotiation_type();
    double power_min;
    double power_optimal;
    double power_max;
    if (power_exceed.neg_type == NEGOTIATIONTYPE_P_INT) {
        power_min = slack_data->P_min;
        power_optimal = slack_data->P_optimal;
        power_max = slack_data->P_max;
        if (slack_data->P_target) {
            power_min -= slack_data->P_corr;
            power_optimal -= slack_data->P_corr;
            power_max -= slack_data->P_corr;
        }
    }
    else {
        power_min = slack_data->Q_min;
        power_optimal = slack_data->Q_optimal;
        power_max = slack_data->Q_max;
        if (slack_data->Q_target) {
            power_min -= slack_data->Q_corr;
            power_optimal -= slack_data->Q_corr;
            power_max -= slack_data->Q_corr;
        }
    }

    /* needed up to optimal */
    power_exceed.needed = (power_sum.needed + power_sum.optional) - power_optimal;
    power_exceed.optional = 0;

    return power_exceed;
}


/*! \brief  producer power exceed
 *  \param  know_neg    affected negotiation knowlegde
 *  \return producer power exceed
 * */
contract_ex Swarm_slack_behavior::get_producer_power_exceed(Negotiation_knowledge * know_neg)
{
    struct contract_ex power_sum = get_power_sum(know_neg->get_negotiation_type(),
        know_neg->get_group_type());

    /* determine the difference between contracted and needed power */
    struct contract_ex power_exceed;
    power_exceed.neg_type = know_neg->get_negotiation_type();
    double power_min;
    double power_optimal;
    double power_max;
    if (power_exceed.neg_type == NEGOTIATIONTYPE_P_INT) {
        power_min = slack_data->P_min;
        power_optimal = slack_data->P_optimal;
        power_max = slack_data->P_max;
        if (slack_data->P_target) {
            power_min -= slack_data->P_corr;
            power_optimal -= slack_data->P_corr;
            power_max -= slack_data->P_corr;
        }
    }
    else {
        power_min = slack_data->Q_min;
        power_optimal = slack_data->Q_optimal;
        power_max = slack_data->Q_max;
        if (slack_data->Q_target) {
            power_min -= slack_data->Q_corr;
            power_optimal -= slack_data->Q_corr;
            power_max -= slack_data->Q_corr;
        }
    }
    
    if (know_neg->get_state() == BASETYPE_PRODUCER_INT) {
        /* up to min */
        power_exceed.needed = power_min - power_sum.needed;
        if (power_exceed.needed > 0) {
            power_exceed.optional = -power_sum.optional;
        }
        else {
            power_exceed.optional = power_optimal - (power_sum.needed + power_sum.optional);
        }
    }
    else {
        /* as a consumer get rid of all producer contracts */
        power_exceed.needed = -power_sum.needed;
        power_exceed.optional = -power_sum.optional;
    }

    return power_exceed;
}


/*! \brief  recalculate prodcuer service
 *  \param  know_neg    affected negotiation knowlegde
 *  \return new service
 * */
service_ex Swarm_slack_behavior::get_new_service(Negotiation_knowledge * know_neg)
{
    struct contract_ex power_sum = get_power_sum(know_neg->get_negotiation_type(),
        know_neg->get_group_type());
    struct contract_ex open_power_sum = get_open_power_sum(know_neg->get_negotiation_type(), 
        know_neg->get_group_type());

    struct service_ex service_new;
    service_new.svc_type = know_neg->get_negotiation_type();
    service_new.t_last_update = t_next;

    double power_min;
    double power_optimal;
    double power_max;
    if (service_new.svc_type == NEGOTIATIONTYPE_P_INT) {
        power_min = slack_data->P_min;
        power_optimal = slack_data->P_optimal;
        power_max = slack_data->P_max;
        if (slack_data->P_target) {
            power_min -= slack_data->P_corr;
            power_optimal -= slack_data->P_corr;
            power_max -= slack_data->P_corr;
        }
    }
    else {
        power_min = slack_data->Q_min;
        power_optimal = slack_data->Q_optimal;
        power_max = slack_data->Q_max;
        if (slack_data->Q_target) {
            power_min -= slack_data->Q_corr;
            power_optimal -= slack_data->Q_corr;
            power_max -= slack_data->Q_corr;
        }
    }

    /* needed up to min, optional up to optimal, minimum up to max */
    if (know_neg->get_state() == BASETYPE_PRODUCER_INT) {
        service_new.needed_max = (power_sum.needed + open_power_sum.needed) - power_min;
        service_new.optional_max = (power_sum.needed + open_power_sum.needed + power_sum.optional +
            open_power_sum.optional) - power_optimal;
        service_new.minimum = (power_sum.needed + open_power_sum.needed + power_sum.optional +
            open_power_sum.optional) - power_max;
    }
    else {
        service_new.needed_max = - power_min;
        service_new.optional_max = - power_optimal;
        service_new.minimum = - power_max;

        if (service_new.svc_type == NEGOTIATIONTYPE_Q_INT && slack_data->Q_target) {
            if (power_sum.needed + power_sum.optional > power_optimal) {
                service_new.optional_max = power_sum.needed + power_sum.optional - power_optimal;
                service_new.needed_max = power_sum.needed + power_sum.optional - power_optimal;
            }
        }
    }

    return service_new;
}


/*! \brief  calculate difference between contracts and power flow
 * */
void Swarm_slack_behavior::determine_power_correction()
{
    struct contract_ex power_sum = get_power_sum(NEGOTIATIONTYPE_P_INT, GROUPTYPE_INTERNAL_INT);
    struct contract_ex open_power_sum = get_open_power_sum(NEGOTIATIONTYPE_P_INT,
        GROUPTYPE_INTERNAL_INT);
    double p_miss = slack_data->P -
        (power_sum.needed + power_sum.optional + open_power_sum.needed + open_power_sum.optional);

    power_sum = get_power_sum(NEGOTIATIONTYPE_Q_INT, GROUPTYPE_INTERNAL_INT);
    open_power_sum = get_open_power_sum(NEGOTIATIONTYPE_Q_INT, GROUPTYPE_INTERNAL_INT);
    double q_miss = slack_data->Q -
        (power_sum.needed + power_sum.optional + open_power_sum.needed + open_power_sum.optional);

    knowledge_sens.add_loss_sample(p_miss, q_miss);
    slack_data->P_corr = knowledge_sens.get_p_loss();
    slack_data->Q_corr = knowledge_sens.get_q_loss();

    IO->log_info("\t\tPower correction: " + std::to_string(slack_data->P_corr) + "W + j"
        + std::to_string(slack_data->Q_corr) + "var");
}


/*! \brief  get current size of swarm (not required for slack)
 *  \param  neg_type    active or reactive power
 *  \return swarm size
 * */
unsigned int Swarm_slack_behavior::get_swarm_size(int neg_type)
{
    return 0;
}


/*! \brief  set current size of swarm (not required for slack)
 *  \param  neg_type    active or reactive power
 *  \param  swarm_size  swarm size
 * */
void Swarm_slack_behavior::set_swarm_size(int neg_type, int swarm_size)
{
}


/*! \brief  get threshold for contract negotiation
 *  \param  neg_type    affected negotiation knowledge
 *  \return threshold
 * */
double Swarm_slack_behavior::get_threshold(Negotiation_knowledge * know_neg)
{
    return slack_data->Vnom*NEGOTIATION_THRESHOLD_FACTOR;
}


/*! \brief  change the set point for voltage (not required for slack)
 * */
void Swarm_slack_behavior::change_setpoint(double setpoint)
{
}

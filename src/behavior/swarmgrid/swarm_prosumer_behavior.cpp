/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/swarmgrid/swarm_prosumer_behavior.h"
#include <string>


/*! \brief  constructor
 *  \param _id              unique ID of agent
 *  \param _type            agent type as defined in config.h
 *  \param _subtype         subtype as defined in config.h
 *  \param step_size        simulation step size
 *  \param logging          indicates if agents logs locally
 *  \param _prosumer_data   pointer to prosumer specific data struct
 * */
Swarm_prosumer_behavior::Swarm_prosumer_behavior(int _id, int _type, std::string _subtype,
    double& step_size, struct data_props _d_props, Prosumer_data * _prosumer_data)
    : Swarm_negotiator_behavior(_id, _type, _subtype, step_size, _d_props),
    prosumer_data(_prosumer_data)
{
}

/*!
 * \brief Destroy the object (close log file)
 */
Swarm_prosumer_behavior::~Swarm_prosumer_behavior()
{
    IO->finish_io();
}

/*! \brief Initialize the agent behavior
 * \param components_ad_nodes [in] vector saving a list of connceted components for each node
 * \param connected_nodes [in] vector saving a list of connected nodes for each node
 * \param components_file [in] data contained in scenario components input file (integers)
 * \param subtypes_file [in] subtypes contained in scenario components input file (strings)
 * \param el_grid_file [in] electrical connections contained in el. grid input file (integers)
 * */
int Swarm_prosumer_behavior::initialize_agent_behavior(
        std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
        std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
        boost::multi_array<int, 2> *components_file,
        boost::multi_array<std::string, 1> *subtypes_file,
        boost::multi_array<int, 2> *el_grid_file) {

    IO->log_info("### Determining initial communication partners ");
    std::list<std::tuple<int,int,int>> node_adjacent = connected_nodes->at(prosumer_data->node_id-1);
    std::list<std::tuple<int,int,int>> node_components = components_at_nodes->at
            (prosumer_data->node_id-1);

    IO->log_info("There are " + std::to_string(node_components.size())
                 + " components connected to this node " + std::to_string(prosumer_data->node_id));

    //set internal agents
    for(auto &comp : node_components){
        int comp_ID = std::get<0>(comp);
        if(comp_ID != id){
            //if not own ID, add a component connected to the same node as internal agent
            IO->log_info("Adding agent (ID, type) (" + std::to_string(comp_ID) + ","
                    + std::to_string(std::get<1>(comp)) + ") to internal agents of this agent");
            knowledge_comm.add_internal(std::get<0>(comp));
        }
    }


    //add trafo of this node to neighborhood swarm:
    int trafo_id = std::get<2>(node_components.front()); // node->get_transformer();
    IO->log_info("Adding transformer " + std::to_string(trafo_id) +
                 " to neighborhood and swarm agents of this agent");
    knowledge_comm.add_neighbor(trafo_id);
    knowledge_comm.add_swarmmember(trafo_id, TYPE_TRANSFORMER_INT, 1);


    // determine neighborhood swarm with respect to this node
    for (auto node_adj : node_adjacent) {
        int adj_node_id=std::get<0>(node_adj);
        int adj_node_type = std::get<1>(node_adj);

        if (adj_node_type == TYPE_NODE_INT) {
            /*add components connected to adjacent node to neighborhood swarm*/
            std::list<std::tuple<int, int, int>> components =
                    components_at_nodes->at(adj_node_id - 1);
            for (auto comp : components) {
                int comp_ID = std::get<0>(comp);
                int comp_type = std::get<1>(comp);
                IO->log_info("Adding component agent (ID,type) (" + std::to_string(comp_ID) + ","
                             + std::to_string(comp_type)+
                             ") to neighborhood and swarm agents of this agent");
                knowledge_comm.add_neighbor(trafo_id);
                knowledge_comm.add_swarmmember(trafo_id, TYPE_TRANSFORMER_INT, 1);

            }

            std::list<std::tuple<int,int,int>> conn = connected_nodes->at(adj_node_id-1);

            for(auto conn_node : conn){
                //check all connected nodes
                int conn_node_id = std::get<0>(conn_node);
                int conn_node_type = std::get<1>(conn_node);
                int conn_node_trafo_id = std::get<2>(conn_node);
                if(conn_node_type == TYPE_TRANSFORMER_INT && conn_node_trafo_id == trafo_id){
                    //if connected node is trafo and tafo id of this trafo equals id of current node
                    // type set to zero to avoid confusion in behavior
                    IO->log_info("Adding trafo agent " + std::to_string(conn_node_id)+
                                 " to neighborhood and swarm agents of this agent");
                    knowledge_comm.add_neighbor(conn_node_id);
                    knowledge_comm.add_swarmmember(conn_node_id, 0, 1);
                }
            }


        } else if (adj_node_type == TYPE_TRANSFORMER_INT) {
            //check if ID of trafo is not own trafo id of this node
            if (trafo_id != adj_node_id) {
                // Trafo to lower voltage level, add to neighborhood swarm
                IO->log_info("Adding transformer " + std::to_string(adj_node_id) +
                             " to neighborhood and swarm agents of this agent");
                knowledge_comm.add_neighbor(adj_node_id);
                knowledge_comm.add_swarmmember(adj_node_id, adj_node_type, 1);
            }
        }
    }

    IO->log_info("Adding DF to neighborhood and swarm agents of this agent");
    knowledge_comm.add_neighbor(TYPE_DF_INT);
    knowledge_comm.add_swarmmember(TYPE_DF_INT, TYPE_DF_INT, 1);

    IO->log_info("### FINISHED Determining initial communication partners ");

    return 0;

}

/*! \brief processes messages, executes behavior rules, adjusts control values
 * */
void Swarm_prosumer_behavior::execute_agent_behavior()
{
    /* execute initial actions at the beginning of first time step */
    if (t_next == t_start) {
        IO->log_info("Initial actions");
        initial_actions();
    }
    /* actions performed every time step */

    save_voltage_measurement();

    IO->log_info("Determine operation range");
    determine_operation_range();

    process_incoming_messages();

    IO->log_info("Determine state");
    determine_state(NEGOTIATIONTYPE_P_INT);
    determine_state(NEGOTIATIONTYPE_Q_INT);

    IO->log_info("Cover power demand");
    if (knowledge_neg_internal_p.get_state() == BASETYPE_CONSUMER_INT) {
        cover_power_demand(&knowledge_neg_internal_p);
        cover_power_demand(&knowledge_neg_swarm_p);
        cover_power_demand(&knowledge_neg_substation_p);
    }
    if (knowledge_neg_internal_q.get_state() == BASETYPE_CONSUMER_INT) {
        cover_power_demand(&knowledge_neg_internal_q);
        cover_power_demand(&knowledge_neg_swarm_q);
        cover_power_demand(&knowledge_neg_substation_q);
    }

    IO->log_info("Release unused power");
    release_contracted_power(&knowledge_neg_internal_p, BASETYPE_CONSUMER_INT, 
        get_threshold(&knowledge_neg_internal_p));
    release_contracted_power(&knowledge_neg_swarm_p, BASETYPE_CONSUMER_INT,
        get_threshold(&knowledge_neg_swarm_p));
    release_contracted_power(&knowledge_neg_substation_p, BASETYPE_CONSUMER_INT, 
        get_threshold(&knowledge_neg_substation_p));
    release_contracted_power(&knowledge_neg_internal_q, BASETYPE_CONSUMER_INT, 
        get_threshold(&knowledge_neg_internal_q));
    release_contracted_power(&knowledge_neg_swarm_q, BASETYPE_CONSUMER_INT,
        get_threshold(&knowledge_neg_swarm_q));
    release_contracted_power(&knowledge_neg_substation_q, BASETYPE_CONSUMER_INT, 
        get_threshold(&knowledge_neg_substation_q));

    IO->log_info("Adapt contracts to generation");
    release_contracted_power(&knowledge_neg_internal_p, BASETYPE_PRODUCER_INT, 
        get_threshold(&knowledge_neg_internal_p));
    release_contracted_power(&knowledge_neg_swarm_p, BASETYPE_PRODUCER_INT,
        get_threshold(&knowledge_neg_swarm_p));
    release_contracted_power(&knowledge_neg_substation_p, BASETYPE_PRODUCER_INT, 
        get_threshold(&knowledge_neg_substation_p));
    release_contracted_power(&knowledge_neg_internal_q, BASETYPE_PRODUCER_INT, 
        get_threshold(&knowledge_neg_internal_q));
    release_contracted_power(&knowledge_neg_swarm_q, BASETYPE_PRODUCER_INT,
        get_threshold(&knowledge_neg_swarm_q));
    release_contracted_power(&knowledge_neg_substation_q, BASETYPE_PRODUCER_INT, 
        get_threshold(&knowledge_neg_substation_q));

    IO->log_info("Update services");
    update_services(&knowledge_neg_internal_p);
    update_services(&knowledge_neg_swarm_p);
    update_services(&knowledge_neg_substation_p);
    update_services(&knowledge_neg_internal_q);
    update_services(&knowledge_neg_swarm_q);
    update_services(&knowledge_neg_substation_q);

    IO->log_info("Determine swarm size");
    determine_swarm_size(NEGOTIATIONTYPE_P_INT);
    determine_swarm_size(NEGOTIATIONTYPE_Q_INT);

    IO->log_info("Set point:" + std::to_string(prosumer_data->v_setpoint));

    knowledge_comm.log_communication_knowledge();
    knowledge_neg_internal_p.log_negotiation_knowledge();
    knowledge_neg_swarm_p.log_negotiation_knowledge();
    knowledge_neg_substation_p.log_negotiation_knowledge();
    knowledge_neg_internal_q.log_negotiation_knowledge();
    knowledge_neg_swarm_q.log_negotiation_knowledge();
    knowledge_neg_substation_q.log_negotiation_knowledge();
    knowledge_sens.log_sensor_knowledge();

    apply_control_values();
}


/*! \brief applies control values according to agent's contracts
 * */
void Swarm_prosumer_behavior::apply_control_values() 
{
    /* active power */
    struct contract_ex power_sum = get_power_sum(NEGOTIATIONTYPE_P_INT, GROUPTYPE_SUBSTATION_INT);
    struct contract_ex open_power_sum = get_open_power_sum(NEGOTIATIONTYPE_P_INT,
        GROUPTYPE_SUBSTATION_INT);

    if (knowledge_neg_internal_p.get_state() == BASETYPE_CONSUMER_INT) {
        /* as a consumer apply concluded contractes for needed and optional power */
        prosumer_data->P_ctrl = power_sum.needed + power_sum.optional;
        if (prosumer_data->P_ctrl < prosumer_data->P_min) {
            prosumer_data->P_ctrl = prosumer_data->P_min;
        }
    }
    else {
        /* as a producer consider concluded and open contracts for needed power and only concluded 
         * contracts for optional power */
        prosumer_data->P_ctrl = power_sum.needed + power_sum.optional + open_power_sum.needed;
        if (prosumer_data->P_ctrl > prosumer_data->P_max) {
            prosumer_data->P_ctrl = prosumer_data->P_max;
        }
    }

    /* reactive power */
    power_sum = get_power_sum(NEGOTIATIONTYPE_Q_INT, GROUPTYPE_SUBSTATION_INT);
    open_power_sum = get_open_power_sum(NEGOTIATIONTYPE_Q_INT, GROUPTYPE_SUBSTATION_INT);
    double q_a = get_q_available(prosumer_data->P_ctrl);

    if (knowledge_neg_internal_q.get_state() == BASETYPE_CONSUMER_INT) {
        /* as a consumer apply concluded contractes for needed and optional power */
        double q_ctrl_temp = power_sum.needed + power_sum.optional;
        if (q_ctrl_temp < -q_a) {
            q_ctrl_temp = -q_a;
        }
        prosumer_data->Q_ctrl = q_ctrl_temp;
        // TODO check if q is adjustable for hp
        if (type == TYPE_COMPENSATOR_INT) {
            prosumer_data->n_ctrl = 0;
        }
    }
    else {
        /* as a producer consider concluded and open contracts for needed power and only concluded 
         * contracts for optional power */
        double q_ctrl_temp = power_sum.needed + power_sum.optional + open_power_sum.needed;
        if (q_ctrl_temp > q_a) {
            q_ctrl_temp = q_a;
        }
        prosumer_data->Q_ctrl = q_ctrl_temp;
        // TODO check if q is adjustable for hp
        if (type == TYPE_COMPENSATOR_INT) {
            /* compensator has discrete steps */
            prosumer_data->n_ctrl = abs(round(prosumer_data->Q_r / prosumer_data->Q_ctrl));
            if (prosumer_data->n_ctrl > prosumer_data->N) {
                prosumer_data->n_ctrl = prosumer_data->N;
            }
        }
    }

    IO->log_info("Applying control values\n\t\tP_ctrl: " + std::to_string(prosumer_data->P_ctrl) +
        "W Q_ctrl: " + std::to_string(prosumer_data->Q_ctrl) + "var");
}


/*! \brief executes action in the first time step (subscribing and initializing services)
 * */
void Swarm_prosumer_behavior::initial_actions()
{
    /* subscribe for active power */
    if (type == TYPE_LOAD_INT || type == TYPE_EV_INT || type == TYPE_BATTERY_INT
            || type == TYPE_HP_INT) {
        /* subscribe to all internal and swarm agents */
        swarm_subscribe_msg new_msg(FIPA_PERF_SUBSCRIBE, id, 0);
        new_msg.dist = 0;
        new_msg.service.svc_type = NEGOTIATIONTYPE_P_INT;

        std::vector<int> _agents;
        knowledge_comm.get_internal_agents(_agents);
        for (auto i : _agents) {
            new_msg.receiver = i;
            send_subscribe_msg(new_msg);
        }

        std::vector<std::pair<int, unsigned int>> _agents_;
        knowledge_comm.get_swarm_agents(_agents_);
        for (auto i : _agents_) {
            new_msg.receiver = i.first;
            new_msg.dist = i.second;
            send_subscribe_msg(new_msg);
        }
    }

    /* subscribe for reactive power */
    if (type == TYPE_LOAD_INT) {
        /* subscribe to all internal and swarm agents */
        swarm_subscribe_msg new_msg(FIPA_PERF_SUBSCRIBE, id, 0);
        new_msg.dist = 0;
        new_msg.service.svc_type = NEGOTIATIONTYPE_Q_INT;

        std::vector<int> _agents;
        knowledge_comm.get_internal_agents(_agents);
        for (auto i : _agents) {
            new_msg.receiver = i;
            send_subscribe_msg(new_msg);
        }
        
        std::vector<std::pair<int, unsigned int>> _agents_;
        knowledge_comm.get_swarm_agents(_agents_);
        for (auto i : _agents_) {
            new_msg.receiver = i.first;
            new_msg.dist = i.second;
            send_subscribe_msg(new_msg);
        }
    }

    /* store own active power supply as service */
    if (type == TYPE_PV_INT || type == TYPE_WEC_INT || type == TYPE_BIOFUEL_INT
            || type == TYPE_BATTERY_INT || type == TYPE_CHP_INT) {
        struct service_ex _service;
        knowledge_neg_internal_p.new_producer_service(id, _service);
        knowledge_neg_swarm_p.new_producer_service(id, _service);
        knowledge_neg_substation_p.new_producer_service(id, _service);
    }

    /* store own reactive power supply as service */
    if (type == TYPE_PV_INT || type == TYPE_WEC_INT || type == TYPE_BIOFUEL_INT
            || type == TYPE_BATTERY_INT || type == TYPE_EV_INT || type == TYPE_CHP_INT) {
        struct service_ex _service;
        _service.svc_type = NEGOTIATIONTYPE_Q_INT;
        knowledge_neg_internal_q.new_producer_service(id, _service);
        knowledge_neg_swarm_q.new_producer_service(id, _service);
        knowledge_neg_substation_q.new_producer_service(id, _service);
    }
}


/*! \brief determine the limits and optimal operation
*/
void Swarm_prosumer_behavior::determine_operation_range()
{
    /* determine the normalized voltage */
    double v_norm = knowledge_sens.get_absolute_voltage(id);
    double v_old = knowledge_sens.get_old_voltage_meas();
    IO->log_info("\tv_norm: " + std::to_string(v_norm));

    if (type == TYPE_LOAD_INT) {
        determine_operation_range_load();
    }
    else if (type == TYPE_PV_INT || type == TYPE_WEC_INT) {
        determine_operation_range_renewable(v_norm, v_old);
    }
    else if (type == TYPE_BATTERY_INT) {
        determine_operation_range_battery(v_norm, v_old);
    }
    else if (type == TYPE_EV_INT) {
        determine_operation_range_ev(v_norm, v_old);
    }
    else if (type == TYPE_HP_INT) {
        determine_operation_range_hp(v_norm, v_old);
    }
    else if (type == TYPE_CHP_INT) {
        determine_operation_range_chp(v_norm, v_old);
    }
    else if (type == TYPE_BIOFUEL_INT) {
        determine_operation_range_bio(v_norm, v_old);
    }
    else if (type == TYPE_COMPENSATOR_INT) {
        determine_operation_range_compensator(v_norm, v_old);
    }

    IO->log_info("\tP_min: " + std::to_string(prosumer_data->P_min) + ", P_optimal: " +
        std::to_string(prosumer_data->P_optimal) + ", P_max: " +
        std::to_string(prosumer_data->P_max));
    IO->log_info("\tQ_min: " + std::to_string(prosumer_data->Q_min) + ", Q_optimal: " +
        std::to_string(prosumer_data->Q_optimal) + ", Q_max: " +
        std::to_string(prosumer_data->Q_max));
}

/*! \brief determine the limits and optimal operation for loads
*/
void Swarm_prosumer_behavior::determine_operation_range_load()
{
    /* no flexibility */
    prosumer_data->P_max = prosumer_data->P_dem;
    prosumer_data->P_optimal = prosumer_data->P_dem;
    prosumer_data->P_min = prosumer_data->P_dem;
    prosumer_data->Q_max = prosumer_data->Q_dem;
    prosumer_data->Q_optimal = prosumer_data->Q_dem;
    prosumer_data->Q_min = prosumer_data->Q_dem;
    return;
}


/*! \brief determine the limits and optimal operation for renewables
*/
void Swarm_prosumer_behavior::determine_operation_range_renewable(double v_norm, double v_old)
{
    prosumer_data->P_max = -prosumer_data->P_gen;
    prosumer_data->P_optimal = -prosumer_data->P_gen;
    prosumer_data->P_min = -prosumer_data->P_gen;
    determine_q_operation_range(v_norm, v_old);
    return;
}


/*! \brief determine the limits and optimal operation for batteries
*/
void Swarm_prosumer_behavior::determine_operation_range_battery(double v_norm, double v_old)
{
    double p_opt_old = prosumer_data->P_optimal;
    /* calculate P_max such that storage can not be overcharged */
    prosumer_data->P_max = (1 - prosumer_data->SOC_el) * prosumer_data->C_el * 3600 / t_step;
    if (prosumer_data->P_max > prosumer_data->P_nom) {
        prosumer_data->P_max = prosumer_data->P_nom;
    }
    /* calculate P_min such that not more energy than stored can be supplied */
    prosumer_data->P_min = -prosumer_data->SOC_el * prosumer_data->C_el * 3600 / t_step;
    struct service_ex int_svc = get_service_sum(NEGOTIATIONTYPE_P_INT, GROUPTYPE_INTERNAL_INT);
    if (int_svc.needed_max > prosumer_data->P_nom) {
        /* if other service is available at same node set P_min to zero (do not offer power) */
        prosumer_data->P_min = 0;
    }
    if (prosumer_data->P_min < -prosumer_data->P_nom) {
        prosumer_data->P_min = -prosumer_data->P_nom;
    }
    if (v_norm < prosumer_data->v_setpoint - 0.05) {
        /* voltage is low -> behave as producer */
        prosumer_data->P_max = 0;
        /* decrease P_opt towards -P_nom at 0.9 */
        prosumer_data->P_optimal = -prosumer_data->P_nom + ((v_norm - 0.9) / ((prosumer_data->v_setpoint -0.05) - 0.9)) * prosumer_data->P_nom;
        if (prosumer_data->P_optimal > p_opt_old) {
            /* hysteresis to prevent oscillations */
            prosumer_data->P_optimal = p_opt_old;
        }
    }
    else if (v_norm < prosumer_data->v_setpoint - 0.025) {
        /* voltage is low -> behave as producer */
        prosumer_data->P_max = 0;
        prosumer_data->P_optimal = 0;
        if (prosumer_data->P_optimal > p_opt_old) {
            /* hysteresis to prevent oscillations */
            prosumer_data->P_optimal = p_opt_old;
        }
    }
    else if (v_norm < prosumer_data->v_setpoint + 0.025) {
        /* voltage is ideal -> storage can behave as both, producer or consumer */
        prosumer_data->P_optimal = 0;
    }
    else if (v_norm < prosumer_data->v_setpoint + 0.05) {
        /* voltage is high -> behave as consumer */
        prosumer_data->P_min = 0;
        prosumer_data->P_optimal = 0;
        if (prosumer_data->P_optimal < p_opt_old) {
            /* hysteresis to prevent oscillations */
            prosumer_data->P_optimal = p_opt_old;
        }
    }
    else {
        /* voltage is high -> behave as consumer */
        prosumer_data->P_min = 0;
        /* increase P_opt towards P_nom at 1.1 */
        prosumer_data->P_optimal = prosumer_data->P_nom - ((1.1 - v_norm) / (1.1 - (prosumer_data->v_setpoint + 0.05))) * prosumer_data->P_nom;
        if (prosumer_data->P_optimal < p_opt_old) {
            /* hysteresis to prevent oscillations */
            prosumer_data->P_optimal = p_opt_old;
        }
    }
    /* make sure power is within the limits */
    if (prosumer_data->P_max > prosumer_data->P_nom) {
        prosumer_data->P_max = prosumer_data->P_nom;
    }
    if (prosumer_data->P_min < -prosumer_data->P_nom) {
        prosumer_data->P_min = -prosumer_data->P_nom;
    }
    if (prosumer_data->P_optimal < prosumer_data->P_min) {
        prosumer_data->P_optimal = prosumer_data->P_min;
    }
    else if (prosumer_data->P_optimal > prosumer_data->P_max) {
        prosumer_data->P_optimal = prosumer_data->P_max;
    }

    determine_q_operation_range(v_norm, v_old);
    return;
}


/*! \brief determine the limits and optimal operation for evs
*/
void Swarm_prosumer_behavior::determine_operation_range_ev(double v_norm, double v_old)
{
    if (prosumer_data->profile1_id != -1) { //flexible EV
        if (prosumer_data->t_connected) {
            /* calculate P_max such that storage can not be overcharged */
            double E_lack = (1 - prosumer_data->SOC_el) * prosumer_data->C_el;
            prosumer_data->P_max = E_lack * 3600 / t_step;
            if (prosumer_data->P_max > prosumer_data->P_nom) {
                prosumer_data->P_max = prosumer_data->P_nom;
            }
            double soc_min = 1.0;
            double soc_opt = 0.8;
            double p_min_old = prosumer_data->P_min;
            double p_opt_old = prosumer_data->P_optimal;
            if (v_norm < prosumer_data->v_setpoint - 0.075) {
                /* voltage is low -> reduce soc_min towards 0.5 at 0.9 */
                soc_min = 0.2 + ((v_norm - 0.9) / ((prosumer_data->v_setpoint - 0.075) - 0.9)) * 0.4;
                E_lack = (soc_min - prosumer_data->SOC_el) * prosumer_data->C_el;
                prosumer_data->P_min = E_lack * 3600 / prosumer_data->t_connected;
                if (prosumer_data->P_min > p_min_old) {
                    /* hysteresis to prevent oscillations */
                    prosumer_data->P_min = p_min_old;
                }
                /* reduce soc_opt towards 0 at 0.9 */
                soc_opt = ((v_norm - 0.9) / ((prosumer_data->v_setpoint - 0.075) - 0.9)) * 0.4;
                E_lack = (soc_opt - prosumer_data->SOC_el) * prosumer_data->C_el;
                prosumer_data->P_optimal = E_lack * 3600 / t_step;
                if (prosumer_data->P_optimal > p_opt_old) {
                    /* hysteresis to prevent oscillations */
                    prosumer_data->P_optimal = p_opt_old;
                }
            }
            else if (v_norm < prosumer_data->v_setpoint - 0.05) {
                soc_min = 0.6 +
                    ((v_norm - (prosumer_data->v_setpoint - 0.075)) /
                    ((prosumer_data->v_setpoint - 0.05) - (prosumer_data->v_setpoint - 0.075))) * 0.4 ;
                E_lack = (soc_min - prosumer_data->SOC_el) * prosumer_data->C_el;
                prosumer_data->P_min = E_lack * 3600 / prosumer_data->t_connected;
                if (prosumer_data->P_min > p_min_old) {
                    /* hysteresis to prevent oscillations */
                    prosumer_data->P_min = p_min_old;
                }
                /* voltage is low -> reduce soc_opt towards 0.4 at 0.925 (v_set-0.075) */
                soc_opt = 0.4 +
                    ((v_norm - (prosumer_data->v_setpoint - 0.075)) /
                    ((prosumer_data->v_setpoint - 0.05) - (prosumer_data->v_setpoint - 0.075))) * 0.4;
                E_lack = (soc_opt - prosumer_data->SOC_el) * prosumer_data->C_el;
                prosumer_data->P_optimal = E_lack * 3600 / t_step;
                if (prosumer_data->P_optimal > p_opt_old) {
                    /* hysteresis to prevent oscillations */
                    prosumer_data->P_optimal = p_opt_old;
                }
            }
            else if (v_norm < prosumer_data->v_setpoint - 0.025) {
                /* voltage ok -> soc_min = 1 and soc_opt = 0.8 */
                soc_min = 1.0;
                E_lack = (soc_min - prosumer_data->SOC_el) * prosumer_data->C_el;
                prosumer_data->P_min = E_lack * 3600 / prosumer_data->t_connected;
                if (prosumer_data->P_min < p_min_old) {
                    prosumer_data->P_min = p_min_old;
                }
                
                soc_opt = 0.8;
                E_lack = (soc_opt - prosumer_data->SOC_el) * prosumer_data->C_el;
                prosumer_data->P_optimal = E_lack * 3600 / t_step;
                if (prosumer_data->P_optimal > p_opt_old) {
                    /* hysteresis to prevent oscillations */
                    prosumer_data->P_optimal = p_opt_old;
                }
            }
            else {
                /* voltage ok -> soc_min = 1 and soc_opt = 0.8 */
                soc_min = 1.0;
                E_lack = (soc_min - prosumer_data->SOC_el) * prosumer_data->C_el;
                prosumer_data->P_min = E_lack * 3600 / prosumer_data->t_connected;
                double p_min_temp = 0.0;
                double t_earlier = prosumer_data->t_connected - 3600 * 2;
                if (t_earlier > 3600 * 2) {
                    p_min_temp = E_lack * 3600 / t_earlier;
                }
                else {
                    p_min_temp = 2*prosumer_data->P_min;
                }
                if (prosumer_data->P_min < p_min_temp) {
                    prosumer_data->P_min = p_min_temp;
                }
                if (prosumer_data->P_min < p_min_old) {
                    prosumer_data->P_min = p_min_old;
                }

                soc_opt = 0.8;
                E_lack = (soc_opt - prosumer_data->SOC_el) * prosumer_data->C_el;
                prosumer_data->P_optimal = E_lack * 3600 / t_step;
            }
            /* make sure power is within the limits */
            if (prosumer_data->P_min < 0) {
                prosumer_data->P_min = 0;
            }
            else if (prosumer_data->P_min > prosumer_data->P_max) {
                prosumer_data->P_min = prosumer_data->P_max;
            }
            if (prosumer_data->P_optimal < prosumer_data->P_min) {
                prosumer_data->P_optimal = prosumer_data->P_min;
            }
            else if (prosumer_data->P_optimal > prosumer_data->P_max) {
                prosumer_data->P_optimal = prosumer_data->P_max;
            }
        }
        else {
            prosumer_data->P_min = 0;
            prosumer_data->P_optimal = 0;
            prosumer_data->P_max = 0;
        }
    }
    else { // non-flexible EV
        prosumer_data->P_max = prosumer_data->P_dem;
        prosumer_data->P_optimal = prosumer_data->P_dem;
        prosumer_data->P_min = prosumer_data->P_dem;
    }

    determine_q_operation_range(v_norm, v_old);
    return;
}


/*! \brief determine the limits and optimal operation for hps
*/
void Swarm_prosumer_behavior::determine_operation_range_hp(double v_norm, double v_old)
{
    double p_opt_old = prosumer_data->P_optimal;
    double p_min_old = prosumer_data->P_min;
    prosumer_data->P_max = prosumer_data->P_nom;
    prosumer_data->P_min = 0;

    /* check if secondary heater is required to cover thermal demand */
    if (prosumer_data->P_th_dem > (prosumer_data->P_nom / prosumer_data->f_el +
            (prosumer_data->E_th * 3600 / t_step))) {
        /* HP has to run at P_nom -> no flexibility */
        prosumer_data->P_min = prosumer_data->P_nom;
        prosumer_data->P_max = prosumer_data->P_nom;
        prosumer_data->P_optimal = prosumer_data->P_nom;
    }
    else {
        /* HP has flexibility, secondary heater not needed */
        if ((prosumer_data->P_nom / prosumer_data->f_el - prosumer_data->P_th_dem)*t_step >
            3600 * (prosumer_data->C_th - prosumer_data->E_th)) {
            /* if there is space in the storage for what can additionally be thermally generated
                * by the HP Pmax is thermal demand coverage + rest that fits in thermal storage */
            prosumer_data->P_max = (prosumer_data->P_th_dem +
                3600 * (prosumer_data->C_th - prosumer_data->E_th) / t_step) * prosumer_data->f_el;
        }
        if (prosumer_data->P_th_dem * t_step > prosumer_data->E_th * 3600) {
            /* thermal demand is larger than stored thermal energy */
            prosumer_data->P_min = (prosumer_data->P_th_dem - prosumer_data->E_th * 3600 / t_step) *
                prosumer_data->f_el;
        }
        if (prosumer_data->P_min > prosumer_data->P_max) {
            prosumer_data->P_min = prosumer_data->P_max;
        }
        if (v_norm < prosumer_data->v_setpoint - 0.05) {
            /* voltage is very low -> P_opt = 0 */
            prosumer_data->P_optimal = 0;
        }
        else if (v_norm < prosumer_data->v_setpoint - 0.025) {
            /* hysteresis to prevent oscillations */
            if (prosumer_data->SOC_th > 0.95) {
                /* P_opt = 0 if soc above 0.95 */
                prosumer_data->P_optimal = 0;
            }
            else {
                prosumer_data->P_optimal = p_opt_old;
                //if (prosumer_data->P_min < p_min_old) {
                //    prosumer_data->P_min = p_min_old;
                //}
            }
        }
        //else if (v_norm < prosumer_data->v_setpoint) {
        //    /* voltage is ok */
        //    if (prosumer_data->SOC_th < 0.25) {
        //        /* start charging storage is soc < 0.25 */
        //        prosumer_data->P_optimal = prosumer_data->P_nom;
        //    }
        //    else {
        //        /* P_opt = 0 if soc not below 0.25 */
        //        prosumer_data->P_optimal = 0;
        //    }
        //    if (std::fabs(p_opt_old - prosumer_data->P_nom) < 0.001) {
        //        /* continue charging if it has started once */
        //        prosumer_data->P_optimal = p_opt_old;
        //    }
        //    if (p_min_old > prosumer_data->P_min) {
        //        prosumer_data->P_min = p_min_old;
        //    }
        //}
        else {
            /* voltage is good */
            if (prosumer_data->SOC_th < 0.25) {
                /* start charging storage is soc < 0.25 */
                //prosumer_data->P_optimal = prosumer_data->P_nom;
                prosumer_data->P_optimal = (1 - ((1.1 - v_norm)/(1.1 - (prosumer_data->v_setpoint-0.025)))) * prosumer_data->P_nom + prosumer_data->P_min;
                if (prosumer_data->P_optimal < p_opt_old) {
                    prosumer_data->P_optimal = p_opt_old;
                }
                //if (prosumer_data->P_min < prosumer_data->P_optimal / 2) {
                //    prosumer_data->P_min = prosumer_data->P_optimal / 2;
                //}
            }
            else if (prosumer_data->SOC_th > 0.95) {
                /* P_opt = 0 if soc above 0.95 */
                prosumer_data->P_optimal = 0;
            }
            else {
                prosumer_data->P_optimal = p_opt_old;
                //if (prosumer_data->P_min < p_min_old) {
                //    prosumer_data->P_min = p_min_old;
                //}
            }
            //if (std::fabs(p_opt_old - prosumer_data->P_nom) < 0.001) {
            //    /* continue charging if it has started once */
            //    prosumer_data->P_optimal = p_opt_old;
                //if (prosumer_data->P_min < prosumer_data->P_nom / 2) {
                //    prosumer_data->P_min = prosumer_data->P_nom / 2;
                //}
            //}
        }
        /* make sure optimal power is within the limits */
        if (prosumer_data->P_min > prosumer_data->P_max) {
            prosumer_data->P_min = prosumer_data->P_max;
        }
        if (prosumer_data->P_optimal < prosumer_data->P_min) {
            prosumer_data->P_optimal = prosumer_data->P_min;
        }
        else if (prosumer_data->P_optimal > prosumer_data->P_max) {
            prosumer_data->P_optimal = prosumer_data->P_max;
        }
    }

    // TODO check reactive power behavior of HP
    prosumer_data->Q_max = 0;
    prosumer_data->Q_optimal = 0;
    prosumer_data->Q_min = 0;
    return;
}


/*! \brief determine the limits and optimal operation for chps
*/
void Swarm_prosumer_behavior::determine_operation_range_chp(double v_norm, double v_old)
{
    double p_opt_old = prosumer_data->P_optimal;
    double p_min_old = prosumer_data->P_min;
    prosumer_data->P_min = -prosumer_data->P_nom;
    prosumer_data->P_max = 0;

    /* check if secondary heater is required to cover thermal demand */
    if (prosumer_data->P_th_dem > (prosumer_data->P_nom / prosumer_data->f_el +
            (prosumer_data->E_th * 3600 / t_step))) {
        /* CHP has to run at P_nom -> no flexibility */
        prosumer_data->P_min = -prosumer_data->P_nom;
        prosumer_data->P_max = -prosumer_data->P_nom;
        prosumer_data->P_optimal = -prosumer_data->P_nom;
    }
    else {
        /* CHP has flexibility, secondary heater not needed */
        if ((prosumer_data->P_nom / prosumer_data->f_el - prosumer_data->P_th_dem)*t_step >
            3600 * (prosumer_data->C_th - prosumer_data->E_th)) {
            /* if there is space in the storage for what can additionally be thermally generated
                * by the CHP Pmin is thermal demand coverage + rest that fits in thermal storage */
            prosumer_data->P_min = -(prosumer_data->P_th_dem +
                3600 * (prosumer_data->C_th - prosumer_data->E_th) / t_step) * prosumer_data->f_el;
        }
        if (prosumer_data->P_th_dem * t_step > prosumer_data->E_th * 3600) {
            /* thermal demand is larger than stored thermal energy */
            prosumer_data->P_max = -(prosumer_data->P_th_dem - prosumer_data->E_th * 3600 / t_step)*
                prosumer_data->f_el;
        }
        if (prosumer_data->P_min > prosumer_data->P_max) {
            prosumer_data->P_min = prosumer_data->P_max;
        }
        if (v_norm > prosumer_data->v_setpoint + 0.05) {
            /* voltage is very high -> P_opt = 0 */
            prosumer_data->P_optimal = 0;
            prosumer_data->P_min = prosumer_data->P_max;
        }
        else if (v_norm > prosumer_data->v_setpoint) {
            /* hysteresis to prevent oscillations */
            prosumer_data->P_optimal = p_opt_old;
            prosumer_data->P_min = p_min_old;
            if (prosumer_data->P_min > prosumer_data->P_max) {
                prosumer_data->P_min = prosumer_data->P_max;
            }
        }
        else {
            /* voltage is ok */
            if (prosumer_data->SOC_th < 0.25) {
                /* start charging storage is soc < 0.25 */
                prosumer_data->P_optimal = -prosumer_data->P_nom;
                //prosumer_data->P_optimal = -((prosumer_data->v_setpoint - v_norm)/(prosumer_data->v_setpoint - 0.9)) * prosumer_data->P_nom;
                //if (prosumer_data->P_optimal > p_opt_old) {
                //    prosumer_data->P_optimal = p_opt_old;
                //}
                //prosumer_data->P_optimal = -prosumer_data->P_nom;
            }
            else if (prosumer_data->SOC_th > 0.95) {
                /* P_opt = 0 if soc above 0.95 */
                prosumer_data->P_optimal = 0;
            }
            else {
                prosumer_data->P_optimal = p_opt_old;
            }
            //if (std::fabs(p_opt_old + prosumer_data->P_nom) < 0.001) {
            //    /* continue charging if it has started once */
            //    prosumer_data->P_optimal = p_opt_old;
            //}
        }
        /* make sure optimal power is within the limits */
        if (prosumer_data->P_optimal < prosumer_data->P_min) {
            prosumer_data->P_optimal = prosumer_data->P_min;
        }
        else if (prosumer_data->P_optimal > prosumer_data->P_max) {
            prosumer_data->P_optimal = prosumer_data->P_max;
        }
    }

    determine_q_operation_range(v_norm, v_old);
    return;
}


/*! \brief determine the limits and optimal operation for biofuels
*/
void Swarm_prosumer_behavior::determine_operation_range_bio(double v_norm, double v_old)
{
    prosumer_data->P_max = 0;
    prosumer_data->P_optimal = -prosumer_data->P_gen;
    prosumer_data->P_min = -prosumer_data->P_gen;

    determine_q_operation_range(v_norm, v_old);
    return;
}


/*! \brief determine the limits and optimal operation for compensators
*/
void Swarm_prosumer_behavior::determine_operation_range_compensator(double v_norm, double v_old)
{
    prosumer_data->P_max = 0;
    prosumer_data->P_optimal = 0;
    prosumer_data->P_min = 0;

    double q_max_old = prosumer_data->Q_max;
    double q_min_old = prosumer_data->Q_min;
    /* adjust optimal power to improve voltage quality */
    if (v_norm < 1) {
        /* voltage is lower than nominal value -> decrease reactive power 
            * (behave more capacitive) */
        prosumer_data->Q_optimal = prosumer_data->Q_ctrl - (prosumer_data->v_setpoint - v_norm)
            * prosumer_data->S_r;
    }
    else {
        /* voltage is higher than nominal value -> increase reactive power 
            * (behave more inductive) */
        prosumer_data->Q_optimal = prosumer_data->Q_ctrl + (v_norm - prosumer_data->v_setpoint)
            * prosumer_data->S_r;
    }
    /* make sure optimal power is within the limits */
    if (prosumer_data->Q_optimal < -prosumer_data->Q_r) {
        prosumer_data->Q_optimal = -prosumer_data->Q_r;
    }
    else if (prosumer_data->Q_optimal > 0) {
        prosumer_data->Q_optimal = 0;
    }
    if (v_old < v_norm) {
        /* voltage has increased */
        if (v_norm < prosumer_data->v_setpoint) {
            /* voltage below set point */
            if (v_norm < prosumer_data->v_setpoint - 0.025) {
                /* voltage below set point - 0.025 */
                prosumer_data->Q_min = -prosumer_data->Q_r;
                prosumer_data->Q_max = q_max_old;
            }
            else {
                /* voltage between set point and set point - 0.025*/
                prosumer_data->Q_min = q_min_old;
                prosumer_data->Q_max = (prosumer_data->Q_optimal / 0.025) *
                    (prosumer_data->v_setpoint - v_norm);
                if (prosumer_data->Q_max < q_max_old) {
                    prosumer_data->Q_max = q_max_old;
                }
            }
        }
        else {
            /* voltage higher than set point*/
            if (v_norm > prosumer_data->v_setpoint + 0.025) {
                /* voltage higher than set point + 0.025 */
                prosumer_data->Q_min = (prosumer_data->Q_optimal / 0.05) *
                    (v_norm - prosumer_data->v_setpoint - 0.025);
                prosumer_data->Q_max = 0;
            }
            else {
                /* voltage between set point and set point + 0.025 */
                prosumer_data->Q_min = -prosumer_data->Q_r - (-prosumer_data->Q_r / 0.025) * (v_norm
                    - prosumer_data->v_setpoint);
                prosumer_data->Q_max = q_max_old;
            }
            if (prosumer_data->Q_min < q_min_old) {
                prosumer_data->Q_min = q_min_old;
            }
        }
    }
    else {
        /* voltage has decreased */
        if (v_norm < prosumer_data->v_setpoint) {
            /* voltage below set point */
            if (v_norm < prosumer_data->v_setpoint - 0.025) {
                /* voltage below set point - 0.025 */
                prosumer_data->Q_min = -prosumer_data->Q_r;
                prosumer_data->Q_max = (prosumer_data->Q_optimal / 0.05) *
                    (prosumer_data->v_setpoint - v_norm - 0.025);
            }
            else {
                /* voltage between set point and set point - 0.025*/
                prosumer_data->Q_min = (-prosumer_data->Q_r / 0.025) *
                    (prosumer_data->v_setpoint - v_norm);
                prosumer_data->Q_max = 0;
            }
            if (prosumer_data->Q_max > q_max_old) {
                prosumer_data->Q_max = q_max_old;
            }
        }
        else {
            /* voltage higher than set point*/
            if (v_norm > prosumer_data->v_setpoint + 0.025) {
                /* voltage higher than set point + 0.025 */
                prosumer_data->Q_min = q_min_old;
                prosumer_data->Q_max = 0;
            }
            else {
                /* voltage between set point and set point + 0.025 */
                prosumer_data->Q_min = (prosumer_data->Q_optimal / 0.025) *
                    (v_norm - prosumer_data->v_setpoint);
                if (prosumer_data->Q_min > q_min_old) {
                    prosumer_data->Q_min = q_min_old;
                }
                prosumer_data->Q_max = q_max_old;
            }

        }
    }
    if (prosumer_data->Q_max < prosumer_data->Q_optimal) {
        prosumer_data->Q_max = prosumer_data->Q_optimal;
    }
    if (prosumer_data->Q_min > prosumer_data->Q_optimal) {
        prosumer_data->Q_min = prosumer_data->Q_optimal;
    }
    return;
}


/*! \brief determine the limits and optimal reactive power operation
*/
void Swarm_prosumer_behavior::determine_q_operation_range(double v_norm, double v_old)
{
    double q_a = get_q_available(prosumer_data->P_ctrl);
    double q_max_old = prosumer_data->Q_max;
    double q_min_old = prosumer_data->Q_min;

    prosumer_data->Q_optimal = prosumer_data->Q_ctrl + (v_norm - prosumer_data->v_setpoint) 
        * prosumer_data->S_r;
    /* make sure optimal power is within the limits */
    if (prosumer_data->Q_optimal < -q_a) {
        prosumer_data->Q_optimal = -q_a;
    }
    else if (prosumer_data->Q_optimal > q_a) {
        prosumer_data->Q_optimal = q_a;
    }
    
    if (v_norm < prosumer_data->v_setpoint - 0.025) {
        /* voltage is very low -> decrease Q_max towards -q_a at 0.925 */
        prosumer_data->Q_min = -q_a;
        if (v_norm > 0.925) {
            prosumer_data->Q_max = -q_a + ((v_norm - 0.925) / ((prosumer_data->v_setpoint - 0.025) - 0.925)) * q_a;
        }
        else {
            prosumer_data->Q_max = -q_a;
        }
        if (prosumer_data->Q_max > q_max_old) {
            /* hysteresis to prevent oscillations */
            prosumer_data->Q_max = q_max_old;
        }
    }
    else if (v_norm < prosumer_data->v_setpoint) {
        /* voltage is low -> behave as producer */
        prosumer_data->Q_min = -q_a;
        prosumer_data->Q_max = q_a;
        //if (prosumer_data->Q_max > q_max_old) {
        //    /* hysteresis to prevent oscillations */
        //    prosumer_data->Q_max = q_max_old;
        //}
    }
    //else if (v_norm < prosumer_data->v_setpoint) {
    //    /* voltage is low -> behave as producer */
    //    prosumer_data->Q_min = -q_a;
    //    prosumer_data->Q_max = 0;
    //}
    //else if (v_norm < prosumer_data->v_setpoint + 0.025) {
    //    /* voltage is high -> behave as consumer */
    //    prosumer_data->Q_min = 0;
    //    prosumer_data->Q_max = q_a;
    //}
    else if (v_norm < prosumer_data->v_setpoint + 0.025) {
        /* voltage is high -> behave as consumer */
        prosumer_data->Q_max = q_a;
        prosumer_data->Q_min = -q_a;
        //if (prosumer_data->Q_min < q_min_old) {
        //    /* hysteresis to prevent oscillations */
        //    prosumer_data->Q_min = q_min_old;
        //}
    }
    else {
        /* voltage is very high -> increase Q_min towards q_a at 1.075 */
        prosumer_data->Q_max = q_a;
        if (v_norm < 1.075) {
            prosumer_data->Q_min = q_a - ((1.075 - v_norm) / (1.075 - (prosumer_data->v_setpoint + 0.025))) * q_a;
        }
        else {
            prosumer_data->Q_min = q_a;
        }
        if (prosumer_data->Q_min < q_min_old) {
            /* hysteresis to prevent oscillations */
            prosumer_data->Q_min = q_min_old;
        }
    }
    
    if (prosumer_data->Q_max > q_a) {
        prosumer_data->Q_max = q_a;
    }
    if (prosumer_data->Q_min < -q_a) {
        prosumer_data->Q_min = -q_a;
    }
    if (prosumer_data->Q_optimal > prosumer_data->Q_max) {
        prosumer_data->Q_optimal = prosumer_data->Q_max;
    }
    else if (prosumer_data->Q_optimal < prosumer_data->Q_min) {
        prosumer_data->Q_optimal = prosumer_data->Q_min;
    }

    return;
}


/*! \brief checks whether state has to be changed from producer to consumer
 *  \param  neg_type    active or reactive power
 * */
void Swarm_prosumer_behavior::determine_state(int neg_type)
{
    int state;
    double power_min;
    double power_max;
    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        state = knowledge_neg_internal_p.get_state();
        power_min = prosumer_data->P_min;
        power_max = prosumer_data->P_max;
    }
    else {
        state = knowledge_neg_internal_q.get_state();
        power_min = prosumer_data->Q_min;
        power_max = prosumer_data->Q_max;
    }
    struct contract_ex power_sum = get_power_sum(neg_type, GROUPTYPE_SUBSTATION_INT);
    struct contract_ex open_power_sum = get_open_power_sum(neg_type, GROUPTYPE_SUBSTATION_INT);

    /* prosumer becomes consumer if there are no producer contracts */
    if (state == BASETYPE_PRODUCER_INT) {
        if (fabs(power_sum.needed + power_sum.optional + open_power_sum.needed +
                open_power_sum.optional) < NEGOTIATION_THRESHOLD_FACTOR * prosumer_data->Vnom) {
            change_state(neg_type, BASETYPE_CONSUMER_INT);
            state = BASETYPE_CONSUMER_INT;
        }
    }
    if (state == BASETYPE_CONSUMER_INT) {
        /* agent cannot operate as concumser due to operation range */
        if (power_max < -NEGOTIATION_THRESHOLD_FACTOR * prosumer_data->Vnom) {
            change_state(neg_type, BASETYPE_PRODUCER_INT);
            state = BASETYPE_PRODUCER_INT;
        }
    }
    else {
        /* agent cannot operate as producer due to operation range */
        if (power_min > NEGOTIATION_THRESHOLD_FACTOR * prosumer_data->Vnom) {
            change_state(neg_type, BASETYPE_CONSUMER_INT);
            state = BASETYPE_CONSUMER_INT;
        }
    }

    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        IO->log_info("\tActive power state: " + std::to_string(state));
    }
    else {
        IO->log_info("\tReactive power state: " + std::to_string(state));
    }
}


/*! \brief get the missing power for specified group
 *  \param  know_neg    affected negotiation knowledge
 *  \return power lack
 * */
contract_ex Swarm_prosumer_behavior::get_power_lack(Negotiation_knowledge* know_neg)
{
    struct contract_ex power_sum = get_power_sum(know_neg->get_negotiation_type(),
        know_neg->get_group_type());
    struct contract_ex open_power_sum = get_open_power_sum(know_neg->get_negotiation_type(), 
        know_neg->get_group_type());

    /* determine the amount of power that is missing */
    struct contract_ex power_lack;
    power_lack.neg_type = know_neg->get_negotiation_type();
    double power_min;
    double power_optimal;
    double power_max;
    if (power_lack.neg_type == NEGOTIATIONTYPE_P_INT) {
        power_min = prosumer_data->P_min;
        power_optimal = prosumer_data->P_optimal;
        power_max = prosumer_data->P_max;
    }
    else {
        power_min = prosumer_data->Q_min;
        power_optimal = prosumer_data->Q_optimal;
        power_max = prosumer_data->Q_max;
    }

    // if (power_lack.neg_type == NEGOTIATIONTYPE_P_INT 
    //         && know_neg->get_group_type() == GROUPTYPE_INTERNAL_INT 
    //         && type != TYPE_BATTERY_INT) {
    if (know_neg->get_group_type() == GROUPTYPE_INTERNAL_INT ) {
        /* internal: needed up to optimal, optional up to max */
        power_lack.needed = power_optimal - (power_sum.needed + open_power_sum.needed);
        if (power_optimal > 0) {
            power_lack.optional = power_max - power_optimal - 
                (power_sum.optional + open_power_sum.optional);
        }
        else {
            power_lack.optional = power_max - (power_sum.optional + open_power_sum.optional);
        }
    }
    else {
        /* other: needed up to min, optional up to optimal */
        power_lack.needed = power_min - (power_sum.needed + open_power_sum.needed);
        if (power_min > 0) {
            power_lack.optional = power_optimal - power_min - 
                (power_sum.optional + open_power_sum.optional);
        }
        else {
            power_lack.optional = power_optimal - (power_sum.optional + open_power_sum.optional);
        }
    }

    return power_lack;
}


/*! \brief  consumer power exceed
 *  \param  know_neg    affected negotiation knowlegde
 *  \return consumer power exceed
 * */
contract_ex Swarm_prosumer_behavior::get_consumer_power_exceed(Negotiation_knowledge* know_neg)
{
    struct contract_ex power_sum = get_power_sum(know_neg->get_negotiation_type(), know_neg->get_group_type());

    /* determine the difference between contracted and needed power */
    struct contract_ex power_exceed;
    power_exceed.neg_type = know_neg->get_negotiation_type();
    double power_min;
    double power_optimal;
    double power_max;
    if (power_exceed.neg_type == NEGOTIATIONTYPE_P_INT) {
        power_min = prosumer_data->P_min;
        power_optimal = prosumer_data->P_optimal;
        power_max = prosumer_data->P_max;
    }
    else {
        power_min = prosumer_data->Q_min;
        power_optimal = prosumer_data->Q_optimal;
        power_max = prosumer_data->Q_max;
    }

    if (know_neg->get_state() == BASETYPE_CONSUMER_INT) {
        // if (power_exceed.neg_type == NEGOTIATIONTYPE_P_INT 
        //         && know_neg->get_group_type() == GROUPTYPE_INTERNAL_INT 
        //         && type != TYPE_BATTERY_INT) {
        if (know_neg->get_group_type() == GROUPTYPE_INTERNAL_INT) {
            /* internal: needed up to optimal, optional up to max */
            power_exceed.needed = power_sum.needed - power_optimal;
            if (power_optimal > 0) {
                power_exceed.optional = power_sum.optional - (power_max - power_optimal);
            }
            else {
                power_exceed.optional = power_sum.optional - power_max;
            }
        }
        else {
            /* other: needed up to min, optional up to optimal */
            power_exceed.needed = power_sum.needed - power_min;
            if (power_min > 0) {
                power_exceed.optional = power_sum.optional - (power_optimal - power_min);
            }
            else {
                power_exceed.optional = power_sum.optional - power_optimal;
            }
        }
    }
    else {
        /* as a producer get rid of all consumer contracts */
        power_exceed.needed = power_sum.needed;
        power_exceed.optional = power_sum.optional;
    }

    return power_exceed;
}


/*! \brief  producer power exceed
 *  \param  know_neg    affected negotiation knowlegde
 *  \return producer power exceed
 * */
contract_ex Swarm_prosumer_behavior::get_producer_power_exceed(Negotiation_knowledge* know_neg)
{
    struct contract_ex power_sum = get_power_sum(know_neg->get_negotiation_type(),
        know_neg->get_group_type());

    /* determine the difference between contracted and needed power */
    struct contract_ex power_exceed;
    power_exceed.neg_type = know_neg->get_negotiation_type();

    double power_min;
    double power_optimal;
    struct contract_ex swarm_p;
    struct contract_ex sub_p;
    if (power_exceed.neg_type == NEGOTIATIONTYPE_P_INT) {
        power_min = prosumer_data->P_min;
        power_optimal = prosumer_data->P_optimal;
        swarm_p = knowledge_neg_swarm_p.get_power();
        sub_p = knowledge_neg_substation_p.get_power();
    }
    else {
        power_min = prosumer_data->Q_min;
        power_optimal = prosumer_data->Q_optimal;
        swarm_p = knowledge_neg_swarm_q.get_power();
        sub_p = knowledge_neg_substation_q.get_power();
    }

    /* needed up to min, optional up to optimal */
    if (know_neg->get_state() == BASETYPE_PRODUCER_INT) {
        if (know_neg->get_group_type() == GROUPTYPE_INTERNAL_INT) {
            power_exceed.needed = power_min - power_sum.needed;
            if (power_exceed.needed > 0) {
                /* if more needed power is contracted than possible get rid of all optional power */
                power_exceed.optional = -power_sum.optional;
            }
            else {
                power_exceed.optional = power_optimal - 
                    (power_sum.needed + swarm_p.needed + sub_p.needed + power_sum.optional);
            }
        }
        else if (know_neg->get_group_type() == GROUPTYPE_SWARM_INT) {
            // power_exceed.needed = power_min - power_sum.needed;
            power_exceed.needed = power_optimal - power_sum.needed;
            if (power_exceed.needed > 0) {
                /* if more needed power is contracted than possible get rid of all optional power */
                power_exceed.optional = -power_sum.optional;
            }
            else {
                power_exceed.optional = power_optimal -
                    (power_sum.needed + sub_p.needed + power_sum.optional);
            }
        }
        else {
            // power_exceed.needed = power_min - power_sum.needed;
            power_exceed.needed = power_optimal - power_sum.needed;
            if (power_exceed.needed > 0) {
                /* if more needed power is contracted than possible get rid of all optional power */
                power_exceed.optional = -power_sum.optional;
            }
            else {
                power_exceed.optional = power_optimal - (power_sum.needed + power_sum.optional);
            }
        }
    }
    else {
        /* as a consumer get rid of all producer contracts */
        power_exceed.needed = -power_sum.needed;
        power_exceed.optional = -power_sum.optional;
    }
	
    return power_exceed;
}


/*! \brief  recalculate prodcuer service
 *  \param  know_neg    affected negotiation knowlegde
 *  \return new service
 * */
service_ex Swarm_prosumer_behavior::get_new_service(Negotiation_knowledge* know_neg)
{
    struct contract_ex power_sum = get_power_sum(know_neg->get_negotiation_type(),
        know_neg->get_group_type());
    struct contract_ex open_power_sum = get_open_power_sum(know_neg->get_negotiation_type(), 
        know_neg->get_group_type());

    struct service_ex service_new;
    service_new.svc_type = know_neg->get_negotiation_type();
    service_new.t_last_update = t_next;

    double power_min;
    double power_optimal;
    double power_max;
    struct contract_ex swarm_p;
    struct contract_ex open_swarm_p;
    struct contract_ex sub_p;
    struct contract_ex open_sub_p;
    if (service_new.svc_type == NEGOTIATIONTYPE_P_INT) {
        power_min = prosumer_data->P_min;
        power_optimal = prosumer_data->P_optimal;
        power_max = prosumer_data->P_max;
        swarm_p = knowledge_neg_swarm_p.get_power();
        open_swarm_p = knowledge_neg_swarm_p.get_open_power();
        sub_p = knowledge_neg_substation_p.get_power();
        open_sub_p = knowledge_neg_substation_p.get_open_power();
    }
    else {
        power_min = prosumer_data->Q_min;
        power_optimal = prosumer_data->Q_optimal;
        power_max = prosumer_data->Q_max;
        swarm_p = knowledge_neg_swarm_q.get_power();
        open_swarm_p = knowledge_neg_swarm_q.get_open_power();
        sub_p = knowledge_neg_substation_q.get_power();
        open_sub_p = knowledge_neg_substation_q.get_open_power();
    }
    
    /* needed up to min, optional up to optimal, minimum up to max */
    if (know_neg->get_state() == BASETYPE_PRODUCER_INT) {
        if (know_neg->get_group_type() == GROUPTYPE_INTERNAL_INT) {
            service_new.needed_max = (power_sum.needed + open_power_sum.needed) - power_min;

            service_new.optional_max = (power_sum.needed + open_power_sum.needed +
                power_sum.optional + open_power_sum.optional + swarm_p.needed +
                open_swarm_p.needed + sub_p.needed + open_sub_p.needed) - power_optimal;

            service_new.minimum = (power_sum.needed + open_power_sum.needed + power_sum.optional +
                open_power_sum.optional + swarm_p.needed + open_swarm_p.needed + sub_p.needed +
                open_sub_p.needed) - power_max;
        }
        else if (know_neg->get_group_type() == GROUPTYPE_SWARM_INT) {
            // service_new.needed_max = (power_sum.needed + open_power_sum.needed) - power_min;
            service_new.needed_max = (power_sum.needed + open_power_sum.needed) - power_optimal;

            service_new.optional_max = (power_sum.needed + open_power_sum.needed +
                power_sum.optional + open_power_sum.optional + sub_p.needed + open_sub_p.needed) -
                power_optimal;

            service_new.minimum = (power_sum.needed + open_power_sum.needed + power_sum.optional + 
                open_power_sum.optional + sub_p.needed + open_sub_p.needed) - power_max;
        }
        else {
            // service_new.needed_max = (power_sum.needed + open_power_sum.needed) - power_min;
            service_new.needed_max = (power_sum.needed + open_power_sum.needed) - power_optimal;

            service_new.optional_max = (power_sum.needed + open_power_sum.needed + 
                power_sum.optional + open_power_sum.optional) - power_optimal;

            service_new.minimum = (power_sum.needed + open_power_sum.needed + power_sum.optional +
                open_power_sum.optional) - power_max;
        }
    }
    else {
        if (know_neg->get_group_type() == GROUPTYPE_INTERNAL_INT) {
            service_new.needed_max = -power_min;
        }
        else {
            service_new.needed_max = -power_optimal;
        }
        service_new.optional_max = -power_optimal;
        service_new.minimum = -power_max;
    }

    return service_new;
}


/*! \brief determine the effective swarm size and adjust list of known agents accordingly
 *  \param neg_type Negotiation type (P or Q)
 * */
void Swarm_prosumer_behavior::determine_swarm_size(int neg_type)
{
    unsigned int swarm_size = get_swarm_size(neg_type);
    Negotiation_knowledge* know_neg_swarm;
    Negotiation_knowledge* know_neg_substation;
    int state;

    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        know_neg_swarm = &knowledge_neg_swarm_p;
        know_neg_substation = &knowledge_neg_substation_p;
        state = knowledge_neg_internal_p.get_state();
    }
    else {
        know_neg_swarm = &knowledge_neg_swarm_q;
        know_neg_substation = &knowledge_neg_substation_q;
        state = knowledge_neg_internal_q.get_state();
    }
    contract_ex power_lack = get_power_lack(know_neg_swarm);

    if (state == BASETYPE_CONSUMER_INT && 
            (power_lack.needed > NEGOTIATION_THRESHOLD_FACTOR * prosumer_data->Vnom ||
                power_lack.optional > NEGOTIATION_THRESHOLD_FACTOR * prosumer_data->Vnom)) {
        /* if consumer with power lack */
        int hops = get_swarm_size(neg_type);
        if (!know_neg_swarm->is_waiting_for_df()) {
            /* consumer is not waiting for DF -> send message to DF */
            int receiver = knowledge_comm.get_df_agent();
            hops++;
            if (hops < MAX_SWARM_SIZE) {
                swarm_recruiting_msg new_msg(FIPA_PERF_PROXY, id, receiver);
                new_msg.base_type = BASETYPE_PRODUCER_INT;
                new_msg.hops = hops;
                new_msg.neg_type = know_neg_swarm->get_negotiation_type();
                new_msg.node_sender = prosumer_data->node_id;
                send_recruiting_msg(new_msg);
                know_neg_swarm->contacted_df();
            }
            else {
                if (hops >= MAX_SWARM_SIZE - 1) {
                    if (!know_neg_substation->subscribed_to(knowledge_comm.get_substation_agent())) {
                        /* swarm size has reached maximum -> subscribe to substation */
                        swarm_subscribe_msg new_msg(FIPA_PERF_SUBSCRIBE, id,
                            knowledge_comm.get_substation_agent());
                        new_msg.service.svc_type = know_neg_substation->get_negotiation_type();
                        new_msg.dist = MAX_SWARM_SIZE;
                        send_subscribe_msg(new_msg);
                        set_swarm_size(neg_type, MAX_SWARM_SIZE);
                        swarm_size = MAX_SWARM_SIZE;
                    }
                }
            }
        }
    }
    else {
        if (swarm_size == MAX_SWARM_SIZE) {
            /* if swarm size is equal to maximum size check if this is still valid */
            swarm_size = know_neg_substation->get_effective_swarm_size(t_next, 
                NEGOTIATION_THRESHOLD_FACTOR * prosumer_data->Vnom);
            unsigned int temp = know_neg_swarm->get_effective_swarm_size(t_next,
                NEGOTIATION_THRESHOLD_FACTOR * prosumer_data->Vnom);
            if (temp > swarm_size) {
                swarm_size = temp;
            }
        }
        else if (!know_neg_swarm->is_waiting_for_df()) {
            /* get swarm size from swarm negotiation knowledge */
            swarm_size = know_neg_swarm->get_effective_swarm_size(t_next, 
                NEGOTIATION_THRESHOLD_FACTOR * prosumer_data->Vnom);
        }

        if (state == BASETYPE_CONSUMER_INT) {
            swarm_subscribe_msg new_msg(CUST_PERF_UNSUBSCRIBE, id, 0);
            new_msg.service.svc_type = neg_type;
            std::vector<int> _producers;
            /* delete unecessary substation producers */
            know_neg_substation->delete_producers(swarm_size, _producers);
            /* unsubscribe to unnecessary producers */
            for (auto i : _producers) {
                new_msg.receiver = i;
                send_subscribe_msg(new_msg);
            }
            _producers.clear();
            /* delete unecessary swarm producers */
            know_neg_swarm->delete_producers(swarm_size, _producers);
            /* unsubscribe to unnecessary producers */
            for (auto i : _producers) {
                new_msg.receiver = i;
                send_subscribe_msg(new_msg);
            }
        }
    }
    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        IO->log_info("\tEffective active power swarm size: " + std::to_string(swarm_size));
        prosumer_data->swarm_size_p = swarm_size;
    }
    else {
        IO->log_info("\tEffective reactive power swarm size: " + std::to_string(swarm_size));
        prosumer_data->swarm_size_q = swarm_size;
    }
}


/*! \brief  take new sample for voltage and store it in sensor knowledge
 * */
void Swarm_prosumer_behavior::save_voltage_measurement()
{
    std::complex<double> meas;
    meas.real(prosumer_data->v_re / (prosumer_data->Vnom / sqrt(3)));
    meas.imag(prosumer_data->v_im / (prosumer_data->Vnom / sqrt(3)));
    struct meas_ex v_sample(id, QUERYTYPE_VOLTAGE, meas, t_next, true);
    knowledge_sens.add_voltage_sample(v_sample);

    prosumer_data->v_meas = knowledge_sens.get_absolute_voltage(id);
}


/*! \brief  change the set point for voltage
 * */
void Swarm_prosumer_behavior::change_setpoint(double setpoint)
{
    prosumer_data->v_setpoint = setpoint;
}


/*! \brief      calculate the maximium available reactive power regarding the limits of converter 
                and rated power
 *  \param  p   produced real power
 *  \return     available reactive power
 * */
double Swarm_prosumer_behavior::get_q_available(double p)
{
    double q1, q2;
    double q_available;

    q1 = sqrt(prosumer_data->S_r * prosumer_data->S_r - p * p);
    q2 = fabs(p * tan(acos(prosumer_data->pf_min)));
    if (q1 < q2)
        q_available = q1;
    else
        q_available = q2;

    return q_available;
}


/*! \brief  get current size of swarm
 *  \param  neg_type    active or reactive power
 *  \return swarm size
 * */
unsigned int Swarm_prosumer_behavior::get_swarm_size(int neg_type)
{
    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        return prosumer_data->swarm_size_p;
    }
    else {
        return prosumer_data->swarm_size_q;
    }
}


/*! \brief  set current size of swarm
 *  \param  neg_type    active or reactive power
 *  \param  swarm_size  swarm size
 * */
void Swarm_prosumer_behavior::set_swarm_size(int neg_type, int swarm_size)
{
    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        prosumer_data->swarm_size_p = swarm_size;
    }
    else {
        prosumer_data->swarm_size_q = swarm_size;
    }
}


/*! \brief  get threshold for contract negotiation
 *  \param  neg_type    affected negotiation knowledge
 *  \return threshold
 * */
double Swarm_prosumer_behavior::get_threshold(Negotiation_knowledge* know_neg)
{
    return NEGOTIATION_THRESHOLD_FACTOR*prosumer_data->Vnom;
}

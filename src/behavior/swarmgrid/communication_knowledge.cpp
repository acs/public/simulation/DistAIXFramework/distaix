/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include <algorithm>

#include "behavior/swarmgrid/communication_knowledge.h"


Communication_knowledge::Communication_knowledge(): id(0), IO(nullptr), df_agent(0),
    substation_agent(0), slack_agent(0)
{
    internal_agents.clear();
    swarm_agents.clear();
    neighborhood_agents.clear();
}


/*! \brief initialization of Knowledge class
 *  \param _id agent ID of owner agent
 *  \param _internal_agents IDs of component agents connected to the same bus with unknown type
 *  \param _swarm_agents IDs of component agents within the same swarm with unknown type
 * */
void Communication_knowledge::init(int &_id, BehaviorIO* _io)
{
    internal_agents.clear();
    swarm_agents.clear();
    neighborhood_agents.clear();

    id = _id;
    IO = _io;
    substation_agent = 0;
    slack_agent = 0;
    df_agent = 0;
}


/*! \brief check if given agent is an internal agent
 *  \param ag_id ID of the agent to check for
 *  \return indicates whether or not specified agent is internal
 * */
bool Communication_knowledge::is_internal_agent(int ag_id)
{
    if (find(internal_agents.begin(), internal_agents.end(), ag_id) != internal_agents.end())
        return true;
    else
        return false;
}


/*! \brief check if given agent is a swarm agent
 *  \param ag_id ID of the agent to check for
 *  \return indicates whether or not specified agent is in swarm
 * */
bool Communication_knowledge::is_swarm_agent(int ag_id)
{
    for (auto& it : swarm_agents) {
        if (it.first == ag_id) {
            return true;
        }
    }
    return false;
}


/*! \brief adds an agent to the list of internal agents
 *  \return returns true if agent was successfully added, false otherwise
 * */
bool Communication_knowledge::add_internal(int ag_id) {
    if (std::find(internal_agents.begin(), internal_agents.end(), ag_id) ==
            internal_agents.end()) {
        internal_agents.push_back(ag_id);
        return true;
    } else {
        return false;
    }

}


/*! \brief adds an agent to the list of neighborhood agents
 *  \return returns true if agent was successfully added, false otherwise
 * */
bool Communication_knowledge::add_neighbor(int ag_id) {
    if (std::find(neighborhood_agents.begin(), neighborhood_agents.end(), ag_id) ==
            neighborhood_agents.end()) {
        neighborhood_agents.push_back(ag_id);
        return true;
    } else {
        return false;
    }
}


/*! \brief adds an agent to the list of swarm agents
 *  \return returns true if agent was successfully added, false otherwise
 * */
bool Communication_knowledge::add_swarmmember(int ag_id, int type, unsigned int dist) {

    bool added = false;
    if (type == TYPE_DF_INT) {
        df_agent = ag_id;
        added= true;
    }
    else if (type == TYPE_TRANSFORMER_INT) {
        substation_agent = ag_id;
        added= true;
    }
    else if (type == TYPE_SLACK_INT) {
        slack_agent = ag_id;
        added= true;
    }
    else {
        if (ag_id != substation_agent) {
            std::pair<int, unsigned int> _agent(ag_id, dist);
            /* check if agent is not already in vector */
            if (std::find(swarm_agents.begin(), swarm_agents.end(), _agent) == swarm_agents.end() &&
                    std::find(internal_agents.begin(), internal_agents.end(), ag_id)
                    == internal_agents.end()) {
                swarm_agents.push_back(_agent);
                added = true;
            }
        } else{
            added = false;
        }
    }
    return added;
}


/*! \brief getter for vector of internal agents
 *  \param _internal_agents [out] vector of internal agent
 * */
void Communication_knowledge::get_internal_agents(std::vector<int>& _internal_agents)
{
    _internal_agents = internal_agents;
}


/*! \brief getter for vector of swarm agents
 *  \param _swarm_agents [out] vector of swarm agent
 * */
void Communication_knowledge::get_swarm_agents(std::vector<std::pair<int,
    unsigned int>>& _swarm_agents)
{
    _swarm_agents = swarm_agents;
}

/*! \brief getter for distance to other agent
 *  \param  _agent_id other agent
 *  \retrun distance to othe agent
 * */
unsigned int Communication_knowledge::get_distance(int _agent_id)
{
    if (is_internal_agent(_agent_id)) {
        return 0;
    }
    else if (is_swarm_agent(_agent_id)) {
        for (auto& it : swarm_agents) {
            if (it.first == _agent_id) {
                return it.second;
            }
        }
    }
    else if (_agent_id == id) {
        return 0;
    }
    return 1000000;
}

/*! \brief getter for DF agent
 *  \retrun ID of DF agent
 * */
int Communication_knowledge::get_df_agent()
{
    return df_agent;
}

/*! \brief getter for substation agent
 *  \retrun ID of substation agent
 * */
int Communication_knowledge::get_substation_agent()
{
    return substation_agent;
}

/*! \brief getter for slack agent
 *  \retrun ID of slack agent
 * */
int Communication_knowledge::get_slack_agent()
{
    return slack_agent;
}


/*! \brief Helper function for logging
 *  \return string containing logging information about knowledge
 * */
void Communication_knowledge::log_communication_knowledge()
{
    std::stringstream temp;
    std::string output;

    temp << "Communication knowledge output: " << std::endl;

    temp << "\tinternal agents: ";
    for (auto ag : internal_agents) {
        temp << ag << ", ";
    }
    temp << std::endl;
    temp << "\tswarm agents: ";
    for (auto ag : swarm_agents) {
        temp << "(" << ag.first << ", " << ag.second << "), ";
    }
    temp << std::endl;

    temp << "\tDF: " << df_agent << std::endl;
    temp << "\tSubstation: " << substation_agent << std::endl;
    temp << "\tSlack: " << slack_agent;

    output = temp.str();
    IO->log_info(output);
}

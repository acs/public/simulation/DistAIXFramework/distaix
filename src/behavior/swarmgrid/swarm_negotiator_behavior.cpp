/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/swarmgrid/swarm_negotiator_behavior.h"


/*! \brief  constructor
 *  \param  _id         unique ID of agent
 *  \param  _type       agent type as defined in config.h
 *  \param  _subtype    subtype as defined in config.h
 *  \param  step_size   simulation step size
 *  \param  logging     indicates if agents logs locally
 * */
Swarm_negotiator_behavior::Swarm_negotiator_behavior(int _id, int _type, std::string _subtype,
    double& step_size, struct data_props _d_props)
    : Swarm_behavior(_id, _type, _subtype, step_size, _d_props)
{
    knowledge_comm.init(_id, IO);
    knowledge_neg_internal_p.init(_id, IO, &knowledge_comm, NEGOTIATIONTYPE_P_INT,
        GROUPTYPE_INTERNAL_INT);
    knowledge_neg_internal_q.init(_id,IO, &knowledge_comm, NEGOTIATIONTYPE_Q_INT,
        GROUPTYPE_INTERNAL_INT);
    knowledge_neg_swarm_p.init(_id, IO, &knowledge_comm, NEGOTIATIONTYPE_P_INT,
        GROUPTYPE_SWARM_INT);
    knowledge_neg_swarm_q.init(_id, IO, &knowledge_comm, NEGOTIATIONTYPE_Q_INT,
        GROUPTYPE_SWARM_INT);
    knowledge_neg_substation_p.init(_id, IO, &knowledge_comm, NEGOTIATIONTYPE_P_INT,
        GROUPTYPE_SUBSTATION_INT);
    knowledge_neg_substation_q.init(_id, IO, &knowledge_comm, NEGOTIATIONTYPE_Q_INT,
        GROUPTYPE_SUBSTATION_INT);
    knowledge_neg_slack_p.init(_id, IO, &knowledge_comm, NEGOTIATIONTYPE_P_INT,
        GROUPTYPE_SLACK_INT);
    knowledge_neg_slack_q.init(_id, IO, &knowledge_comm, NEGOTIATIONTYPE_Q_INT,
        GROUPTYPE_SLACK_INT);
    knowledge_sens.init(_id, IO, &knowledge_comm, step_size);

    IO->init_logging();
}


/*! \brief handles a received message with contract-net protocol (contract negotiation)
 *  \param msg contract-net message
 * */
void Swarm_negotiator_behavior::process_contract_net_msg(swarm_contract_net_msg &msg)
{
    swarm_contract_net_msg new_msg(0, id, msg.sender);
    new_msg.time = t_next;
    struct contract_ex _power = msg.power;

    /* decide which negotiation knowledge is affected */
    Negotiation_knowledge* know_neg = get_affected_negotiation_knowledge(msg.sender,
        _power.neg_type);

    switch (msg.performative) {
        case FIPA_PERF_CALL_FOR_PROPOSAL: {
            /* received a call for proposal from consumer -> check if a proposal can be made */
            bool send_proposal = decide_cfp(msg.sender, _power);
            if (send_proposal && !know_neg->open_contract_exists(id, msg.sender)) {
                /* make proposal and create new contract */
                know_neg->new_contract(id, msg.sender, _power, t_next);
                new_msg.performative = FIPA_PERF_PROPOSE;
                new_msg.power = _power;
                if (know_neg->get_state() == BASETYPE_CONSUMER_INT && 
                        type != TYPE_TRANSFORMER_INT) {
                    /* agent was consumer before -> change state to producer */
                    double threshold = get_threshold(know_neg);
                    if (_power.needed > threshold || _power.optional > threshold) {
                        change_state(know_neg->get_negotiation_type(), BASETYPE_PRODUCER_INT);
                    }
                }
            }
            else {
                /* send refuse */
                new_msg.power = _power;
                new_msg.performative = FIPA_PERF_REFUSE;
            }
            send_contract_net_msg(new_msg);
            break;
        }
        case FIPA_PERF_PROPOSE: {
            /* received a proposal from a producer -> open contract should exist, decide if it 
             * should be accepted */
            bool send_accept = decide_proposal(msg.sender, _power);
            if (send_accept) {
                /* send accept and conclude open contract */
                know_neg->change_open_contract(msg.sender, id, _power, t_next);
                know_neg->conclude_contract(msg.sender, id, t_next);
                new_msg.performative = FIPA_PERF_ACCEPT_PROPOSAL;
                new_msg.power = _power;
            }
            else {
                /* send reject */
                new_msg.power = _power;
                new_msg.performative = FIPA_PERF_REJECT_PROPOSAL;
            }
            send_contract_net_msg(new_msg);
            break;
        }
        case FIPA_PERF_REFUSE: {
            /* producer refuses to send proposal -> delete open contract and set service to 0 */
            _power = know_neg->get_open_contract(msg.sender, id);
            struct service_ex _service = know_neg->get_producer_service(msg.sender);
            if (_power.needed > POWER_ACCURACY) {
                _service.needed_max = 0;
                _service.optional_max = 0;
                _service.minimum = 0;
            } else if (_power.optional > POWER_ACCURACY) {
                _service.optional_max = 0;
                _service.minimum = 0;
            }
            know_neg->delete_open_contract(msg.sender, id);
            know_neg->update_producer_service(msg.sender, _service);
            break;
        }
        case FIPA_PERF_ACCEPT_PROPOSAL: {
            /* consumer accepts proposal -> conclude contract and check if state has to be changed */
            know_neg->conclude_contract(id, msg.sender, msg.time);
            if (know_neg->get_state() == BASETYPE_CONSUMER_INT && 
                    type != TYPE_TRANSFORMER_INT) {
                /* agent was consumer before -> change state to producer */
                double threshold = get_threshold(know_neg);
                if (_power.needed > threshold || _power.optional > threshold) {
                    change_state(know_neg->get_negotiation_type(), BASETYPE_PRODUCER_INT);
                }
            }
            break;
        }
        case FIPA_PERF_REJECT_PROPOSAL: {
            /* consumer rejects proposal -> delete open contract */
            know_neg->delete_open_contract(id, msg.sender);
            break;
        }
        case FIPA_PERF_INFORM: {
            /* adjust contract according to inform msg */
            know_neg->handle_contract_inform_msg(_power.producer, _power.consumer, _power, msg.time);
            break;
        }
        default: {
            // TODO ERROR HANDLING
        }
    }
}


/*! \brief handles a received message with subscribe protocol (producer service promotion)
 *  \param msg subscribe message
 * */
void Swarm_negotiator_behavior::process_subscribe_msg(swarm_subscribe_msg &msg)
{
    /* get content of received message */
    swarm_subscribe_msg new_msg(0, id, msg.sender);
    new_msg.dist = msg.dist;
    struct service_ex _service = msg.service;

    /* decide which negotiation knowledge is affected */
    Negotiation_knowledge* know_neg = get_affected_negotiation_knowledge(msg.sender, 
        _service.svc_type);

    switch (msg.performative) {
        case FIPA_PERF_SUBSCRIBE: {
            if (_service.svc_type == NEGOTIATIONTYPE_P_INT) {
                if (type == TYPE_LOAD_INT || type == TYPE_HP_INT || type == TYPE_EV_INT) {
                    /* agent is no producer -> reply with refuse msg */
                    new_msg.performative = FIPA_PERF_REFUSE;
                }
                else {
                    /* agent is producer -> reply with agree msg and add agent to consumer list */
                    _service = know_neg->get_producer_service(id);
                    know_neg->add_consumer(msg.sender, msg.dist, t_next);
                    new_msg.performative = FIPA_PERF_AGREE;
                    new_msg.service = _service;
                }
            }
            else {
                if (type == TYPE_LOAD_INT || type == TYPE_HP_INT) {
                    /* agent is no producer -> reply with refuse msg */
                    new_msg.performative = FIPA_PERF_REFUSE;
                }
                else {
                    /* agent is producer -> reply with agree msg and add agent to consumer list */
                    _service = know_neg->get_producer_service(id);
                    know_neg->add_consumer(msg.sender, msg.dist, t_next);
                    new_msg.performative = FIPA_PERF_AGREE;
                    new_msg.service = _service;
                }
            }
            send_subscribe_msg(new_msg);
            break;
        }
        case FIPA_PERF_AGREE: {
            /* received agree from producer -> add producer to list and create service */
            know_neg->add_producer(msg.sender, msg.dist, t_next);
            know_neg->new_producer_service(msg.sender, _service);
            break;
        }
        case FIPA_PERF_REFUSE: {
            //know_neg->add_consumer(msg.sender, dist, t_next);
            break;
        }
        case FIPA_PERF_INFORM: {
            /* a producer informs about a change in the available power -> update knowledge about 
             * producer */
            know_neg->update_producer_service(msg.sender, _service);
            break;
        }
        case CUST_PERF_UNSUBSCRIBE: {
            /* consumer unsubcribes -> delete it from list of consumers */
            know_neg->delete_consumer(msg.sender);
            break;
        }
        default: {
            // TODO ERROR HANDLING
        }
    }
}


/*! \brief handles a received message with query protocol (measurement queries)
 *  \param msg query message
 * */
void Swarm_negotiator_behavior::process_query_msg(swarm_query_msg &msg)
{
    swarm_query_msg new_msg(0, id, msg.sender);

    switch (msg.performative) {
        case FIPA_PERF_QUERY_REF: {
            /* received query -> get querried measurement and send it */
            std::complex<double> meas;
            switch (msg.meas_type) {
                case QUERYTYPE_VOLTAGE:
                    meas = knowledge_sens.get_voltage_meas(id);
                    break;
                case QUERYTYPE_CURRENT:
                    //TODO
                    break;
            }
            new_msg.performative = FIPA_PERF_INFORM;
            new_msg.meas_type = msg.meas_type;
            new_msg.time = t_next;
            new_msg.meas_re = meas.real();
            new_msg.meas_im = meas.imag();
            send_query_msg(new_msg);
            break;
        }
        case FIPA_PERF_INFORM: {
            /* received previously querried measurement -> store it */
            std::complex<double> meas(msg.meas_re, msg.meas_im);
            switch (msg.meas_type) {
                case QUERYTYPE_VOLTAGE: {
                    knowledge_sens.update_voltage_meas(msg.sender, meas, msg.time);
                    break;
                }
            }
            break;
        }
        default: {
            // TODO ERROR HANDLING
        }
    }
}


/*! \brief handles a received message with Recruiting protocol (swarm forming)
 *  \param msg recruiting message
 * */
void Swarm_negotiator_behavior::process_recruiting_msg(swarm_recruiting_msg &msg)
{
    if (msg.base_type != BASETYPE_PRODUCER_INT) {
        // error: should not happen
    }
    swarm_subscribe_msg new_msg(FIPA_PERF_SUBSCRIBE, id, 0);

    /* decide which negotiation knowledge is affected */
    Negotiation_knowledge* know_neg;
    if (msg.neg_type == NEGOTIATIONTYPE_P_INT) {
        know_neg = &knowledge_neg_swarm_p;
    }
    else {
        know_neg = &knowledge_neg_swarm_q;
    }

    switch (msg.performative) {
        case FIPA_PERF_INFORM: {
            /* received an inform message -> adapt swarm size and store new producers */
            new_msg.service.svc_type = know_neg->get_negotiation_type();
            set_swarm_size(know_neg->get_negotiation_type(), msg.hops);
            know_neg->received_df_reply();
            for (unsigned int i = 0; i < msg.agents.size(); i++) {
                /* add unknown agents to swarm and subscribe to them */
                if (!know_neg->is_in_group(msg.agents[i].first) && 
                        !knowledge_comm.is_internal_agent(msg.agents[i].first)
                    && msg.agents[i].first != knowledge_comm.get_slack_agent() 
                    && msg.agents[i].first != knowledge_comm.get_substation_agent()) {
                    new_msg.receiver = msg.agents[i].first;
                    new_msg.dist = msg.agents[i].second;
                    send_subscribe_msg(new_msg);
                    knowledge_comm.add_swarmmember(msg.agents[i].first, 0, msg.agents[i].second);
                }
            }
            break;
        }
        default: {
            // TODO ERROR HANDLING
        }
    }
}


/*! \brief handles a received message with request protocol
 *  \param msg request message
 * */
void Swarm_negotiator_behavior::process_request_msg(swarm_request_msg &msg)
{
    switch (msg.performative) {
        case FIPA_PERF_REQUEST: {
            /* received request -> change voltage set point */
            if (msg.request_type == REQUESTTYPE_SETPOINT) {
                change_setpoint(msg.request_value);
            }
            break;
        }
        default: {
            // TODO ERROR HANDLING
        }
    }
}


/*! \brief if power demand is not covered, new negotiations with producers are started
 *  \param  know_neg    affected negotiation knowledge
 * */
void Swarm_negotiator_behavior::cover_power_demand(Negotiation_knowledge* know_neg)
{
    double threshold = get_threshold(know_neg);
    contract_ex power_lack = get_power_lack(know_neg);

    if (power_lack.needed < 0) {
        power_lack.needed = 0;
    }
    if (power_lack.optional < 0) {
        power_lack.optional = 0;
    }
    IO->log_info("\t" + know_neg->get_negotiation_header() + " lack: (" +
        std::to_string(power_lack.needed) + ") (" + std::to_string(power_lack.optional) + ")");

    /* while power is still missing */
    while (power_lack.needed > threshold || power_lack.optional > threshold) {
        struct contract_ex power_cfp = power_lack;
        power_cfp.consumer = id;
        /* pick one producer */
        power_cfp.producer = know_neg->get_suitable_producer(power_cfp, threshold);
        /* ToDo: check if 0 really is an invalid ID */
        if (power_cfp.producer != 0) {
            /* send call for proposal to producer and make new contract */
            if (power_cfp.needed > power_lack.needed) {
                power_cfp.needed = power_lack.needed;
            }
            if (power_cfp.optional > power_lack.optional) {
                power_cfp.optional = power_lack.optional;
            }
            swarm_contract_net_msg new_msg(FIPA_PERF_CALL_FOR_PROPOSAL, id,
                power_cfp.producer);
            new_msg.time = t_next;
            new_msg.power = power_cfp;
            send_contract_net_msg(new_msg);
            know_neg->new_contract(power_cfp.producer, id, power_cfp, t_next);
            /* recalculate remaining power lack */
            power_lack.needed -= power_cfp.needed;
            power_lack.optional -= power_cfp.optional;
        }
        else {
            break;
        }
    }
}


/*! \brief  if more power is contracted than needed, reduce contracted power
 *  \param  know_neg    affected negotiation knowledge
 * */
void Swarm_negotiator_behavior::release_contracted_power(Negotiation_knowledge* know_neg, int state,
    double threshold)
{
    struct contract_ex power_exceed;
    std::vector<Contract> _contracts;
    std::vector<Contract>::reverse_iterator it;
    struct contract_ex power_contract;
    power_contract.neg_type = know_neg->get_negotiation_type();
    bool change_contract = false;
    swarm_contract_net_msg new_msg(FIPA_PERF_INFORM, id, 0);
    new_msg.time = t_next;

    /* decide which contracts have to adapted according to prosumer's state */
    switch (state) {
        case BASETYPE_CONSUMER_INT:
            _contracts = know_neg->get_consumer_contracts();
            power_exceed = get_consumer_power_exceed(know_neg);
            break;
        case BASETYPE_PRODUCER_INT:
            _contracts = know_neg->get_producer_contracts();
            power_exceed = get_producer_power_exceed(know_neg);
            break;
    }

    if (power_exceed.needed < 0) {
        power_exceed.needed = 0;
    }
    if (power_exceed.optional < 0) {
        power_exceed.optional = 0;
    }
    IO->log_info("\t" + know_neg->get_negotiation_header() + " exceed: (" +
        std::to_string(power_exceed.needed) + ") (" + std::to_string(power_exceed.optional) + ")");

    it = _contracts.rbegin();
    /* while difference is still greater than 0, release more power */
    while ((power_exceed.needed > threshold || power_exceed.optional > threshold) && 
            it != _contracts.rend()) {
        power_contract = it->get_power();
        power_contract.consumer = it->get_consumer_id();
        power_contract.producer = it->get_producer_id();
        if (know_neg->open_contract_exists(power_contract.producer, power_contract.consumer)) {
            /* do not alter contract if there is still an open contract with the same agent */
            it++;
            continue;
        }
        change_contract = false;
        if (power_exceed.needed > 0 && power_contract.needed > 0) {
            /* too much needed power is contract and current contract contains needed power 
             * -> reduce it*/
            change_contract = true;
            if (power_exceed.needed < power_contract.needed) {
                /* current contract is sufficient to reduce total contract exceed */
                power_contract.needed -= power_exceed.needed;
                power_exceed.needed = 0;
            }
            else {
                /* current contract is not sufficient to reduce total contract exceed */
                power_exceed.needed -= power_contract.needed;
                power_contract.needed = 0;
            }
        }
        if (power_exceed.optional > 0 && power_contract.optional > 0) {
            /* too much optional power is contract and current contract contains needed power 
             * -> reduce it*/
            change_contract = true;
            if (power_exceed.optional < power_contract.optional) {
                /* current contract is sufficient to reduce total contract exceed */
                power_contract.optional -= power_exceed.optional;
                power_exceed.optional = 0;
            }
            else {
                /* current contract is not sufficient to reduce total contract exceed */
                power_exceed.optional -= power_contract.optional;
                power_contract.optional = 0;
            }
        }
        if (change_contract) {
            /* contact agent to inform it about the contract change */
            new_msg.power = power_contract;
            switch (state) {
                case BASETYPE_CONSUMER_INT:
                    new_msg.receiver = it->get_producer_id();
                    break;
                case BASETYPE_PRODUCER_INT:
                    new_msg.receiver = it->get_consumer_id();
                    break;
            }
            send_contract_net_msg(new_msg);
            /* update knowledge */
            know_neg->change_contract(it->get_producer_id(), it->get_consumer_id(), power_contract,
                t_next);
        }
        it++;
    }
}


/*! \brief  informs subscribed consumers if generation capabilities change
 *  \param  know_neg    affected negotiation knowledge
 * */
void Swarm_negotiator_behavior::update_services(Negotiation_knowledge* know_neg)
{
    double threshold = get_threshold(know_neg);
    struct service_ex svc_old = know_neg->get_producer_service(id);
    struct service_ex svc_new = get_new_service(know_neg);
    swarm_subscribe_msg new_msg(FIPA_PERF_INFORM, id, 0);

    if (svc_new.needed_max < threshold) {
        svc_new.needed_max = 0;
    }
    if (svc_new.optional_max < threshold) {
        svc_new.optional_max = 0;
    }
    if (svc_new.minimum < threshold) {
        svc_new.minimum = 0;
    }


    /* change service only if it has changed more than threshold and service is below 
     * 1000*threshold */
    if ((fabs(svc_new.needed_max - svc_old.needed_max) > threshold 
            && (svc_new.needed_max < 1000 * threshold || svc_old.needed_max < 1000 * threshold)) 
        || (fabs(svc_new.optional_max - svc_old.optional_max) > threshold 
            && (svc_new.optional_max < 1000 * threshold || svc_old.optional_max < 1000 * threshold))
        || (fabs(svc_new.minimum - svc_old.minimum) > threshold 
            && (svc_new.minimum < 1000 * threshold || svc_old.minimum < 1000 * threshold))) {
        IO->log_info("\tNew " + know_neg->get_negotiation_header() + " service: ("
            + std::to_string(svc_new.minimum) + ", " + std::to_string(svc_new.optional_max)
            + ", " + std::to_string(svc_new.needed_max) + ")");
        know_neg->update_producer_service(id, svc_new);
    }

    svc_new = know_neg->get_producer_service(id);
    if (know_neg->get_group_type() == GROUPTYPE_INTERNAL_INT) {
        /* send service directly whenever it changes */
        if (t_next > svc_new.t_last_update - (t_step / 2) - 0.0001 
                && t_next < svc_new.t_last_update + (t_step / 2)) {
            std::vector<std::pair<int, unsigned int>> _agents;
            know_neg->get_consumers(_agents);
            new_msg.service = svc_new;
            for (auto i : _agents) {
                new_msg.receiver = i.first;
                new_msg.dist = i.second;
                send_subscribe_msg(new_msg);
            }
        }
    }
    else if (know_neg->get_group_type() == GROUPTYPE_SWARM_INT) {
        std::vector<std::pair<int, unsigned int>> _agents;
        know_neg->get_consumers(_agents);
        if (svc_old.needed_max - svc_new.needed_max > threshold 
                || svc_old.optional_max - svc_new.optional_max > threshold
                || svc_old.minimum - svc_new.minimum > threshold) {
            /* if service has decreased send message whenever service has changed */
            if (t_next > svc_new.t_last_update - (t_step / 2) - 0.0001 
                    && t_next < svc_new.t_last_update + (t_step / 2)) {
                new_msg.service = svc_new;
                for (auto i : _agents) {
                    new_msg.receiver = i.first;
                    new_msg.dist = i.second;
                    send_subscribe_msg(new_msg);
                }
            }
        }
        else {
            /* send service when it has changed 10*(dist/20)s ago and did not change again since */
            new_msg.service = svc_new;
            for (auto i : _agents) {
                if (t_next > svc_new.t_last_update + (10 * i.second / 20.0) - (t_step / 2) - 0.0001 
                        && t_next < svc_new.t_last_update + (10 * i.second / 20.0) + (t_step / 2)) {
                    new_msg.receiver = i.first;
                    new_msg.dist = i.second;
                    send_subscribe_msg(new_msg);
                }
            }
        }
    }
    else {
        std::vector<std::pair<int, unsigned int>> _agents;
        know_neg->get_consumers(_agents);
        if (svc_old.needed_max - svc_new.needed_max > threshold 
            || svc_old.optional_max - svc_new.optional_max > threshold
            || svc_old.minimum - svc_new.minimum > threshold) {
            /* if service has decreased send message whenever service has changed */
            if (t_next > svc_new.t_last_update - (t_step / 2) - 0.0001 
                    && t_next < svc_new.t_last_update + (t_step / 2)) {
                new_msg.service = svc_new;
                for (auto i : _agents) {
                    new_msg.receiver = i.first;
                    new_msg.dist = i.second;
                    send_subscribe_msg(new_msg);
                }
            }
        }
        else {
            /* send service when it has changed 10*(dist/20)s ago and did not change again since */
            if (t_next > svc_new.t_last_update + 10 - (t_step / 2) - 0.0001 
                    && t_next < svc_new.t_last_update + 10 + (t_step / 2)) {
                new_msg.service = svc_new;
                for (auto i : _agents) {
                    new_msg.receiver = i.first;
                    new_msg.dist = i.second;
                    send_subscribe_msg(new_msg);
                }
            }
        }
    }
}


/*! \brief  decide whether or not a call for proposal can be answered with a proposal
 *  \param  _consumer_id    ID of consumer agent which is calling for a proposal
 *  \param  _power          [in|out] amount of power that is required | amount of power that can be
 *                          offered
 *  \return indicates whether or not a proposal can be sent
 * */
bool Swarm_negotiator_behavior::decide_cfp(int _consumer_id, struct contract_ex& _power)
{
    bool send_proposal = false;
    double needed_available = 0.0;
    double optional_available = 0.0;

    /* decide which negotiation knowledge is affected */
    Negotiation_knowledge* know_neg = get_affected_negotiation_knowledge(_consumer_id, 
        _power.neg_type);
    double threshold = get_threshold(know_neg);
    /* get currently available power */
    struct service_ex _service = get_new_service(know_neg);
    needed_available = _service.needed_max;
    optional_available = _service.optional_max;

    if (needed_available > 0 || optional_available > 0) {
        if (_power.needed > 0 && needed_available > 0) {
            /* needed power can be supplied */
            send_proposal = true;
            if (needed_available < _power.needed) {
                _power.needed = needed_available;
            }
        }
        if (_power.optional > 0 && optional_available - _power.needed > 0) {
            /* optional power can be supplied */
            send_proposal = true;
            if (optional_available < _power.needed + _power.optional) {
                _power.optional = optional_available - _power.needed;
            }
        }
        else {
            _power.optional = 0;
        }
    }
    else {
        /* no power can be supplied */
        _power.needed = 0;
        _power.optional = 0;
    }

    if (fabs(_power.needed) < threshold && fabs(_power.optional) < threshold) {
        /* do not send proposals below the threshold */
        send_proposal = false;
    }

    return send_proposal;
}


/*! \brief decide whether or not a proposal can be answered with accept
 *  \param _producer_id ID of producer agent which is sending a proposal
 *  \param _power [in|out] amount of power that is proposed
 *  \return indicates whether or not a accept can be sent
 * */
bool Swarm_negotiator_behavior::decide_proposal(int _producer_id, struct contract_ex& _power)
{
    /* currently proposales are always accepted */
    bool send_accept = true;
    return send_accept;
}


/*! \brief  determines the negotiation knowledge the specified agent belongs to
 *  \param  agent_id    ID of agent
 *  \param  neg_type    active or reactive power
 *  \return pointer to affected negotiation knowledge
 * */
Negotiation_knowledge* Swarm_negotiator_behavior::get_affected_negotiation_knowledge(int agent_id, 
    int neg_type)
{
    Negotiation_knowledge* know_neg;
    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        if (knowledge_comm.is_internal_agent(agent_id)) {
            know_neg = &knowledge_neg_internal_p;
        }
        else if (agent_id == knowledge_comm.get_substation_agent()) {
            know_neg = &knowledge_neg_substation_p;
        }
        else if (agent_id == knowledge_comm.get_slack_agent()) {
            know_neg = &knowledge_neg_slack_p;
        }
        else {
            know_neg = &knowledge_neg_swarm_p;
        }
    }
    else {
        if (knowledge_comm.is_internal_agent(agent_id)) {
            know_neg = &knowledge_neg_internal_q;
        }
        else if (agent_id == knowledge_comm.get_substation_agent()) {
            know_neg = &knowledge_neg_substation_q;
        }
        else if (agent_id == knowledge_comm.get_slack_agent()) {
            know_neg = &knowledge_neg_slack_q;
        }
        else {
            know_neg = &knowledge_neg_swarm_q;
        }
    }

    return know_neg;
}


/*! \brief  calculates the sum of all power values up to specified group type
 *  \param  neg_type    active or reactive power
 *  \param  group_type  internal, swarm, substation or slack
 *  \return sum of all power values up to group type
 * */
struct contract_ex Swarm_negotiator_behavior::get_power_sum(int neg_type, int group_type)
{
    struct contract_ex internal_p;
    struct contract_ex swarm_p;
    struct contract_ex substation_p;
    struct contract_ex slack_p;
    struct contract_ex power_sum;
    //double loss;

    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        power_sum.neg_type = NEGOTIATIONTYPE_P_INT;
        internal_p = knowledge_neg_internal_p.get_power();
        swarm_p = knowledge_neg_swarm_p.get_power();
        substation_p = knowledge_neg_substation_p.get_power();
        slack_p = knowledge_neg_slack_p.get_power();
        //loss = substation_data->P_loss;
    }
    else {
        power_sum.neg_type = NEGOTIATIONTYPE_Q_INT;
        internal_p = knowledge_neg_internal_q.get_power();
        swarm_p = knowledge_neg_swarm_q.get_power();
        substation_p = knowledge_neg_substation_q.get_power();
        slack_p = knowledge_neg_slack_q.get_power();
        //loss = substation_data->Q_loss;
    }

    if (group_type == GROUPTYPE_INTERNAL_INT) {
        power_sum.needed = internal_p.needed; //+ loss;
        power_sum.optional = internal_p.optional;
    }
    else if (group_type == GROUPTYPE_SWARM_INT) {
        power_sum.needed = internal_p.needed + swarm_p.needed; // + loss;
        power_sum.optional = internal_p.optional + swarm_p.optional;
    }
    else if (group_type == GROUPTYPE_SUBSTATION_INT) {
        power_sum.needed = internal_p.needed + swarm_p.needed + substation_p.needed; // + loss;
        power_sum.optional = internal_p.optional + swarm_p.optional + substation_p.optional;
    }
    else {
        power_sum.needed = internal_p.needed + swarm_p.needed + substation_p.needed +
            slack_p.needed; // + loss;
        power_sum.optional = internal_p.optional + swarm_p.optional + substation_p.optional +
            slack_p.optional;
    }
    return power_sum;
}


/*! \brief  calculates the sum of all open power values up to specified group type
 *  \param  neg_type    active or reactive power
 *  \param  group_type  internal, swarm, substation or slack
 *  \return sum of all open power values up to group type
 * */
struct contract_ex Swarm_negotiator_behavior::get_open_power_sum(int neg_type, int group_type)
{
    struct contract_ex open_internal_p;
    struct contract_ex open_swarm_p;
    struct contract_ex open_substation_p;
    struct contract_ex open_slack_p;
    struct contract_ex power_sum;

    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        open_internal_p = knowledge_neg_internal_p.get_open_power();
        open_swarm_p = knowledge_neg_swarm_p.get_open_power();
        open_substation_p = knowledge_neg_substation_p.get_open_power();
        open_slack_p = knowledge_neg_slack_p.get_open_power();
    }
    else {
        open_internal_p = knowledge_neg_internal_q.get_open_power();
        open_swarm_p = knowledge_neg_swarm_q.get_open_power();
        open_substation_p = knowledge_neg_substation_q.get_open_power();
        open_slack_p = knowledge_neg_slack_q.get_open_power();
    }

    if (group_type == GROUPTYPE_INTERNAL_INT) {
        power_sum.needed = open_internal_p.needed;
        power_sum.optional = open_internal_p.optional;
    }
    else if (group_type == GROUPTYPE_SWARM_INT) {
        power_sum.needed = open_internal_p.needed + open_swarm_p.needed;
        power_sum.optional = open_internal_p.optional + open_swarm_p.optional;
    }
    else if (group_type == GROUPTYPE_SUBSTATION_INT) {
        power_sum.needed = open_internal_p.needed + open_swarm_p.needed + open_substation_p.needed;
        power_sum.optional = open_internal_p.optional + open_swarm_p.optional +
            open_substation_p.optional;
    }
    else {
        power_sum.needed = open_internal_p.needed + open_swarm_p.needed + open_substation_p.needed 
            + open_slack_p.needed;
        power_sum.optional = open_internal_p.optional + open_swarm_p.optional +
            open_substation_p.optional + open_slack_p.optional;
    }
    return power_sum;
}


/*! \brief  calculates the sum of all service values up to specified group type
 *  \param  neg_type    active or reactive power
 *  \param  group_type  internal, swarm, substation or slack
 *  \return sum of all service values up to group type
 * */
struct service_ex Swarm_negotiator_behavior::get_service_sum(int neg_type, int group_type)
{
    struct service_ex int_svc;
    struct service_ex swarm_svc;
    struct service_ex sub_svc;
    struct service_ex slack_svc;
    struct service_ex sum_svc;

    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        int_svc = knowledge_neg_internal_p.get_service_sum();
        swarm_svc = knowledge_neg_swarm_p.get_service_sum();
        sub_svc = knowledge_neg_substation_p.get_service_sum();
        slack_svc = knowledge_neg_slack_p.get_service_sum();
    }
    else {
        int_svc = knowledge_neg_internal_q.get_service_sum();
        swarm_svc = knowledge_neg_swarm_q.get_service_sum();
        sub_svc = knowledge_neg_substation_q.get_service_sum();
        slack_svc = knowledge_neg_slack_q.get_service_sum();
    }

    if (group_type == GROUPTYPE_INTERNAL_INT) {
        sum_svc.minimum = int_svc.minimum;
        sum_svc.needed_max = int_svc.needed_max;
        sum_svc.optional_max = int_svc.optional_max;
    }
    else if (group_type == GROUPTYPE_SWARM_INT) {
        sum_svc.minimum = int_svc.minimum + swarm_svc.minimum;
        sum_svc.needed_max = int_svc.needed_max + swarm_svc.needed_max;
        sum_svc.optional_max = int_svc.optional_max + swarm_svc.optional_max;
    }
    else if (group_type == GROUPTYPE_SUBSTATION_INT) {
        sum_svc.minimum = int_svc.minimum + swarm_svc.minimum + sub_svc.minimum;
        sum_svc.needed_max = int_svc.needed_max + swarm_svc.needed_max + sub_svc.needed_max;
        sum_svc.optional_max = int_svc.optional_max + swarm_svc.optional_max + sub_svc.optional_max;
    }
    else {
        sum_svc.minimum = int_svc.minimum + swarm_svc.minimum + sub_svc.minimum + slack_svc.minimum;
        sum_svc.needed_max = int_svc.needed_max + swarm_svc.needed_max + sub_svc.needed_max 
            + slack_svc.needed_max;
        sum_svc.optional_max = int_svc.optional_max + swarm_svc.optional_max + sub_svc.optional_max 
            + slack_svc.optional_max;
    }
    return sum_svc;
}


/*! \brief  changes the state in all negotiation knowledge objects
 *  \param  neg_type    active or reactive power
 *  \param  state       new state (consumer or producer)
 * */
void Swarm_negotiator_behavior::change_state(int neg_type, int state)
{
    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        knowledge_neg_internal_p.set_state(state);
        knowledge_neg_swarm_p.set_state(state);
        knowledge_neg_substation_p.set_state(state);
        knowledge_neg_slack_p.set_state(state);
    }
    else {
        knowledge_neg_internal_q.set_state(state);
        knowledge_neg_swarm_q.set_state(state);
        knowledge_neg_substation_q.set_state(state);
        knowledge_neg_slack_q.set_state(state);
    }
}

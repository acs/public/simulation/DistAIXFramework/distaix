/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/swarmgrid/swarm_substation_behavior.h"
#include <tuple>


/*! \brief  constructor
 *  \param _id              unique ID of agent
 *  \param _type            agent type as defined in config.h
 *  \param _subtype         subtype as defined in config.h
 *  \param step_size        simulation step size
 *  \param logging          indicates if agents logs locally
 *  \param _substation_data pointer to substation specific data struct
 * */
Swarm_substation_behavior::Swarm_substation_behavior(int _id, int _type, std::string _subtype,
    double& step_size, struct data_props _d_props, Substation_data * _substation_data)
    : Swarm_negotiator_behavior(_id, _type, _subtype, step_size, _d_props),
    substation_data(_substation_data)
{
}

/*!
 * \brief Destroy the object (close log file)
 */
Swarm_substation_behavior::~Swarm_substation_behavior()
{
    IO->finish_io();
}


/*! \brief Initialize the agent behavior
 * \param components_ad_nodes [in] vector saving a list of connceted components for each node
 * \param connected_nodes [in] vector saving a list of connected nodes for each node
 * \param components_file [in] data contained in scenario components input file (integers)
 * \param subtypes_file [in] subtypes contained in scenario components input file (strings)
 * \param el_grid_file [in] electrical connections contained in el. grid input file (integers)
 * */
int Swarm_substation_behavior::initialize_agent_behavior(
        std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
        std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
        boost::multi_array<int, 2> *components_file,
        boost::multi_array<std::string, 1> *subtypes_file,
        boost::multi_array<int, 2> *el_grid_file
        ) {

    IO->log_info("### Determining initial communication partners ");
    /*genrate list of all node ids that belong to this trafo*/
    std::vector<int> trafo_node_ids;
    unsigned long number_of_components = components_file->shape()[1];

    for (unsigned int s = 0; s < number_of_components; s++) {
        int agent_type = components_file->data()[s+number_of_components*1];
        if(agent_type != TYPE_NODE_INT && agent_type != TYPE_TRANSFORMER_INT){
            //if agent is not a node and not a transformer: nothing to do for this agent
            continue;
        }

        int agent_id = components_file->data()[s+number_of_components*0];
        std::string id_trafo_str = subtypes_file->data()[s];
        auto id_trafo = (int) strtol(id_trafo_str.c_str(), nullptr, 10);


        if(agent_type == TYPE_NODE_INT && id_trafo == id){
            //std::get<2>(components_at_nodes[agent_id-1].front()) is the trafo id of the
            // transformers with id agent_id
            // if element is a node or trafo and belongs to subgrid of this trafo
            IO->log_info("Adding node " + std::to_string(agent_id) + " to list of nodes in subgrid of trafo");
            trafo_node_ids.push_back(agent_id);

            std::list<std::tuple<int,int,int>> conn = connected_nodes->at(agent_id-1);
            for(auto conn_node : conn){
                //check all connected nodes
                int conn_node_id = std::get<0>(conn_node);
                int conn_node_type = std::get<1>(conn_node);
                int conn_node_trafo_id = std::get<2>(conn_node);
                if(conn_node_type == TYPE_TRANSFORMER_INT && conn_node_trafo_id == id){
                    //if connected node is trafo and tafo id of this trafo equals id of current node
                    //the following if avoids double insertion of conn_node_id to trafo_node_ids
                    if(std::find(trafo_node_ids.begin(), trafo_node_ids.end(), conn_node_id) == trafo_node_ids.end()) {
                        IO->log_info("Adding trafo " + std::to_string(conn_node_id) + " to list of nodes in subgrid");
                        trafo_node_ids.push_back(conn_node_id);
                    }
                }
            }

        }else if((agent_type == TYPE_TRANSFORMER_INT
                  && std::get<2>(components_at_nodes->at(agent_id-1).front()) == id)){
            if(std::find(trafo_node_ids.begin(), trafo_node_ids.end(), agent_id) == trafo_node_ids.end()) {
                IO->log_info("Adding trafo " + std::to_string(agent_id) + " to list of nodes in subgrid");
                trafo_node_ids.push_back(agent_id);
            }
        }

    }
    IO->log_info("I have " + std::to_string(trafo_node_ids.size()) + " nodes in my subgrid");


    // iterate through list of node ids and find all components connected to these nodes
    for(auto t : trafo_node_ids) {
        std::list<std::tuple<int, int, int>> components = components_at_nodes->at(t-1);
        if(components.size() == 1 && std::get<1>(components.front()) == TYPE_TRANSFORMER_INT){
            //t is trafo
            if (std::get<2>(components.front()) == id) {
                IO->log_info("Adding trafo agent " + std::to_string(std::get<0>(components.front())) +
                             " to list of internal agents");
                knowledge_comm.add_internal(std::get<0>(components.front()));
            }
        }
        else {
            //t is node
            for (auto comp : components) {
                IO->log_info("Adding component agent " + std::to_string(std::get<0>(comp)) +
                             " to list of internal agents");
                knowledge_comm.add_internal(std::get<0>(comp));
            }
        }
    }


    /* determine neighborhood swarm of trafo */
    std::list<std::tuple<int,int,int>> node_adjacent = connected_nodes->at(id-1);

    for (auto node_adj : node_adjacent) {
        int adj_node_id=std::get<0>(node_adj);
        int adj_node_type = std::get<1>(node_adj);
        /* determine ID of trafo that this node belongs to */
        int sup_trafo_id = 0;
        for (unsigned int k = 0; k < components_file->shape()[1]; k++) {
            if (adj_node_id == components_file->data()[k+number_of_components*0]) {
                if (components_file->data()[k+number_of_components*1] == TYPE_NODE_INT) {
                    sup_trafo_id = std::stoi(subtypes_file->data()[k]);
                }
                break;
            }
        }

        if (sup_trafo_id == id) {
            /* node is part of transformers subgrid */
            continue;
        }
        else {
            /* node is part of superior grid (there is only one node with this property for this trafo) */
            if (adj_node_type == TYPE_NODE_INT && sup_trafo_id != 0) {
                //if sup_trafo_id != 0 this node is not a node between topmost trafo and slack
                IO->log_info("Adding transformer " + std::to_string(sup_trafo_id) +
                             " to neighborhood and swarm agents of this agent");
                knowledge_comm.add_neighbor(sup_trafo_id);
                knowledge_comm.add_swarmmember(sup_trafo_id, TYPE_TRANSFORMER_INT, 1);

            }
            else if (adj_node_type == TYPE_SLACK_INT || sup_trafo_id == 0) {
                //if sup_trafo_id == 0 the adjacent node is a node between topmost trafo and slack, add slack anyway
                /* if transformer is connected to the slack, add slack to swarm members */
                IO->log_info("Adding Slack to neighborhood and swarm agents of this agent");
                knowledge_comm.add_neighbor(SLACK_ID);
                knowledge_comm.add_swarmmember(SLACK_ID, TYPE_SLACK_INT, 1);
            }

            /* determine all neighboring nodes of the node next to the transformer */
            std::vector<std::pair<int,int>> neighbor_nodes;
            unsigned long number_of_connections = el_grid_file->shape()[1];
            for (unsigned int k = 0; k < number_of_connections; k++) {
                int id1 = el_grid_file->data()[k+number_of_connections*0];
                int id2 = el_grid_file->data()[k+number_of_connections*1];
                //neighboring node should not be trafo itself
                if (id1 == adj_node_id && id2 != id) {
                    int agent_type = components_file->data()[id2-1 + number_of_components*1];
                    if(agent_type == TYPE_NODE_INT) {
                        std::pair<int, int> new_id(id2, agent_type);
                        neighbor_nodes.push_back(new_id);
                    }
                } else if (id2 == adj_node_id && id1 != id) {
                    int agent_type = components_file->data()[id1-1 + number_of_components*1];
                    if(agent_type == TYPE_NODE_INT) {
                        std::pair<int, int> new_id(id1, agent_type);
                        neighbor_nodes.push_back(new_id);
                    }
                }
            }

            /* iterate through neighboring nodes of the node next to the transformer
             * and add connected components to neighborhood swarm*/
            for (auto &u : neighbor_nodes) {
                if(u.second == TYPE_NODE_INT) {
                    IO->log_info("Checking node " + std::to_string(u.first));
                    std::list<std::tuple<int, int, int>> components
                            = components_at_nodes->at(u.first - 1);
                    for (auto comp : components) {
                        int comp_ID = std::get<0>(comp);
                        int comp_type = std::get<1>(comp);
                        IO->log_info("Adding agent (ID type) (" + std::to_string(comp_ID)
                                     + "," + std::to_string(comp_type) +
                                     ") to neighborhood and swarm agents of this agent");
                        knowledge_comm.add_neighbor(comp_ID);
                        knowledge_comm.add_swarmmember(comp_ID, comp_type, 1);
                    }
                }
            } //iterating through neighbor nodes vector
        } //node is NOT part of substation's subgrid
    }// iterating through nodes adjacent to this substation

    /* add DF agent to swarm members */
    IO->log_info("Adding DF to neighborhood and swarm agents of this agent");
    knowledge_comm.add_neighbor(TYPE_DF_INT);
    knowledge_comm.add_swarmmember(TYPE_DF_INT, TYPE_DF_INT, 1);

    IO->log_info("### FINISHED Determining initial communication partners ");
    return 0;
}

/*! \brief processes messages, executes behavior rules, adjusts control values
 * */
void Swarm_substation_behavior::execute_agent_behavior()
{
    if (t_next == t_start) {
        IO->log_info("Initial actions");
        initial_actions();
    }

    IO->log_info("Process incoming messages");
    process_incoming_messages();

    // IO.log_info("\tDetermine grid losses");
    // determine_grid_losses();

    IO->log_info("\tDetermine state");
    determine_state(NEGOTIATIONTYPE_P_INT);
    determine_state(NEGOTIATIONTYPE_Q_INT);

    IO->log_info("Cover power demand");
    cover_power_demand(&knowledge_neg_internal_p);
    if (knowledge_neg_internal_p.get_state() == BASETYPE_CONSUMER_INT) {
        cover_power_demand(&knowledge_neg_swarm_p);
        cover_power_demand(&knowledge_neg_substation_p);
        cover_power_demand(&knowledge_neg_slack_p);
    }
    cover_power_demand(&knowledge_neg_internal_q);
    if (knowledge_neg_internal_q.get_state() == BASETYPE_CONSUMER_INT) {
        cover_power_demand(&knowledge_neg_swarm_q);
        cover_power_demand(&knowledge_neg_substation_q);
        cover_power_demand(&knowledge_neg_slack_q);
    }

    IO->log_info("Release unused power");
    if (knowledge_neg_internal_p.get_state() == BASETYPE_CONSUMER_INT) {
        release_contracted_power(&knowledge_neg_internal_p, BASETYPE_CONSUMER_INT, 
            get_threshold(&knowledge_neg_internal_p));
        release_contracted_power(&knowledge_neg_swarm_p, BASETYPE_CONSUMER_INT,
            get_threshold(&knowledge_neg_swarm_p));
        release_contracted_power(&knowledge_neg_substation_p, BASETYPE_CONSUMER_INT, 
            get_threshold(&knowledge_neg_substation_p));
        release_contracted_power(&knowledge_neg_slack_p, BASETYPE_CONSUMER_INT,
            get_threshold(&knowledge_neg_slack_p));
    }
    else {
        release_contracted_power(&knowledge_neg_slack_p, BASETYPE_CONSUMER_INT, 
            get_threshold(&knowledge_neg_internal_p));
        release_contracted_power(&knowledge_neg_substation_p, BASETYPE_CONSUMER_INT, 
            get_threshold(&knowledge_neg_internal_p));
        release_contracted_power(&knowledge_neg_swarm_p, BASETYPE_CONSUMER_INT, 
            get_threshold(&knowledge_neg_internal_p));
        release_contracted_power(&knowledge_neg_internal_p, BASETYPE_CONSUMER_INT, 
            get_threshold(&knowledge_neg_internal_p));
    }
    if (knowledge_neg_internal_q.get_state() == BASETYPE_CONSUMER_INT) {
        release_contracted_power(&knowledge_neg_internal_q, BASETYPE_CONSUMER_INT, 
            get_threshold(&knowledge_neg_internal_q));
        release_contracted_power(&knowledge_neg_swarm_q, BASETYPE_CONSUMER_INT,
            get_threshold(&knowledge_neg_swarm_q));
        release_contracted_power(&knowledge_neg_substation_q, BASETYPE_CONSUMER_INT, 
            get_threshold(&knowledge_neg_substation_q));
        release_contracted_power(&knowledge_neg_slack_q, BASETYPE_CONSUMER_INT,
            get_threshold(&knowledge_neg_slack_q));
    }
    else {
        release_contracted_power(&knowledge_neg_slack_q, BASETYPE_CONSUMER_INT, 
            get_threshold(&knowledge_neg_internal_q));
        release_contracted_power(&knowledge_neg_substation_q, BASETYPE_CONSUMER_INT, 
            get_threshold(&knowledge_neg_internal_q));
        release_contracted_power(&knowledge_neg_swarm_q, BASETYPE_CONSUMER_INT, 
            get_threshold(&knowledge_neg_internal_q));
        release_contracted_power(&knowledge_neg_internal_q, BASETYPE_CONSUMER_INT, 
            get_threshold(&knowledge_neg_internal_q));
    }

    IO->log_info("Adapt contracts to generation");
    if (knowledge_neg_internal_p.get_state() == BASETYPE_PRODUCER_INT) {
        release_contracted_power(&knowledge_neg_internal_p, BASETYPE_PRODUCER_INT, 
            get_threshold(&knowledge_neg_internal_p));
        release_contracted_power(&knowledge_neg_swarm_p, BASETYPE_PRODUCER_INT,
            get_threshold(&knowledge_neg_swarm_p));
        release_contracted_power(&knowledge_neg_substation_p, BASETYPE_PRODUCER_INT, 
            get_threshold(&knowledge_neg_substation_p));
        release_contracted_power(&knowledge_neg_slack_p, BASETYPE_PRODUCER_INT,
            get_threshold(&knowledge_neg_slack_p));
    }
    else {
        release_contracted_power(&knowledge_neg_slack_p, BASETYPE_PRODUCER_INT, 
            get_threshold(&knowledge_neg_internal_p));
        release_contracted_power(&knowledge_neg_substation_p, BASETYPE_PRODUCER_INT, 
            get_threshold(&knowledge_neg_internal_p));
        release_contracted_power(&knowledge_neg_swarm_p, BASETYPE_PRODUCER_INT, 
            get_threshold(&knowledge_neg_internal_p));
        release_contracted_power(&knowledge_neg_internal_p, BASETYPE_PRODUCER_INT, 
            get_threshold(&knowledge_neg_internal_p));
    }
    if (knowledge_neg_internal_q.get_state() == BASETYPE_PRODUCER_INT) {
        release_contracted_power(&knowledge_neg_internal_q, BASETYPE_PRODUCER_INT, 
            get_threshold(&knowledge_neg_internal_q));
        release_contracted_power(&knowledge_neg_swarm_q, BASETYPE_PRODUCER_INT,
            get_threshold(&knowledge_neg_swarm_q));
        release_contracted_power(&knowledge_neg_substation_q, BASETYPE_PRODUCER_INT, 
            get_threshold(&knowledge_neg_substation_q));
        release_contracted_power(&knowledge_neg_slack_q, BASETYPE_PRODUCER_INT,
            get_threshold(&knowledge_neg_slack_q));
    }
    else {
        release_contracted_power(&knowledge_neg_slack_q, BASETYPE_PRODUCER_INT, 
            get_threshold(&knowledge_neg_internal_q));
        release_contracted_power(&knowledge_neg_substation_q, BASETYPE_PRODUCER_INT, 
            get_threshold(&knowledge_neg_internal_q));
        release_contracted_power(&knowledge_neg_swarm_q, BASETYPE_PRODUCER_INT, 
            get_threshold(&knowledge_neg_internal_q));
        release_contracted_power(&knowledge_neg_internal_q, BASETYPE_PRODUCER_INT, 
            get_threshold(&knowledge_neg_internal_q));
    }

    IO->log_info("Update services");
    update_services(&knowledge_neg_internal_p);
    update_services(&knowledge_neg_swarm_p);
    update_services(&knowledge_neg_substation_p);
    update_services(&knowledge_neg_slack_p);
    update_services(&knowledge_neg_internal_q);
    update_services(&knowledge_neg_swarm_q);
    update_services(&knowledge_neg_substation_q);
    update_services(&knowledge_neg_slack_q);

    IO->log_info("Determine swarm size");
    determine_swarm_size(NEGOTIATIONTYPE_P_INT);
    determine_swarm_size(NEGOTIATIONTYPE_Q_INT);

    IO->log_info("Query voltage measurements");
    query_voltage_meas();

    IO->log_info("Set point:" + std::to_string(substation_data->v2_setpoint));

    save_voltage_measurement();
	
    knowledge_comm.log_communication_knowledge();
    knowledge_neg_internal_p.log_negotiation_knowledge();
    knowledge_neg_swarm_p.log_negotiation_knowledge();
    knowledge_neg_substation_p.log_negotiation_knowledge();
    knowledge_neg_slack_p.log_negotiation_knowledge();
    knowledge_neg_internal_q.log_negotiation_knowledge();
    knowledge_neg_swarm_q.log_negotiation_knowledge();
    knowledge_neg_substation_q.log_negotiation_knowledge();
    knowledge_neg_slack_q.log_negotiation_knowledge();
    knowledge_sens.log_sensor_knowledge();

    apply_control_values();
}


/*! \brief applies control values according to agent's contracts
 * */
void Swarm_substation_behavior::apply_control_values()
{
    /* determine optimal tap position and apply it */
    if (knowledge_sens.all_voltage_meas_updated()) {
        double v_mean = knowledge_sens.get_mean_voltage();
        double v_min = knowledge_sens.get_min_voltage();
        double v_max = knowledge_sens.get_max_voltage();
        double v_median = v_min + ((v_max-v_min)/2);
        substation_data->v_min = v_min;
        substation_data->v_max = v_max;
        substation_data->v_mean = v_mean;
        substation_data->v_median = v_median;
        double v_hv = knowledge_sens.get_absolute_voltage(id);
        struct service_ex slack_svc = knowledge_neg_slack_q.get_service_sum();
        struct service_ex sub_svc = knowledge_neg_slack_q.get_producer_service(id);
        IO->log_info("\tmin voltage : " + std::to_string(v_min) + "pu, mean voltage: " +
            std::to_string(v_mean) + "pu, max voltage: " + std::to_string(v_max) +
            "pu, median voltage: " + std::to_string(v_median) + "pu, high voltage side: " +
            std::to_string(v_hv) + "pu");
        int n_delta = 0;
        double v_setpoint_temp = substation_data->v2_setpoint;

        if (substation_data->N > 0 && substation_data->range > 0.001) {
            if (v_hv < 0.95) {
                /* voltage in superior grid is low -> decrease voltage in own grid as much as possible
                 * in order to stimulate DERs to behave capacitively
                 * determine tap such that v_min - delta_n * r/N > 0.9 + 1.0 * r/N */
                n_delta = floor(-0.9 * ((100.0*substation_data->N) / substation_data->range) - 1.0 + v_min
                    * ((100.0*substation_data->N) / substation_data->range));
            }
            else if (v_hv > 1.05) {
                /* voltage in superior grid is high -> increase voltage in own grid as much as possible
                 * in order to stimulate DERs to behave inductively
                 * determin tap such that v_max - n_delta * r/N < 1.1 - 1.0 * r/N */
                n_delta = ceil(-1.1 * ((100.0*substation_data->N) / substation_data->range) + 1.0 + v_max
                    * ((100.0*substation_data->N) / substation_data->range));
            }
            else if (knowledge_comm.get_slack_agent() != 0
                && slack_svc.needed_max < substation_data->Vnom1*NEGOTIATION_THRESHOLD_FACTOR) {
                //&& sub_svc.needed_max < 10*substation_data->Vnom1*NEGOTIATION_THRESHOLD_FACTOR) {
                /* slack cannot be provided with reactive power -> decrease voltage as much as possible
                 * in order to have reactive power flexibility */
                n_delta = floor(-0.9 * ((100.0*substation_data->N) / substation_data->range) - 1.0 + v_min
                    * ((100.0*substation_data->N) / substation_data->range));
                if (n_delta > 1) {
                    n_delta = 1;
                }
                else if (n_delta < 0) {
                    if (v_min > 0.9125) {
                        n_delta = 0;
                    }
                }
                substation_data->v1_setpoint = 1.0 + (substation_data->range / (100.0 * substation_data->N));
            }
            else if (knowledge_comm.get_slack_agent() != 0
                && slack_svc.optional_max > substation_data->Vnom1*NEGOTIATION_THRESHOLD_FACTOR) {
                /* slack wants to supply more reactive power -> increase voltage as much as possible
                 * in order to have reactive power flexibility */
                n_delta = ceil(-1.1 * ((100.0*substation_data->N) / substation_data->range) + 1.0 + v_max
                    * ((100.0*substation_data->N) / substation_data->range));
                if (n_delta < -1) {
                    n_delta = -1;
                }
                substation_data->v1_setpoint = 1.0 - (substation_data->range / (100.0 * substation_data->N));
            }
            //else if (knowledge_comm.get_slack_agent() != 0
            //    && slack_svc.needed_max < substation_data->Vnom1*NEGOTIATION_THRESHOLD_FACTOR) {
            //    /* slack can not supply reactive power -> decrease voltage as much as possible in order
            //     * to have reactive power flexibility */
            //    n_delta = floor(-0.9 * ((100.0*substation_data->N) / substation_data->range) - 1.5 + v_min
            //        * ((100.0*substation_data->N) / substation_data->range));
            //    substation_data->v1_setpoint = 1.0 + (substation_data->range / (100.0 * substation_data->N));
            //}
            else {
                /* voltage in superior grid is ok -> optimize voltage in own grid
                 * determine boundaries of tap setting regarding extreme values of volatge */
                if (knowledge_comm.get_slack_agent() != 0) {
                    substation_data->v1_setpoint = 1.0;
                }
                int n_delta_upper = floor(((100.0*substation_data->N) /
                    substation_data->range) * (v_min - 0.9));
                int n_delta_lower = ceil(((100.0*substation_data->N) /
                    substation_data->range) * (v_max - 1.1));
                double delta_temp;
                //if (v_min > 0.9 + 0.5 * (substation_data->range/(100.0*substation_data->N)) && v_max < 1.1 - 0.5 * (substation_data->range/(100.0*substation_data->N))) {
                //if (v_min > 0.95 && v_max < 1.05) {
                //    /* determine optimal set point regarding mean voltage */
                //    delta_temp = (v_mean - 1.0) / (substation_data->range / (100.0 * substation_data->N));
                //}
                //else {
                //    /* determine optimal set point regarding median voltage */
                //    delta_temp = (v_median - 1.0) / (substation_data->range / (100.0 * substation_data->N));
                //}
                delta_temp = ((v_mean+v_median)/2.0 -1.0) / (substation_data->range / (100.0 * substation_data->N));
                //if (v_min > 0.925 && v_max < 1.075) {
                //    if (fabs(delta_temp) > 1.0) {
                //        n_delta = round(delta_temp);
                //    }
                //} else {
                    if (fabs(delta_temp) > 0.6) {
                        n_delta = round(delta_temp);
                    }
                //}
                if (n_delta_lower <= n_delta_upper) {
                    /* if extreme values of voltage can be maintained within limits */
                    if (n_delta < n_delta_lower) {
                        n_delta = n_delta_lower;
                    }
                    else if (n_delta >= n_delta_upper) {
                        n_delta = n_delta_upper;
                    }
                }
            }

            /* apply tap change to control value */
            substation_data->n_ctrl = substation_data->n + n_delta;
            if (substation_data->n_ctrl < -substation_data->N) {
                substation_data->n_ctrl = -substation_data->N;
            }
            else if (substation_data->n_ctrl > substation_data->N) {
                substation_data->n_ctrl = substation_data->N;
            }

            /* determine new voltage set point for internal agents */
            if (substation_data->n_ctrl < 0) {
                if (substation_data->v1_setpoint <= (1.0 + 0.0001)) {
                    substation_data->v2_setpoint = substation_data->v1_setpoint
                        + 0.5 * (substation_data->range / (100.0 * substation_data->N));
                }
                else {
                    substation_data->v2_setpoint = substation_data->v1_setpoint;
                }
            }
            else if (substation_data->n_ctrl > 0) {
                if (substation_data->v1_setpoint >= (1.0 - 0.0001)) {
                    substation_data->v2_setpoint = substation_data->v1_setpoint
                        - 0.5 * (substation_data->range / (100.0 * substation_data->N));
                }
                else {
                    substation_data->v2_setpoint = substation_data->v1_setpoint;
                }
            }
            else {
                substation_data->v2_setpoint = substation_data->v1_setpoint;
            }
        } else {
            substation_data->n_ctrl = 0;
            substation_data->v2_setpoint = substation_data->v1_setpoint;
        }
        if (fabs(substation_data->v2_setpoint - v_setpoint_temp) > 0.0001) {
            /* set point has changed -> inform internal agents */
            request_setpoint_change();
        }

        IO->log_info("\tTap: " + std::to_string(substation_data->n_ctrl));
        knowledge_sens.reset_voltage_meas();
    }
}


/*! \brief executes action in the first time step
 * */
void Swarm_substation_behavior::initial_actions()
{
    /* subscribe to all internal and swarm agents for active power */
    swarm_subscribe_msg new_msg(FIPA_PERF_SUBSCRIBE, id, 0);
    new_msg.dist = 0;
    new_msg.service.svc_type = NEGOTIATIONTYPE_P_INT;

    std::vector<int> _agents;
    knowledge_comm.get_internal_agents(_agents);
    for (auto i : _agents) {
        new_msg.receiver = i;
        send_subscribe_msg(new_msg);
    }

    std::vector<std::pair<int, unsigned int>> _agents_;
    knowledge_comm.get_swarm_agents(_agents_);
    for (auto i : _agents_) {
        new_msg.receiver = i.first;
        new_msg.dist = i.second;
        send_subscribe_msg(new_msg);
    }

    /* subscribe to all internal and swarm agents for reactive power */
    new_msg.service.svc_type = NEGOTIATIONTYPE_Q_INT;
    for (auto i : _agents) {
        new_msg.receiver = i;
        send_subscribe_msg(new_msg);
    }
    for (auto i : _agents_) {
        new_msg.receiver = i.first;
        new_msg.dist = i.second;
        send_subscribe_msg(new_msg);
    }

    /* if substation is connected directly to the slack, subscribe to it */
    int slack_id = knowledge_comm.get_slack_agent();
    new_msg.receiver = slack_id;
    if (slack_id != 0) {
        new_msg.service.svc_type = NEGOTIATIONTYPE_P_INT;
        send_subscribe_msg(new_msg);
        new_msg.service.svc_type = NEGOTIATIONTYPE_Q_INT;
        send_subscribe_msg(new_msg);
    }

    /* store own active power supply as service */
    struct service_ex _service;
    _service.svc_type = NEGOTIATIONTYPE_P_INT;
    knowledge_neg_internal_p.new_producer_service(id, _service);
    knowledge_neg_swarm_p.new_producer_service(id, _service);
    knowledge_neg_substation_p.new_producer_service(id, _service);
    knowledge_neg_slack_p.new_producer_service(id, _service);

    /* store own reactive power supply as service */
    _service.svc_type = NEGOTIATIONTYPE_Q_INT;
    knowledge_neg_internal_q.new_producer_service(id, _service);
    knowledge_neg_swarm_q.new_producer_service(id, _service);
    knowledge_neg_substation_q.new_producer_service(id, _service);
    knowledge_neg_slack_q.new_producer_service(id, _service);

    /* query voltage measurements of 50% of agents */
    struct swarm_query_msg new_query_msg(FIPA_PERF_QUERY_REF, id, 0);
    new_query_msg.meas_type = QUERYTYPE_VOLTAGE;
    unsigned int number_of_meas = _agents.size() / 2;
    if (number_of_meas > 0) {
        for (unsigned int i = 0; i < number_of_meas; i++) {
            new_query_msg.receiver = _agents[i * _agents.size() / number_of_meas];
            send_query_msg(new_query_msg);
        }
    }
    else {
        if (_agents.size() > 0) {
            int ref_ag = _agents.size()/2;
            new_query_msg.receiver = _agents[ref_ag];
            send_query_msg(new_query_msg);
        }
    }
}


/*! \brief checks whether state has to be changed from producer to consumer
 *  \param  neg_type    active or reactive power
 * */
void Swarm_substation_behavior::determine_state(int neg_type)
{
    struct contract_ex power_sum;
    struct contract_ex open_power_sum;
    struct service_ex swarm_service;
    struct contract_ex internal_sum;
    struct contract_ex internal_open_sum;

    int state;

    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        power_sum = get_power_sum(NEGOTIATIONTYPE_P_INT, GROUPTYPE_SLACK_INT);
        open_power_sum = get_open_power_sum(NEGOTIATIONTYPE_P_INT, GROUPTYPE_SLACK_INT);
        internal_sum = knowledge_neg_internal_p.get_power();
        internal_open_sum = knowledge_neg_internal_p.get_open_power();
        power_sum.needed -= internal_sum.needed;
        power_sum.optional -= internal_sum.optional;
        open_power_sum.needed -= internal_open_sum.needed;
        open_power_sum.optional -= internal_open_sum.optional;
        swarm_service = knowledge_neg_swarm_p.get_producer_service(id);
        state = knowledge_neg_swarm_p.get_state();
    }
    else {
        power_sum = get_power_sum(NEGOTIATIONTYPE_Q_INT, GROUPTYPE_SLACK_INT);
        open_power_sum = get_open_power_sum(NEGOTIATIONTYPE_Q_INT, GROUPTYPE_SLACK_INT);
        internal_sum = knowledge_neg_internal_q.get_power();
        internal_open_sum = knowledge_neg_internal_q.get_open_power();
        power_sum.needed -= internal_sum.needed;
        power_sum.optional -= internal_sum.optional;
        open_power_sum.needed -= internal_open_sum.needed;
        open_power_sum.optional -= internal_open_sum.optional;
        swarm_service = knowledge_neg_swarm_q.get_producer_service(id);
        state = knowledge_neg_swarm_q.get_state();
    }

    /* substation becomes consumer in swarm if there are no swarm contracts as producer and there is
     * no service to be published in swarm */
    if (state == BASETYPE_PRODUCER_INT) {
        if (power_sum.needed + power_sum.optional + open_power_sum.needed + open_power_sum.optional > 
            -NEGOTIATION_THRESHOLD_FACTOR * substation_data->Vnom1 //) {
                && swarm_service.needed_max < NEGOTIATION_THRESHOLD_FACTOR * substation_data->Vnom1) {
            change_state(neg_type, BASETYPE_CONSUMER_INT);
            state = BASETYPE_CONSUMER_INT;
        }
    }
    /* substation becomes producer in swarm if there are no swarm contracts as consumer and a
     * service can be advertised in swarm */
    else {
        if (power_sum.needed + power_sum.optional + open_power_sum.needed + open_power_sum.optional < 
            NEGOTIATION_THRESHOLD_FACTOR * substation_data->Vnom1
            && swarm_service.needed_max > NEGOTIATION_THRESHOLD_FACTOR * substation_data->Vnom1) {
            change_state(neg_type, BASETYPE_PRODUCER_INT);
            state = BASETYPE_PRODUCER_INT;
        }
    }

    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        IO->log_info("\tActive power state: " + std::to_string(state));
    }
    else {
        IO->log_info("\tReactive power state: " + std::to_string(state));
    }
}


/*! \brief get the missing power for specified group
 *  \param  know_neg    affected negotiation knowledge
 *  \return power lack
 * */
contract_ex Swarm_substation_behavior::get_power_lack(Negotiation_knowledge* know_neg)
{
    struct contract_ex power_sum;
    struct contract_ex open_power_sum;
    if (know_neg->get_state() == BASETYPE_CONSUMER_INT) {
        power_sum = get_power_sum(know_neg->get_negotiation_type(), know_neg->get_group_type());
        open_power_sum = get_open_power_sum(know_neg->get_negotiation_type(),
            know_neg->get_group_type());
    }
    else {
        power_sum = get_power_sum(know_neg->get_negotiation_type(), GROUPTYPE_SLACK_INT);
        open_power_sum = get_open_power_sum(know_neg->get_negotiation_type(), GROUPTYPE_SLACK_INT);
    }

    /* determine the amount of power that is missing */
    struct contract_ex power_lack;
    power_lack.neg_type = know_neg->get_negotiation_type();

    /* power has to be balanced */
    power_lack.needed = -(power_sum.needed + open_power_sum.needed);
    power_lack.optional = -(power_sum.optional + open_power_sum.optional);

    return power_lack;
}


/*! \brief  consumer power exceed
 *  \param  know_neg    affected negotiation knowlegde
 *  \return consumer power exceed
 * */
contract_ex Swarm_substation_behavior::get_consumer_power_exceed(Negotiation_knowledge* know_neg)
{
    struct contract_ex power_sum;
    struct contract_ex open_power_sum;
    if (know_neg->get_state() == BASETYPE_CONSUMER_INT) {
        power_sum = get_power_sum(know_neg->get_negotiation_type(), know_neg->get_group_type());
        open_power_sum = get_open_power_sum(know_neg->get_negotiation_type(),
            know_neg->get_group_type());
    }
    else {
        power_sum = get_power_sum(know_neg->get_negotiation_type(), GROUPTYPE_SLACK_INT);
        open_power_sum = get_open_power_sum(know_neg->get_negotiation_type(), GROUPTYPE_SLACK_INT);
    }

    /* determine difference between contracted and needed power */
    struct contract_ex power_exceed;
    power_exceed.neg_type = know_neg->get_negotiation_type();

    /* power has to be balanced */
    power_exceed.needed = power_sum.needed + open_power_sum.needed;
    power_exceed.optional = power_sum.optional + open_power_sum.optional;

    return power_exceed;
}


/*! \brief  producer power exceed
 *  \param  know_neg    affected negotiation knowlegde
 *  \return producer power exceed
 * */
contract_ex Swarm_substation_behavior::get_producer_power_exceed(Negotiation_knowledge* know_neg)
{
    struct service_ex sum_svc;
    if (know_neg->get_group_type() == GROUPTYPE_INTERNAL_INT) {
        sum_svc = get_service_sum(know_neg->get_negotiation_type(), GROUPTYPE_SLACK_INT);
    }
    else {
        sum_svc = get_service_sum(know_neg->get_negotiation_type(), GROUPTYPE_INTERNAL_INT);
    }

    struct contract_ex power_sum;
    struct contract_ex open_power_sum;
    if (know_neg->get_state() == BASETYPE_CONSUMER_INT) {
        power_sum = get_power_sum(know_neg->get_negotiation_type(), GROUPTYPE_SLACK_INT);
        open_power_sum = get_open_power_sum(know_neg->get_negotiation_type(), GROUPTYPE_SLACK_INT);
    }
    else {
        power_sum = get_power_sum(know_neg->get_negotiation_type(), know_neg->get_group_type());
        open_power_sum = get_open_power_sum(know_neg->get_negotiation_type(),
            know_neg->get_group_type());
    }

    /* determine difference between contracted and needed power */
    struct contract_ex power_exceed;
    power_exceed.neg_type = know_neg->get_negotiation_type();

    power_exceed.needed = -(power_sum.needed + open_power_sum.needed) - sum_svc.needed_max;
    power_exceed.optional = -(power_sum.optional + open_power_sum.optional) - sum_svc.optional_max;

    return power_exceed;
}


/*! \brief  recalculate prodcuer service
*   \param  know_neg    affected negotiation knowlegde
*   \return new service
*/
service_ex Swarm_substation_behavior::get_new_service(Negotiation_knowledge* know_neg)
{
    struct service_ex sum_svc;
    if (know_neg->get_group_type() == GROUPTYPE_INTERNAL_INT) {
        sum_svc = get_service_sum(know_neg->get_negotiation_type(), GROUPTYPE_SLACK_INT);
    }
    else {
        sum_svc = get_service_sum(know_neg->get_negotiation_type(), GROUPTYPE_INTERNAL_INT);
    }

    struct contract_ex power_sum;
    struct contract_ex open_power_sum;
    if (know_neg->get_state() == BASETYPE_CONSUMER_INT) {
        power_sum = get_power_sum(know_neg->get_negotiation_type(), GROUPTYPE_SLACK_INT);
        open_power_sum = get_open_power_sum(know_neg->get_negotiation_type(), GROUPTYPE_SLACK_INT);
    }
    else {
        power_sum = get_power_sum(know_neg->get_negotiation_type(), know_neg->get_group_type());
        open_power_sum = get_open_power_sum(know_neg->get_negotiation_type(),
            know_neg->get_group_type());
    }

    struct service_ex service_new;
    service_new.svc_type = know_neg->get_negotiation_type();
    service_new.t_last_update = t_next;

    service_new.needed_max = sum_svc.needed_max + power_sum.needed + open_power_sum.needed;
    service_new.optional_max = sum_svc.optional_max + power_sum.optional +
        open_power_sum.optional;
    service_new.minimum = sum_svc.minimum + power_sum.needed + open_power_sum.needed +
        power_sum.optional + open_power_sum.optional;

    return service_new;
}


/*! \brief determine the effective swarm size and adjust list of known agents accordingly
 * \param neg_type Negotiation type (P or Q)
 * */
void Swarm_substation_behavior::determine_swarm_size(int neg_type)
{
    unsigned int swarm_size = 0;
    Negotiation_knowledge* know_neg_swarm;
    Negotiation_knowledge* know_neg_substation;
    int state;

    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        know_neg_swarm = &knowledge_neg_swarm_p;
        know_neg_substation = &knowledge_neg_substation_p;
        swarm_size = substation_data->swarm_size_p;
        state = knowledge_neg_internal_p.get_state();
    }
    else {
        know_neg_swarm = &knowledge_neg_swarm_q;
        know_neg_substation = &knowledge_neg_substation_q;
        swarm_size = substation_data->swarm_size_q;
        state = knowledge_neg_internal_q.get_state();
    }

    if (state == BASETYPE_CONSUMER_INT) {
        struct service_ex swarm_svc = know_neg_swarm->get_service_sum();
        if (swarm_svc.needed_max < substation_data->S_r / 10) {
            /* swarm has to grow */
            if (!know_neg_swarm->is_waiting_for_df()) {
                /* send message to DF */
                int receiver = knowledge_comm.get_df_agent();
                swarm_size++;
                if (swarm_size < MAX_SWARM_SIZE-1) {
                    swarm_recruiting_msg new_msg(FIPA_PERF_PROXY, id, receiver);
                    new_msg.base_type = BASETYPE_PRODUCER_INT;
                    new_msg.hops = swarm_size;
                    new_msg.neg_type = neg_type;
                    new_msg.node_sender = substation_data->node_id;
                    send_recruiting_msg(new_msg);
                    know_neg_swarm->contacted_df();
                }
                else {
                    if (knowledge_comm.get_substation_agent() != 0) {
                        if (!know_neg_substation->is_producer(knowledge_comm.get_substation_agent())) {
                            /* swarm has reached maximum size -> subscribe to primary substation */
                            swarm_subscribe_msg new_msg(FIPA_PERF_SUBSCRIBE, id,
                            knowledge_comm.get_substation_agent());
                            new_msg.service.svc_type = neg_type;
                            new_msg.dist = MAX_SWARM_SIZE;
                            send_subscribe_msg(new_msg);

                        }
                    }
                    swarm_size = MAX_SWARM_SIZE;
                }
            }
        }
        else {
            /* swarm shrink */
            if (swarm_size == MAX_SWARM_SIZE) {
                swarm_size = know_neg_substation->get_effective_swarm_size(t_next,
                    substation_data->S_r / 10);
                unsigned int temp = know_neg_swarm->get_effective_swarm_size(t_next,
                    substation_data->S_r / 10);
                if (temp > swarm_size) {
                    swarm_size = temp;
                }
            }
            else if (!know_neg_swarm->is_waiting_for_df()) {
                swarm_size = know_neg_swarm->get_effective_swarm_size(t_next, substation_data->S_r / 10);
            }
            swarm_subscribe_msg new_msg(CUST_PERF_UNSUBSCRIBE, id, 0);
            new_msg.service.svc_type = neg_type;
            std::vector<int> _producers;
            /* delete unecessary producers */
            know_neg_substation->delete_producers(swarm_size, _producers);
            /* unsubscribe to unnecessary producers */
            for (auto i : _producers) {
                new_msg.receiver = i;
                send_subscribe_msg(new_msg);
            }
            _producers.clear();
            /* delete unecessary producers */
            know_neg_swarm->delete_producers(swarm_size, _producers);
            /* unsubscribe to unnecessary producers */
            for (auto i : _producers) {
                new_msg.receiver = i;
                send_subscribe_msg(new_msg);
            }
        }
    }
    else {
        swarm_subscribe_msg new_msg(CUST_PERF_UNSUBSCRIBE, id, 0);
            new_msg.service.svc_type = neg_type;
        std::vector<int> _producers;
        /* delete unecessary producers */
        know_neg_substation->delete_producers(0, _producers);
        /* unsubscribe to unnecessary producers */
        for (auto i : _producers) {
            new_msg.receiver = i;
            send_subscribe_msg(new_msg);
        }
        _producers.clear();
        /* delete unecessary producers */
        know_neg_swarm->delete_producers(0, _producers);
        /* unsubscribe to unnecessary producers */
        for (auto i : _producers) {
            new_msg.receiver = i;
            send_subscribe_msg(new_msg);
        }
    }

    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        IO->log_info("\tEffective active power swarm size: " + std::to_string(swarm_size));
        substation_data->swarm_size_p = swarm_size;
    }
    else {
        IO->log_info("\tEffective reactive power swarm size: " + std::to_string(swarm_size));
        substation_data->swarm_size_q = swarm_size;
    }
}


/*! \brief periodically query voltage sensor values
 * */
void Swarm_substation_behavior::query_voltage_meas()
{
    /* query voltage measurements every 5min (300s) */
    if (substation_data->Vnom2 < 1000) {
        if (t_next - ((int)(t_next / 300)) * 300 < t_step) {
            std::vector<int> ids = knowledge_sens.get_ids_voltage_meas();
            swarm_query_msg new_msg(FIPA_PERF_QUERY_REF, id, 0);
            new_msg.meas_type = QUERYTYPE_VOLTAGE;
            for (unsigned int i = 0; i < ids.size(); i++) {
                new_msg.receiver = ids[i];
                send_query_msg(new_msg);
            }
        }
    }
    else {
        /* offset of 150 s */
        if ((t_next+150) - ((int)((t_next+150) / 300)) * 300 < t_step) {
            std::vector<int> ids = knowledge_sens.get_ids_voltage_meas();
            swarm_query_msg new_msg(FIPA_PERF_QUERY_REF, id, 0);
            new_msg.meas_type = QUERYTYPE_VOLTAGE;
            for (unsigned int i = 0; i < ids.size(); i++) {
                new_msg.receiver = ids[i];
                send_query_msg(new_msg);
            }
        }
    }
}


/*! \brief  send a request for a voltage set point change to internal agents
 * */
void Swarm_substation_behavior::request_setpoint_change()
{
    std::vector<int> _agents;
    knowledge_comm.get_internal_agents(_agents);
    swarm_request_msg new_msg(FIPA_PERF_REQUEST, id, 0);
    new_msg.request_type = REQUESTTYPE_SETPOINT;
    new_msg.request_value = substation_data->v2_setpoint;
    for (auto i : _agents) {
        new_msg.receiver = i;
        send_request_msg(new_msg);
    }
}


/*! \brief  change the set point for voltage
 * */
void Swarm_substation_behavior::change_setpoint(double setpoint)
{
    substation_data->v1_setpoint = setpoint;
}


/*! \brief  take new sample for voltage and store it in sensor knowledge
 * */
void Swarm_substation_behavior::save_voltage_measurement()
{
    std::complex<double> meas;
    meas.real(substation_data->v1_re / (substation_data->Vnom1 / sqrt(3)));
    meas.imag(substation_data->v1_im / (substation_data->Vnom1 / sqrt(3)));
    struct meas_ex v_sample(id, QUERYTYPE_VOLTAGE, meas, t_next, true);
    knowledge_sens.add_voltage_sample(v_sample);

    substation_data->v_meas = knowledge_sens.get_absolute_voltage(id);
}


/*! \brief  get current size of swarm
 *  \param  neg_type    active or reactive power
 *  \return swarm size
 * */
unsigned int Swarm_substation_behavior::get_swarm_size(int neg_type)
{
    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        return substation_data->swarm_size_p;
    }
    else {
        return substation_data->swarm_size_q;
    }
}


/*! \brief  set current size of swarm
 *  \param  neg_type    active or reactive power
 *  \param  swarm_size  swarm size
 * */
void Swarm_substation_behavior::set_swarm_size(int neg_type, int swarm_size)
{
    if (neg_type == NEGOTIATIONTYPE_P_INT) {
        substation_data->swarm_size_p = swarm_size;
    }
    else {
        substation_data->swarm_size_q = swarm_size;
    }
}


/*! \brief  get threshold for contract negotiation
 *  \param  neg_type    affected negotiation knowledge
 *  \return threshold
 */
double Swarm_substation_behavior::get_threshold(Negotiation_knowledge* know_neg)
{
    if (know_neg->get_group_type() == GROUPTYPE_INTERNAL_INT) {
        return NEGOTIATION_THRESHOLD_FACTOR*substation_data->Vnom2;
    }
    else {
        return NEGOTIATION_THRESHOLD_FACTOR*substation_data->Vnom1;
    }
}

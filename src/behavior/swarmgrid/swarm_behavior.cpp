/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/swarmgrid/swarm_behavior.h"


/*! \brief  constructor
 *  \param  _id         unique ID of agent
 *  \param  _type       agent type as defined in config.h
 *  \param  _subtype    subtype as defined in config.h
 *  \param  step_size   simulation step size
 *  \param  logging     indicates if agents logs locally
 * */
Swarm_behavior::Swarm_behavior(int _id, int _type, std::string _subtype, double& step_size,
        struct data_props _d_props)
    : Agent_behavior(_id, _type, _subtype, step_size, _d_props)
{
}


/*! \brief process all messages that have been received since the last time step and call protocol
 * specific process function
 * */
void Swarm_behavior::process_incoming_messages() {
    while (!incoming_messages.empty()) {
        Agent_message msg = incoming_messages.front();
        IO->log_info("\tReceived message: " + msg.get_message_output());
        switch (msg.protocol) {
            case FIPA_PROT_CONTRACT_NET : {
                swarm_contract_net_msg swarm_msg(msg.performative, msg.sender, msg.receiver);
                std::stringstream _content(msg.content);
                _content >> swarm_msg.time >> swarm_msg.power.neg_type >> swarm_msg.power.producer
                    >> swarm_msg.power.consumer >> swarm_msg.power.needed
                    >> swarm_msg.power.optional;
                this->process_contract_net_msg(swarm_msg);
                break;
            }
            case FIPA_PROT_SUBSCRIBE : {
                swarm_subscribe_msg swarm_msg(msg.performative, msg.sender, msg.receiver);
                std::stringstream _content(msg.content);
                _content >> swarm_msg.dist >> swarm_msg.service.t_last_update 
                    >> swarm_msg.service.svc_type >> swarm_msg.service.minimum 
                    >> swarm_msg.service.optional_max >> swarm_msg.service.needed_max;
                this->process_subscribe_msg(swarm_msg);
                break;
            }
            case FIPA_PROT_QUERY : {
                swarm_query_msg swarm_msg(msg.performative, msg.sender, msg.receiver);
                std::stringstream _content(msg.content);
                _content >> swarm_msg.meas_type >> swarm_msg.time >> swarm_msg.meas_re >> swarm_msg.meas_im;
                this->process_query_msg(swarm_msg);
                break;
            }
            case FIPA_PROT_REQUEST : {
                swarm_request_msg swarm_msg(msg.performative, msg.sender, msg.receiver);
                std::stringstream _content(msg.content);
                _content >> swarm_msg.request_type >> swarm_msg.request_value;
                this->process_request_msg(swarm_msg);
                break;
            }
            case FIPA_PROT_RECRUITING : {
                swarm_recruiting_msg swarm_msg(msg.performative, msg.sender, msg.receiver);
                std::stringstream _content(msg.content);
                _content >> swarm_msg.base_type >> swarm_msg.neg_type >> swarm_msg.hops
                    >> swarm_msg.node_sender;
                std::pair<int, unsigned int> _agent_id;
                while (!_content.eof()) {
                    _content >> _agent_id.first;
                    _content >> _agent_id.second;
                    swarm_msg.agents.push_back(_agent_id);
                }
                this->process_recruiting_msg(swarm_msg);
                break;
            }
            default : {
                // TODO ERROR HANDLING
            }
        }
        incoming_messages.pop_front();
    }
}


/*!
 * \brief sending function for swarm contract net messages
 * \param msg message to be sent
 */
void Swarm_behavior::send_contract_net_msg(swarm_contract_net_msg &msg)
{
    std::string content;
    std::stringstream _content;
    _content << msg.time << " " << msg.power.neg_type << " " << msg.power.producer << " "
        << msg.power.consumer << " " << msg.power.needed << " " << msg.power.optional;
    content = _content.str();
    Agent_message ag_msg(msg.performative, msg.sender, msg.receiver, content, FIPA_PROT_CONTRACT_NET);
    send_message(ag_msg);
}


/*!
 * \brief sending function for swarm subscribe messages
 * \param msg message to be sent
 */
void Swarm_behavior::send_subscribe_msg(swarm_subscribe_msg &msg)
{
    std::string content;
    std::stringstream _content;
    _content << msg.dist << " " << msg.service.t_last_update << " " << msg.service.svc_type << " "
        << msg.service.minimum << " " << msg.service.optional_max << " " << msg.service.needed_max;
    content = _content.str();
    Agent_message ag_msg(msg.performative, msg.sender, msg.receiver, content, FIPA_PROT_SUBSCRIBE);
    send_message(ag_msg);
}


/*!
 * \brief sending function for swarm recruiting messages
 * \param msg message to be sent
 */
void Swarm_behavior::send_recruiting_msg(swarm_recruiting_msg &msg)
{
    std::string content;
    std::stringstream _content;
    _content << msg.base_type << " " << msg.neg_type << " " << msg.hops << " " << msg.node_sender;
    for (unsigned int i = 0; i < msg.agents.size(); i++) {
        _content << " " << msg.agents[i].first << " " << msg.agents[i].second;
    }
    content = _content.str();
    Agent_message ag_msg(msg.performative, msg.sender, msg.receiver, content, FIPA_PROT_RECRUITING);
    send_message(ag_msg);
}


/*!
 * \brief sending function for swarm query messages
 * \param msg message to be sent
 */
void Swarm_behavior::send_query_msg(swarm_query_msg &msg)
{
    std::string content;
    std::stringstream _content;
    _content << msg.meas_type << " " << msg.time << " " << msg.meas_re << " " << msg.meas_im;
    content = _content.str();
    Agent_message ag_msg(msg.performative, msg.sender, msg.receiver, content, FIPA_PROT_QUERY);
    send_message(ag_msg);
}


/*!
 * \brief sending function for swarm request messages
 * \param msg message to be sent
 */
void Swarm_behavior::send_request_msg(swarm_request_msg &msg)
{
    std::string content;
    std::stringstream _content;
    _content << msg.request_type << " " << msg.request_value;
    content = _content.str();
    Agent_message ag_msg(msg.performative, msg.sender, msg.receiver, content, FIPA_PROT_REQUEST);
    send_message(ag_msg);
}

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include <algorithm>

#include "behavior/swarmgrid/negotiation_knowledge.h"
#include "model/config.h"


Negotiation_knowledge::Negotiation_knowledge() : id(0), IO(nullptr), knowledge_comm(nullptr),
    negotiation_type(NEGOTIATIONTYPE_P_INT), state(BASETYPE_CONSUMER_INT),
    group_type(GROUPTYPE_INTERNAL_INT), waiting_for_df(false)
{
    producers.clear();
    consumers.clear();
    services.clear();
    contracts.clear();
    open_contracts.clear();
}

/*! \brief initialization of Knowledge class
 *  \param _id agent ID of owner agent
 *  \param _internal_agents IDs of component agents connected to the same bus with unknown type
 *  \param _swarm_agents IDs of component agents within the same swarm with unknown type
 * */
void Negotiation_knowledge::init(int &_id, BehaviorIO* _io,
    Communication_knowledge* know_comm, int neg_type, int _group_type)
{
    producers.clear();
    consumers.clear();
    services.clear();
    contracts.clear();
    open_contracts.clear();

    id = _id;
    IO = _io;
    knowledge_comm = know_comm;
    negotiation_type = neg_type;
    group_type = _group_type;
    state = BASETYPE_CONSUMER_INT;
    waiting_for_df = false;

    power.neg_type = neg_type;
    power.needed = 0;
    power.optional = 0;
    open_power.neg_type = neg_type;
    open_power.needed = 0;
    open_power.optional = 0;
}



/*! \brief get an iterator to specified open contract
 *  \param _producer_id id of producer agent
 *  \param _consumer_id ID of consumer agent
 *  \return iterator pointing to searched contract, pointing to end if contract does not exist
 * */
std::vector<Contract>::iterator Negotiation_knowledge::get_it_open_contracts(int _producer_id,
    int _consumer_id)
{
    std::vector<Contract>::iterator it;
    for (it = open_contracts.begin(); it != open_contracts.end(); it++) {
        if (it->get_producer_id() == _producer_id && it->get_consumer_id() == _consumer_id) {
            break;
        }
    }
    return it;
}


/*! \brief get an iterator to specified contract
 *  \param _producer_id id of producer agent
 *  \param _consumer_id ID of consumer agent
 *  \return iterator pointing to searched contract, pointing to end if contract does not exist
 * */
std::vector<Contract>::iterator Negotiation_knowledge::get_it_contracts(int _producer_id,
    int _consumer_id)
{
    std::vector<Contract>::iterator it;
    for (it = contracts.begin(); it != contracts.end(); it++) {
        if (it->get_producer_id() == _producer_id && it->get_consumer_id() == _consumer_id) {
            break;
        }
    }
    return it;
}


/*! \brief create a new open contract
 *  \param _producer_id id of producer agent
 *  \param _consumer_id ID of consumer agent
 *  \param _power power to be contracted
 *  \return indicator of success (0) or error (-1)
 * */
int Negotiation_knowledge::new_contract(int _producer_id, int _consumer_id,
    struct contract_ex _power, double t)
{
    if (!open_contract_exists(_producer_id, _consumer_id)) {
        _power.neg_type = negotiation_type;
        Contract con(_producer_id, _consumer_id, _power, t);

        open_contracts.push_back(con);

		/* update sum of open contracts */
        if (_producer_id == id) {
            open_power.needed -= _power.needed;
            open_power.optional -= _power.optional;
            /* if agent is unknown add it to the list of known agents */
            if (!knowledge_comm->is_swarm_agent(_consumer_id) 
                    && !knowledge_comm->is_internal_agent(_consumer_id)
                    && _consumer_id != knowledge_comm->get_substation_agent()) {
                add_consumer(_consumer_id, 0, t);
            }
        }
        else {
            open_power.needed += _power.needed;
            open_power.optional += _power.optional;
            /* if agent is unknown add it to the list of known agents */
            if (!knowledge_comm->is_swarm_agent(_producer_id)
                    && !knowledge_comm->is_internal_agent(_producer_id)
                    && _producer_id != knowledge_comm->get_substation_agent()) {
                add_producer(_producer_id, 0, t);
            }
            /* reduce service */
            else {
                struct service_ex _service = get_producer_service(_producer_id);
                _service.svc_type = negotiation_type;
                _service.t_last_update = t;
                _service.needed_max -= _power.needed;
                if (_service.needed_max < POWER_ACCURACY) {
                    _service.needed_max = 0;
                }
                _service.optional_max -= (_power.needed + _power.optional);
                if (_service.optional_max < POWER_ACCURACY) {
                    _service.optional_max = 0;
                }
                _service.minimum -= (_power.needed + _power.optional);
                if (_service.minimum < POWER_ACCURACY) {
                    _service.minimum = 0;
                }
                update_producer_service(_producer_id, _service);
            }
        }
        return 0;
    }
    else {
        return -1;
    }
}


/*! \brief conclude an open contract and push it to the corresponding vector
 *  \param _producer_id id of producer agent
 *  \param _consumer_id ID of consumer agent
 *  \param t            time
 *  \return indicator of success (0) or error (-1)
 * */
int Negotiation_knowledge::conclude_contract(int _producer_id, int _consumer_id, double t)
{
    std::vector<Contract>::iterator it_open;
    struct contract_ex _open_power;

    it_open = get_it_open_contracts(_producer_id, _consumer_id);
    if (it_open != open_contracts.end()) {
        _open_power = it_open->get_power();
        std::vector<Contract>::iterator it = get_it_contracts(_producer_id, _consumer_id);
        if (it == contracts.end()) {
            /* if no concluded contracts exists create a new one */
            Contract con(_producer_id, _consumer_id, _open_power, t);
            contracts.push_back(con);
        }
        else {
            /* if there already is a concluded contract change it */
            struct contract_ex _concluded_power = it->get_power();
            _concluded_power.needed += _open_power.needed;
            _concluded_power.optional += _open_power.optional;
            it->change_contract(_concluded_power, t);
        }
        /* delete open contract */
        open_contracts.erase(it_open);

        /* adapt power sum */
        if (_producer_id == id) {
            power.needed -= _open_power.needed;
            power.optional -= _open_power.optional;
            open_power.needed += _open_power.needed;
            open_power.optional += _open_power.optional;
        }
        else {
            power.needed += _open_power.needed;
            power.optional += _open_power.optional;
            open_power.needed -= _open_power.needed;
            open_power.optional -= _open_power.optional;
        }
        if (fabs(open_power.needed) < POWER_ACCURACY) {
            open_power.needed = 0;
        }
        if (fabs(open_power.optional) < POWER_ACCURACY) {
            open_power.optional = 0;
        }
        return 0;
    }
    else {
        return -1;
    }
}


/*! \brief handles the content of a contract-net inform message; reduces the contracted power
 *  \param _producer_id ID of producer agent
 *  \param _consumer_id ID of consumer agent
 *  \param _power new power according to inform message
 *  \return indicator of success (0) or error (-1)
 * */
int Negotiation_knowledge::handle_contract_inform_msg(int _producer_id, int _consumer_id,
    struct contract_ex _power, double t)
{
    std::vector<Contract>::iterator it;
    it = get_it_contracts(_producer_id, _consumer_id);
    struct contract_ex power_old;

    if (it == contracts.end()) {
        return -1;
    }
    else {
        /* check if power accroding to inform is really smaller than current power */
        power_old = it->get_power();
        //if (_power.needed > power_old.needed + power_old.optional) {
        //    _power.needed = _power.needed + _power.optional;
        //    _power.optional = 0;
        //}
        //else if (_power.needed + _power.optional > power_old.needed + power_old.optional) {
        //    _power.optional = (power_old.needed + power_old.optional) - _power.needed;
        //}
        // TODO special treatment for q?
        if (_power.needed > power_old.needed) {
            _power.needed = power_old.needed;
        }
        if (_power.optional > power_old.optional) {
            _power.optional = power_old.optional;
        }

        return change_contract(_producer_id, _consumer_id, _power, t);
    }
}


/*! \brief changes a concluded contract (e.g. after a contract-net inform msg was received
 *  \param _producer_id ID of producer agent
 *  \param _consumer_id ID of consumer agent
 *  \param _power new power for contract
 *  \return indicator of success (0) or error (-1)
 * */
int Negotiation_knowledge::change_contract(int _producer_id, int _consumer_id,
    struct contract_ex _power, double t)
{
    std::vector<Contract>::iterator it;
    it = get_it_contracts(_producer_id, _consumer_id);
    struct contract_ex power_old;

    if (it == contracts.end()) {
        return -1;
    }
    else {
        power_old = it->get_power();
        it->change_contract(_power, t);

        /* update power sum */
        if (_producer_id == id) {
            power.needed -= (_power.needed - power_old.needed);
            power.optional -= (_power.optional - power_old.optional);
        }
        else {
            power.needed += (_power.needed - power_old.needed);
            power.optional += (_power.optional - power_old.optional);
        }
        if (fabs(power.needed) < POWER_ACCURACY) {
            power.needed = 0;
        }
        if (fabs(power.optional) < POWER_ACCURACY) {
            power.optional = 0;
        }
        return 0;
    }
}


/*! \brief changes an open contract (e.g. after a contract-net inform msg was received
 *  \param _producer_id ID of producer agent
 *  \param _consumer_id ID of consumer agent
 *  \param _power new power for contract
 *  \return indicator of success (0) or error (-1)
 * */
int Negotiation_knowledge::change_open_contract(int _producer_id, int _consumer_id,
    struct contract_ex _power, double t)
{
    std::vector<Contract>::iterator it;
    it = get_it_open_contracts(_producer_id, _consumer_id);
    struct contract_ex power_old;

    if (it == open_contracts.end()) {
        return -1;
    }
    else {
        power_old = it->get_power();
        it->change_contract(_power, t);

        /* update open power sum */
        if (_producer_id == id) {
            open_power.needed -= (_power.needed - power_old.needed);
            open_power.optional -= (_power.optional - power_old.optional);
        }
        else {
            open_power.needed += (_power.needed - power_old.needed);
            open_power.optional += (_power.optional - power_old.optional);
        }
        if (fabs(open_power.needed) < POWER_ACCURACY) {
            open_power.needed = 0;
        }
        if (fabs(open_power.optional) < POWER_ACCURACY) {
            open_power.optional = 0;
        }
        return 0;
    }
}


/*! \brief deletes an open contract (e.g. after a contract-net inform msg was received
 *  \param _producer_id ID of producer agent
 *  \param _consumer_id ID of consumer agent
 *  \return indicator of success (0) or error (-1)
 * */
int Negotiation_knowledge::delete_open_contract(int _producer_id, int _consumer_id)
{
    std::vector<Contract>::iterator it;
    it = get_it_open_contracts(_producer_id, _consumer_id);
    struct contract_ex power_old;

    if (it == open_contracts.end()) {
        return -1;
    }
    else {
        power_old = it->get_power();
        open_contracts.erase(it);

        /* update open power sum */
        if (_producer_id == id) {
            open_power.needed += power_old.needed;
            open_power.optional += power_old.optional;
        }
        else {
            open_power.needed -= power_old.needed;
            open_power.optional -= power_old.optional;
        }
        if (fabs(open_power.needed) < POWER_ACCURACY) {
            open_power.needed = 0;
        }
        if (fabs(open_power.optional) < POWER_ACCURACY) {
            open_power.optional = 0;
        }
        return 0;
    }
}


/*! \brief checks whether or not the an open contract exists
*   \param _producer_id id of producer agent
*   \param _consumer_id ID of consumer agent
*   \return existance indicator
* */
bool Negotiation_knowledge::open_contract_exists(int _producer_id, int _consumer_id)
{
    auto it = get_it_open_contracts(_producer_id, _consumer_id);

    if (it != open_contracts.end()) {
        return true;
    }
    else {
        return false;
    }
}

/*! \brief getter for negotiation type (P or Q)
 *  \return negotiation type
 * */
int Negotiation_knowledge::get_negotiation_type()
{
    return negotiation_type;
}

/*! \brief getter for open contract
 *  \param _producer_id ID of producer agent
 *  \param _consumer_id ID of consumer agent
 *  \return power
 * */
struct contract_ex Negotiation_knowledge::get_open_contract(int _producer_id, int _consumer_id)
{
    struct contract_ex _power;
    _power.neg_type = negotiation_type;
    std::vector<Contract>::iterator it = get_it_open_contracts(_producer_id, _consumer_id);
    if (it != open_contracts.end()) {
        _power = it->get_power();
    }
    return _power;
}

/*! \brief getter for contracted power
 *  \param _producer_id ID of producer agent
 *  \param _consumer_id ID of consumer agent
 *  \return power
 * */
struct contract_ex Negotiation_knowledge::get_contracted_power(int _producer_id, int _consumer_id)
{
    struct contract_ex _power;
    _power.neg_type = negotiation_type;
    std::vector<Contract>::iterator it = get_it_contracts(_producer_id, _consumer_id);
    if (it != contracts.end()) {
        _power = it->get_power();
    }
    return _power;
}

/*! \brief get a producer agent that could provide power (to be called only by consumers)
 *  \param _power [in|out] required power | available power
 *  \threshold threshold for service
 *  \return ID of a suitable producer
 * */
int Negotiation_knowledge::get_suitable_producer(struct contract_ex& _power, double threshold)
{
    int producer_id = 0;
    double minimum_temp = -1;
    std::vector<std::pair<int, struct service_ex>>::iterator it;

    /* power is needed */
    if (_power.needed > threshold) {
        for (it = services.begin(); it != services.end(); it++) {
            if (it->first != id && it->second.needed_max > threshold 
                    && !open_contract_exists(it->first, id)) {
                if (it->second.minimum - minimum_temp > POWER_ACCURACY) {
                    producer_id = it->first;
                    _power.needed = it->second.needed_max;
                    _power.optional = it->second.optional_max;
                    minimum_temp = it->second.minimum;
                    if (it->second.minimum > threshold) {
                        break;
                    }
                }
            }
        }
    }
    /* power is optional */
    if (_power.optional > threshold && producer_id == 0) {
        for (it = services.begin(); it != services.end(); it++) {
            if (it->first != id && it->second.optional_max > threshold 
                    && !open_contract_exists(it->first, id)) {
                if (it->second.minimum - minimum_temp > POWER_ACCURACY) {
                    producer_id = it->first;
                    _power.needed = it->second.needed_max;
                    _power.optional = it->second.optional_max;
                    minimum_temp = it->second.minimum;
                    if (it->second.minimum > threshold) {
                        break;
                    }
                }
            }
        }
    }
    return producer_id;
}


/*! \brief adds a service promoted by a producer the list of services
 *  \param _producer_id ID of the producer
 *  \param _service the service to be stored
 * */
void Negotiation_knowledge::new_producer_service(int _producer_id, struct service_ex _service)
{
    std::pair<int, struct service_ex> service_temp(_producer_id, _service);

    unsigned int dist_new = knowledge_comm->get_distance(_producer_id);
    bool inserted = false;
    bool unknown = true;
    for (unsigned int i = 0; i < services.size(); i++) {
        if (services[i].first == _producer_id) {
            unknown = false;
            break;
        }
    }
    if (unknown) {
        /* insert only if producer is not yet known */
        for (auto it = services.begin(); it != services.end(); it++) {
            if (dist_new < knowledge_comm->get_distance(it->first)) {
                /* sort services by distance */
                services.insert(it, service_temp);
                inserted = true;
                break;
            }
        }
        if (!inserted) {
            services.push_back(service_temp);
        }
    }
}

/*! \brief adds a service promoted by a producer the list of services
 *  \param _producer_id ID of the producer
 *  \param _service the service to be stored
 * */
void Negotiation_knowledge::update_producer_service(int _producer_id, struct service_ex _service)
{
    std::vector<std::pair<int, struct service_ex>>::iterator it;

    for (it = services.begin(); it != services.end(); it++) {
        if (it->first == _producer_id) {
            it->second = _service;
            if (group_type == GROUPTYPE_SUBSTATION_INT && _producer_id != id) {
                struct service_ex _service_own = get_producer_service(id);
                it->second.needed_max -= _service_own.needed_max;
                it->second.optional_max -= _service_own.optional_max;
                it->second.minimum -= _service_own.minimum;
                if (it->second.needed_max < POWER_ACCURACY) {
                    it->second.needed_max = 0;
                }
                if (it->second.optional_max < POWER_ACCURACY) {
                    it->second.optional_max = 0;
                }
                if (it->second.minimum < POWER_ACCURACY) {
                    it->second.minimum = 0;
                }
            }
            break;
        }
    }
}

/*! \brief get a service promoted by a producer from the list of services
 *  \param _producer_id ID of the producer
 *  \return _service stored service
 * */
struct service_ex Negotiation_knowledge::get_producer_service(int _producer_id)
{
    struct service_ex _service;
    _service.svc_type = negotiation_type;
    std::vector<std::pair<int, struct service_ex>>::iterator it;
    for (it = services.begin(); it != services.end(); it++) {
        if (it->first == _producer_id) {
            _service = it->second;
            break;
        }
    }

    return _service;
}

/*! \brief get producer services older than certain time
 *  \param older_services [out] vector of services
 *  \param t                    time to compare with
 * */
void Negotiation_knowledge::get_older_producer_services(
    std::vector<std::pair<int, struct service_ex>>& older_services, double t)
{
    std::vector<std::pair<int, struct service_ex>>::iterator it;
    for (it = services.begin(); it != services.end(); it++) {
        if (it->second.t_last_update < t && it->first != id) {
            older_services.push_back((*it));
        }
    }
}

/*! \brief getter for sum of all services
 *  \return sum of services
 * */
struct service_ex Negotiation_knowledge::get_service_sum()
{
    struct service_ex svc_sum;
    svc_sum.svc_type = negotiation_type;
    for (unsigned int i = 0; i < services.size(); i++) {
        if (services[i].first != id) {
            svc_sum.minimum += services[i].second.minimum;
            svc_sum.optional_max += services[i].second.optional_max;
            svc_sum.needed_max += services[i].second.needed_max;
        }
    }
    return svc_sum;
}

/*! \brief adds a consumer to the list of consumer agents
 *  \param _consumer_id ID of consumer agent
 *  \param dist         distance to consumer
 *  \param t            time
 * */
void Negotiation_knowledge::add_consumer(int _consumer_id, unsigned int dist, double t)
{
    std::pair<int, unsigned int> _agent(_consumer_id, dist);
    if (std::find(consumers.begin(), consumers.end(), _agent) == consumers.end()) {
        consumers.push_back(_agent);
    }
    knowledge_comm->add_swarmmember(_consumer_id, BASETYPE_CONSUMER_INT, dist);

    if (!open_contract_exists(id, _consumer_id)) {
        struct contract_ex _power;
        new_contract(id, _consumer_id, _power, t);
        conclude_contract(id, _consumer_id, t);
    }
}

/*! \brief deletes a consumer from the list of consumer agents
 *  \param _consumer_id ID of consumer agent
 * */
void Negotiation_knowledge::delete_consumer(int _consumer_id)
{
    std::vector<std::pair<int, unsigned int>>::iterator it;
    for (it = consumers.begin(); it != consumers.end(); it++) {
        if (it->first == _consumer_id) {
            consumers.erase(it);
            break;
        }
    }

    /* delete corresponding contract */
    std::vector<Contract>::iterator contract_it = get_it_contracts(id, _consumer_id);
    if (contract_it != contracts.end()) {
        struct contract_ex _power;
        change_contract(contract_it->get_producer_id(), contract_it->get_consumer_id(), _power, 0);
        contracts.erase(contract_it);
    }
}

/*! \brief adds a producer to the list of producer agents
 *  \param _producer_id ID of producer agent
 *  \param dist         distance to consumer
 *  \param t            time
 * */
void Negotiation_knowledge::add_producer(int _producer_id, unsigned int dist, double t)
{
    std::pair<int, unsigned int> _agent(_producer_id, dist);
    if (std::find(producers.begin(), producers.end(), _agent) == producers.end()) {
        producers.push_back(_agent);
    }
    knowledge_comm->add_swarmmember(_producer_id, BASETYPE_PRODUCER_INT, dist);

    if (!open_contract_exists(_producer_id, id)) {
        struct contract_ex _power;
        new_contract(_producer_id, id, _power, t);
        conclude_contract(_producer_id, id, t);
    }
}


/*! \brief shrink the swarm of a consumer according to specified size
 *  \param swarm_size       desired size in hops
 *  \param _producers [out] producers that are erased from swarm
 * */
void Negotiation_knowledge::delete_producers(unsigned int swarm_size, std::vector<int>& _producers)
{
    /* swarm */
    std::vector<std::pair<int, unsigned int>>::iterator it = producers.begin();
    while (it != producers.end()) {
        if (it->second > swarm_size) {
            _producers.push_back(it->first);
            it = producers.erase(it);
        }
        else {
            it++;
        }
    }

    /* delete corresponding services */
    std::vector<std::pair<int, struct service_ex>>::iterator service_it = services.begin();
    while (service_it != services.end()) {
        if (find(_producers.begin(), _producers.end(), service_it->first) != _producers.end()) {
            service_it = services.erase(service_it);
        }
        else {
            service_it++;
        }
    }

    /* delete corresponding contracts */
    std::vector<Contract>::iterator contract_it = contracts.begin();
    while (contract_it != contracts.end()) {
        if (find(_producers.begin(), _producers.end(), contract_it->get_producer_id()) !=
                _producers.end()) {
            contract_it = contracts.erase(contract_it);
        }
        else {
            contract_it++;
        }
    }
}


/*! \brief get the effective swarm size in hops
*   \param t    current time
 *  \param pmin minimum power
*   \return swarm size [hops]
*/
unsigned int Negotiation_knowledge::get_effective_swarm_size(double t, double pmin)
{
    unsigned int max_size = 0;

    if (state == BASETYPE_CONSUMER_INT) {

        if (group_type == GROUPTYPE_SUBSTATION_INT) {
            struct service_ex sub_svc = get_producer_service(knowledge_comm->get_substation_agent());
            if (sub_svc.needed_max < pmin) {
                max_size = MAX_SWARM_SIZE;
            }

            /* if consumer has a contract with substation the effective swarm size is equal to the
             * maximum swarm size */
            for (auto& it : contracts) {
                struct contract_ex _power = it.get_power();
                if (fabs(_power.needed) > POWER_ACCURACY || fabs(_power.optional) > POWER_ACCURACY
                        || it.get_t_last_update() > t - 600) {
                    max_size = MAX_SWARM_SIZE;
                }
            }
            for (auto& it : open_contracts) {
                struct contract_ex _power = it.get_power();
                if (fabs(_power.needed) > POWER_ACCURACY || fabs(_power.optional) > POWER_ACCURACY) {
                    max_size = MAX_SWARM_SIZE;
                }
            }
        }
        else if (group_type == GROUPTYPE_SWARM_INT) {
            double swarm_svc_needed = 0.0;
            for (auto svc : services) {
                swarm_svc_needed += svc.second.needed_max;
                if (swarm_svc_needed > pmin) {
                    max_size = knowledge_comm->get_distance(svc.first);
                    break;
                }
            }

            /*else effective swarm size is equal to the maximum of the largest distance in
             * open_contracts or concluded_contracts greater zero */
            for (auto& it : open_contracts) {
                int pro_id = it.get_producer_id();
                unsigned int dist = knowledge_comm->get_distance(pro_id);
                if (dist > max_size) {
                    max_size = dist;
                }
            }

            for (auto& it : contracts) {
                struct contract_ex _power = it.get_power();
                int pro_id = it.get_producer_id();
                if ((fabs(_power.needed) > POWER_ACCURACY || fabs(_power.optional) > POWER_ACCURACY
                        || it.get_t_last_update() > t - 600) && pro_id != id) {
                    unsigned int  dist = knowledge_comm->get_distance(pro_id);
                    if (dist > max_size) {
                        max_size = dist;
                    }
                }
            }
        }
        else {
            max_size = 0;
        }
    }
    else {
        for (auto& it : contracts) {
            struct contract_ex _power = it.get_power();
            int con_id = it.get_consumer_id();
            if ((fabs(_power.needed) > POWER_ACCURACY || fabs(_power.optional) > POWER_ACCURACY
                    || it.get_t_last_update() > t - 600) && con_id != id) {
                unsigned int  dist = knowledge_comm->get_distance(con_id);
                if (dist > max_size) {
                    max_size = dist;
                }
            }
        }
    }
    return max_size;
}

/*! \brief get total concludeed power
 *  \return power
 * */
struct contract_ex Negotiation_knowledge::get_power()
{
    return power;
}

/*! \brief get open power
 *  \return open power
 * */
struct contract_ex Negotiation_knowledge::get_open_power()
{
    return open_power;
}

/*! \brief getter for all contracts with consumer role
 *  \return vector of contracts
 * */
std::vector<Contract> Negotiation_knowledge::get_consumer_contracts()
{
    std::vector<Contract> consumer_contracts;
    std::vector<Contract>::iterator it;

    for (it = contracts.begin(); it != contracts.end(); it++) {
        if (it->get_consumer_id() == id) {
            consumer_contracts.push_back(*it);
        }
    }
    return consumer_contracts;
}

/*! \brief getter for all contracts with producer role
 *  \return vector of contracts
 * */
std::vector<Contract> Negotiation_knowledge::get_producer_contracts()
{
    std::vector<Contract> producer_contracts;
    std::vector<Contract>::iterator it;

    for (it = contracts.begin(); it != contracts.end(); it++) {
        if (it->get_producer_id() == id) {
            producer_contracts.push_back(*it);
        }
    }
    return producer_contracts;
}

/*! \brief getter for state (producer or consumer)
 *  \return state
 * */
int Negotiation_knowledge::get_state()
{
    return state;
}

/*! \brief setter for state (producer or consumer)
 *  \param _state state
 * */
void Negotiation_knowledge::set_state(int _state)
{
    state = _state;
}

/*! \brief check if given agent is a consumer
 *  \param _id ID of the agent to check for
 *  \return indicates whether or not specified agent is consumer
 * */
bool Negotiation_knowledge::is_consumer(int _id)
{
    for (auto& it : consumers) {
        if (it.first == _id) {
            return true;
        }
    }
    return false;
}

/*! \brief check if given agent is a producer
 *  \param _id ID of the agent to check for
 *  \return indicates whether or not specified agent is producer
 * */
bool Negotiation_knowledge::is_producer(int _id)
{
    for (auto& it : producers) {
        if (it.first == _id) {
            return true;
        }
    }
    return false;
}

/*! \brief check if given agent is in group
 *  \param _id ID of the agent to check for
 *  \return indicates whether or not specified agent is in group
 * */
bool Negotiation_knowledge::is_in_group(int _id)
{
    for (auto& it : producers) {
        if (it.first == _id) {
            return true;
        }
    }
    for (auto& it : consumers) {
        if (it.first == _id) {
            return true;
        }
    }
    return false;
}

/*! \brief check if agent has subscribed to given agent
 *  \param _id ID of the agent to check for
 *  \return indicates whether or not agent has subscribed
 * */
bool Negotiation_knowledge::subscribed_to(int _id)
{
    for (auto& it : producers) {
        if (it.first == _id) {
            return true;
        }
    }
    return false;
}

/*! \brief getter for group type (internal, swarm, substation, slack)
 *  \return group type
 * */
int Negotiation_knowledge::get_group_type()
{
    return group_type;
}

/*! \brief getter for vector of consumer agents
 *  \param _consumers [out] vector of consumer agents
 * */
void Negotiation_knowledge::get_consumers(std::vector<std::pair<int, unsigned int>>& _consumers)
{
    _consumers = consumers;
}

/*! \brief sets the indicator for being waiting for the DF to reply
 * */
void Negotiation_knowledge::contacted_df()
{
    waiting_for_df = true;
}

/*! \brief deltes the indicator for being waiting for the DF to reply
 * */
void Negotiation_knowledge::received_df_reply()
{
    waiting_for_df = false;
}

/*! \brief gives the current status of communication with DF
 *  \return indicator for being waiting for the DF to reply
 * */
bool Negotiation_knowledge::is_waiting_for_df()
{
    return waiting_for_df;
}

/*! \brief Helper function for logging
 *  \return string containing logging information about knowledge
 * */
void Negotiation_knowledge::log_negotiation_knowledge()
{
    std::stringstream temp;
    std::string output;
    std::vector<Contract>::iterator it_con;
    std::vector<int>::iterator it_ag;
    std::vector<std::pair<int, struct service_ex>>::iterator it_service;

    temp << get_negotiation_header() << " negotiation knowledge output: " << std::endl;

    temp << "\tconsumers: ";
    for (auto it_ag = consumers.begin(); it_ag != consumers.end(); it_ag++) {
        temp << "(" << (*it_ag).first << ", " << (*it_ag).second << "), ";
    }
    temp << std::endl;
    temp << "\tproducers: ";
    for (auto it_ag = producers.begin(); it_ag != producers.end(); it_ag++) {
        temp << "(" << (*it_ag).first << ", " << (*it_ag).second << "), ";
    }
    temp << std::endl;

    temp << "\tcontracts: " << std::endl;
    for (it_con = contracts.begin(); it_con != contracts.end(); it_con++) {
        temp << "\t\t" << it_con->get_contract_output() << std::endl;
    }
    temp << "\tpower: needed("
        << power.needed << ") optional("
        << power.optional << ")" << std::endl;

    temp << "\topen contracts: " << std::endl;
    for (auto con : open_contracts) {
        temp << "\t\t" << con.get_contract_output() << std::endl;
    }
    temp << "\topen power: needed("
        << open_power.needed << ") optional("
        << open_power.optional << ")" << std::endl;

    temp << "\tservices: ";
    for (it_service = services.begin(); it_service != services.end(); it_service++) {
        temp << std::endl << "\t\t" << it_service->first
            << ": (" << it_service->second.minimum << ", " << it_service->second.optional_max  
            << ", " << it_service->second.needed_max << "), Time: "
            << it_service->second.t_last_update;
    }

    output = temp.str();
    IO->log_info(output);
}

/*! \brief helper for logging
 *  \return string containing header
 * */
std::string Negotiation_knowledge::get_negotiation_header()
{
    std::stringstream temp;
    std::string output;

    if (negotiation_type == NEGOTIATIONTYPE_P_INT) {
        if (group_type == GROUPTYPE_INTERNAL_INT) {
            temp << "Internal active power";
        }
        else if (group_type == GROUPTYPE_SWARM_INT) {
            temp << "Swarm active power";
        }
        else if (group_type == GROUPTYPE_SUBSTATION_INT) {
            temp << "Substation active power";
        }
        else {
            temp << "Slack active power";
        }
    }
    else {
        if (group_type == GROUPTYPE_INTERNAL_INT) {
            temp << "Internal reactive power";
        }
        else if (group_type == GROUPTYPE_SWARM_INT) {
            temp << "Swarm reactive power";
        }
        else if (group_type == GROUPTYPE_SUBSTATION_INT) {
            temp << "Substation reactive power";
        }
        else {
            temp << "Slack reactive power";
        }
    }
    output = temp.str();
    return output;
}

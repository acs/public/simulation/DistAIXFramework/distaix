/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/swarmgrid/swarm_df_behavior.h"

#include <list>

/*! \brief  constructor
 *  \param  _id         unique ID of agent
 *  \param  _type       agent type as defined in config.h
 *  \param  _subtype    subtype as defined in config.h
 *  \param  step_size   simulation step size
 *  \param  logging     indicates if agents logs locally
 *  \param  _df_data pointer to type specific data struct
 * */
Swarm_df_behavior::Swarm_df_behavior(int _id, int _type, std::string _subtype,
    double& step_size, struct data_props _d_props, DF_data * _df_data, int _rank)
    : Swarm_behavior(_id, _type, _subtype, step_size, _d_props), df_data(_df_data)
{
    std::string path = "runlog/behavior/agent_DF" + std::to_string(_rank) + ".log";
    std::string path_empty = "";
    IO->update_file_paths(path, path_empty);
    IO->init_logging();
}

/*!
 * \brief Destroy the object (close log file)
 */
Swarm_df_behavior::~Swarm_df_behavior()
{
    IO->finish_io();
}

/*! \brief Initialize the agent behavior
 * \param components_ad_nodes [in] vector saving a list of connceted components for each node
 * \param connected_nodes [in] vector saving a list of connected nodes for each node
 * \param components_file [in] data contained in scenario components input file (integers)
 * \param subtypes_file [in] subtypes contained in scenario components input file (strings)
 * \param el_grid_file [in] electrical connections contained in el. grid input file (integers)
 * */
int Swarm_df_behavior::initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                                  std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                                  boost::multi_array<int, 2> *components_file,
                                                  boost::multi_array<std::string, 1> *subtypes_file,
                                                  boost::multi_array<int, 2> *el_grid_file) {
    return 0;
}

/*! \brief processes messages
 * */
void Swarm_df_behavior::execute_agent_behavior()
{
    process_incoming_messages();
}

/*! \brief handles a received message with contract-net protocol (contract negotiation)
 *  \param msg contract-net message
 * */
void Swarm_df_behavior::process_contract_net_msg(swarm_contract_net_msg &msg)
{
}

/*! \brief handles a received message with subscribe protocol (producer service promotion)
 *  \param msg subscribe message
 * */
void Swarm_df_behavior::process_subscribe_msg(swarm_subscribe_msg &msg)
{
}

/*! \brief handles a received message with query protocol (measurement queries)
 *  \param msg query message
 * */
void Swarm_df_behavior::process_query_msg(swarm_query_msg &msg)
{
}

/*! \brief handles a received message with request protocol
 *  \param msg request message
 * */
void Swarm_df_behavior::process_request_msg(swarm_request_msg &msg)
{
}

/*! \brief handles a received message with Recruiting protocol
 *  \param msg subscribe message
 * */
void Swarm_df_behavior::process_recruiting_msg(swarm_recruiting_msg &msg)
{
    switch (msg.performative) {
        case FIPA_PERF_PROXY: {
            /* received proxy message -> determine agents that match request and send them back */
            swarm_recruiting_msg new_msg(FIPA_PERF_INFORM, id, msg.sender);
            new_msg.agents.clear();
            new_msg.base_type = msg.base_type;
            new_msg.neg_type = msg.neg_type;
            new_msg.node_sender = msg.node_sender;
            new_msg.hops = msg.hops;
            switch (msg.base_type) {
                case BASETYPE_CONSUMER_INT: {
                    get_agents(msg.sender, msg.node_sender, "consumer", msg.neg_type,
                        msg.hops, new_msg.agents);
                    break;
                }
                case BASETYPE_PRODUCER_INT: {
                    get_agents(msg.sender, msg.node_sender, "producer", msg.neg_type,
                        msg.hops, new_msg.agents);
                    break;
                }
            }
            send_recruiting_msg(new_msg);
            break;
        }
        default: {
            // ERROR
        }
    }
}


/*!
 *  \brief  returns a vector of agents of a certain type and within a certain hop distance from the
 *          source
 *  \param source_id    agent id of the source of this request
 *  \param node_source  node id of the source of this request
 *  \param s_type       string describing the type of agent, can be either "producer" or "consumer"
 *  \param hops         maximal number of hops that an agent is allowed to be away from the source
 *  \param agents       vector of agents which is the result of the query
 * */
void Swarm_df_behavior::get_agents(int source_id, int node_source, std::string s_type, int _negtype,
        unsigned int hops, std::vector<std::pair<int, unsigned int>> &agents) {
    std::pair<int, unsigned int> _agent;

    std::list<std::tuple<int,int,int>> components_at_source =
        df_data->components_at_nodes[node_source-1];
    int trafo_id_source = std::get<2>(components_at_source.front());
    int source_type = 0;
    for (auto component : components_at_source) {
        if (source_id == std::get<0>(component)) {
            source_type = std::get<1>(component);
        }
    }

    for (int destination = 0; destination < (int) df_data->components_at_nodes.size();
            destination++){
        if(destination+1 == node_source){
            continue;
        }
        //check if this destination is connected to ICT
        if(!df_data->ict_connectivity_of_nodes[destination]){
            continue;
        }

        std::list<std::tuple<int,int,int>> components_at_destination =
            df_data->components_at_nodes[destination];
        // check only agents who are not at the source node of the request and have the same
        // trafo_id as the source
        if ((destination != node_source-1) && (std::get<2>(components_at_destination.front()) ==
                trafo_id_source)){
            // distance to destination is smaller than maximal allowed number of hops
            unsigned int hop_distance = df_data->mc->hop_distance(node_source, destination +1);
            // if(df_data->el_hop_matrix[(node_source - 1) * df_data->components_at_nodes.size()
            // + destination] <= hops){
            if (hop_distance <= hops){
                for (auto component : components_at_destination) {
                    int component_id = std::get<0>(component);
                    int component_type = std::get<1>(component);
                    if (s_type == "producer") {
                        if (_negtype == NEGOTIATIONTYPE_P_INT) {
                            if (source_type != TYPE_BATTERY_INT) {
                                if (component_type == TYPE_WEC_INT ||
                                    component_type == TYPE_PV_INT ||
                                    component_type == TYPE_CHP_INT ||
                                    component_type == TYPE_BATTERY_INT ||
                                    component_type == TYPE_BIOFUEL_INT ||
                                    component_type == TYPE_TRANSFORMER_INT ||
                                    component_type == TYPE_SLACK_INT) {
                                    _agent.first = component_id;
                                    _agent.second = hop_distance;
                                    agents.push_back(_agent);
                                }
                            }
                            else {
                                /* do not suggest other batteries as producer to a battery */
                                if (component_type == TYPE_WEC_INT ||
                                    component_type == TYPE_PV_INT ||
                                    component_type == TYPE_CHP_INT ||
                                    component_type == TYPE_BIOFUEL_INT ||
                                    component_type == TYPE_TRANSFORMER_INT ||
                                    component_type == TYPE_SLACK_INT) {
                                    _agent.first = component_id;
                                    _agent.second = hop_distance;
                                    agents.push_back(_agent);
                                }
                            }
                        }
                        else if (_negtype == NEGOTIATIONTYPE_Q_INT) {
                            if (component_type == TYPE_WEC_INT ||
                                component_type == TYPE_PV_INT ||
                                component_type == TYPE_CHP_INT ||
                                component_type == TYPE_BATTERY_INT ||
                                component_type == TYPE_BIOFUEL_INT ||
                                component_type == TYPE_EV_INT ||
                                component_type == TYPE_TRANSFORMER_INT ||
                                component_type == TYPE_SLACK_INT) {
                                _agent.first = component_id;
                                _agent.second = hop_distance;
                                agents.push_back(_agent);
                            }
                        }

                    } else if (s_type == "consumer") {
                        if (_negtype == NEGOTIATIONTYPE_P_INT) {
                            if (component_type == TYPE_LOAD_INT ||
                                component_type == TYPE_EV_INT ||
                                component_type == TYPE_BATTERY_INT ||
                                component_type == TYPE_HP_INT||
                                component_type == TYPE_TRANSFORMER_INT ||
                                component_type == TYPE_SLACK_INT) {
                                _agent.first = component_id;
                                _agent.second = hop_distance;
                                agents.push_back(_agent);
                            }
                        }
                        else if (_negtype == NEGOTIATIONTYPE_Q_INT) {
                            if (component_type == TYPE_LOAD_INT ||
                                component_type == TYPE_TRANSFORMER_INT ||
                                component_type == TYPE_SLACK_INT) {
                                _agent.first = component_id;
                                _agent.second = hop_distance;
                                agents.push_back(_agent);
                            }
                        }

                    } else {
                        std::cerr << "WARNING in Directoryfacilitator_agent::getagents(...): s_type "
                                << s_type << " is unknown." << std::endl;
                    }
                }

            }
        }
    }
}

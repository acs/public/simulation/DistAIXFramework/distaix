/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include <algorithm>

#include "behavior/swarmgrid/sensor_knowledge.h"
#include "model/config.h"


Sensor_knowledge::Sensor_knowledge() : id(0), IO(nullptr), knowledge_comm(nullptr), voltage_old(0.0),
    filter_size_v(1), v_re_sum(1), v_im_sum(0), filter_size_p(1), p_loss_sum(0), q_loss_sum(0)
{
    voltage_meas.clear();
}

/*! \brief initialization of Knowledge class
 *  \param _id agent ID of owner agent
 *  \param _io pointer to agent's IO object
 *  \param know_comm pointer to agent's communication knowledge
 *  \param step_size simulation step size in s
 * */
void Sensor_knowledge::init(int &_id, BehaviorIO* _io, Communication_knowledge* know_comm,
    double step_size)
{
    voltage_meas.clear();

    id = _id;
    IO = _io;
    knowledge_comm = know_comm;

    /* filter size should be such that voltage is averaged within 20ms (one period) but at least 3 */
    voltage_old = 1;
    filter_size_v = 1;// 0.02 / step_size;
    //if (filter_size_v < 10) {
    //    filter_size_v = 10;
    //}
    struct meas_ex v_sample;
    v_sample.meas.real(1.0);
    for (unsigned int i = 0; i < filter_size_v; i++) {
        voltage_samples.push_back(v_sample);
    }
    v_re_sum = 1.0 * filter_size_v;
    v_im_sum = 0;

    /* loss filter size to 5min (at least 10 samples) */
    filter_size_p = 120 / step_size;
    if(filter_size_p < 10){
        filter_size_p = 10;
    }
    for (unsigned int i = 0; i < filter_size_p; i++) {
        p_loss_samples.push_back(0);
        q_loss_samples.push_back(0);
    }
    p_loss_sum = 0;
    q_loss_sum = 0;
}

/*! \brief add voltage measurement to be considered for moving average filter
 *  \param v_sample voltage measurement
 * */
void Sensor_knowledge::add_voltage_sample(struct meas_ex v_sample)
{
    struct meas_ex v_sample_old = voltage_samples.front();
    v_re_sum -= v_sample_old.meas.real();
    v_im_sum -= v_sample_old.meas.imag();
    voltage_samples.pop_front();
    voltage_samples.push_back(v_sample);
    v_re_sum += v_sample.meas.real();
    v_im_sum += v_sample.meas.imag();
    
    std::complex<double> voltage;
    voltage.real(v_re_sum / filter_size_v);
    voltage.imag(v_im_sum / filter_size_v);
    voltage_old = get_absolute_voltage(id);
    update_voltage_meas(id, voltage, v_sample.t);
}

/*! \brief getter for last voltage measurement
 *  \return old voltage measurement
 * */
double Sensor_knowledge::get_old_voltage_meas()
{
    return voltage_old;
}

/*! \brief  updates the specified voltage measurement values; if measurement entry does not exist,
            it will be created
 *  \param agent_id ID of the agent that has done the measurement
 *  \param voltage voltage measurement
 *  \param t time stamp of the measurement
 * */
void Sensor_knowledge::update_voltage_meas(int agent_id, std::complex<double> voltage, double t)
{
    for (auto& measurement : voltage_meas) {
        if (measurement.id == agent_id) {
            measurement.meas = voltage;
            measurement.t = t;
            measurement.updated = true;
            return;
        }
    }
    struct meas_ex meas(agent_id, QUERYTYPE_VOLTAGE, voltage, t, false);
    voltage_meas.push_back(meas);
}

/*! \brief get the specified voltage measurement
 *  \param agent_id ID of the agent that has made the measurement
 *  \return voltage measurement value
 * */
std::complex<double> Sensor_knowledge::get_voltage_meas(int agent_id)
{
    std::complex<double> voltage(0, 0);
    for (auto measurement : voltage_meas) {
        if (measurement.id == agent_id) {
            voltage = measurement.meas;
            break;
        }
    }
    return voltage;
}

/*! \brief get the specified absolute value of voltage measurement
 *  \param agent_id ID of the agent that has made the measurement
 *  \return voltage measurement value
 * */
double Sensor_knowledge::get_absolute_voltage(int agent_id)
{
    double v_abs = 0.0;
    for (auto measurement : voltage_meas) {
        if (measurement.id == agent_id) {
            v_abs = std::abs(measurement.meas);
            break;
        }
    }
    return v_abs;
}


/*! \brief  get the mean value of all voltage that have been updated since the last call of this
 *          function
 *  \return mean voltage value
 * */
double Sensor_knowledge::get_mean_voltage()
{
    double mean_voltage = 0;
    unsigned int meas_count = 0;
    for (auto& measurement : voltage_meas) {
        if (measurement.id != id) {
            meas_count++;
            mean_voltage += std::abs(measurement.meas);
            measurement.updated = false;
        }
    }
    
    if (meas_count > 0) {
        mean_voltage = mean_voltage / meas_count;
    }
    return mean_voltage;
}


/*! \brief get the minimum value of all voltages
 *  \return min voltage value
 * */
double Sensor_knowledge::get_min_voltage()
{
    double min_voltage = 10000000;
    for (auto& measurement : voltage_meas) {
        if (measurement.id != id) {
            double temp = std::abs(measurement.meas);
            if (temp < min_voltage) {
                min_voltage = temp;
            }
        }
    }
    return min_voltage;
}

/*! \brief get the maximum value of all voltages
 *  \return min voltage value
 * */
double Sensor_knowledge::get_max_voltage()
{
    double max_voltage = 0;
    for (auto& measurement : voltage_meas) {
        if (measurement.id != id) {
            double temp = std::abs(measurement.meas);
            if (temp > max_voltage) {
                max_voltage = temp;
            }
        }
    }
    return max_voltage;
}

/*! \brief check if all known voltage measurements have been updated since last evaluation
 *  \return indicates if values have been updated
 * */
bool Sensor_knowledge::all_voltage_meas_updated()
{
    bool all_values_updated = true;
    for (auto measurement : voltage_meas) {
        if (measurement.id != id) {
            if (!measurement.updated) {
                all_values_updated = false;
                break;
            }
        }
    }
    if (voltage_meas.size() == 0) {
        all_values_updated = false;
    }
    else if (voltage_meas.size() == 1 && voltage_meas[0].id == id) {
        all_values_updated = false;
    }
    
    return all_values_updated;
}

/*! \brief sets all voltage measurements to not updated (to be called after evaluation)
 * */
void Sensor_knowledge::reset_voltage_meas()
{
    for (auto& measurement : voltage_meas) {
        if (measurement.id != id) {
            measurement.updated = false;
        }
    }
}

/*! \brief get the IDs of all values which voltage measurement is currently stored
 *  \return vector of agent IDs
 * */
std::vector<int> Sensor_knowledge::get_ids_voltage_meas()
{
    std::vector<int> ids;
    for (auto measurement : voltage_meas) {
        if (measurement.id != id) {
            ids.push_back(measurement.id);
        }
    }
    return ids;
}

/*! \brief add single sample for power loss calculation
 *  \param p_loss active power loss sample
 *  \param q_loss reactive power loss sample
 * */
void Sensor_knowledge::add_loss_sample(double p_loss, double q_loss)
{
    double p_loss_sample_old = p_loss_samples.front();
    p_loss_sum -= p_loss_sample_old;
    p_loss_samples.pop_front();
    p_loss_samples.push_back(p_loss);
    p_loss_sum += p_loss;

    double q_loss_sample_old = q_loss_samples.front();
    q_loss_sum -= q_loss_sample_old;
    q_loss_samples.pop_front();
    q_loss_samples.push_back(q_loss);
    q_loss_sum += q_loss;
}

/*! \brief getter for active power loss
 *  \return active power loss
 * */
double Sensor_knowledge::get_p_loss()
{
    return p_loss_sum / filter_size_p;
}

/*! \brief getter for reactive power loss
 *  \return reactive power loss
 * */
double Sensor_knowledge::get_q_loss()
{
    return q_loss_sum / filter_size_p;
}

/*! \brief helper function for logging
 * */
void Sensor_knowledge::log_sensor_knowledge()
{
    std::stringstream temp;
    std::string output;

    temp << "\tSensor knowledge: " << std::endl;

    temp << "\t\tvoltage sensor values: ";
    for (unsigned int i = 0; i < voltage_meas.size(); i++) {
        temp << std::endl << "\t\t\t" << voltage_meas[i].id << ": " << voltage_meas[i].t << " " 
            << voltage_meas[i].meas;
    }
    temp << std::endl << "\t\tvoltage sample values: ";
    for (auto i : voltage_samples) {
        temp << std::endl << "\t\t\t" << i.id << ": " << i.t << " " << i.meas;
    }
    temp << std::endl << "\t\tactive power losses: " << get_p_loss() << " reactive power losses : "
        << get_q_loss();

    output = temp.str();
    IO->log_info(output);
}

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/swarmgrid/contract.h"

/*! \brief Constructor of class Contract
 *  \param _producer agent ID of producer agent
 *  \param _consumer agent ID of consumer agent
 *  \param _contracted_power power that was negotiated
 *  \param t time of the last update of this contract
 * */
Contract::Contract(int _producer, int _consumer, struct contract_ex _contracted_power, double t) :
    producer_id(_producer), consumer_id(_consumer), contracted_power(_contracted_power),
    t_last_update(t)
{}

/*! \brief Constructor of Contract class
 * */
Contract::Contract()
{
    producer_id = 0;
    consumer_id = 0;
}

/*! \brief getter for needed power
 *  \return needed power
 * */
double Contract::get_needed_power()
{
    return contracted_power.needed;
}

/*! \brief getter for optional power
 *  \return optional power
 * */
double Contract::get_optional_power()
{
    return contracted_power.optional;
}

/*! \brief Get the contracted power
 *  \return contracted power
 * */
struct contract_ex Contract::get_power()
{
    return contracted_power;
}

/*! \brief getter for producer ID
 *  \return agent ID of producer
 * */
int Contract::get_producer_id()
{
    return producer_id;
}

/*! \brief getter for consumer ID
 *  \return agent ID of consumer
 * */
int Contract::get_consumer_id()
{
    return consumer_id;
}

/*! \brief set id of contract partners
 *  \param _producer ID of producer agent
 *  \param _consumer ID of consumer agent
 * */
void Contract::set_contract_partners(int _producer, int _consumer)
{
    producer_id = _producer;
    consumer_id = _consumer;
}

/*! \brief function for changing an existing contract
 *  \param _contracted_power new power
 *  \param t time
 * */
void Contract::change_contract(struct contract_ex _contracted_power, double t)
{
    contracted_power = _contracted_power;
    if (fabs(contracted_power.needed) < POWER_ACCURACY) {
        contracted_power.needed = 0;
    }
    if (fabs(contracted_power.optional) < POWER_ACCURACY) {
        contracted_power.optional = 0;
    }
    t_last_update = t;
}

/*! \brief Helper function for logging
 *  \return string containing logging information about contract
 * */
std::string Contract::get_contract_output()
{
    std::stringstream temp;
    std::string output;

    temp << "Contract between: " << producer_id << " and " 
            << consumer_id << "; needed(" 
            << contracted_power.needed << ") optional(" 
            << contracted_power.optional << "), Time: " << t_last_update;
    output = temp.str();
    return output;
}

/*! \brief get time of last update
 *  \return time of last update
 * */
double Contract::get_t_last_update()
{
    return t_last_update;
}

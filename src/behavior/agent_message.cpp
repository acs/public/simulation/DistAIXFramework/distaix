/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/agent_message.h"

/*! \brief Default constructor for an agent message
 * */
Agent_message::Agent_message() : performative(0), sender(0), receiver(0), content(""), protocol(0),
    time_pending(0.0)
{
}

/*! \brief Constructor for an agent message
 * \param _perf [in]     The performative of the message
 * \param _sender [in]   ID of the sending agent
 * \param _receiver [in] ID of the receiving agent
 * \param _content [in]  content of the message as string representation
 * \param _protocol [in] message protocol in use
 * */
Agent_message::Agent_message(unsigned int _perf, int _sender, int _receiver,
                             std::string _content, unsigned _protocol):
                              performative(_perf), sender(_sender), receiver(_receiver),
                              content(std::move(_content)), protocol(_protocol), time_pending(0.0)
{
}

/*! \brief Copy constructor for an agent message
 * \param msg [in]   Reference to the message based on which the copy is created
 * */
Agent_message::Agent_message(const Agent_message &msg) {
    performative = msg.performative;
    sender = msg.sender;
    receiver = msg.receiver;
    content = msg.content;
    protocol = msg.protocol;
    time_pending = msg.time_pending;
}

/*! \brief Return the receiver of an agent message
 * \return ID of the receiver of the agent message
 * */
int Agent_message::get_receiver() {
    return  receiver;
}


/*!
*   \brief Writes the content of a message to a string for debugging
*   \return Message content as string
* */
std::string Agent_message::get_message_output()
{
    std::stringstream temp;
    std::string output;

    temp << "Sender: " << sender
            << " Receiver: " << receiver
            << " Protocol: ";
    switch (protocol) {
    case FIPA_PROT_CONTRACT_NET:
        temp << "CONTRACT NET";
        break;
    case FIPA_PROT_SUBSCRIBE:
        temp << "SUBSCRIBE";
        break;
    case FIPA_PROT_QUERY:
        temp << "QUERY";
        break;
    case FIPA_PROT_REQUEST:
        temp << "REQUEST";
        break;
    case FIPA_PROT_RECRUITING:
        temp << "RECRUITING";
        break;
    default :
        temp << "unknown";
        break;
    }
    temp << " Performative: ";
    switch (performative) {
    case FIPA_PERF_ACCEPT_PROPOSAL :
        temp << "ACCEPT PROPOSAL";
        break;
    case FIPA_PERF_AGREE :
        temp << "AGREE";
        break;
    case FIPA_PERF_CALL_FOR_PROPOSAL :
        temp << "CALL FOR PROPOSAL";
        break;
    case FIPA_PERF_FAILURE :
        temp << "FAILURE";
        break;
    case FIPA_PERF_INFORM :
        temp << "INFORM";
        break;
    case FIPA_PERF_NOT_UNDERSTOOD :
        temp << "NOT UNDERSTOOD";
        break;
    case FIPA_PERF_PROPOSE :
        temp << "PROPOSE";
        break;
    case FIPA_PERF_REFUSE :
        temp << "REFUSE";
        break;
    case FIPA_PERF_REJECT_PROPOSAL :
        temp << "REJECT PROPOSAL";
        break;
    case FIPA_PERF_REQUEST :
        temp << "REQUEST";
        break;
    case FIPA_PERF_SUBSCRIBE :
        temp << "SUBSCRIBE";
        break;
    case CUST_PERF_UNSUBSCRIBE :
        temp << "UNSUBSCRIBE";
        break;
    case FIPA_PERF_PROXY :
        temp << "PROXY";
        break;
    case FIPA_PERF_QUERY_REF :
        temp << "QUERY REF";
        break;
    default :
        temp << "unknown";
        break;
    }
    temp << " Content: " << content;
    temp << " Pending time: " << time_pending;

    output = temp.str();
    return output;
}

/*!
*   \brief Returns the time which a message is pending in the outgoing queue of an agent
*   \return Pending time in seconds
* */
double Agent_message::get_time_pending() {
    return time_pending;
}

/*!
*   \brief Updates the time which a message is pending in the outgoing queue of an agent
*   \param _delay [in] Time in seconds to be added to the pending time
* */
void Agent_message::set_time_pending(double _delay) {
    time_pending += _delay;
}

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "behavior/mqtt_highload/mqtt_highload_message.h"
#include <sstream>

std::vector<Meta_infos> Mqtt_highload_msg::mqtt_highload_msg_meta = std::vector<Meta_infos>();
bool Mqtt_highload_msg::meta_initialized = false;

Mqtt_highload_msg::Mqtt_highload_msg() : Villas_message() {

    performative = nullptr;
    sender = nullptr;
    receiver = nullptr;
    measurement_type_1 = nullptr;
    measurement_value_1 = nullptr;
    measurement_type_2 = nullptr;
    measurement_value_2 = nullptr;
    measurement_type_3 = nullptr;
    measurement_value_3 = nullptr;

    init_message(0,0,0,0,0.0,0,0.0,0,0.0);
}

Mqtt_highload_msg::Mqtt_highload_msg(Villas_message &msg) : Villas_message(){
    data = msg.data;
    length = msg.length;

}


Mqtt_highload_msg::Mqtt_highload_msg(int _performative, int _sender, int _receiver) {
    performative = nullptr;
    sender = nullptr;
    receiver = nullptr;
    measurement_type_1 = nullptr;
    measurement_value_1 = nullptr;
    measurement_type_2 = nullptr;
    measurement_value_2 = nullptr;
    measurement_type_3 = nullptr;
    measurement_value_3 = nullptr;

    init_message(_performative,_sender,_receiver,0,0.0,0,0.0,0,0.0);
}

void Mqtt_highload_msg::set_pointers() {

    //set the pointer to the elements in data field
    //if you change the order or data types of data elements you have to adapt the indices here
    performative = &(data[0].i);
    sender = &(data[1].i);
    receiver = &(data[2].i);
    measurement_type_1 = &(data[3].i);
    measurement_value_1 = &(data[4].f);
    measurement_type_2 = &(data[5].i);
    measurement_value_2 = &(data[6].f);
    measurement_type_3 = &(data[7].i);
    measurement_value_3 = &(data[8].f);
}

void Mqtt_highload_msg::init_message(int _performative, int _sender, int _receiver, 
                                     int _measurement_type_1, double _measurement_value_1, 
                                     int _measurement_type_2, double _measurement_value_2, 
                                     int _measurement_type_3, double _measurement_value_3) {

    //add all data and meta elements of this message type
    Data_element perf_data;
    perf_data.i = _performative;
    add_element(perf_data, VILLAS_DATA_TYPE_INT64);

    Data_element sender_data;
    sender_data.i = _sender;
    add_element(sender_data, VILLAS_DATA_TYPE_INT64);

    Data_element receiver_data;
    receiver_data.i = _receiver;
    add_element(receiver_data, VILLAS_DATA_TYPE_INT64);


    Data_element measurement_type_data_1;
    measurement_type_data_1.i = _measurement_type_1;
    add_element(measurement_type_data_1, VILLAS_DATA_TYPE_INT64);

    Data_element measurement_value_data_1;
    measurement_value_data_1.f = _measurement_value_1;
    add_element(measurement_value_data_1, VILLAS_DATA_TYPE_DOUBLE);


    Data_element measurement_type_data_2;
    measurement_type_data_2.i = _measurement_type_2;
    add_element(measurement_type_data_2, VILLAS_DATA_TYPE_INT64);

    Data_element measurement_value_data_2;
    measurement_value_data_2.f = _measurement_value_2;
    add_element(measurement_value_data_2, VILLAS_DATA_TYPE_DOUBLE);


    Data_element measurement_type_data_3;
    measurement_type_data_3.i = _measurement_type_3;
    add_element(measurement_type_data_3, VILLAS_DATA_TYPE_INT64);

    Data_element measurement_value_data_3;
    measurement_value_data_3.f = _measurement_value_3;
    add_element(measurement_value_data_3, VILLAS_DATA_TYPE_DOUBLE);

    //set the pointers of the message to the corresponding fields in data
    set_pointers();
}


void Mqtt_highload_msg::init_meta() {
    // returns immediately if static meta information has already been initialized for this message type
    // initialize meta info only once
    if(!Mqtt_highload_msg::meta_initialized) {
        Mqtt_highload_msg::meta_initialized = true;
        Mqtt_highload_msg::mqtt_highload_msg_meta.clear();

        Meta_infos perf_meta;
        perf_meta.name = "performative";
        perf_meta.unit = "peformative of this message";
        perf_meta.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_highload_msg::mqtt_highload_msg_meta.push_back(perf_meta);

        Meta_infos sender_meta;
        sender_meta.name = "sender";
        sender_meta.unit = "sender of this message";
        sender_meta.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_highload_msg::mqtt_highload_msg_meta.push_back(sender_meta);

        Meta_infos receiver_meta;
        receiver_meta.name = "receiver";
        receiver_meta.unit = "receiver of this message";
        receiver_meta.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_highload_msg::mqtt_highload_msg_meta.push_back(receiver_meta);


        Meta_infos measurement_type_meta_1;
        measurement_type_meta_1.name = "measurement_type_1";
        measurement_type_meta_1.unit = "measurement_type_1 of this message";
        measurement_type_meta_1.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_highload_msg::mqtt_highload_msg_meta.push_back(measurement_type_meta_1);

        Meta_infos measurement_value_meta_1;
        measurement_value_meta_1.name = "measurement_value_1";
        measurement_value_meta_1.unit = "measurement_value_1 of this message";
        measurement_value_meta_1.type = VILLAS_DATA_TYPE_DOUBLE;
        Mqtt_highload_msg::mqtt_highload_msg_meta.push_back(measurement_value_meta_1);

                Meta_infos measurement_type_meta_2;
        measurement_type_meta_2.name = "measurement_type_2";
        measurement_type_meta_2.unit = "measurement_type_2 of this message";
        measurement_type_meta_2.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_highload_msg::mqtt_highload_msg_meta.push_back(measurement_type_meta_2);

        Meta_infos measurement_value_meta_2;
        measurement_value_meta_2.name = "measurement_value_2";
        measurement_value_meta_2.unit = "measurement_value_2 of this message";
        measurement_value_meta_2.type = VILLAS_DATA_TYPE_DOUBLE;
        Mqtt_highload_msg::mqtt_highload_msg_meta.push_back(measurement_value_meta_2);

                Meta_infos measurement_type_meta_3;
        measurement_type_meta_3.name = "measurement_type_3";
        measurement_type_meta_3.unit = "measurement_type_3 of this message";
        measurement_type_meta_3.type = VILLAS_DATA_TYPE_INT64;
        Mqtt_highload_msg::mqtt_highload_msg_meta.push_back(measurement_type_meta_3);

        Meta_infos measurement_value_meta_3;
        measurement_value_meta_3.name = "measurement_value_3";
        measurement_value_meta_3.unit = "measurement_value_3 of this message";
        measurement_value_meta_3.type = VILLAS_DATA_TYPE_DOUBLE;
        Mqtt_highload_msg::mqtt_highload_msg_meta.push_back(measurement_value_meta_3);

    }

}

/*!
*   \brief Writes the content of a message to a string for debugging
*   \return Message content as string
* */
std::string Mqtt_highload_msg::get_message_output()
{
    std::stringstream temp;
    std::string output;

    temp    << "Time: " << time_sec
            << " Sender: " << *sender
            << " Receiver: " << *receiver
            << " Performative: " << *performative
            << " Type_1: " << *measurement_type_1
            << " Value_1: " << *measurement_value_1
            << " Type_2: " << *measurement_type_2
            << " Value_2: " << *measurement_value_2
            << " Type_3: " << *measurement_type_3
            << " Value_3: " << *measurement_value_3;

    output = temp.str();
    return output;
}
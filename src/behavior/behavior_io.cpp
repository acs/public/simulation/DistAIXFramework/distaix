/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/


#include <boost/mpi.hpp>
#include "behavior/behavior_io.h"

/*!
 * \brief Construct a new BehaviorIO object
 * \param _id Id of the agent
 * \param _d_props Struct containing information about log file
 */
BehaviorIO::BehaviorIO(int &_id, struct data_props _d_props) : IO_object(_d_props), id(_id)
{
    if(id == TYPE_DF_INT){
        d_props.log.path_log_file = "runlog/behavior/agent_DF" + std::to_string(_id) + ".log";
    }

}

/*!
 * \brief initialization of behavior logging
 */
void BehaviorIO::init_logging()
{
    init_logfile();
    update_t(0.0);
}

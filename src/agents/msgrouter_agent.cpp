/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "agents/msgrouter_agent.h"

/*! \brief Constructor used for creation of copies of non-local agents
 *  \param id [in]          RepastHPC agent id
 *  \param is_local [in]    true if local agent is created, false otherwise
 * */
Messagerouter_agent::Messagerouter_agent(repast::AgentId &id, bool is_local) : Agent(id, is_local) {}

/*! \brief Constructor used for creation of agents in model
 *  \param id [in]                  RepastHPC agent id
 *  \param is_local [in]            true if local agent is created, false otherwise
 *  \param step_size [in]           size of a simulation step in seconds
 *  \param _behavior_type [in]      user selected behavior for agents (0=non-intelligent, 1=swarm intelligence)
 *  \param _model_type [in]         user selected type of el. models (0=static phasor models, 1=dynamic phasor models)
 *  \param _subtype [in]            subtype of the agent
 *  \param _d_props [in]            properties of logging and result saving
 *  \param _villas_config [in]      configuration for villas node
 *  \param _use_commdata [in]       true if latencies shall be taken from an input file, false if default latency shall be used
 * */
Messagerouter_agent::Messagerouter_agent(repast::AgentId &id,
                                         bool is_local,
                                         double &step_size,
                                         int &_behavior_type,
                                         int &_model_type,
                                         std::string &_subtype,
                                         struct data_props _d_props,
                                         villas_node_config *_villas_config,
                                         bool &_use_commdata
) : Agent(id, is_local, step_size, _behavior_type, _model_type, _subtype, _d_props) {

    if (is_local_instance) {

        IO->init_logging();

        IO->log_info("Creating Message Router agent " + IO->id2str(id));

        mt_gen = new boost::mt19937(time(0)); //time(0) is seed = randomness
        drop_probability = new boost::uniform_int<>(0,1000);
        drop_probability_generator = new boost::variate_generator<boost::mt19937, boost::uniform_int<> >(*mt_gen,*drop_probability);
    }

    model_data.use_commdata =_use_commdata;
    model_data.msg_transmitted = 0;
    model_data.msg_pending = 0;
    model_data.msg_to_other_MR = 0;
    model_data.msg_dropped =0;
    model_data.msg_new =0;

    model_data.latency_matrix = nullptr;
    model_data.per_matrix = nullptr;
    model_data.from_to_matrix = nullptr;
    model_data.from_to_matrix_max = nullptr;
    model_data.from_to_matrix_step = nullptr;
    model_data.from_to_DF = nullptr;
    model_data.from_to_DF_max = nullptr;
    model_data.from_to_DF_step = nullptr;
    model_data.number_of_nodes = 0;

    model_data.number_of_steps = 0;
}


/*! \brief Destructor of a Message Router agent*/
Messagerouter_agent::~Messagerouter_agent() {
    if(is_local_instance){
        if(model_data.use_commdata) {
            //this->print_from_to_matrix();
            //IO->log_from_to_matrix(&model_data);
            //log the comm matrices
            log_comm_matrices();
            free(model_data.from_to_matrix);
            free(model_data.from_to_matrix_max);
            free(model_data.from_to_matrix_step);
            free(model_data.from_to_DF);
            free(model_data.from_to_DF_max);
            free(model_data.from_to_DF_step);
        }
        delete drop_probability_generator;
        delete drop_probability;
        delete mt_gen;
        IO->finish_io();
    }


}

/*! \brief Route all agent messages that can be delivered inside of the agents's process
 * \param agent_network_comm [in] RepastHPC shared network containing all edges between Message Router agents
 * */
void Messagerouter_agent::route_outgoing_agent_messages_own(repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> >* agent_network_comm) {

    //first: process all pending messages:
    this->process_pending_messages(agent_network_comm);
    // at this point pending messages are either routed to their target or remain pending

    /*get all adjacent agents*/
    std::vector<Agent*> adjacent_agents;
    agent_network_comm->adjacent(this, adjacent_agents);
    int counter = 0;
    for(unsigned int i = 0; i<adjacent_agents.size(); i++){
        Agent* a = adjacent_agents[i];
        if(a->getId().agentType() != TYPE_MR_INT && a->getId().agentType() != TYPE_NODE_INT){
            while(!a->outgoing_is_empty()){
                /*process all outgoing messages of agent a*/
                model_data.msg_new++;
                Agent_message next_msg = a->get_next_outgoing_message();

                double delay = message_delay(next_msg);
                if(!message_dropped(next_msg)) {
                    if (delay - t_step < EPSILON) {
                        // if delay is smaller or equal simulation step size
                        //message can be sent directly
                        int receiver = next_msg.get_receiver();
                        //check if receiver is in own process
                        bool receiver_in_own_process = false;
                        Agent *rec = nullptr;
                        for (auto r : adjacent_agents) {
                            if (r->getId().id() == receiver) {
                                receiver_in_own_process = true;
                                rec = r;
                                break;
                            }
                        }

                        if (receiver_in_own_process) {
                            rec->receive_message(next_msg);
                        } else {
                            /*if receiver is not in own process, add message to outgoing queue of this process*/
                            send_message(next_msg);
                            model_data.msg_to_other_MR++;
                        }

                        model_data.msg_transmitted++;

                        count_msg(next_msg);

                    } else {
                        //otherwise: message is added to pending messages at least for one simulation step
                        this->add_to_pending(next_msg);
                    }
                }
                else{
                }
                counter++;
            }
        }
    }
}

/*! \brief Route all agent messages that come from agents in other processes an have to be delivered in this process
 * \param agent_network_comm [in] RepastHPC shared network containing all edges between Message Router agents
 * */
void Messagerouter_agent::route_outgoing_agent_messages_other(repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> >* agent_network_comm) {
    /*get all adjacent agents*/
    std::vector<Agent*> adjacent_agents;
    agent_network_comm->adjacent(this, adjacent_agents);
    for(unsigned int i = 0; i<adjacent_agents.size(); i++) {
        Agent *a = adjacent_agents[i];
        if (a->getId().agentType() == TYPE_MR_INT) {
            /*process all outgoing messages of other processes*/
            while(!a->outgoing_is_empty()) {
                Agent_message next_msg = a->get_next_outgoing_message();
                int receiver = next_msg.get_receiver();
                //check if receiver is in own process
                bool receiver_in_own_process = false;
                Agent *rec = nullptr;
                for (auto r : adjacent_agents) {
                    if (r->getId().id() == receiver) {
                        receiver_in_own_process = true;
                        rec = r;
                        break;
                    }
                }

                if (receiver_in_own_process) {
                    rec->receive_message(next_msg);
                }
            }
        }
    }
    outgoing_messages.clear();
}

/*! \brief advance a simulation step
 * */
void Messagerouter_agent::step(){
    //proceed one time step

    if (is_local_instance) {
        IO->save_result(&model_data, first_step, tm_csv, tm_db, tm_db_serialize, tm_db_add);
#ifdef USE_DB
        if (first_step) {
            IO->save_meta(&model_data);
        }
#endif

        t_next += t_step;
        IO->update_t(t_next);

        //reset counters
        model_data.msg_transmitted = 0;
        model_data.msg_pending = 0;
        model_data.msg_to_other_MR = 0;
        model_data.msg_dropped = 0;
        model_data.msg_new = 0;

        IO->log_info("Step: " + std::to_string(t_next) + "s");
        IO->flush_log();
    }
    if(first_step) {
        first_step = false;
    }

    if(model_data.use_commdata) {
        //ignore a certain amount of steps to ignore build up phase of swarms
        if (model_data.number_of_steps >= NUMBER_OF_IGNORED_STEPS) {
            /*determine new max based on step matrix*/
            for (int i = 0; i < model_data.number_of_nodes * model_data.number_of_nodes; i++) {
                if (model_data.from_to_matrix_step[i] > model_data.from_to_matrix_max[i]) {
                    model_data.from_to_matrix_max[i] = model_data.from_to_matrix_step[i];
                }
                if(model_data.number_of_steps * t_step >= START_EXTRA1_LOGGING_SEC && model_data.number_of_steps * t_step < END_EXTRA1_LOGGING_SEC){
                    //extra1 logging here
                    if (model_data.from_to_matrix_step_extra1[i] > model_data.from_to_matrix_max_extra1[i]) {
                        model_data.from_to_matrix_max_extra1[i] = model_data.from_to_matrix_step_extra1[i];
                    }
                }
                else if(model_data.number_of_steps * t_step >= START_EXTRA2_LOGGING_SEC && model_data.number_of_steps * t_step < END_EXTRA2_LOGGING_SEC){
                    //extra2 logging here
                    if (model_data.from_to_matrix_step_extra2[i] > model_data.from_to_matrix_max_extra2[i]) {
                        model_data.from_to_matrix_max_extra2[i] = model_data.from_to_matrix_step_extra2[i];
                    }
                }
            }
            for (int i = 0; i < model_data.number_of_nodes; i++) {
                if (model_data.from_to_DF_step[i] > model_data.from_to_DF_max[i]) {
                    model_data.from_to_DF_max[i] = model_data.from_to_DF_step[i];
                }
            }

            /*reset from_to_matrix_step*/
            for (int i = 0; i < model_data.number_of_nodes * model_data.number_of_nodes; i++) {
                model_data.from_to_matrix_step[i] = 0;
                model_data.from_to_matrix_step_extra1[i] = 0;
                model_data.from_to_matrix_step_extra2[i] = 0;
            }
            for (int i = 0; i < model_data.number_of_nodes; i++) {
                model_data.from_to_DF_step[i] = 0;
            }
        }

        //increment number of steps
        model_data.number_of_steps++;
    }
}

/*! \brief append an agent message to the queue of outgoing messages
 * \param msg [in] message to be sent
 * */
void Messagerouter_agent::send_message(Agent_message msg) {

    /*insert message into outgoing queue of target process*/
    outgoing_messages.push_back(msg);
    msg_sent++;
    IO->log_info("\t\tSend message: " + msg.get_message_output());

}



/*! \brief Add a message to the pending messages vector
 *  \param msg [in] message to be sent
 * */
void Messagerouter_agent::add_to_pending(Agent_message msg) {
    //being added to pending messages means that is has to wait at least one simulation step
    msg.set_time_pending(t_step);
    pending_messages.push_back(msg);
    model_data.msg_pending++;
}


/*! \brief Process all pending messages; either they stay in pending messages vector or they are sent
 *  \param agent_network_comm [in] RepastHPC shared network containing all edges between Message Router agents
 * */
void Messagerouter_agent::process_pending_messages(repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> >* agent_network_comm) {

    std::vector<Agent*> adjacent_agents;
    agent_network_comm->adjacent(this, adjacent_agents);
    std::list<Agent_message> pending_old = pending_messages;
    pending_messages.clear();

    for(auto i : pending_old){
        double pending_time = i.get_time_pending();
        Agent_message next_msg = i;
        double delay = message_delay(next_msg);
        if(delay - pending_time < EPSILON){
            //message to be send in this time step
            int receiver = next_msg.get_receiver();
            //check if receiver is in own process
            bool receiver_in_own_process = false;
            Agent * rec = nullptr;
            for(auto r : adjacent_agents){
                if(r->getId().id() == receiver){
                    receiver_in_own_process = true;
                    rec = r;
                    break;
                }
            }

            if(receiver_in_own_process){
                rec->receive_message(next_msg);
            }
            else{
                /*if receiver is not in own process, add message to outgoing queue of this process*/
                send_message(next_msg);
                model_data.msg_to_other_MR++;
            }
            model_data.msg_transmitted++;

            count_msg(next_msg);

        }
        else{
            // message needs to be revisited in next simulation step
            this->add_to_pending(next_msg);

        }

    }
}

/*! \brief decide if a message is dropped
 *  \param msg [in]     Agent_message for which a decision has to be made
 *  \return             true if message is dropped and false otherwise
 * */
bool Messagerouter_agent::message_dropped(Agent_message &msg) {

    /* TODO the following line is added to simulate without dropped messages
     * TODO as soon as agent behavior can handle lost messages, the commented code of this function should be used*/
    return false;

//    double prob = (double) drop_probability_generator->operator()() / 1000.0;
//    bool dropped;
//    double PER = 0;
//
//    if(!model_data.use_commdata) {
//        PER = PACKAGE_DROP_PROB_DEFAULT;
//        if (prob <= PER) {
//            dropped = true;
//            model_data.msg_dropped++;
//        }
//        else {
//            dropped = false;
//        }
//    }
//    else{
//
//        //determine nodes:
//        int sender_node = (*model_data.agents_to_nodes_map.find(msg.sender)).second;
//        int receiver_node = (*model_data.agents_to_nodes_map.find(msg.receiver)).second;
//        PER = model_data.per_matrix[(sender_node-1)*model_data.number_of_nodes+(receiver_node-1)];
//
//        if (prob <= PER) {
//            dropped = true;
//            model_data.msg_dropped++;
//        }
//        else {
//            dropped = false;
//        }
//    }
//
//    return dropped;

}

/*! \brief determine the delay of a message based on the link latency
 *  \param msg [in]     Agent_message for which a delay shall be determined
 *  \return             delay of the message in seconds
 * */
double Messagerouter_agent::message_delay(Agent_message &msg){
    double latency;

#ifndef COMM_DELAY_START_OFF
    if(!model_data.use_commdata){
        latency = COMM_DELAY_DEFAULT;
    }
    else{
        //determine nodes:
        if(msg.sender != TYPE_DF_INT && msg.receiver != TYPE_DF_INT) {
            int sender_node = get_node(msg.sender);//(*model_data.agents_to_nodes_map.find(msg.sender)).second;
            int receiver_node = get_node(msg.receiver); //(*model_data.agents_to_nodes_map.find(msg.receiver)).second;


            latency = model_data.latency_matrix[(sender_node - 1) * model_data.number_of_nodes + (receiver_node - 1)];
        }
        else{
            latency = 0.0; //TODO: communication to DF is assumed to have latency 0; is this a valid assumption?
        }
    }
#else
    if (t_next < COMM_DELAY_TIME_ON) {
        latency = 0.0;
    }
    else {
        if(!model_data.use_commdata){
            latency = COMM_DELAY_DEFAULT;
        }
        else{
            //determine nodes:
            if(msg.sender != TYPE_DF_INT && msg.receiver != TYPE_DF_INT) {
                int sender_node = get_node(msg.sender);//(*model_data.agents_to_nodes_map.find(msg.sender)).second;
                int receiver_node = get_node(msg.receiver); //(*model_data.agents_to_nodes_map.find(msg.receiver)).second;

                latency = model_data.latency_matrix[(sender_node - 1) * model_data.number_of_nodes + (receiver_node - 1)];
            }
            else{
                latency = 0.0; //TODO: communication to DF is assumed to have latency 0; is this a valid assumption?
            }
        }
    }
#endif

    return latency;
}

/*! \brief ser the latency matrix
 *  \param _latency_matrix [in] latency matrix to be used (coming from model)
 * */
void Messagerouter_agent::set_latency(double *_latency_matrix) {

    model_data.latency_matrix = _latency_matrix;
}


/*! \brief set the latency and packet error rate matrices
 *  \param _latency_matrix [in] latency matrix to be used (coming from model)
 *  \param _per_matrix [in] packet error rate matrix to be used (coming from model)
 * */
void Messagerouter_agent::set_comm_data(double *_latency_matrix, double *_per_matrix) {

    model_data.latency_matrix = _latency_matrix;
    model_data.per_matrix = _per_matrix;
}

/*! \brief initialize the matrices for counting message exchange between nodes
 *  \param _number_of_nodes [in] number of nodes in the system
 * */
void Messagerouter_agent::init_count_matrix(int _number_of_nodes){
    model_data.number_of_nodes = _number_of_nodes;

    if(model_data.use_commdata) {

        //allocate memory for from_to_matrix
        model_data.from_to_matrix = (unsigned int *) malloc(
                model_data.number_of_nodes * model_data.number_of_nodes * sizeof(unsigned int));
        model_data.from_to_matrix_max = (int *) malloc(
                model_data.number_of_nodes * model_data.number_of_nodes * sizeof(unsigned int));
        model_data.from_to_matrix_step = (int *) malloc(
                model_data.number_of_nodes * model_data.number_of_nodes * sizeof(unsigned int));

        //allocate memory for from_to_matrix_extra1
        model_data.from_to_matrix_extra1 = (unsigned int *) malloc(
                model_data.number_of_nodes * model_data.number_of_nodes * sizeof(unsigned int));
        model_data.from_to_matrix_max_extra1 = (int *) malloc(
                model_data.number_of_nodes * model_data.number_of_nodes * sizeof(unsigned int));
        model_data.from_to_matrix_step_extra1 = (int *) malloc(
                model_data.number_of_nodes * model_data.number_of_nodes * sizeof(unsigned int));

        //allocate memory for from_to_matrix_extra2
        model_data.from_to_matrix_extra2 = (unsigned int *) malloc(
                model_data.number_of_nodes * model_data.number_of_nodes * sizeof(unsigned int));
        model_data.from_to_matrix_max_extra2 = (int *) malloc(
                model_data.number_of_nodes * model_data.number_of_nodes * sizeof(unsigned int));
        model_data.from_to_matrix_step_extra2 = (int *) malloc(
                model_data.number_of_nodes * model_data.number_of_nodes * sizeof(unsigned int));

        //allocate memory for DF message vectors
        model_data.from_to_DF = (unsigned int *) malloc(model_data.number_of_nodes * sizeof(unsigned int));
        model_data.from_to_DF_max = (int *) malloc(model_data.number_of_nodes * sizeof(unsigned int));
        model_data.from_to_DF_step = (int *) malloc(model_data.number_of_nodes * sizeof(unsigned int));

        //init matrices and vectors
        for (int i = 0; i < model_data.number_of_nodes * model_data.number_of_nodes; i++) {
            model_data.from_to_matrix[i] = 0;
            model_data.from_to_matrix_max[i] = -1;
            model_data.from_to_matrix_step[i] = 0;

            model_data.from_to_matrix_extra1[i] = 0;
            model_data.from_to_matrix_max_extra1[i] = -1;
            model_data.from_to_matrix_step_extra1[i] = 0;

            model_data.from_to_matrix_extra2[i] = 0;
            model_data.from_to_matrix_max_extra2[i] = -1;
            model_data.from_to_matrix_step_extra2[i] = 0;
        }
        for (int i = 0; i < model_data.number_of_nodes; i++) {
            model_data.from_to_DF[i] = 0;
            model_data.from_to_DF_max[i] = -1;
            model_data.from_to_DF_step[i] = 0;
        }
    }
}

/*! \brief Set the reference to a vector containing information about which components are connected to which node
 * \param _components_at_nodes [in] Reference to vector containing information about which components are connected to which node
 * */
void Messagerouter_agent::set_components_at_nodes(std::vector<std::list<std::tuple<int,int,int>>> &_components_at_nodes){
    model_data.components_at_nodes = _components_at_nodes;
}


/*! \brief Obtain the node ID for a given component ID
 * \param _component_id [in]    Component ID for which the node ID shall be determined
 * \return                      Node ID
 * */
int Messagerouter_agent::get_node(int _component_id) {
   int node_id = 1;
   bool node_found = false;
   for(auto components : model_data.components_at_nodes){
       for(auto comp : components){
           if(std::get<0>(comp) == _component_id){
               node_found = true;
               break;
           }

       }
       if(node_found){
           break;
       }
       node_id++;
   }
   if(node_found){
       return node_id;
   }
   else{
       //return -1 in case node ID was not found
       return -1;
   }

}

/*! \brief Count the message exchange between nodes
 * \param msg [in] message to be added to the count
 * */
void Messagerouter_agent::count_msg(Agent_message &msg){
    if(model_data.use_commdata) {
        //ignore a certain amount of steps at beginning to ignore swarm setup phase
        if (model_data.number_of_steps >= NUMBER_OF_IGNORED_STEPS) {
            if (model_data.use_commdata && msg.receiver != TYPE_DF_INT &&
                msg.sender != TYPE_DF_INT) {
                int sender_node = get_node(msg.sender);
                int receiver_node = get_node(msg.receiver);
                model_data.from_to_matrix[(sender_node - 1) * model_data.number_of_nodes +
                                          (receiver_node - 1)]++;
                model_data.from_to_matrix_step[(sender_node - 1) * model_data.number_of_nodes +
                                               (receiver_node - 1)]++;
                if (model_data.number_of_steps * t_step >= START_EXTRA1_LOGGING_SEC &&
                    model_data.number_of_steps * t_step < END_EXTRA1_LOGGING_SEC) {
                    //extra1 logging here
                    model_data.from_to_matrix_extra1[
                            (sender_node - 1) * model_data.number_of_nodes + (receiver_node - 1)]++;
                    model_data.from_to_matrix_step_extra1[
                            (sender_node - 1) * model_data.number_of_nodes +
                            (receiver_node - 1)]++;
                } else if (model_data.number_of_steps * t_step >= START_EXTRA2_LOGGING_SEC &&
                           model_data.number_of_steps * t_step < END_EXTRA2_LOGGING_SEC) {
                    //extra2 logging here
                    model_data.from_to_matrix_extra2[
                            (sender_node - 1) * model_data.number_of_nodes + (receiver_node - 1)]++;
                    model_data.from_to_matrix_step_extra2[
                            (sender_node - 1) * model_data.number_of_nodes +
                            (receiver_node - 1)]++;
                }
            } else if (model_data.use_commdata) {
                //count communication with DF
                int node;
                if (msg.sender == TYPE_DF_INT) {
                    node = get_node(msg.receiver);
                } else {
                    node = get_node(msg.sender);
                }
                model_data.from_to_DF[node - 1]++;
                model_data.from_to_DF_step[node - 1]++;
            }
        }
    }
}

/*! \brief Write the matrices counting message exchange between nodes to files
 * */
void Messagerouter_agent::log_comm_matrices() {
    if(model_data.use_commdata){

        //create subfolder for comm matrices
        std::string foldername = "MR_commdata";
        boost::filesystem::path destination(foldername);
        boost::filesystem::create_directory(destination);

        std::string path_result_file_comm_matrix = foldername +"/agent_MR" + std::to_string(id_.currentRank()) + "_comm_matrix.csv";
        std::string path_result_file_comm_matrix_max = foldername +"/agent_MR" + std::to_string(id_.currentRank()) + "_comm_matrix_max.csv";
        std::string path_result_file_comm_matrix_extra1 = foldername +"/agent_MR" + std::to_string(id_.currentRank()) + "_comm_matrix_extra1.csv";
        std::string path_result_file_comm_matrix_max_extra1 = foldername +"/agent_MR" + std::to_string(id_.currentRank()) + "_comm_matrix_max_extra1.csv";
        std::string path_result_file_comm_matrix_extra2 = foldername +"/agent_MR" + std::to_string(id_.currentRank()) + "_comm_matrix_extra2.csv";
        std::string path_result_file_comm_matrix_max_extra2 = foldername +"/agent_MR" + std::to_string(id_.currentRank()) + "_comm_matrix_max_extra2.csv";
        std::string path_result_file_comm_DF = foldername +"/agent_MR" + std::to_string(id_.currentRank()) + "_comm_DF.csv";
        std::string path_result_file_comm_DF_max = foldername +"/agent_MR" + std::to_string(id_.currentRank()) + "_comm_DF_max.csv";

        std::ofstream result_comm_matrix;
        std::ofstream result_comm_matrix_max;
        std::ofstream result_comm_matrix_extra1;
        std::ofstream result_comm_matrix_max_extra1;
        std::ofstream result_comm_matrix_extra2;
        std::ofstream result_comm_matrix_max_extra2;
        std::ofstream result_comm_DF;
        std::ofstream result_comm_DF_max;

        //open result streams
        try {
            result_comm_matrix.exceptions(std::ofstream::failbit | std::ofstream::badbit);
            result_comm_matrix_max.exceptions(std::ofstream::failbit | std::ofstream::badbit);
            result_comm_matrix_extra1.exceptions(std::ofstream::failbit | std::ofstream::badbit);
            result_comm_matrix_max_extra1.exceptions(std::ofstream::failbit | std::ofstream::badbit);
            result_comm_matrix_extra2.exceptions(std::ofstream::failbit | std::ofstream::badbit);
            result_comm_matrix_max_extra2.exceptions(std::ofstream::failbit | std::ofstream::badbit);
            result_comm_DF.exceptions(std::ofstream::failbit | std::ofstream::badbit);
            result_comm_DF_max.exceptions(std::ofstream::failbit | std::ofstream::badbit);
        }
        catch(std::ofstream::failure &err){
            std::cerr << "Agent " << id_ << ": set exceptions (msg router streams): " << err.what() << std::endl;
            MPI_Abort(MPI_COMM_WORLD, -2);
        }

        try{
            result_comm_matrix.open(path_result_file_comm_matrix);
            result_comm_matrix_max.open(path_result_file_comm_matrix_max);
            result_comm_matrix_extra1.open(path_result_file_comm_matrix_extra1);
            result_comm_matrix_max_extra1.open(path_result_file_comm_matrix_max_extra1);
            result_comm_matrix_extra2.open(path_result_file_comm_matrix_extra2);
            result_comm_matrix_max_extra2.open(path_result_file_comm_matrix_max_extra2);
            result_comm_DF.open(path_result_file_comm_DF);
            result_comm_DF_max.open(path_result_file_comm_DF_max);
        }
        catch(std::ofstream::failure &err){
            std::cerr << "Agent " << id_ << ": open/ write (msg router streams): " << err.what() << std::endl;
            result_comm_matrix.close();
            result_comm_matrix_max.close();
            result_comm_matrix_extra1.close();
            result_comm_matrix_max_extra1.close();
            result_comm_matrix_extra2.close();
            result_comm_matrix_max_extra2.close();
            result_comm_DF.close();
            result_comm_DF_max.close();
            MPI_Abort(MPI_COMM_WORLD, -3);
        }

        //save matrices
        for(int i = 0; i<model_data.number_of_nodes; i++){
            for(int k = 0; k<model_data.number_of_nodes; k++){
                result_comm_matrix << model_data.from_to_matrix[i*model_data.number_of_nodes + k];
                result_comm_matrix_max << model_data.from_to_matrix_max[i*model_data.number_of_nodes + k];

                result_comm_matrix_extra1 << model_data.from_to_matrix_extra1[i*model_data.number_of_nodes + k];
                result_comm_matrix_max_extra1 << model_data.from_to_matrix_max_extra1[i*model_data.number_of_nodes + k];
                result_comm_matrix_extra2 << model_data.from_to_matrix_extra2[i*model_data.number_of_nodes + k];
                result_comm_matrix_max_extra2 << model_data.from_to_matrix_max_extra2[i*model_data.number_of_nodes + k];

                if(k < model_data.number_of_nodes-1){
                    result_comm_matrix << ",";
                    result_comm_matrix_max << ",";
                    result_comm_matrix_extra1 << ",";
                    result_comm_matrix_max_extra1 << ",";
                    result_comm_matrix_extra2 << ",";
                    result_comm_matrix_max_extra2 << ",";
                }
                else{
                    result_comm_matrix << std::endl;
                    result_comm_matrix_max << std::endl;
                    result_comm_matrix_extra1 << std::endl;
                    result_comm_matrix_max_extra1 << std::endl;
                    result_comm_matrix_extra2 << std::endl;
                    result_comm_matrix_max_extra2 << std::endl;
                }
            }
        }

        //save DF vectors
        for(int i = 0; i<model_data.number_of_nodes; i++){
            result_comm_DF << model_data.from_to_DF[i] << std::endl;
            result_comm_DF_max << model_data.from_to_DF_max[i] << std::endl;
        }

        //flush result streams
        result_comm_matrix.flush();
        result_comm_matrix_max.flush();
        result_comm_matrix_extra1.flush();
        result_comm_matrix_max_extra1.flush();
        result_comm_matrix_extra2.flush();
        result_comm_matrix_max_extra2.flush();
        result_comm_DF.flush();
        result_comm_DF_max.flush();

        //close result streams
        result_comm_matrix.close();
        result_comm_matrix_max.close();
        result_comm_matrix_extra1.close();
        result_comm_matrix_max_extra1.close();
        result_comm_matrix_extra2.close();
        result_comm_matrix_max_extra2.close();
        result_comm_DF.close();
        result_comm_DF_max.close();
    }

}

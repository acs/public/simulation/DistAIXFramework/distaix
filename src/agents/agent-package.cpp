/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include <fstream>
#include "agents/agent-package.h"


BOOST_CLASS_EXPORT_GUID(repast::SpecializedProjectionInfoPacket<repast::RepastEdgeContent<Agent> >, "SpecializedProjectionInfoPacket_EDGE");

/* Serializable Agent Package Data */


/*! \brief Default constructor of AgentPackage class
 * */
AgentPackage::AgentPackage() { }


/*! \brief Constructor used in synchronization during FBS algorithm
 * \param _id [in]                      ID of the agent for which the package is created
 * \param _rank [in]                    starting rank (process number) of the agent for which the package is crated
 * \param _type [in]                    type of the agent for which the package is created
 * \param _currentRank [in]             current rank of the agent for which the package is created
 * \param _current [in]                 complex value of the current that shall be stored in the package
 * \param _voltage [in]                 complex value of the voltage that shall be stored in the package
 * \param _next_action_expected [in]    value of the next expected action in FBS algorithm to be stored in the package
 * \param _convergence [in]             boolean convergence variable to be stored in the package
 * */
AgentPackage::AgentPackage(int _id, int _rank, int _type, int _currentRank, std::complex<double> _current, std::complex<double> _voltage, int _next_action_expected, bool _convergence):
        id(_id), rank(_rank), type(_type), currentRank(_currentRank), current(_current), voltage(_voltage), next_action_expected(_next_action_expected), convergence(_convergence)
        {
}

/*! \brief Constructor used in synchronization if MR agents
 * \param _id [in]              ID of the agent for which the package is created
 * \param _rank [in]            starting rank (process number) of the agent for which the package is crated
 * \param _type [in]            type of the agent for which the package is created
 * \param _currentRank [in]     current rank of the agent for which the package is created
 * \param _msg_queue [in]       outgoing message queue of the MR agent to be stored in the package
 * */
AgentPackage::AgentPackage(int _id, int _rank, int _type, int _currentRank, std::list<Agent_message> _msg_queue):
                            id(_id), rank(_rank), type(_type), currentRank(_currentRank), msg_queue(std::move(_msg_queue))
{
}

/*! \brief Default constructor of AgentPackageProvider class
 * \param agentPtr [in] pointer to a RepastHPC Shared Context containing references to all agents which are contained in the providing process
 *
 * This class is used to provide AgentPackages to other processes during the repasthpc::synchronizeAgentStates(...) method called in Model::synchronize()
 * */
AgentPackageProvider::AgentPackageProvider(repast::SharedContext<Agent>* agentPtr): agents(agentPtr){}


/*! \brief Prepare an AgentPackage for a specific agent and append it to the vector of outgoing AgentPackages
 * \param agent [in]    pointer to the agent for which the AgentPackage shall be prepared
 * \param out [out]     reference to a vector of outgoing AgentPackages
 * */
void AgentPackageProvider::providePackage(Agent * agent, std::vector<AgentPackage>& out){
    repast::AgentId id = agent->getId();
    if(id.agentType() == TYPE_MR_INT){
        AgentPackage package(id.id(), id.startingRank(), id.agentType(), id.currentRank(), agent->get_outgoing_queue());
        out.push_back(package);
    }
    else{
        AgentPackage package(id.id(), id.startingRank(), id.agentType(), id.currentRank(), agent->get_current(),
                             agent->get_voltage(), agent->get_next_action_expected(), agent->get_convergence());
        out.push_back(package);
    }
}

/*! \brief Provide all AgentPackages for agents contained in a request
 * \param req [in]      RepastHPC AgentRequest object containing agents for which packages have to be created
 * \param out [out]     Reference to a vector of outgoing AgentPackages
 * */
void AgentPackageProvider::provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out){

    std::vector<repast::AgentId> ids = req.requestedAgents();
    for(auto i : ids){
        try {
            providePackage(agents->getAgent(i), out);
        }
        catch (std::exception &err){
            std::cerr << "Error in provide content. What: " << err.what() << std::endl;
        }
    }
}

/*! \brief Default constructor of AgentPackageReceiver class
 * \param agentPtr [in] pointer to a RepastHPC Shared Context containing references to all agents which are contained in the receiving process
 *
 * This class is used to receive AgentPackages from other processes during the repasthpc::synchronizeAgentStates(...) method called in Model::synchronize()
 * */
AgentPackageReceiver::AgentPackageReceiver(repast::SharedContext<Agent>* agentPtr): agents(agentPtr){}


/*! \brief Create a copy of an agent from another process in this process based on a received AgentPackage
 * \param package [in]  received AgentPackage
 * \return              pointer to the newly created agent copy
 * */
Agent * AgentPackageReceiver::createAgent(AgentPackage package){

    repast::AgentId id(package.id, package.rank, package.type, package.currentRank);
    if(package.type == TYPE_MR_INT ) {
        return new Agent(id, false, package.msg_queue);
    }
    else{
        return new Agent(id, false, package.current, package.voltage, package.next_action_expected,
                         package.convergence);
    }

}

/*! \brief Update the copy of an agent from another process in this process based on a received AgentPackage
 * \param package [in] received AgentPackage, the content of this package is used to update the agent copy
 * */
void AgentPackageReceiver::updateAgent(AgentPackage package){
    repast::AgentId id(package.id, package.rank, package.type);
    Agent * agent = agents->getAgent(id);
    if(package.type == TYPE_MR_INT){
        agent->set(package.currentRank, package.msg_queue);
    }
    else {

        agent->set(package.currentRank, package.current, package.voltage, package.next_action_expected,
                   package.convergence);
    }
}

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "agents/prosumer_agent.h"
#include "behavior/swarmgrid/swarm_prosumer_behavior.h"
#include "behavior/reference/ref_prosumer_behavior.h"
#include "behavior/ref_intcomm/refic_prosumer_behavior.h"
#include "behavior/mqtt_reference/mqtt_ref_prosumer_behavior.h"
#include "behavior/mqtt_pingpong/mqtt_pingpong_prosumer_behavior.h"
#include "behavior/ensure/ensure_prosumer_behavior.h"
#include "behavior/mqtt_highload/mqtt_highload_prosumer_behavior.h"
#include "component/battery.h"
#include "component/bio.h"
#include "component/chp.h"
#include "component/compensator.h"
#include "component/ev.h"
#include "component/hp.h"
#include "component/load.h"
#include "component/pv.h"
#include "component/wec.h"

/*! \brief Constructor used for creation of copies of non-local agents
 *  \param id [in]          RepastHPC agent id
 *  \param is_local [in]    true if local agent is created, false otherwise
 * */
Prosumer_agent::Prosumer_agent(repast::AgentId &id, bool is_local) : Agent(id, is_local) {}

/*! \brief Constructor used for creation of agents in model
 *  \param id [in]                  RepastHPC agent id
 *  \param is_local [in]            true if local agent is created, false otherwise
 *  \param step_size [in]           size of a simulation step in seconds
 *  \param _behavior_type [in]      user selected behavior for agents
 *  \param _model_type [in]         user selected type of el. models (0=static phasor models, 1=dynamic phasor models)
 *  \param profiles [in]            pointer to profile object
 *  \param interpolation_type [in]  interpolation type for the profile
 *  \param _d_props [in]            properties of logging and result saving
 *  \param _villas_config [in]      configuration for villas node
 *  \param _prosumer_data [in]      pointer to Prosumer data
 * */
Prosumer_agent::Prosumer_agent(repast::AgentId &id,
        bool is_local,
        double &step_size,
        int &_behavior_type,
        int &_model_type,
        Profile* profiles,
        std::string &interpolation_type,
        struct data_props _d_props,
        villas_node_config *_villas_config,
        Prosumer_data * _prosumer_data)
    : Agent(id, is_local, step_size, _behavior_type, _model_type, _prosumer_data->subtype, _d_props),
        prosumer_data(_prosumer_data)
{
    if (is_local_instance) {
        IO->init_logging();
        IO->log_info("Creating Prosumer agent " + IO->id2str(id) + " at node " +
            std::to_string(prosumer_data->node_id));

        /* el model */

        switch (prosumer_data->type) {
            case TYPE_BATTERY_INT: {
                prosumer_model = new Battery(model_type, step_size, prosumer_data->Vnom, prosumer_data->S_r,
                    prosumer_data->pf_min, prosumer_data->C_el, prosumer_data->P_nom, prosumer_data->SOC_el);
                break;
            }
            case TYPE_BIOFUEL_INT: {
                prosumer_model = new Bio(model_type, step_size, prosumer_data->Vnom, prosumer_data->S_r,
                    prosumer_data->pf_min, profiles, prosumer_data->profile1_id, prosumer_data->profile_scale);
                break;
            }
            case TYPE_CHP_INT: {
                prosumer_model = new CHP(model_type, step_size, prosumer_data->Vnom, prosumer_data->S_r,
                    prosumer_data->pf_min, prosumer_data->P_nom, prosumer_data->f_el, prosumer_data->P_sec, prosumer_data->f_el_sec, prosumer_data->C_th,
                    prosumer_data->P_nom/prosumer_data->f_el, prosumer_data->SOC_th, profiles,
                    prosumer_data->profile1_id, prosumer_data->profile2_id, prosumer_data->profile_scale, prosumer_data->profile_scale_ww);
                break;
            }
            case TYPE_COMPENSATOR_INT: {
                prosumer_model = new Compensator(model_type, step_size, prosumer_data->Vnom, prosumer_data->S_r,
                    prosumer_data->N);
                break;
            }
            case TYPE_EV_INT: {
                prosumer_model = new EV(model_type, step_size, prosumer_data->Vnom, prosumer_data->S_r,
                    prosumer_data->pf_min, prosumer_data->C_el, prosumer_data->P_nom, prosumer_data->SOC_el,
                    profiles, prosumer_data->profile1_id, prosumer_data->profile2_id, prosumer_data->profile_scale);
                break;
            }
            case TYPE_HP_INT: {
                prosumer_model = new HP(model_type, step_size, prosumer_data->Vnom, prosumer_data->S_r,
                    prosumer_data->pf_min, prosumer_data->P_nom, prosumer_data->f_el, prosumer_data->P_sec, prosumer_data->f_el_sec, prosumer_data->C_th,
                    prosumer_data->P_nom/prosumer_data->f_el, prosumer_data->SOC_th, profiles,
                    prosumer_data->profile1_id, prosumer_data->profile2_id, prosumer_data->profile_scale, prosumer_data->profile_scale_ww);
                break;
            }
            case TYPE_LOAD_INT: {
                prosumer_model = new Load(model_type, step_size, prosumer_data->Vnom, prosumer_data->S_r,
                    prosumer_data->pf_min, profiles, prosumer_data->profile1_id, prosumer_data->profile2_id,
                    prosumer_data->profile_scale);
                break;
            }
            case TYPE_PV_INT: {
                prosumer_model = new PV(model_type, step_size, prosumer_data->Vnom, prosumer_data->S_r,
                    prosumer_data->pf_min, profiles, prosumer_data->profile1_id, prosumer_data->profile_scale);
                break;
            }
            case TYPE_WEC_INT: {
                prosumer_model = new WEC(model_type, step_size, prosumer_data->Vnom, prosumer_data->S_r,
                    prosumer_data->pf_min, profiles, prosumer_data->profile1_id, prosumer_data->profile_scale);
                break;
            }
            default: {
                /* error */
                break;
            }
        }

        /* behavior */
        if(!prosumer_data->ict_connected && behavior_type == CTRL_INTELLIGENT){
            // set behavior to non-intelligent, if this prosumer is not connected to ICT
            behavior_type = CTRL_NON_INTELLIGENT;
        }
	IO->log_info("ICT connectivity is: " + std::to_string(prosumer_data->ict_connected));
	IO->log_info("Behavior type is: " + std::to_string(behavior_type));

        switch (behavior_type) {
    
            case CTRL_NON_INTELLIGENT: {
                behavior = new Ref_prosumer_behavior(id_.id(), prosumer_data->type, prosumer_data->subtype,
                    t_step, _d_props, prosumer_data);
                break;
            }
            case CTRL_INTELLIGENT: {
                behavior = new Swarm_prosumer_behavior(id_.id(), prosumer_data->type, prosumer_data->subtype,
                    t_step, _d_props, prosumer_data);
                break;
            }
            case CTRL_MQTT_TEST:{
                behavior = new Mqtt_ref_prosumer_behavior(id_.id(), prosumer_data->type, id_.currentRank(),
                    prosumer_data->subtype, t_step, _d_props, _villas_config, prosumer_data);
                break;
            }
            case CTRL_MQTT_PINGPONG:{
                behavior = new Mqtt_pingpong_prosumer_behavior(id_.id(), prosumer_data->type, id_.currentRank(),
                        prosumer_data->subtype, t_step, _d_props, _villas_config, prosumer_data);
                break;
            }
            case CTRL_ENSURE:{
                behavior = new Ensure_prosumer_behavior(id_.id(), prosumer_data->type, id_.currentRank(),
                        prosumer_data->subtype, t_step, _d_props, _villas_config, prosumer_data);
                break;
            }
            case CTRL_MQTT_HIGHLOAD:{
                behavior = new Mqtt_highload_prosumer_behavior(id_.id(), prosumer_data->type, id_.currentRank(),
                    prosumer_data->subtype, t_step, _d_props, _villas_config, prosumer_data);
                break;
            }
            case CTRL_DPSIM_COSIM_REF:{
                behavior = new Ref_prosumer_behavior(id_.id(), prosumer_data->type, prosumer_data->subtype,
                    t_step, _d_props, prosumer_data);
                break;
            }
            case CTRL_DPSIM_COSIM_SWARMGRIDX:{
                behavior = new Swarm_prosumer_behavior(id_.id(), prosumer_data->type, prosumer_data->subtype,
                    t_step, _d_props, prosumer_data);
                break;
            }
            case CTRL_INTCOMM:{
                behavior = new Refic_prosumer_behavior(id_.id(), prosumer_data->type, prosumer_data->subtype,
                                                       t_step, _d_props, prosumer_data);
                break;
            }
            default: {
                behavior = new Ref_prosumer_behavior(id_.id(), prosumer_data->type, prosumer_data->subtype,
                    t_step, _d_props, prosumer_data);
                break;
            }
        }
    }
}

/*! \brief Destructor of CHP_agent class
 * */
Prosumer_agent::~Prosumer_agent() {
    delete behavior;
    delete prosumer_data;
    delete prosumer_model;
    IO->finish_io();
}

/*! \brief advance a simulation step
 * */
void Prosumer_agent::step() {
    if(is_local_instance) {
        //  get outputs
        prosumer_data->msg_sent = behavior->get_msg_sent();
        prosumer_data->msg_received = behavior->get_msg_received();
        prosumer_model->post_processing();
        prosumer_model->get_power(prosumer_data->P, prosumer_data->Q);
        if (prosumer_data->type == TYPE_BATTERY_INT) {
            Battery* battery_model_cast = (Battery*) prosumer_model;
            prosumer_data->SOC_el = battery_model_cast->get_soc();
        } else if (prosumer_data->type == TYPE_CHP_INT) {
            CHP* chp_model_cast = (CHP*) prosumer_model;
            prosumer_data->E_th = chp_model_cast->get_energy();
            prosumer_data->SOC_th = chp_model_cast->get_soc();
            prosumer_data->sec_heater_P_gen_el = chp_model_cast->get_P_sec();
            prosumer_data->P -= prosumer_data->sec_heater_P_gen_el;
        } else if (prosumer_data->type == TYPE_EV_INT) {
            EV* ev_model_cast = (EV*) prosumer_model;
            if(prosumer_data->profile1_id != -1) {
                prosumer_data->SOC_el = ev_model_cast->get_soc();
            }
        } else if (prosumer_data->type == TYPE_HP_INT) {
            HP* hp_model_cast = (HP*) prosumer_model;
            prosumer_data->E_th = hp_model_cast->get_energy();
            prosumer_data->SOC_th = hp_model_cast->get_soc();
            prosumer_data->sec_heater_P_gen_el = hp_model_cast->get_P_sec();
            prosumer_data->P -= prosumer_data->sec_heater_P_gen_el;
        }

        /* save results */
        IO->save_result(prosumer_data, first_step, tm_csv, tm_db, tm_db_serialize, tm_db_add);
#ifdef USE_DB
        if(first_step){
            IO->save_meta(prosumer_data);
        }
#endif

        /* proceed one time step */
        t_next += t_step;
        IO->update_t(t_next);
        behavior->update_t(t_next);
        prosumer_model->step(t_next);
        next_action_expected = FORWARD_SWEEP;
        prosumer_data->number_of_solves = 0;
        msg_received=0;
        msg_sent=0;
    }
    if(first_step) {
        first_step = false;
    }
}

/*! \brief calculation of model for a simulation step
 * */
void Prosumer_agent::calculate() {
    if(is_local_instance) {

        prosumer_data->v_im = prev_nodes_elec[0]->voltage.imag();
        prosumer_data->v_re = prev_nodes_elec[0]->voltage.real();
        voltage.real(prosumer_data->v_re);
        voltage.imag(prosumer_data->v_im);

        IO->log_info("\t\tCalculate model for input voltage: (" + std::to_string(prosumer_data->v_re) +
            "," + std::to_string(prosumer_data->v_im) + ")");

        if(prosumer_data->number_of_solves == 0) {
            if (prosumer_data->type == TYPE_COMPENSATOR_INT) {
                Compensator* comp_model_cast = (Compensator*) prosumer_model;
                comp_model_cast->set_n_ctrl((unsigned int) prosumer_data->n_ctrl);
            } else {
                prosumer_model->set_power_ctrl(prosumer_data->P_ctrl, prosumer_data->Q_ctrl);
            }
            prosumer_model->pre_processing();
        }
        prosumer_model->solve(prosumer_data->v_re, prosumer_data->v_im, prosumer_data->i_re, prosumer_data->i_im);

        current.imag(prosumer_data->i_im);
        current.real(prosumer_data->i_re);

        IO->log_info("\t\tCalculated current: (" + std::to_string(current.real()) + "," +
            std::to_string(current.imag()) + ")");

        prosumer_data->number_of_solves++;
    }
}


/*! \brief update the power generation capabilities according to profiles
*/
void Prosumer_agent::update_input_data()
{
    if (prosumer_data->type == TYPE_BIOFUEL_INT) {
        Bio* bio_model_cast = (Bio*) prosumer_model;
        prosumer_data->P_gen = bio_model_cast->get_generation();
    } else if (prosumer_data->type == TYPE_CHP_INT) {
        CHP* chp_model_cast = (CHP*) prosumer_model;
        prosumer_data->P_th_dem = chp_model_cast->get_th_demand();
    } else if (prosumer_data->type == TYPE_EV_INT) {
        EV* ev_model_cast = (EV*) prosumer_model;
        if(prosumer_data->profile1_id != -1) { // flexible EV
            prosumer_data->t_connected = ev_model_cast->get_t_connected();
            prosumer_data->connected = ev_model_cast->get_connected();
        }
        else{ //non-flexible EV
            ev_model_cast->get_demand(prosumer_data->P_dem, prosumer_data->Q_dem);
        }
    } else if (prosumer_data->type == TYPE_HP_INT) {
        HP* hp_model_cast = (HP*) prosumer_model;
        prosumer_data->P_th_dem = hp_model_cast->get_th_demand();
    } else if (prosumer_data->type == TYPE_LOAD_INT) {
        Load* load_model_cast = (Load*) prosumer_model;
        load_model_cast->get_demand(prosumer_data->P_dem, prosumer_data->Q_dem);
    } else if (prosumer_data->type == TYPE_PV_INT) {
        PV* pv_model_cast = (PV*) prosumer_model;
        prosumer_data->P_gen = pv_model_cast->get_generation();
    } else if (prosumer_data->type == TYPE_WEC_INT) {
        WEC* wec_model_cast = (WEC*) prosumer_model;
        prosumer_data->P_gen = wec_model_cast->get_generation();
    }
}

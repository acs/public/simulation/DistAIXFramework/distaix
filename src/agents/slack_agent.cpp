/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/
#include <complex>

#include "agents/slack_agent.h"
#include "behavior/swarmgrid/swarm_slack_behavior.h"
#include "behavior/swarmgrid/swarm_dpsim_cosim_slack_behavior.h"
#include "behavior/reference/ref_slack_behavior.h"
#include "behavior/ref_intcomm/refic_slack_behavior.h"
#include "behavior/ensure/ensure_slack_behavior.h"
#include "behavior/dpsim_cosim/dpsim_cosim_slack_behavior.h"

/*! \brief Constructor used for creation of copies of non-local agents
 *  \param id [in]          RepastHPC agent id
 *  \param is_local [in]    true if local agent is created, false otherwise
 * */
Slack_agent::Slack_agent(repast::AgentId &id, bool is_local) : Agent(id, is_local) {}

/*! \brief Constructor used for creation of agents in model
 *  \param id [in]                  RepastHPC agent id
 *  \param is_local [in]            true if local agent is created, false otherwise
 *  \param _Vnom [in]               nominal voltage of slack in V
 *  \param _Vre [in]                real part of initial complex voltage
 *  \param _Vim [in]                imaginary part of initial complex voltage
 *  \param step_size [in]           size of a simulation step in seconds
 *  \param _behavior_type [in]      user selected behavior for agents
 *  \param _model_type [in]         user selected type of el. models (0=static phasor models, 1=dynamic phasor models)
 *  \param _subtype [in]            subtype of the agent
 *  \param _d_props [in]            properties of logging and result saving
 *  \param _villas_config [in]      configuration for villas node
 * */
Slack_agent::Slack_agent(repast::AgentId &id,
            bool is_local,
            double &step_size,
            double _Vnom,
            double _Vre,
            double _Vim,
            int &_behavior_type,
            int &_model_type,
            std::string &_subtype,
            struct data_props _d_props,
            villas_node_config *_villas_config,
            bool _ict_connected,
            bool _realtime
) : Slack_agent(id, is_local, step_size, _Vnom, _behavior_type, _model_type, _subtype, _d_props, _villas_config, _ict_connected, _realtime){

    // For cosimulation case, calculations do not use Vnom. Therefore model data voltage has to be set manually
    model_data.v_re = _Vre;
    model_data.v_im = _Vim;

    if(model_data.v_re == 0.0 && model_data.v_im == 0.0){
        IO->log_info("No initialization values for slack were given --> Fallback calculation using Vnom...");
        model_data.v_re = (model_data.Vnom / sqrt(3.0)) * cos(model_data.phiV);
        model_data.v_im = (model_data.Vnom / sqrt(3.0)) * sin(model_data.phiV);
    }

    voltage.real(model_data.v_re);
    voltage.imag(model_data.v_im);

    IO->log_info("Input params: Vnom="+ std::to_string(_Vnom) + "/ V_re="+ std::to_string(model_data.v_re) +
        "/ V_im=" + std::to_string(model_data.v_im));
    IO->log_info("Voltage.real = " + std::to_string(voltage.real()) + " / Voltage.imag = " + std::to_string(voltage.imag()));
}

/*! \brief Constructor used for creation of agents in model
 *  \param id [in]                  RepastHPC agent id
 *  \param is_local [in]            true if local agent is created, false otherwise
 *  \param _Vnom [in]               nominal voltage of slack in V
 *  \param step_size [in]           size of a simulation step in seconds
 *  \param _behavior_type [in]      user selected behavior for agents
 *  \param _model_type [in]         user selected type of el. models (0=static phasor models, 1=dynamic phasor models)
 *  \param _subtype [in]            subtype of the agent
 *  \param _d_props [in]            properties of logging and result saving
 *  \param _villas_config [in]      configuration for villas node
 * */
Slack_agent::Slack_agent(repast::AgentId &id,
                         bool is_local,
                         double &step_size,
                         double _Vnom,
                         int &_behavior_type,
                         int &_model_type,
                         std::string &_subtype,
                         struct data_props _d_props,
                         villas_node_config *_villas_config,
                         bool _ict_connected,
                         bool _realtime
) : Agent(id, is_local, step_size, _behavior_type, _model_type,_subtype, _d_props){


    if (is_local_instance) {
        IO->init_logging();
        IO->log_info("Creating slack agent " + IO->id2str(id));

        //set parameters of slack agent
        model_data.phiV = 0; //initial voltage angle
        //model_data.f = 50; //frequency
        model_data.Vnom = _Vnom; //nominal voltage
        model_data.v_re = (model_data.Vnom / sqrt(3.0)) * cos(model_data.phiV);
        model_data.v_im = (model_data.Vnom / sqrt(3.0)) * sin(model_data.phiV);
        voltage.real(model_data.v_re);
        voltage.imag(model_data.v_im);

        if(behavior_type != CTRL_DPSIM_COSIM_REF && behavior_type != CTRL_DPSIM_COSIM_SWARMGRIDX){
            IO->log_info("Input params: Vnom="+ std::to_string(_Vnom) + "/ V_re="+ std::to_string(model_data.v_re) +
            "/ V_im=" + std::to_string(model_data.v_im));
        }
    }

    next_action_expected = BACKWARD_SWEEP;
    convergence = false;
    get_new_vals = false; // Variable needed to make sure new values are only received once in each step

    switch (behavior_type) {
        case CTRL_NON_INTELLIGENT: {
            behavior = new Ref_slack_behavior(id_.id(), TYPE_SLACK_INT, _subtype, t_step, _d_props);
            break;
        }
        case CTRL_INTELLIGENT: {
            behavior = new Swarm_slack_behavior(id_.id(), TYPE_SLACK_INT, _subtype, t_step, _d_props, &model_data);
            break;
        }
        case CTRL_MQTT_TEST:{
            behavior = new Ref_slack_behavior(id_.id(), TYPE_SLACK_INT, _subtype, t_step, _d_props);
            break;
        }
        case CTRL_ENSURE:{
                behavior = new Ensure_slack_behavior(id_.id(), TYPE_SLACK_INT, id_.currentRank(),
                        _subtype, t_step, _d_props, _villas_config, &model_data);
                break;
            }
        case CTRL_DPSIM_COSIM_REF:{
            behavior = new Dpsim_cosim_slack_behavior(id_.id(), TYPE_SLACK_INT, id_.currentRank(), _subtype, t_step, _d_props, _villas_config, _realtime, true);            
            break;
        }
        case CTRL_DPSIM_COSIM_SWARMGRIDX:{
            behavior = new Swarm_dpsim_cosim_slack_behavior(id_.id(), TYPE_SLACK_INT, id_.currentRank(), _subtype, t_step, _d_props, &model_data, _villas_config, _realtime, true);
            break;
        }
        case CTRL_INTCOMM:{
            behavior = new Refic_slack_behavior(id_.id(), TYPE_SLACK_INT, _subtype, t_step, _d_props);
            break;
        }
        default: {
            behavior = new Ref_slack_behavior(id_.id(), TYPE_SLACK_INT, _subtype, t_step, _d_props);
            break;
        }
    }
}

/*! \brief Destructor of Slack_agent class
 * */
Slack_agent::~Slack_agent() {
    delete behavior;
    IO->finish_io();

}

/*! \brief advance a simulation step
 * */
void Slack_agent::step() {

    if(is_local_instance) {

        model_data.msg_sent = behavior->get_msg_sent();
        model_data.msg_received = behavior->get_msg_received();
        IO->save_result(&model_data, first_step, tm_csv, tm_db, tm_db_serialize, tm_db_add);
#ifdef USE_DB
        if(first_step){
            IO->save_meta(&model_data);
        }
#endif
        //proceed one time step
        t_next += t_step;
        IO->update_t(t_next);
        behavior->update_t(t_next);
        next_action_expected = BACKWARD_SWEEP;
        model_data.number_of_iterations = 0;

        msg_received = 0;
        msg_sent = 0;

    }
    if(first_step) {
        first_step = false;
    }

    // needed to prevent distaix to report to dpsim after last timestep
    if(behavior_type == CTRL_DPSIM_COSIM_REF || behavior_type == CTRL_DPSIM_COSIM_SWARMGRIDX) {
        get_new_vals = true;
    }
}

/*! \brief calculation of model for a simulation step
 * */
void Slack_agent::calculate(){
    if(is_local_instance) {

        //set inputs
        model_data.i_im = -current.imag();
        model_data.i_re = -current.real();

        this->solve();
        //get outputs
        voltage_prev = voltage;
        voltage.imag(model_data.v_im);
        voltage.real(model_data.v_re);
    }

}

/*! \brief perform actions of the backward sweep in FBS algorithm
 * \param agent_network_elec RepastHPC Shared Network containing electrical connections of agents in this process
 * \return true of agent has computed current in this call, false otherwise
 * */
bool Slack_agent::do_backward_sweep(
        repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) {
    //determine if all next nodes have finished BACKWARD sweep
    bool all_next_nodes_finished_BACKWARD = true;
    for(auto i : next_nodes_elec){
        if(i->get_next_action_expected() == BACKWARD_SWEEP){
            all_next_nodes_finished_BACKWARD = false;
            break;
        }
    }

    if(next_action_expected == BACKWARD_SWEEP && all_next_nodes_finished_BACKWARD) {
        IO->log_info("\tSLACK in BACKWARD SWEEP: ");

        // get_new_vals makes sure report_to_superior grid is only executed once per timestep.
        // Therefore it implicitly avoids message exchange when no slack convergence is reached
        if ((behavior_type == CTRL_DPSIM_COSIM_REF || behavior_type == CTRL_DPSIM_COSIM_SWARMGRIDX) && get_new_vals) {
            std::list<Villas_message> incoming_villas_messages;

            if (behavior_type == CTRL_DPSIM_COSIM_REF) {
                // Dynamically cast the behavior of the correct behavior type to enable calling of behavior specific methods
                auto *cosim_behavior = dynamic_cast<Dpsim_cosim_slack_behavior *>(behavior);

                Dpsim_cosim_msg out_msg;

                out_msg.value->real((float) -current.real());
                out_msg.value->imag((float) -current.imag());

                cosim_behavior->report_to_superior_grid(out_msg, &incoming_villas_messages);
            } else if (behavior_type == CTRL_DPSIM_COSIM_SWARMGRIDX) {
                auto *cosim_behavior = dynamic_cast<Swarm_dpsim_cosim_slack_behavior *>(behavior);

                Dpsim_cosim_msg out_msg;

                out_msg.value->real((float) -current.real());
                out_msg.value->imag((float) -current.imag());

                cosim_behavior->report_to_superior_grid(out_msg, &incoming_villas_messages);
            }

            // TODO:
            // As DPsim might simulate much faster than DistAIX, there can be multiple messages in the queue
            // Find some strategy what to do: Currently the messages will be simply popped from front

            Dpsim_cosim_msg in_msg;
            if (!incoming_villas_messages.empty()) {
                in_msg = (Dpsim_cosim_msg) incoming_villas_messages.front();
                // set pointer of data to members
                in_msg.set_pointers();

                incoming_villas_messages.pop_front();
            } else {
                if (realtime) {
                    IO->log_info("[REALTIME] No new values at socket...");
                } else {
                    IO->log_info(
                            "ERROR IN BACKWARD SWEEP! BUSY WAITING RETURNED BUT NO RESPONSE FROM DPSIM COULD BE READ");
                }

                IO->log_info("\tTake old value again...");

                in_msg.value->real((float) model_data.v_re);
                in_msg.value->imag((float) model_data.i_im);
            }

            voltage_prev = voltage;
            IO->log_info("V_re = " + std::to_string(in_msg.value->real()));
            IO->log_info("V_im = " + std::to_string(in_msg.value->imag()));
            model_data.v_re = in_msg.value->real();
            model_data.v_im = in_msg.value->imag();

            IO->log_info("\t Set model_data.v_re = " + std::to_string(model_data.v_re));
            IO->log_info("\t Set model_data.v_im = " + std::to_string(model_data.v_im));

            voltage.real(in_msg.value->real());
            voltage.imag(in_msg.value->imag());
            get_new_vals = false;
        }


        current.imag(0.0);
        current.real(0.0);

        //add current that flows to next nodes (if any)
        for (auto i : next_nodes_elec) {
            current.real(current.real() + i->get_current().real());
            current.imag(current.imag() + i->get_current().imag());
        }

        model_data.i_re = -current.real();
        model_data.i_im = -current.imag();

        next_action_expected=FORWARD_SWEEP;
        IO->log_info("calculated current_backward_sweep = (" + std::to_string(current.real()) + "," +
                     std::to_string(current.imag()) + ")");
        return true;
    } else{
        IO->log_info("\tNothing to do.");
        return false;
    }

}

/*! \brief perform actions of the forward sweep in FBS algorithm
 * \param agent_network_elec RepastHPC Shared Network containing electrical connections of agents in this process
 * */
uint Slack_agent::do_forward_sweep(
        repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) {

    if(next_action_expected == FORWARD_SWEEP){

        //determine if all prev nodes have finished FORWARD sweep
        bool all_prev_nodes_finished_FORWARD = true;
        for(auto i : prev_nodes_elec){
            if(i->get_next_action_expected() == FORWARD_SWEEP){
                all_prev_nodes_finished_FORWARD = false;
                break;
            }
        }

        if(all_prev_nodes_finished_FORWARD) {

            IO->log_info("\tSLACK in FORWARD SWEEP: ");
            calculate();
            IO->log_info("calculated voltage = (" + std::to_string(voltage.real()) + "," +
                         std::to_string(voltage.imag()) + ")");
            next_action_expected = CONVERGENCE_CHECK;
        }
    }
    if(next_action_expected==CONVERGENCE_CHECK){

        //determine if all next nodes expect BACKWARD sweep next
        bool all_next_nodes_expect_BACKWARD = true;
        for(auto i : next_nodes_elec){
            if((i->get_next_action_expected() != BACKWARD_SWEEP)){
                all_next_nodes_expect_BACKWARD = false;
                break;
            }
        }

        if(all_next_nodes_expect_BACKWARD) {

            IO->log_info("\tSLACK in CONVERGENCE CHECK: ");
            bool next_convergence = true;
            for (auto i : next_nodes_elec) {
                if (!(i->get_convergence())) {
                    next_convergence = false;
                    break;
                }
            }

            if (!next_convergence) {
                convergence = false;
            }
            else {
                //check for local convergence by comparing voltages of this and previous sweep
                if ((fabs(voltage.real() - voltage_prev.real()) < EPSILON) &&
                    (fabs(voltage.imag() - voltage_prev.imag()) < EPSILON)) {

                    convergence = true;
                }
                else {
                    convergence = false;
                }
            }

            model_data.number_of_iterations++;
            next_action_expected = BACKWARD_SWEEP;
            IO->log_info("determined convergence to = " + std::to_string(convergence));
            return DONE_CONVERGENCE_CHECK;
        }
        return DONE_FORWARD_SWEEP;
    }
    else{
        if(next_action_expected == BACKWARD_SWEEP){
                IO->log_info("\tNothing to do (already finished in this sweep).");
                return ALREADY_FINISHED;
        } else {
                IO->log_info("\tNothing to do.");
                return DONE_NOTHING;
        }
    }

}

/*! \brief calculate the component current as a function of input voltage
*/
void Slack_agent::solve() {
    if (model_data.number_of_iterations == 0) {
        /*save state of model*/
        save_model_state();
    }
    else {
        /*restore state of model: do not kill input data*/
        restore_model_state();
    }

    /* in case of cosimulation, slack must not calculate voltages as they were received by other simulator */
    if(behavior_type != CTRL_DPSIM_COSIM_REF && behavior_type != CTRL_DPSIM_COSIM_SWARMGRIDX){
        /* calculate output voltage v */
        if(model_type == MODEL_TYPE_STEADY_STATE){
            /* Solve Steady State equations */
            this->solve_steady_state();
        }
        else if(model_type == MODEL_TYPE_DYNAMIC_PHASOR) {
            if (t_next == t_start) {
                /* Steady State Init */
                this->solve_steady_state();
            } else {
                /* Solve Dynamic Phasor equations */
                this->solve_dynamic_phasor();
            }
            /* ToDo: override previous value with actual value here */
            
        }
    }



    /* update measurement variables */
    //model_data.Vpp = sqrt(3.0) * model_data.Vnom;
    // model_data.Vpp = sqrt(3.0) * sqrt(model_data.v_re*model_data.v_re + model_data.v_im*model_data.v_im);
    // model_data.Vph = sqrt(model_data.v_re*model_data.v_re + model_data.v_im*model_data.v_im);
    // model_data.Iph = sqrt(model_data.i_re * model_data.i_re + model_data.i_im * model_data.i_im);
    //model_data.P = 3 * model_data.Vnom * model_data.i_re;
    model_data.P = 3 * (model_data.v_re * model_data.i_re + model_data.v_im * model_data.i_im);
    model_data.Q = 3 * (model_data.v_im * model_data.i_re - model_data.v_re * model_data.i_im);
}

/*! \brief solve steady state equations
*/
void Slack_agent::solve_steady_state() {
    model_data.v_re = (model_data.Vnom / sqrt(3.0)) * cos(model_data.phiV);
    model_data.v_im = (model_data.Vnom / sqrt(3.0)) * sin(model_data.phiV);

}

/*! \brief solve dynamic phasor equations
*/
void Slack_agent::solve_dynamic_phasor() {
    /* ToDo: Add the Dynamic Phasor equations here */
    
    model_data.v_re = (model_data.Vnom / sqrt(3.0)) * cos(model_data.phiV);
    model_data.v_im = (model_data.Vnom / sqrt(3.0)) * sin(model_data.phiV);
    
}

/*! \brief store any state variables of slack (necessary, because state has to be restored before model can be executed for the same time again)
*/
void Slack_agent::save_model_state() {
    /*save state of model in rollback state*/
    /* no rollback state necessary; Slack has no internal state */
}

/*! \brief restore state variables of slack
*/
void Slack_agent::restore_model_state() {
    /*restore state of model to rollback state*/
    /* no rollback state necessary; Slack has no internal state */
}


/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "agents/node_agent.h"
#include "edges/cable.h"


/*! \brief Constructor used for creation of copies of non-local agents
 *  \param id [in]          RepastHPC agent id
 *  \param is_local [in]    true if local agent is created, false otherwise
 * */
Node_agent::Node_agent(repast::AgentId &id, bool is_local) : Agent(id, is_local) {}

/*! \brief Constructor used for creation of agents in model
 *  \param id [in]                  RepastHPC agent id
 *  \param is_local [in]            true if local agent is created, false otherwise
 *  \param logging [in]             indicates if agents logs locally
 *  \param _Vnom [in]               nominal voltage of the node in V
 *  \param _behavior_type [in]      user selected behavior for agents
 *  \param _model_type [in]         user selected type of el. models (0=static phasor models, 1=dynamic phasor models)
 *  \param _subtype [in]            subtype of the agent
 *  \param _d_props [in]            properties of logging and result saving
 *  \param _villas_config [in]      configuration for villas node
 *  \param _ifht_id [in]            ID of node in IFHT model
 * */
Node_agent::Node_agent(repast::AgentId &id,
                       bool is_local,
                       double &step_size,
                       double _Vnom,
                       int &_behavior_type,
                       int &_model_type,
                       std::string &_subtype,
                       struct data_props _d_props,
                       villas_node_config *_villas_config,
                       int _ifht_id,
                       bool _ict_connected
) : Agent(id, is_local, step_size, _behavior_type, _model_type, _subtype, _d_props) {

    if (is_local_instance) {
        IO->init_logging();
        IO->log_info("Creating Node agent " + IO->id2str(id));

        IO->log_info("Input params: Vnom="+ std::to_string(_Vnom));

    }
    next_action_expected = BACKWARD_SWEEP;
    //number_of_iterations = 0;
    model_data.number_of_iterations = 0;
    //set start value of volatges to nominal voltage
    model_data.Vnom = _Vnom;
    voltage.real(model_data.Vnom/sqrt(3.0));
    voltage.imag(0.0);

    model_data.v_im = voltage.imag();
    model_data.v_re = voltage.real();
    ifht_id = _ifht_id; //IFHT ID is 0 for non-IFHT scenario
    model_data.transformer_id = std::stoi(_subtype);
    model_data.ict_connected = _ict_connected;
}

/*! \brief Destructor of Node_agent class
 * */
Node_agent::~Node_agent() {
    IO->finish_io();
}

/*! \brief advance a simulation step
 * */
void Node_agent::step() {
    if(is_local_instance) {

        model_data.i_im = current.imag();
        model_data.i_re = current.real();
        model_data.v_re = voltage.real();
        model_data.v_im = voltage.imag();
        model_data.v_norm = std::abs(voltage) / (model_data.Vnom / sqrt(3.0));

        IO->save_result(&model_data, first_step, tm_csv, tm_db, tm_db_serialize, tm_db_add);
#ifdef USE_DB
        if(first_step){
            IO->save_meta(&model_data);
        }
#endif
        //proceed one time step
        t_next += t_step;
        IO->update_t(t_next);
        next_action_expected = BACKWARD_SWEEP;
        model_data.number_of_iterations = 0;
        //number_of_iterations = 0;
        convergence = false;
        IO->log_info("Step: " + std::to_string(t_next) + "s");
        IO->flush_log();
    }

    if(first_step) {
        first_step = false;
    }

}

/*! \brief perform actions of the backward sweep in FBS algorithm
 * \param agent_network_elec RepastHPC Shared Network containing electrical connections of agents in this process
 * \return true if agent has computed current in this call, false if nothing was done
 * */
bool Node_agent::do_backward_sweep(
        repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) {

    //determine if all next nodes have finished BACKWARD sweep
    bool all_next_nodes_finished_BACKWARD = true;
    for(auto i : next_nodes_elec){
        if(i->get_next_action_expected() == BACKWARD_SWEEP){
            all_next_nodes_finished_BACKWARD = false;
            break;
        }
    }

    if(next_action_expected == BACKWARD_SWEEP && all_next_nodes_finished_BACKWARD){
        IO->log_info("\tNODE in BACKWARD SWEEP: ");

        //current_prev = current;
        current.imag(0.0);
        current.real(0.0);
        std::complex<double> current_n(0.0,0.0);
        std::complex<double> current_c(0.0, 0.0);
        
        //calculate current that flows into components (if any)
        for(auto i : components){
            int current_sign = 1; //i->get_current_sign(id_.id());
            current_c.real(current_c.real() + current_sign * i->get_current().real());
            current_c.imag(current_c.imag() + current_sign * i->get_current().imag());
        }
        //add current that flows to next nodes (if any)
        for(auto i : next_nodes_elec) {
            current_n.real(current_n.real() + i->get_current().real());
            current_n.imag(current_n.imag() + i->get_current().imag());
        }

        IO->log_info("Calculated current = (" + std::to_string(current_n.real()) + "," + std::to_string(current_n.imag()) + ")");

        boost::shared_ptr<Edge<Agent>> cable = agent_network_elec->findEdge(prev_nodes_elec[0], this);
        std::complex<double> i_leak = cable->get_leakage_current();
        current = current_n + current_c + i_leak;
        next_action_expected=FORWARD_SWEEP;
        IO->log_info("calculated current_backward_sweep = (" + std::to_string(current.real()) + "," + std::to_string(current.imag()) + ")");
	    return true;
    }
    else{
        IO->log_info("\tNothing to do.");
	    return false;
    }


}

/*! \brief perform actions of the forward sweep in FBS algorithm
 * \param agent_network_elec RepastHPC Shared Network containing electrical connections of agents in this process
 * */
uint Node_agent::do_forward_sweep(
        repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) {


    if(next_action_expected == FORWARD_SWEEP){

        //determine if all prev nodes have finished FORWARD sweep
        bool all_prev_nodes_finished_FORWARD = true;
        for(auto i : prev_nodes_elec){
            if(i->get_next_action_expected() == FORWARD_SWEEP){
                all_prev_nodes_finished_FORWARD = false;
                break;
            }
        }

        if(all_prev_nodes_finished_FORWARD) {
            IO->log_info("\tNODE in FORWARD SWEEP: ");

            std::complex<double> p_voltage = prev_nodes_elec[0]->get_voltage();

            boost::shared_ptr<Edge<Agent>> cable = agent_network_elec->findEdge(prev_nodes_elec[0],
                                                                                this);
            // and determine the losses via the edge (cable)
            cable->calculate(p_voltage.real(), p_voltage.imag(), current.real(), current.imag());
            voltage_prev = voltage;
            voltage = cable->get_voltage_output();
            IO->log_info("calculated voltage = (" + std::to_string(voltage.real()) + "," +
                         std::to_string(voltage.imag()) + ")");

            next_action_expected = CONVERGENCE_CHECK;
        }
    }
    if(next_action_expected==CONVERGENCE_CHECK){

        //determine if all next nodes expect BACKWARD sweep next
        bool all_next_nodes_expect_BACKWARD = true;
        for(auto i : next_nodes_elec){
            if((i->get_next_action_expected() != BACKWARD_SWEEP)){
                all_next_nodes_expect_BACKWARD = false;
                break;
            }
        }

        if(all_next_nodes_expect_BACKWARD) {
            IO->log_info("\tNODE in CONVERGENCE CHECK: ");

            bool next_convergence = true;
            for (auto i : next_nodes_elec) {
                if (!(i->get_convergence())) {
                    next_convergence = false;
                    break;
                }
            }

            if (!next_convergence) {
                convergence = false;
            } else {
                //check for local convergence by comparing voltages of this and previous sweep
                if ((fabs(voltage.real() - voltage_prev.real()) < EPSILON) &&
                    (fabs(voltage.imag() - voltage_prev.imag()) < EPSILON)) {
                    convergence = true;
                } else {
                    convergence = false;
                }
            }

            model_data.number_of_iterations++;
            next_action_expected = BACKWARD_SWEEP;
            IO->log_info("determined convergence to = " + std::to_string(convergence));

            return DONE_CONVERGENCE_CHECK;
        }
        return DONE_FORWARD_SWEEP;
    }
    else{
        if(next_action_expected == BACKWARD_SWEEP){
                IO->log_info("\tNothing to do (already finished in this sweep).");
            return ALREADY_FINISHED;
        } else {
                IO->log_info("\tNothing to do.");
                return DONE_NOTHING;
        }
    }

}


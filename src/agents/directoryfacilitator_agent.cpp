/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "agents/directoryfacilitator_agent.h"
#include "behavior/swarmgrid/swarm_df_behavior.h"
#include "behavior/reference/ref_df_behavior.h"
#include "behavior/ref_intcomm/refic_df_behavior.h"

/*! \brief Constructor used for creation of copies of non-local agents
 *  \param id [in]          RepastHPC agent id
 *  \param is_local [in]    true if local agent is created, false otherwise
 * */
Directoryfacilitator_agent::Directoryfacilitator_agent(repast::AgentId &id, bool is_local) : Agent(id, is_local) {}

/*! \brief Constructor used for creation of agents in model
 *  \param id [in]                  RepastHPC agent id
 *  \param is_local [in]            true if local agent is created, false otherwise
 *  \param step_size [in]           size of a simulation step in seconds
 *  \param _behavior_type [in]      user selected behavior for agents
 *  \param _model_type [in]         user selected type of el. models (0=static phasor models, 1=dynamic phasor models)
 *  \param _subtype [in]            subtype of the agent
 *  \param _d_props [in]            properties of logging and result saving
 *  \param _villas_config [in]      configuration for villas node
 * */
Directoryfacilitator_agent::Directoryfacilitator_agent(repast::AgentId &id,
                                                       bool is_local,
                                                       double &step_size,
                                                       int &_behavior_type,
                                                       int &_model_type,
                                                       std::string &_subtype,
                                                       struct data_props _d_props,
                                                       villas_node_config *_villas_config
) : Agent(id, is_local, step_size, _behavior_type, _model_type,_subtype, _d_props) {

    if (is_local_instance) {

        IO->init_logging();

        IO->log_info("Creating Directory Facilitator agent " + IO->id2str(id));

        model_data.msg_received = 0;
        model_data.msg_sent = 0;

        switch (behavior_type) {
            case CTRL_NON_INTELLIGENT: {
                behavior = new Ref_df_behavior(id_.id(), TYPE_DF_INT, _subtype, t_step, _d_props, id_.currentRank());
                break;
            }
            case CTRL_INTELLIGENT: {
                behavior = new Swarm_df_behavior(id_.id(), TYPE_DF_INT, _subtype, t_step, _d_props, &model_data, id_.currentRank());
                break;
            }
            case CTRL_MQTT_TEST:{
                behavior = new Ref_df_behavior(id_.id(), TYPE_DF_INT, _subtype, t_step, _d_props, id_.currentRank());
                break;
            }
            case CTRL_DPSIM_COSIM_REF:{
                behavior = new Ref_df_behavior(id_.id(), TYPE_DF_INT, _subtype, t_step, _d_props, id_.currentRank());
                break;
            }
            case CTRL_DPSIM_COSIM_SWARMGRIDX:{
                behavior = new Swarm_df_behavior(id_.id(), TYPE_DF_INT, _subtype, t_step, _d_props, &model_data, id_.currentRank());
                break;
            }
            case CTRL_INTCOMM:{
                behavior = new Refic_df_behavior(id_.id(), TYPE_DF_INT, _subtype, t_step, _d_props, id_.currentRank());
                break;
            }
            default: {
                behavior = new Ref_df_behavior(id_.id(), TYPE_DF_INT, _subtype, t_step, _d_props, id_.currentRank());
                break;
            }
        }
    }
}


/*! \brief Destructor of a Directory Faciliator agent*/
Directoryfacilitator_agent::~Directoryfacilitator_agent() {
    delete behavior;
    IO->finish_io();
}

/*! \brief Set the pointer to an Agent_scenario_creator object
 * \param _mc [in] Pointer to Model_creator object
 * */
void Directoryfacilitator_agent::set_model_creator(Model_creator *_mc) {
    model_data.mc = _mc;
}

/*! \brief Set the reference to a vector containing information about which components are connected to which node
 * \param _components_at_nodes [in] Reference to vector containing information about which components are connected to which node
 * */
void Directoryfacilitator_agent::set_components_at_nodes(std::vector<std::list<std::tuple<int,int,int>>> &_components_at_nodes){
    model_data.components_at_nodes = _components_at_nodes;
}


/*! \brief Set the reference to a vector containing information about ICT connectivity of all nodes
 * \param _ict_connectivity_of_nodes [in] Reference to vector containing information about ICT connectivity of all nodes
 * */
void Directoryfacilitator_agent::set_ict_connectivity_of_nodes(std::vector<bool> &_ict_connectivity_of_nodes){
    model_data.ict_connectivity_of_nodes = _ict_connectivity_of_nodes;
}

/*! \brief advance a simulation step
 * */
void Directoryfacilitator_agent::step(){


    if(is_local_instance) {
        model_data.msg_sent = behavior->get_msg_sent();
        model_data.msg_received = behavior->get_msg_received();

        IO->save_result(&model_data, first_step, tm_csv, tm_db, tm_db_serialize, tm_db_add);
#ifdef USE_DB
        if(first_step){
            IO->save_meta(&model_data);
        }
#endif

        //proceed one time step
        t_next+=t_step;
        IO->update_t(t_next);
        behavior->update_t(t_next);
        //reset values for message counters
        msg_received=0;
        msg_sent=0;

        IO->log_info("Step: " + std::to_string(t_next) + "s");
        IO->flush_log();
    }
    if(first_step) {
        first_step = false;
    }

}

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "agents/agent.h"


/*! \brief Constructor used for creation of copies of non-local agents
 *  \param id [in]          RepastHPC agent id
 *  \param is_local [in]    true if local agent is created, false otherwise
 * */
Agent::Agent(repast::AgentId &id, bool is_local): id_(id), is_local_instance(is_local){
    msg_received=0;
    msg_sent=0;
}

/*! \brief Constructor used for creation of copies of MR-agents
 *  \param id [in]          RepastHPC agent id
 *  \param is_local [in]    true if local agent is created, false otherwise
 *  \param _msg_queue [in]  the message queue of an agents (to be synchronized via MPI)
 * */
Agent::Agent(repast::AgentId &id, bool is_local, std::list<Agent_message> _msg_queue):
        id_(id), is_local_instance(is_local){
    outgoing_messages = std::move(_msg_queue);
    msg_received=0;
    msg_sent=0;
}

/*! \brief Constructor used for creation of copies of Node, Transformer and Slack agents
 *  \param id [in]                      RepastHPC agent id
 *  \param is_local [in]                true if local agent is created, false otherwise
 *  \param _current [in]                complex current value (to be synchronized via MPI)
 *  \param _voltage [in]                complex voltage value (to be synchronized via MPI)
 *  \param _next_action_expected [in]   next expected action in forward-backward sweep algorithm (to be synchronized via MPI)
 *  \param _convergence [in]            boolean value indicating if FBS algorithm has converged (true=convergence, false=no convergence) (to be synchronized via MPI)
 * */
Agent::Agent(repast::AgentId &id, bool is_local, std::complex<double> _current,
             std::complex<double> _voltage, int _next_action_expected, bool _convergence)
        : id_(id), is_local_instance(is_local), current(_current), voltage(_voltage),
          next_action_expected(_next_action_expected), convergence(_convergence)
{
    msg_received=0;
    msg_sent=0;
}

/*! \brief Constructor used for creation of agents in model
 *  \param id [in]              RepastHPC agent id
 *  \param is_local [in]        true if local agent is created, false otherwise
 *  \param step_size [in]       size of a simulation step in seconds
 *  \param _behavior_type [in]  user selected behavior for agents
 *  \param _model_type [in]     user selected type of el. models (0=static phasor models, 1=dynamic phasor models)
 *  \param _subtype [in]        subtype of the agent
 *  \param _d_props [in]        properties of logging and result saving
 * */
Agent::Agent(repast::AgentId &id,
        bool is_local,
        double &step_size,
        int &_behavior_type,
        int &_model_type,
        std::string &_subtype,
        struct data_props _d_props
) : id_(id), is_local_instance(is_local), model_type(_model_type), subtype(_subtype) {

    first_step = true;
    first_step = true;
    behavior_type = _behavior_type;

    if (is_local_instance) {

        t_start = 0.0;
        t_step = step_size;
        t_next = t_start;

        IO = new AgentIO(id_, behavior_type, _d_props);
        IO->update_t(t_next);

        voltage = std::complex<double>(0,0);
        current = std::complex<double>(0,0);
        convergence = false;

        next_action_expected = FORWARD_SWEEP;
        //number_of_iterations=0;
        next_nodes_elec.clear();
        prev_nodes_elec.clear();
        components.clear();

        //starting values for message counters
        msg_received=0;
        msg_sent=0;

        behavior= nullptr;
    }
}

/*! \brief Destructor of Agent class
 * */
Agent::~Agent(){
    //interface.stop();
}

/*! \brief set data of agent
 *  \param currentRank [in]             current rank of the agent
 *  \param _current [in]                current of the agent
 *  \param _voltage [in]                voltage of the agent
 *  \param _next_action_expected [in]   next expected action of the agent
 *  \param _convergence [in]            convergence state of the agent
 *  */
void Agent::set(int currentRank, std::complex<double> _current, std::complex<double> _voltage,
                int _next_action_expected,  bool _convergence) {
    id_.currentRank(currentRank);
    current = _current;
    voltage = _voltage;
    next_action_expected= _next_action_expected;
    convergence = _convergence;
}


/*! \brief set data of MR agent
 *  \param currentRank [in]     current rank of the agent
 *  \param _msg_queue [in]      message queue of the agent
 *  */
void Agent::set(int currentRank, std::list<Agent_message> _msg_queue) {
    id_.currentRank(currentRank);
    outgoing_messages.clear();
    outgoing_messages = std::move(_msg_queue);
}

/*! \brief advance a simulation step
 *
 * virtual function, to be re-implemented in all derived agent classes
 * */
void Agent::step(){
    //dummy
}

/*! \brief calculation of model for a simulation step
 *
 * virtual function, to be re-implemented in all derived agent classes that solve an el. model
 * */
void Agent::calculate(){
    //dummy
}

/*! \brief return the value of member current
 * */
std::complex<double> Agent::get_current(){
    return current;
}

/*! \brief return the value of member voltage
 * */
std::complex<double> Agent::get_voltage() {
    return voltage;
}

/*! \brief return the value of member convergence
 * */
bool Agent::get_convergence(){
    return convergence;
}

/*!
 * \brief Checks which kind of iteration this node expects next
 * \return returns 1 if a forward iteration is expected next, 0 if a backward iteration is expected next and, 2 if a convergence check is expected next
 * */
int Agent::get_next_action_expected() {
    return next_action_expected;
}


/*! \brief save components, previous and next electrical nodes (required for FBS algorithm)
 *  \param agent_network_elec [in] RepastHPC SharedNetwork of electrical connections of agents in this process
 * */
void Agent::save_adjacent_agents(
        repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) {
    std::vector<Agent*> all_el_connections;
    agent_network_elec->adjacent(this, all_el_connections);

    for(auto i : all_el_connections){
        if(i->getId().agentType() == TYPE_LOAD_INT ||
           i->getId().agentType() == TYPE_PV_INT ||
           i->getId().agentType() == TYPE_BATTERY_INT ||
           i->getId().agentType() == TYPE_CHP_INT ||
           i->getId().agentType() == TYPE_EV_INT||
           i->getId().agentType() == TYPE_COMPENSATOR_INT ||
           i->getId().agentType() == TYPE_WEC_INT ||
		   i->getId().agentType() == TYPE_HP_INT||
		   i->getId().agentType() == TYPE_BIOFUEL_INT){
            components.push_back(i);
        }
        else if(i->getId().agentType() == TYPE_NODE_INT ||
                i->getId().agentType() == TYPE_SLACK_INT ||
                i->getId().agentType() == TYPE_TRANSFORMER_INT){
            if(i->getId().id() >= id_.id()){
                next_nodes_elec.push_back(i);
            }
            else{
                prev_nodes_elec.push_back(i);
            }
        }
    }

}

/*! \brief calculation of backward sweep
 *  \param agent_network_elec [in] RepastHPC SharedNetwork of electrical connections of agents in this process
 *
 * virtual function, to be re-implemented in all derived agent classes that are involved in FBS algorithm
 * */
bool Agent::do_backward_sweep(
        repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) {
    //dummy
    return false;
}

/*! \brief calculation of forward sweep
*  \param agent_network_elec [in] RepastHPC SharedNetwork of electrical connections of agents in this process
*
* virtual function, to be re-implemented in all derived agent classes that are involved in FBS algorithm
* */
uint Agent::do_forward_sweep(
        repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) {
    //dummy
    return DONE_NOTHING;
}

/*! \brief process all messages that are received in the current simulation step
 *
 * first update the input data based on profile information, then execute the agent behavior
* */
void Agent::process_incoming_messages() {
    /* update power demand */
    IO->log_info("\tUpdate input data");
    this->update_input_data();

    if (id_.agentType() != TYPE_NODE_INT && id_.agentType() != TYPE_MR_INT) {
        behavior->execute_agent_behavior();
    }

    IO->log_info("\tElectrical calculation");
}

/*! \brief calculation of forward sweep
*
* virtual function, to be re-implemented in all derived agent classes that use profile data (time series data) as input
* */
void Agent::update_input_data()
{
    /* dummy */
}

/*! \brief append an agent message to the queue of incoming messages
 * \param msg [in] message to be received
 * */
void Agent::receive_message(Agent_message &msg) {

    if (id_.agentType() == TYPE_MR_INT) {
        /*insert message into incoming queue sorted by sender id*/
        if (incoming_messages.empty()) {
            incoming_messages.push_back(msg);
        } else {
            bool inserted = false;
            for (auto it = incoming_messages.begin(); it != incoming_messages.end(); it++) {
                if (it->sender > msg.sender) {
                    incoming_messages.insert(it, msg);
                    inserted = true;
                    break;
                }
            }
            if (!inserted) {
                incoming_messages.push_back(msg);
            }
        }

        msg_received++;
    } else {
        behavior->receive_message(msg);
    }
}

/*! \brief return the next outgoing agent message
 * \return Agent message that is next in the outgoing queue
 * */
Agent_message Agent::get_next_outgoing_message() {
    if (id_.agentType() == TYPE_MR_INT) {
        Agent_message next_msg = outgoing_messages.front();
        outgoing_messages.pop_front();
        return next_msg;
    }
    else {
        return behavior->get_next_outgoing_message();
    }
}

/*! \brief return true if the queue of outgoing messages is empty, false otherwise
 *  \return true if outgoing queue is empty, false otherwise
 * */
bool Agent::outgoing_is_empty() {
    if (id_.agentType() != TYPE_NODE_INT && id_.agentType() != TYPE_MR_INT) {
        return behavior->outgoing_is_empty();
    }
    else if (id_.agentType() == TYPE_MR_INT) {

        return outgoing_messages.empty();
    }
    else{
        return true;
    }
}

/*! \brief return the queue of outgoing messages
 *  \return queue of outgoing message as list structure
 * */
std::list<Agent_message> Agent::get_outgoing_queue() {
    if (id_.agentType() != TYPE_NODE_INT && id_.agentType() != TYPE_MR_INT) {
        return behavior->get_outgoing_queue();
    }
    else {
        return outgoing_messages;
    }
}

/*! \brief Initialize the agent behavior
 * \param components_ad_nodes [in] vector saving a list of connceted components for each node
 * \param connected_nodes [in] vector saving a list of connected nodes for each node
 * \param components_file [in] data contained in scenario components input file (integers)
 * \param subtypes_file [in] subtypes contained in scenario components input file (strings)
 * \param el_grid_file [in] electrical connections contained in el. grid input file (integers)
 * */
int Agent::init_behavior(std::vector<std::list<std::tuple<int, int, int>>> *components_at_nodes,
                          std::vector<std::list<std::tuple<int, int, int>>> *connected_nodes,
                          boost::multi_array<int, 2> *components_file,
                          boost::multi_array<std::string, 1> *subtypes_file,
                          boost::multi_array<int, 2> *el_grid_file) {
                              
    int ret = behavior->initialize_agent_behavior(components_at_nodes, connected_nodes, components_file,
                                        subtypes_file, el_grid_file);
    return ret;
}


int Agent::villas_interface_disconnect() {

    if (behavior != nullptr) {
        //destroy villas interface if this agent has a behavior
        int ret = behavior->destroy_villas_interface();
        return ret;
    }
    return 0;
}

/*! \brief Set pointers of time measurements (called by model class for each agent)
 * \param _tm_csv pointer to time measurement for csv saving from model class
 * \param _tm_db pointer to time measurement for DB saving from model class
 * \param _tm_db_serialize pointer to time measurement for serializing data for DB from model class
 * \param _tm_db_add pointer to time measurement for adding data to DB from model class
 */
void Agent::set_tm(Time_measurement *_tm_csv, Time_measurement *_tm_db, Time_measurement *_tm_db_serialize,
                   Time_measurement *_tm_db_add) {

    tm_csv = _tm_csv;
    tm_db = _tm_db;
    tm_db_serialize = _tm_db_serialize;
    tm_db_add = _tm_db_add;

}

/*! \brief Abort the simulation (called in case of an error)
 * \param code Error code identifying the error
 *
 * An MPI Abort signal is sent to all MPI processes in the simulation.*/
void Agent::do_exit(int code){
    std::cout << "Agent Error. Aborting!" << std::endl;
    MPI_Abort(MPI_COMM_WORLD, code);
}

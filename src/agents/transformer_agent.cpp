/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#include "agents/transformer_agent.h"
#include "behavior/swarmgrid/swarm_substation_behavior.h"
#include "behavior/reference/ref_substation_behavior.h"
#include "behavior/ref_intcomm/refic_substation_behavior.h"
#include "behavior/mqtt_reference/mqtt_ref_substation_behavior.h"
#include "behavior/mqtt_pingpong/mqtt_pingpong_substation_behavior.h"
#include "behavior/ensure/ensure_substation_behavior.h"
#include "behavior/mqtt_highload/mqtt_highload_substation_behavior.h"

/*! \brief Constructor used for creation of copies of non-local agents
 *  \param id [in]          RepastHPC agent id
 *  \param is_local [in]    true if local agent is created, false otherwise
 * */
Transformer_agent::Transformer_agent(repast::AgentId &id, bool is_local) : Agent(id, is_local) {}

/*! \brief Constructor used for creation of agents in model
 *  \param id [in]                  RepastHPC agent id
 *  \param is_local [in]            true if local agent is created, false otherwise
 *  \param _node_id [in]            id of the transformer
 *  \param step_size [in]           size of a simulation step in seconds
 *  \param _behavior_type [in]      user selected behavior for agents
 *  \param _model_type [in]         user selected type of el. models (0=static phasor models, 1=dynamic phasor models)
 *  \param _subtype [in]            subtype of the agent
 *  \param _d_props [in]            properties of logging and result saving
 *  \param _villas_config [in]      configuration for villas node
 *  \param _Vnom1 [in]              voltage at primary side in V
 *  \param _Vnom2 [in]              voltage at secondary side in V
 *  \param R [in]                   resistance in Ohm
 *  \param X [in]                   reactance in ???
 *  \param _Sr [in]                 rated apparent power in W
 *  \param _N [in]                  number of switch states
 *  \param _range [in]              range of adjustable ratio in percent
 *  TODO: complete documentation of transformer parameters
 * */
Transformer_agent::Transformer_agent(repast::AgentId &id,
                                     bool is_local,
                                     double &step_size,
                                     int &node_id,
                                     int &_behavior_type,
                                     int &_model_type,
                                     std::string &_subtype,
                                     struct data_props _d_props,
                                     villas_node_config *_villas_config,
                                     double _Vnom1,
                                     double _Vnom2,
                                     double R,
                                     double X,
                                     double _Sr,
                                     int _N,
                                     int _range,
                                     bool _ict_connected
) : Agent(id, is_local, step_size, _behavior_type, _model_type, _subtype, _d_props) {


    if (is_local_instance) {
        IO->init_logging();
        IO->log_info("Creating transformer agent " + IO->id2str(id));

        substation_data.Vnom1 = _Vnom1;
        substation_data.Vnom2 = _Vnom2;
        substation_data.ratio = substation_data.Vnom1/substation_data.Vnom2;
        substation_data.N = _N;
        substation_data.range = _range;
        substation_data.n = 0;
        substation_data.n_ctrl = 0;
        substation_data.S_r = _Sr;
        substation_data.R = R;
        substation_data.X = X;
        substation_data.Z = sqrt(R*R + X*X);

        IO->log_info("Input params: Vnom1 = " + std::to_string(_Vnom1) +
                     " | Vnom2 = "  + std::to_string(_Vnom2) +
                     " | R = " + std::to_string(substation_data.R) +
                     " | Z = " + std::to_string(substation_data.Z) +
                     " | X = " + std::to_string(substation_data.X) +
                     " | S_r = " + std::to_string(substation_data.S_r) +
                     " | N = " + std::to_string(_N) +
                     " | range = " + std::to_string(_range));

    }


    switch (behavior_type) {
        case CTRL_NON_INTELLIGENT: {
            behavior = new Ref_substation_behavior(id_.id(), TYPE_TRANSFORMER_INT, _subtype, t_step, _d_props, &substation_data);
            break;
        }
        case CTRL_INTELLIGENT: {
            behavior = new Swarm_substation_behavior(id_.id(), TYPE_TRANSFORMER_INT, _subtype, t_step, _d_props, &substation_data);
            break;
        }
        case CTRL_MQTT_TEST:{
            //behavior = new Ref_substation_behavior(id_.id(), TYPE_TRANSFORMER_INT, _subtype, t_step, logging, &substation_data, &substation_data);
            behavior = new Mqtt_ref_substation_behavior(id_.id(), TYPE_TRANSFORMER_INT,id_.currentRank(), _subtype, t_step, _d_props, _villas_config, &substation_data);
            break;
        }
        case CTRL_MQTT_PINGPONG:{
            behavior = new Mqtt_pingpong_substation_behavior(id_.id(), TYPE_TRANSFORMER_INT,id_.currentRank(), _subtype, t_step, _d_props, _villas_config, &substation_data);
            break;
        }
        case CTRL_ENSURE:{
            behavior = new Ensure_substation_behavior(id_.id(), TYPE_TRANSFORMER_INT,id_.currentRank(), _subtype, t_step, _d_props, _villas_config, &substation_data);
            break;
        }
       case CTRL_MQTT_HIGHLOAD:{
            behavior = new Mqtt_highload_substation_behavior(id_.id(), TYPE_TRANSFORMER_INT,id_.currentRank(), _subtype, t_step, _d_props, _villas_config, &substation_data);
            break;
        }
        case CTRL_DPSIM_COSIM_REF:{
            behavior = new Ref_substation_behavior(id_.id(), TYPE_TRANSFORMER_INT, _subtype, t_step, _d_props, &substation_data);
            break;
        }
        case  CTRL_DPSIM_COSIM_SWARMGRIDX:{
            behavior = new Swarm_substation_behavior(id_.id(), TYPE_TRANSFORMER_INT, _subtype, t_step, _d_props, &substation_data);
            break;
        }
        case CTRL_INTCOMM:{
            behavior = new Refic_substation_behavior(id_.id(), TYPE_TRANSFORMER_INT, _subtype, t_step, _d_props, &substation_data);
            break;
        }
        default: {
            behavior = new Ref_substation_behavior(id_.id(), TYPE_TRANSFORMER_INT, _subtype, t_step, _d_props, &substation_data);
            break;
        }
    }

    transformer_model = new Transformer(_model_type, step_size, _Vnom1, _Sr, substation_data.R, substation_data.X, substation_data.ratio, substation_data.N, substation_data.range);

    next_action_expected = BACKWARD_SWEEP;
    substation_data.number_of_solves = 0;
    convergence = false;
    substation_data.node_id = node_id;

}

/*! \brief Destructor of Transformer_agent class
 * */
Transformer_agent::~Transformer_agent() {
    delete behavior;
    IO->finish_io();
}

/*! \brief advance a simulation step
 * */
void Transformer_agent::step() {

    if(is_local_instance) {

        substation_data.n = transformer_model->get_n();

        substation_data.msg_sent = behavior->get_msg_sent();
        substation_data.msg_received = behavior->get_msg_received();
        transformer_model->post_processing();

        IO->save_result(&substation_data, first_step, tm_csv, tm_db, tm_db_serialize, tm_db_add);
#ifdef USE_DB
        if(first_step){
            IO->save_meta(&substation_data);
        }
#endif
        t_next+=t_step;
        IO->update_t(t_next);
        behavior->update_t(t_next);
        transformer_model->step(t_next);
        next_action_expected = BACKWARD_SWEEP;
        substation_data.number_of_solves = 0;
        convergence = false;

        //reset values for message counters
        msg_received=0;
        msg_sent=0;

    }
    if(first_step)
        first_step = false;

}

/*! \brief calculation of model for a simulation step
 * */
void Transformer_agent::calculate() {
    if(is_local_instance) {
        // calculate transformer model
        substation_data.i1_im = current.imag();
        substation_data.i1_re = current.real();

        IO->log_info("\tcalculating model for i1 = (" + std::to_string(substation_data.i1_re) + "," + std::to_string(substation_data.i1_im) + ") and v1 = ("
                  + std::to_string(substation_data.v1_re) + "," + std::to_string(substation_data.v1_im) + ")");

        transformer_model->solve(substation_data.v1_re, substation_data.v1_im, substation_data.i1_re, substation_data.i1_im,
            substation_data.v2_re, substation_data.v2_im, substation_data.i2_re, substation_data.i2_im);

        voltage_prev = voltage;
        voltage.imag(substation_data.v2_im);
        voltage.real(substation_data.v2_re);
    }

}

/*! \brief perform actions of the backward sweep in FBS algorithm
 * \param agent_network_elec RepastHPC Shared Network containing electrical connections of agents in this process
 * */
bool Transformer_agent::do_backward_sweep(
        repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) {

    if(substation_data.number_of_solves == 0) {
        transformer_model->set_n_ctrl(substation_data.n_ctrl);
        transformer_model->pre_processing();
    }

    //determine if all next nodes have finished BACKWARD sweep
    bool all_next_nodes_finished_BACKWARD = true;
    for(auto i : next_nodes_elec){
        if(i->get_next_action_expected() == BACKWARD_SWEEP){
            all_next_nodes_finished_BACKWARD = false;
            break;
        }
    }

    if(next_action_expected == BACKWARD_SWEEP && all_next_nodes_finished_BACKWARD){
        IO->log_info("\tTRANSFORMER in BACKWARD SWEEP: ");

        //current_prev = current;
        current.imag(0.0);
        current.real(0.0);

        std::complex<double> current_n(0.0, 0.0);

        //add current that flows to next nodes (if any)
        for(auto i : next_nodes_elec) {
            current_n.real(current_n.real() + i->get_current().real());
            current_n.imag(current_n.imag() + i->get_current().imag());
        }

        //divide current by ratio of transformer
        /* calculate multiplicator for ratio */
        double ratio_mul = transformer_model->get_ratio_mul();
        
        current_n.real(current_n.real() / (substation_data.ratio * ratio_mul));
        current_n.imag(current_n.imag() / (substation_data.ratio * ratio_mul));
        
        boost::shared_ptr<Edge<Agent>> cable = agent_network_elec->findEdge(prev_nodes_elec[0], this);
        std::complex<double> i_leak = cable->get_leakage_current();
        current = current_n + i_leak;

        next_action_expected=FORWARD_SWEEP;
        IO->log_info("calculated current_backward_sweep = (" + std::to_string(current.real()) + "," + std::to_string(current.imag()) + ") ratio=" + (std::to_string(substation_data.ratio * ratio_mul)));
	    return true;
    }
    else{
        IO->log_info("\tNothing to do.");
	    return false;
    }


}

/*! \brief perform actions of the forward sweep in FBS algorithm
 * \param agent_network_elec RepastHPC Shared Network containing electrical connections of agents in this process
 * */
uint Transformer_agent::do_forward_sweep(
        repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) {


    if(next_action_expected == FORWARD_SWEEP){

        //determine if all prev nodes have finished FORWARD sweep
        bool all_prev_nodes_finished_FORWARD = true;
        for(auto i : prev_nodes_elec){
            if(i->get_next_action_expected() != CONVERGENCE_CHECK){
                all_prev_nodes_finished_FORWARD = false;
                IO->log_info("Prev_node " + IO->id2str(i->getId()) + " is not finished with FORWARD, it expects: " +  std::to_string(i->get_next_action_expected()));
                break;
            }
        }

        if(all_prev_nodes_finished_FORWARD) {

            IO->log_info("\tTRANSFORMER in FORWARD SWEEP: ");

            std::complex<double> p_voltage = prev_nodes_elec[0]->get_voltage();
            boost::shared_ptr<Edge<Agent>> cable = agent_network_elec->findEdge(prev_nodes_elec[0],
                                                                                this);
            // and determine the losses via the edge (cable)
            cable->calculate(p_voltage.real(), p_voltage.imag(), current.real(), current.imag());
            substation_data.v1_re = cable->get_voltage_output().real();
            substation_data.v1_im = cable->get_voltage_output().imag();

            //calculate
            this->calculate();

            IO->log_info(
                    "calculated lower side voltage = (" + std::to_string(voltage.real()) + "," +
                    std::to_string(voltage.imag()) + ")");
            next_action_expected = CONVERGENCE_CHECK;
        }
        //return false;
    }

    if(next_action_expected==CONVERGENCE_CHECK){

        //determine if all next nodes expect BACKWARD sweep next
        bool all_next_nodes_expect_BACKWARD = true;
        for(auto i : next_nodes_elec){
            if((i->get_next_action_expected() != BACKWARD_SWEEP)){
                all_next_nodes_expect_BACKWARD = false;
                break;
            }
        }

        if(all_next_nodes_expect_BACKWARD) {

            IO->log_info("\tTRANSFORMER in CONVERGENCE CHECK: ");
            bool next_convergence = true;
            for (auto i : next_nodes_elec) {
                if (!(i->get_convergence())) {
                    next_convergence = false;
                    break;
                }
            }

            if (!next_convergence) {
                convergence = false;
            } else {
                //check for local convergence by comparing voltages of this and previous sweep
                if ((fabs(voltage.real() - voltage_prev.real()) < EPSILON) &&
                    (fabs(voltage.imag() - voltage_prev.imag()) < EPSILON)) {
                    convergence = true;
                } else {
                    convergence = false;
                }
            }
            substation_data.number_of_solves++;
            next_action_expected = BACKWARD_SWEEP;
            IO->log_info("determined convergence to = " + std::to_string(convergence));
            return DONE_CONVERGENCE_CHECK;
        }
        return DONE_FORWARD_SWEEP;
    }
    else{
	if(next_action_expected == BACKWARD_SWEEP){
            IO->log_info("\tNothing to do (already finished in this sweep).");
            return ALREADY_FINISHED;
        } else {
            IO->log_info("\tNothing to do.");
            return DONE_NOTHING;
        }
    }
}

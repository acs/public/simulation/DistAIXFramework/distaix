/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/
#include "agents/agent_io.h"
#include "agents/agent_datastructs.h"

#ifdef USE_DB
#include "DBException.h"
#include "df_agent.pb.h"
#include "mr_agent.pb.h"
#include "node_agent.pb.h"
#include "slack_agent.pb.h"
#include "transformer_agent.pb.h"
#include "prosumer_agent.pb.h"
#endif

/*! \brief Constructor of AgentIO class
 * \param _id [in]                  Agent ID of the Agent that creates the class object
 * \param _behavior_type [in]       specifies the selected control method (agent behavior)
 * \param _d_props [in]             contains parameters for log and resultfiles
 *
 * This class is used to manage all logging and result saving activities of an agent
 * */
AgentIO::AgentIO(repast::AgentId &_id, int &_behavior_type, struct data_props _d_props)
        :IO_object(_d_props), id(_id), behavior_type(_behavior_type)
{
    t_next = 0;
}

/*! \brief initialize the logging and result saving csv files as well as the result types in the DB depending on the selected save method(s)
 * */
void AgentIO::init_logging() {

    init_logfile();
    init_resultfile();
    update_t(0.0);

    //if required, set up DB Saving
    if(d_props.result.db_results){
        this->set_result_types();
    }

}

/*! \brief Log simulation results for the current time step depending on the selected save method
 * \param _model_data [in]  Datastruct containing specific data for the agent's model
 * \param first_step [in]   true if this method is called for the first time, false if not
 * */
void AgentIO::save_result(void *_model_data, bool first_step,
        Time_measurement* tm_csv, Time_measurement* tm_db, Time_measurement* tm_db_serialize, Time_measurement* tm_db_add) {
    tm_csv->start();
    this->save_result_csv(_model_data);
    this->flush_result();
    tm_csv->stop();

    tm_db->start();
    this->save_result_db(_model_data, first_step, tm_db_serialize, tm_db_add);
    tm_db->stop();

}

void AgentIO::save_result_csv_prosumer(void *_model_data){
    if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
        auto *model_data = (Prosumer_data *) _model_data;
        result_stream << t_next << ","
                      << model_data->P << ","
                      << model_data->Q;
        if (behavior_type != CTRL_NON_INTELLIGENT) {
            result_stream << ","
                          << model_data->msg_sent << ","
                          << model_data->msg_received;
        }
        if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
            result_stream << ","
                          << model_data->P_min << ","
                          << model_data->P_optimal << ","
                          << model_data->P_max << ","
                          << model_data->Q_min << ","
                          << model_data->Q_optimal << ","
                          << model_data->Q_max << ","
                          << model_data->swarm_size_p << ","
                          << model_data->swarm_size_q;
        }
    }
}

/*! \brief Log simulation results to csv file
 * \param _model_data [in]  Datastruct containing specific data for the agent's model
 * */
void AgentIO::save_result_csv(void *_model_data) {
    if(d_props.result.csv_results) {
        int type = id.agentType();
        if(type== TYPE_BATTERY_INT) {
            save_result_csv_prosumer(_model_data);
            if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                auto *model_data = (Prosumer_data *) _model_data;
                result_stream << ","
                             << model_data->v_meas << ","
                             << model_data->SOC_el;
            }
        }
        else if (type == TYPE_CHP_INT || type == TYPE_HP_INT) {
            save_result_csv_prosumer(_model_data);

            if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                auto *model_data = (Prosumer_data *) _model_data;
                result_stream << ","
                             << model_data->v_meas << ","
                             << model_data->E_th;
            }
        }
        else if(type == TYPE_COMPENSATOR_INT) {
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                auto *model_data = (Prosumer_data *) _model_data;
                result_stream << t_next << ","
                             << model_data->Q;
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << ","
                                 << model_data->msg_sent << ","
                                 << model_data->msg_received;
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << ","
                                 << model_data->Q_min << ","
                                 << model_data->Q_optimal << ","
                                 << model_data->Q_max << ","
                                 << model_data->swarm_size_q;
                }

                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << ","
                                 << model_data->v_meas;
                }
            }
        }
        else if(type == TYPE_DF_INT) {
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                auto *model_data = (DF_data *) _model_data;
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << t_next << ","
                                 << model_data->msg_sent << ","
                                 << model_data->msg_received;
                }
            }
        }
        else if(type == TYPE_EV_INT) {
            save_result_csv_prosumer(_model_data);
            if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                auto *model_data = (Prosumer_data *) _model_data;
                result_stream << ","
                             << model_data->v_meas << ","
                             << model_data->connected << ","
                             << model_data->SOC_el;
            }
        }
        else if(type == TYPE_LOAD_INT) {
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                auto *model_data = (Prosumer_data *) _model_data;
                result_stream << t_next << ","
                             << model_data->P << ","
                             << model_data->Q;
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << ","
                                 << model_data->msg_sent << ","
                                 << model_data->msg_received;
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << ","
                                 << model_data->swarm_size_p << ","
                                 << model_data->swarm_size_q;
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << ","
                                 << model_data->v_meas;
                }
            }
        }
        else if(type == TYPE_MR_INT) {
            auto * model_data  = (MR_data*) _model_data;
            if(behavior_type != CTRL_NON_INTELLIGENT) {
                result_stream << t_next  << "," << model_data->msg_transmitted;
                if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                    result_stream << "," << model_data->msg_new << ","
                                           << model_data->msg_pending << ","
                                           << model_data->msg_to_other_MR << ","
                                           << model_data->msg_dropped;
                }
            }
        }
        else if(type == TYPE_NODE_INT) {
            auto * model_data  = (Node_data*) _model_data;
            result_stream << t_next << ",";
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                result_stream << model_data->v_re << ","
                             << model_data->v_im << ",";
            }
            result_stream << model_data->v_norm;
        }
        else if(type == TYPE_PV_INT || type == TYPE_WEC_INT || type == TYPE_BIOFUEL_INT) {
            save_result_csv_prosumer(_model_data);
            if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                auto *model_data = (Prosumer_data *) _model_data;
                result_stream << ","
                             << model_data->v_meas;
            }
        }
        else if(type == TYPE_SLACK_INT) {
            auto * model_data  = (Slack_data*) _model_data;
            result_stream << t_next << ","
                    << model_data->P << ","
                    << model_data->Q << ","
                    << model_data->number_of_iterations;
            if(d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                result_stream << ","
                             << model_data->i_re << ","
                             << model_data->i_im;
            }
        }
        else if(type == TYPE_TRANSFORMER_INT) {
            auto *model_data = (Substation_data *)_model_data;

            result_stream << t_next << ","
                         << model_data->n;
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << ","
                                 << model_data->msg_sent << ","
                                 << model_data->msg_received;
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << ","
                                 << model_data->swarm_size_p << ","
                                 << model_data->swarm_size_q;

                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << ","
                                 << model_data->v_meas << ","
                                 << model_data->v_min << ","
                                 << model_data->v_mean << ","
                                 << model_data->v_median << ","
                                 << model_data->v_max << ","
                                 << model_data->i1_re << ","
                                 << model_data->i1_im << ","
                                 << model_data->i2_re << ","
                                 << model_data->i2_im;
                }
            }

        }

        result_stream << std::endl; //new line in any case
    }
}

/*! \brief Log simulation results to database
 * \param _model_data [in]  Datastruct containing specific data for the agent's model
 * \param first_step [in]   true if this method is called for the first time, false if not
 * */
void AgentIO::save_result_db(void *_model_data, bool first_step, Time_measurement* tm_db_serialize, Time_measurement* tm_db_add) {
#ifdef USE_DB

    int aid = id.id();
    int at  = id.agentType();
    int size = 0;
    void* buffer = nullptr;

    if(d_props.result.db_results){ // save results of Prosumer Agent
        tm_db_serialize->start();
        int type = id.agentType();
        if(type== TYPE_BATTERY_INT ||
            type == TYPE_CHP_INT ||
            type == TYPE_COMPENSATOR_INT ||
            type == TYPE_EV_INT ||
            type == TYPE_LOAD_INT ||
            type == TYPE_PV_INT ||
            type == TYPE_WEC_INT ||
            type == TYPE_HP_INT ||
            type == TYPE_BIOFUEL_INT) {

            auto *model_data = (Prosumer_data *) _model_data;
            ProsumerResult proto_res;
            // all fields of the proto_res structure are filled, even if a specific value is
            // meaningless for a type of agent (e.g. P for Compensator Agent)
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                proto_res.set_p(model_data->P);
                proto_res.set_q(model_data->Q);
                proto_res.set_used(true);

                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    proto_res.set_msg_sent(model_data->msg_sent);
                    proto_res.set_msg_received(model_data->msg_received);
                }

                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    proto_res.set_p_min(model_data->P_min);
                    proto_res.set_p_optimal(model_data->P_optimal);
                    proto_res.set_p_max(model_data->P_max);
                    proto_res.set_q_min(model_data->Q_min);
                    proto_res.set_q_optimal(model_data->Q_optimal);
                    proto_res.set_q_max(model_data->Q_max);
                    proto_res.set_swarm_size_p(model_data->swarm_size_p);
                    proto_res.set_swarm_size_q(model_data->swarm_size_q);
                }

                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    proto_res.set_v_meas(model_data->v_meas);
                    proto_res.set_soc_el(model_data->SOC_el);
                    proto_res.set_eth(model_data->E_th);
                    proto_res.set_connected(model_data->connected);
                }
            }

            size = proto_res.ByteSize();
            if(size>0) {
                buffer = malloc(size);
                proto_res.SerializeToArray(buffer, size);
            }
        }
        else if(type == TYPE_DF_INT) {
            aid = aid*1000 - id.startingRank();
            auto * model_data  = (DF_data*) _model_data;
            DFResult proto_res;

            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    proto_res.set_msg_sent(model_data->msg_sent);
                    proto_res.set_msg_received(model_data->msg_received);
                    proto_res.set_used(true);
                }
            }
            size = proto_res.ByteSize();
            if(size>0) {
                buffer = malloc(size);
                proto_res.SerializeToArray(buffer, size);
            }
        }
        else if(type == TYPE_MR_INT) {
            aid = aid * 1000 - id.startingRank();
            auto *model_data = (MR_data *) _model_data;
            MRResult proto_res;

            if(behavior_type != CTRL_NON_INTELLIGENT) {
                proto_res.set_msg_transm(model_data->msg_transmitted);
                proto_res.set_used(true);
                if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                    proto_res.set_msg_new(model_data->msg_new);
                    proto_res.set_msg_pending(model_data->msg_pending);
                    proto_res.set_msg_to_other_mr(model_data->msg_to_other_MR);
                    proto_res.set_msg_dropped(model_data->msg_dropped);
                }
            }

            size = proto_res.ByteSize();
            if(size>0) {
                buffer = malloc(size);
                proto_res.SerializeToArray(buffer, size);
            }
        }
        else if(type == TYPE_NODE_INT) {
            auto * model_data  = (Node_data*) _model_data;
            NodeResult proto_res;

            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                proto_res.set_v_re(model_data->v_re);
                proto_res.set_v_im(model_data->v_im);
            }
            proto_res.set_v_norm(model_data->v_norm);
            proto_res.set_used(true);
            
            size = proto_res.ByteSize();
            if(size>0) {
                buffer = malloc(size);
                proto_res.SerializeToArray(buffer, size);
            }
        }
        else if(type == TYPE_SLACK_INT) {
            auto * model_data  = (Slack_data*) _model_data;
            SlackResult proto_res;

            proto_res.set_p(model_data->P);
            proto_res.set_q(model_data->Q);
            proto_res.set_used(true);
            proto_res.set_number_of_fb_sweeps(model_data->number_of_iterations);
            if(d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                proto_res.set_i_im(model_data->i_im);
                proto_res.set_i_re(model_data->i_re);
            }
            size = proto_res.ByteSize();
            if(size>0) {
                buffer = malloc(size);
                proto_res.SerializeToArray(buffer, size);
            }
        }
        else if(type == TYPE_TRANSFORMER_INT) {
            auto *model_data = (Substation_data *)_model_data;
            TransformerResult proto_res;

            proto_res.set_n(model_data->n);
            proto_res.set_used(true);
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    proto_res.set_msg_sent(model_data->msg_sent);
                    proto_res.set_msg_received(model_data->msg_received);
                }

                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    proto_res.set_swarm_size_p(model_data->swarm_size_p);
                    proto_res.set_swarm_size_q(model_data->swarm_size_q);
                }

                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    proto_res.set_i1_re(model_data->i1_re);
                    proto_res.set_i2_re(model_data->i2_re);
                    proto_res.set_i1_im(model_data->i1_im);
                    proto_res.set_i2_im(model_data->i2_im);
                    proto_res.set_v_min(model_data->v_min);
                    proto_res.set_v_mean(model_data->v_mean);
                    proto_res.set_v_meas(model_data->v_meas);
                    proto_res.set_v_median(model_data->v_median);
                    proto_res.set_v_max(model_data->v_max);
                }
            }

            size = proto_res.ByteSize();
            if(size>0) {
                buffer = malloc(size);
                proto_res.SerializeToArray(buffer, size);
            }
        }
        tm_db_serialize->stop();

        if(buffer != nullptr) {
            tm_db_add->start();
            try {
                d_props.result.dbconn->addResultSerialized(aid, at, t_next, buffer, size, first_step);
            }
            catch(const DBException &ex){
                std::cerr  << "WARNING: Agent " << id << " caught DBException in AgentIO:" << ex.what() << std::endl;
                std::cout  << "WARNING: Agent " << id << " caught DBException in AgentIO:" << ex.what() << std::endl;
            }

            //free buffer
            free(buffer);
            tm_db_add->stop();
        }

    }
#endif
}

/*! \brief Set the header of the csv result file
 * */
void AgentIO::set_result_file_header() {

    //determine type of agent and write correct header to the csv result log file
    std::string header_prosumer_normal = "sim time [s],P [W],Q [var],msg sent,msg received";
    std::string header_prosumer_normal_non_int = "sim time [s],P [W],Q [var]";
    std::string header_prosumer_verbose = ",P_min [W],P_optimal [W],P_max [W],Q_min [var],Q_optimal [var],Q_max [var],swarm size P,swarm size Q,v_meas";
    std::string header_prosumer_verbose_non_int = ",v_meas";
    std::string header_prosumer_minimal = "";
    int type = id.agentType();
    switch(type){
        case TYPE_BATTERY_INT:
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_normal;
                } else {
                    result_stream << header_prosumer_normal_non_int;
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_verbose;
                    result_stream << ",SOC";
                } else if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << header_prosumer_verbose_non_int;
                    result_stream << ",SOC";
                }
                result_stream << std::endl;
            }

            break;
        case TYPE_CHP_INT:
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_normal << ",sec_heater.P_gen_el [W]";
                } else {
                    result_stream << header_prosumer_normal_non_int << ",sec_heater.P_gen_el [W]";
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_verbose;
                    result_stream << ",E_th [Wh]";
                } else if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << header_prosumer_verbose_non_int;
                    result_stream << ",E_th [Wh]";
                }
                result_stream << std::endl;
            }

            break;
        case TYPE_COMPENSATOR_INT:
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << "sim time [s],Q [var],msg sent,msg received";
                } else {
                    result_stream << "sim time [s],Q [var]";
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << ",Q_min [var],Q_optimal [var],Q_max [var],swarm size Q,v_meas";
                } else if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << header_prosumer_verbose_non_int;
                }
                result_stream << std::endl;
            }

            break;
        case TYPE_DF_INT:
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << "sim time [s],msg sent,msg received" << std::endl;
                } else {
                    result_stream << std::endl;
                }
            }
            break;
        case TYPE_EV_INT:
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_normal;
                } else {
                    result_stream << header_prosumer_normal_non_int;
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_verbose;
                    result_stream << ",connected,SOC";
                } else if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << header_prosumer_verbose_non_int;
                    result_stream << ",connected,SOC";
                }

                result_stream << std::endl;
            }

            break;
        case TYPE_LOAD_INT:
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_normal;
                } else {
                    result_stream << header_prosumer_normal_non_int;
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << ",swarm size P,swarm size Q,v_meas";
                } else if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << header_prosumer_verbose_non_int;
                }
                result_stream << std::endl;
            }
            break;
        case TYPE_MR_INT:
            if(behavior_type != CTRL_NON_INTELLIGENT && d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                result_stream << "sim time [s],msg transmitted,msg new,msg pending,msg to other MRs,msg dropped"<< std::endl;
            }
            else if(behavior_type != CTRL_NON_INTELLIGENT && d_props.result.loglevel == LOG_LEVEL_MINIMAL){
                result_stream << "sim time [s],msg transmitted"<< std::endl;
            }
            else{
                result_stream << std::endl;
            }
            break;
        case TYPE_NODE_INT:
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                result_stream << "sim time [s],v_re [V],v_im [V],v_norm [pu]" << std::endl;
            }
            else{
                result_stream << "sim time [s],v_norm [pu]" << std::endl;
            }

            break;
        case TYPE_PV_INT:
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_normal;
                } else {
                    result_stream << header_prosumer_normal_non_int;
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_verbose;
                } else if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << header_prosumer_verbose_non_int;
                }
                result_stream << std::endl;
            }

            break;
        case TYPE_SLACK_INT:
            result_stream << "sim time [s],P [W],Q [var],number of fb sweeps";
            if(d_props.result.loglevel == LOG_LEVEL_VERBOSE){
                result_stream << ",i_re [A],i_im [A]";
            }
            result_stream << std::endl;

            break;
        case TYPE_TRANSFORMER_INT:
            if(behavior_type != CTRL_NON_INTELLIGENT && d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                result_stream << "sim time [s],n,msg sent,msg received";
            }
            else{
                result_stream << "sim time [s],n";
            }
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream
                            << ",swarm size P,swarm size Q,v_meas,v_min,v_mean,v_median,v_max,i1_re [A],i1_im [A],i2_re [A],i2_im [A]";
                } else if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << ",v_meas,v_min,v_mean,v_median,v_max,i1_re [A],i1_im [A],i2_re [A],i2_im [A]";
                }
            }
            result_stream << std::endl;

            break;
        case TYPE_WEC_INT:
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_normal;
                } else {
                    result_stream << header_prosumer_normal_non_int;
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_verbose;
                } else if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << header_prosumer_verbose_non_int;
                }
                result_stream << std::endl;
            }

            break;
        case TYPE_HP_INT:
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_normal << ",sec_heater.P_gen_el [W]";
                } else {
                    result_stream << header_prosumer_normal_non_int << ",sec_heater.P_gen_el [W]";
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_verbose;
                    result_stream << ",E_th [Wh]";
                } else if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << header_prosumer_verbose_non_int;
                    result_stream << ",E_th [Wh]";
                }
                result_stream << std::endl;
            }
            break;
        case TYPE_BIOFUEL_INT:
            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_normal;
                } else {
                    result_stream << header_prosumer_normal_non_int;
                }
                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    result_stream << header_prosumer_verbose;
                } else if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    result_stream << header_prosumer_verbose_non_int;
                }
                result_stream << std::endl;
            }
            break;
        default:
            result_stream << "unknown agent type" << std::endl;
            break;
    }
}

/*! \brief Set the result types in the database
 * */
void AgentIO::set_result_types() {
#ifdef USE_DB
    //determine type of agent and add corresponding result types to the database
    try {
        int type = id.agentType();
        if(type== TYPE_BATTERY_INT ||
           type == TYPE_CHP_INT ||
           type == TYPE_COMPENSATOR_INT ||
           type == TYPE_EV_INT ||
           type == TYPE_LOAD_INT ||
           type == TYPE_PV_INT ||
           type == TYPE_WEC_INT ||
           type == TYPE_HP_INT ||
           type == TYPE_BIOFUEL_INT){ // result types for all prosumer agents

            if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                ProsumerResult proto_res; // for resulttype names
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(3)->name(), "W", "P");
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(7)->name(), "var", "Q");
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(16)->name(), "-", "used");
                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(8)->name(), "", "msg_sent");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(9)->name(), "", "msg_received");
                }

                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(0)->name(), "W", "P_min");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(1)->name(), "W", "P_optimal");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(2)->name(), "W", "P_max");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(4)->name(), "var", "Q_min");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(5)->name(), "var", "Q_optimal");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(6)->name(), "var", "Q_max");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(10)->name(), "", "swarm_size_p");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(11)->name(), "", "swarm_size_q");
                }

                if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(12)->name(), "", "v_meas");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(13)->name(), "", "SOC_el");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(14)->name(), "Ws", "E_th");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(15)->name(), "", "connected");
                }
            }


        }

        switch(type){
            case TYPE_BATTERY_INT: {
                //meta:
                d_props.result.dbconn->addResultType("Vnom", "V", "");
                d_props.result.dbconn->addResultType("C_nom", "Wh", "Usable capacity of battery");
                d_props.result.dbconn->addResultType("pf", "", "power factor");
                d_props.result.dbconn->addResultType("pf_min", "", "minimum power factor (converter parameter)");
                d_props.result.dbconn->addResultType("S_r", "", "rated apparent power (converter parameter)");
                d_props.result.dbconn->addResultType("eta_ch", "", "charging efficiency");
                d_props.result.dbconn->addResultType("eta_disch", "", "discharging efficiency");
                d_props.result.dbconn->addResultType("P_max", "W", "maximum charging and discharging power");
                d_props.result.dbconn->addResultType("P_in_max", "W", "maximal charging power");
                d_props.result.dbconn->addResultType("P_out_max", "W", "maximal discharging power");
                break;
            }
            case TYPE_CHP_INT: {
                //meta:
                d_props.result.dbconn->addResultType("Vnom", "V", "");
                d_props.result.dbconn->addResultType("C_th", "Wh", "Capacity of attached thermal storage");
                d_props.result.dbconn->addResultType("pf", "", "power factor");
                d_props.result.dbconn->addResultType("pf_min", "", "minimum power factor (converter parameter)");
                d_props.result.dbconn->addResultType("S_r", "", "rated apparent power (converter parameter)");
                d_props.result.dbconn->addResultType("Pr", "W", "rated power of combustion engine");
                d_props.result.dbconn->addResultType("eff_el", "", "electrical efficiency");
                d_props.result.dbconn->addResultType("eff_th", "", "thermal efficiency");
                break;
            }
            case TYPE_COMPENSATOR_INT: {
                //meta
                d_props.result.dbconn->addResultType("Vnom","","nominal voltage");
                d_props.result.dbconn->addResultType("Qr", "","rated reactive power");
                d_props.result.dbconn->addResultType("Br", "","susceptance");
                d_props.result.dbconn->addResultType("N", "","number of switching states");
                break;
            }
            case TYPE_DF_INT: {
                if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                    DFResult proto_res;
                    if (behavior_type != CTRL_NON_INTELLIGENT) {
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(0)->name(), "",
                                              "number of sent messages per time step");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(1)->name(), "",
                                              "number of received messages per time step");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(2)->name(), "-", "used");
                    }
                }

                //meta
                d_props.result.dbconn->addResultType("rank", "", "rank of DF");
                break;
            }
            case TYPE_EV_INT: {
                //meta
                d_props.result.dbconn->addResultType("Vnom", "V", "");
                d_props.result.dbconn->addResultType("C_nom", "Wh", "Capacity of battery");
                d_props.result.dbconn->addResultType("pf", "", "power factor");
                d_props.result.dbconn->addResultType("pf_min", "", "minimum power factor (converter parameter)");
                d_props.result.dbconn->addResultType("S_r", "", "rated apparent power (converter parameter)");
                d_props.result.dbconn->addResultType("P_con", "W", "average power consumption when driving");
                break;
            }
            case TYPE_LOAD_INT: {
                //meta
                d_props.result.dbconn->addResultType("Vnom", "V", "");
                d_props.result.dbconn->addResultType("pf", "", "power factor");
                break;
            }
            case TYPE_MR_INT: {
                MRResult proto_res;

                if (behavior_type != CTRL_NON_INTELLIGENT) {
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(0)->name(), "-",
                                          "number of transmitted messages");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(5)->name(), "-", "used");
                    if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(1)->name(), "-",
                                              "number of messages pending due to link latency");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(2)->name(), "-",
                                              "number of messages to other MR Agents");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(3)->name(), "-",
                                              "number of dropped messages due to packet error rate");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(4)->name(), "-",
                                              "number of new messages");
                    }
                }

                //meta
                d_props.result.dbconn->addResultType("number_of_nodes","","number of of nodes including slack and trafos, excluding DFs");
                d_props.result.dbconn->addResultType("rank","","rank of MR agent");
                break;
            }
            case TYPE_NODE_INT: {
                NodeResult proto_res;
                if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(0)->name(), "V", "v_im");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(1)->name(), "V", "v_re");
                }
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(2)->name(), "", "v_norm");
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(3)->name(), "-", "used");
                //meta:
                d_props.result.dbconn->addResultType("Vnom", "V", "");
                break;
            }
            case TYPE_PV_INT: {
                //meta
                d_props.result.dbconn->addResultType("Vnom", "V", "");
                d_props.result.dbconn->addResultType("pf", "", "power factor");
                d_props.result.dbconn->addResultType("pf_min", "", "minimum power factor (converter parameter)");
                d_props.result.dbconn->addResultType("S_r", "", "rated apparent power (converter parameter)");
                break;
            }
            case TYPE_SLACK_INT: {
                SlackResult proto_res;

                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(0)->name(),"W","P");
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(1)->name(),"var","Q");
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(4)->name(),"","number_of_fb_sweeps");
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(5)->name(), "-", "used");
                if(d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(2)->name(), "A", "i_im");
                    d_props.result.dbconn->addResultType(proto_res.descriptor()->field(3)->name(), "A", "i_re");
                }

                //meta
                d_props.result.dbconn->addResultType("Vnom", "V", "");
                break;
            }
            case TYPE_TRANSFORMER_INT: {
                TransformerResult proto_res;

                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(0)->name(),"","n");
                d_props.result.dbconn->addResultType(proto_res.descriptor()->field(14)->name(), "-", "used");
                if(d_props.result.loglevel != LOG_LEVEL_MINIMAL) {
                    if (behavior_type != CTRL_NON_INTELLIGENT) {
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(1)->name(), "", "msg_sent");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(2)->name(), "", "msg_received");
                    }
                    if (d_props.result.loglevel == LOG_LEVEL_VERBOSE && behavior_type != CTRL_NON_INTELLIGENT) {
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(3)->name(), "", "swarm_size_p");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(4)->name(), "", "swarm_size_q");
                    }

                    if (d_props.result.loglevel == LOG_LEVEL_VERBOSE) {
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(5)->name(), "", "v_meas");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(6)->name(), "", "v_min");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(7)->name(), "", "v_mean");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(8)->name(), "", "v_median");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(9)->name(), "", "v_max");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(10)->name(), "A", "i1_re");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(11)->name(), "A", "i1_im");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(12)->name(), "A", "i2_re");
                        d_props.result.dbconn->addResultType(proto_res.descriptor()->field(13)->name(), "A", "i2_im");
                    }
                }
                //meta
                d_props.result.dbconn->addResultType("Vnom1", "V", "");
                d_props.result.dbconn->addResultType("Vnom2", "V", "");
                d_props.result.dbconn->addResultType("Sr", "V", "");
                d_props.result.dbconn->addResultType("URr", "", "");
                d_props.result.dbconn->addResultType("Ukr", "", "");
                break;
            }
            case TYPE_WEC_INT:{
                //meta
                d_props.result.dbconn->addResultType("Vnom", "V", "");
                d_props.result.dbconn->addResultType("pf", "", "power factor");
                d_props.result.dbconn->addResultType("pf_min", "", "minimum power factor (converter parameter)");
                d_props.result.dbconn->addResultType("S_r", "", "rated apparent power (converter parameter)");
                d_props.result.dbconn->addResultType("Pnom", "W", "peak power");
                break;
            }
            case TYPE_HP_INT: {
                //meta
                d_props.result.dbconn->addResultType("Vnom", "V", "");
                d_props.result.dbconn->addResultType("pf", "", "power factor");
                break;
            }
            case TYPE_BIOFUEL_INT: {
                //meta
                d_props.result.dbconn->addResultType("Vnom", "V", "");
                d_props.result.dbconn->addResultType("pf", "", "power factor");
                d_props.result.dbconn->addResultType("pf_min", "", "minimum power factor (converter parameter)");
                d_props.result.dbconn->addResultType("S_r", "", "rated apparent power (converter parameter)");
                break;
            }
            default:
                break;
        }
    }
    catch(const DBException &ex){
        std::cerr  << "WARNING: Agent " << id << " caught DBException in AgentIO:" << ex.what() << std::endl;
        std::cout  << "WARNING: Agent " << id << " caught DBEcxeption in AgentIO:" << ex.what() << std::endl;
    }
#endif
}

/*! \brief Log agent's meta data to the database (in general, this should happen only once at the beginning)
 * \param _model_data [in]  Datastruct containing specific data for the agent's model
 * */
void AgentIO::save_meta(void *_model_data) {
#ifdef USE_DB
    int aid = id.id();
    int at  = id.agentType();
    try {


        if (d_props.result.db_results) {
            int type = id.agentType();
            if (type == TYPE_BATTERY_INT) {
                auto *model_data = (Prosumer_data *) _model_data;
                d_props.result.dbconn->addAgentMetaEntry("Vnom", aid, at, model_data->Vnom);
                d_props.result.dbconn->addAgentMetaEntry("C_nom", aid, at, model_data->C_el);
                d_props.result.dbconn->addAgentMetaEntry("pf", aid, at, model_data->pf);
                d_props.result.dbconn->addAgentMetaEntry("pf_min", aid, at, model_data->pf_min);
                d_props.result.dbconn->addAgentMetaEntry("S_r", aid, at, model_data->S_r);
                //d_props.result.dbconn->addAgentMetaEntry("eta_ch", aid, at, model_data->eta_ch);
                //d_props.result.dbconn->addAgentMetaEntry("eta_disch", aid, at, model_data->eta_disch);
                d_props.result.dbconn->addAgentMetaEntry("P_max", aid, at, model_data->P_nom);
                //d_props.result.dbconn->addAgentMetaEntry("P_in_max", aid, at, model_data->P_max);
                //d_props.result.dbconn->addAgentMetaEntry("P_out_max", aid, at, model_data->P_min);
            } else if (type == TYPE_CHP_INT) {
                auto *model_data = (Prosumer_data *) _model_data;
                d_props.result.dbconn->addAgentMetaEntry("Vnom", aid, at, model_data->Vnom);
                d_props.result.dbconn->addAgentMetaEntry("C_th", aid, at, model_data->C_th);
                d_props.result.dbconn->addAgentMetaEntry("pf", aid, at, model_data->pf);
                d_props.result.dbconn->addAgentMetaEntry("pf_min", aid, at, model_data->pf_min);
                d_props.result.dbconn->addAgentMetaEntry("S_r", aid, at, model_data->S_r);
                /* TODO: is it ok to leave these out? */
                //d_props.result.dbconn->addAgentMetaEntry("Pr", aid, at, model_data->Pr);
                //d_props.result.dbconn->addAgentMetaEntry("eff_el", aid, at, model_data->eff_el);
                //d_props.result.dbconn->addAgentMetaEntry("eff_th", aid, at, model_data->eff_th);
                d_props.result.dbconn->addAgentMetaEntry("f_el", aid, at, model_data->f_el);
            } else if (type == TYPE_COMPENSATOR_INT) {
                auto *model_data = (Prosumer_data *) _model_data;
                //d_props.result.dbconn->addAgentMetaEntry("Vnom", aid, at, model_data->Vnom);
                //d_props.result.dbconn->addAgentMetaEntry("Qr", aid, at, model_data->Qr);
                //d_props.result.dbconn->addAgentMetaEntry("Br", aid, at, model_data->Br);
                d_props.result.dbconn->addAgentMetaEntry("N", aid, at, model_data->N);
                d_props.result.dbconn->addAgentMetaEntry("Br", aid, at, model_data->S_r);
            } else if (type == TYPE_DF_INT) {
                aid = aid * 1000 - id.startingRank();
                d_props.result.dbconn->addAgentMetaEntry("rank", aid, at, id.currentRank());
            } else if (type == TYPE_EV_INT) {
                auto *model_data = (Prosumer_data *) _model_data;
                d_props.result.dbconn->addAgentMetaEntry("Vnom", aid, at, model_data->Vnom);
                d_props.result.dbconn->addAgentMetaEntry("C_nom", aid, at, model_data->C_el);
                d_props.result.dbconn->addAgentMetaEntry("pf_min", aid, at, model_data->pf_min);
                d_props.result.dbconn->addAgentMetaEntry("S_r", aid, at, model_data->S_r);
                d_props.result.dbconn->addAgentMetaEntry("P_con", aid, at, model_data->P_nom);
            } else if (type == TYPE_LOAD_INT) {
                auto *model_data = (Prosumer_data *) _model_data;
                d_props.result.dbconn->addAgentMetaEntry("Vnom", aid, at, model_data->Vnom);
                d_props.result.dbconn->addAgentMetaEntry("pf", aid, at, model_data->pf);
            } else if (type == TYPE_MR_INT) {
                aid = aid * 1000 - id.startingRank();
                auto *model_data = (MR_data *) _model_data;
                d_props.result.dbconn->addAgentMetaEntry("number_of_nodes", aid, at, model_data->number_of_nodes);
                d_props.result.dbconn->addAgentMetaEntry("rank", aid, at, id.currentRank());
            } else if (type == TYPE_NODE_INT) {
                auto *model_data = (Node_data *) _model_data;
                d_props.result.dbconn->addAgentMetaEntry("Vnom", aid, at, model_data->Vnom);
            } else if (type == TYPE_PV_INT) {
                auto *model_data = (Prosumer_data *) _model_data;
                d_props.result.dbconn->addAgentMetaEntry("Vnom", aid, at, model_data->Vnom);
                d_props.result.dbconn->addAgentMetaEntry("pf", aid, at, model_data->pf);
                d_props.result.dbconn->addAgentMetaEntry("pf_min", aid, at, model_data->pf_min);
                d_props.result.dbconn->addAgentMetaEntry("S_r", aid, at, model_data->S_r);
            } else if (type == TYPE_SLACK_INT) {
                auto *model_data = (Slack_data *) _model_data;
                d_props.result.dbconn->addAgentMetaEntry("Vnom", aid, at, model_data->Vnom);
            } else if (type == TYPE_TRANSFORMER_INT) {
                auto *model_data = (Substation_data *) _model_data;
                d_props.result.dbconn->addAgentMetaEntry("Vnom1", aid, at, model_data->Vnom1);
                d_props.result.dbconn->addAgentMetaEntry("Vnom2", aid, at, model_data->Vnom2);
                d_props.result.dbconn->addAgentMetaEntry("R", aid, at, model_data->R);
                d_props.result.dbconn->addAgentMetaEntry("X", aid, at, model_data->X);
                d_props.result.dbconn->addAgentMetaEntry("Z", aid, at, model_data->Z);
                d_props.result.dbconn->addAgentMetaEntry("S_r", aid, at, model_data->S_r);
                d_props.result.dbconn->addAgentMetaEntry("N", aid, at, model_data->N);
                d_props.result.dbconn->addAgentMetaEntry("range", aid, at, model_data->range);
            } else if (type == TYPE_WEC_INT) {
                auto *model_data = (Prosumer_data *) _model_data;
                d_props.result.dbconn->addAgentMetaEntry("Vnom", aid, at, model_data->Vnom);
                d_props.result.dbconn->addAgentMetaEntry("pf_min", aid, at, model_data->pf_min);
                d_props.result.dbconn->addAgentMetaEntry("S_r", aid, at, model_data->S_r);
                d_props.result.dbconn->addAgentMetaEntry("Pnom", aid, at, model_data->P_nom);
            } else if (type == TYPE_HP_INT) {
                auto *model_data = (Prosumer_data *) _model_data;
                d_props.result.dbconn->addAgentMetaEntry("Vnom", aid, at, model_data->Vnom);
                d_props.result.dbconn->addAgentMetaEntry("pf_min", aid, at, model_data->pf_min);
            } else if (type == TYPE_BIOFUEL_INT) {
                auto *model_data = (Prosumer_data *) _model_data;
                d_props.result.dbconn->addAgentMetaEntry("Vnom", aid, at, model_data->Vnom);
                d_props.result.dbconn->addAgentMetaEntry("pf", aid, at, model_data->pf);
                d_props.result.dbconn->addAgentMetaEntry("pf_min", aid, at, model_data->pf_min);
                d_props.result.dbconn->addAgentMetaEntry("S_r", aid, at, model_data->S_r);
            }


        }
    }
    catch(const DBException &ex){
        std::cerr  << "WARNING: Agent " << id << " caught DBException in AgentIO:" << ex.what() << std::endl;
        std::cout  << "WARNING: Agent " << id << " caught DBException in AgentIO:" << ex.what() << std::endl;
    }
#endif
}

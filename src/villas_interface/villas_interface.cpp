/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/


#include "villas_interface/villas_interface.h"

#include <cstring>
#include <string>
#include "model/io_object.h"


#ifdef WITH_VILLAS
    #include "villas/nodes/mqtt.hpp"
    #include "villas/nodes/nanomsg.hpp"
    #include "villas/node.h"
    #include "villas/format.hpp"
    #include "villas/signal.h"
    #include "villas/log.hpp"
#endif



/*! \brief Constructor of Villas_interface class
 *
 * */
Villas_interface::Villas_interface(villas_node_config *_config, IO_object *IO_obj, std::string _name, std::vector<Meta_infos> &_meta) {


    IO = IO_obj; //use IO object of holing instance
    IO->log_info("Villas_interface: constructor for node " + _name);

#ifdef WITH_VILLAS
    meta = _meta;
    number_of_sent_messages = 0;
    with_node = _config->with_node;

    //ranks set logging properties for VILLASnode lib
    if(_name.find("rank_") != std::string::npos){
        json_t *json_logging;
        json_error_t * j_err = nullptr;

        // available log levels of VILLASnode lib:
        // trace : ???
        // debug : log debug messages
        // info : log info messages
        // warning : log warnings
        // error : log error messages
        // critical : ???
        // off : no logging at all

        //json_logging = json_loads("{\"level\" : \"warning\", \"file\" : \"villas.log\"}", 0, j_err);
        std::string params = R"({"level" : ")" + _config->loglevel + "\"}";
        json_logging = json_loads(params.c_str(), 0, j_err);

        villas::logging.parse(json_logging);

    }

    //lookup node type
    IO->log_info("Villas_interface: Node type lookup for node " + _name + " Type: " + _config->type_name + ".");
    type =  node_type_lookup(_config->type_name);
    if(type== nullptr){
        IO->log_info("Villas_interface: ERROR: something went wrong in node type lookup");
        throw std::runtime_error("Villas_interface: node_type_lookup failed for type "  + _config->type_name);
    }

    if(with_node) {
        int ret; //for return values of villas functions

        //set several states of node internal objects to STATE_DESTROYED
        n = (struct vnode*) malloc(sizeof(struct vnode));
        n->state = State::DESTROYED;
        n->in.state = State::DESTROYED;
        n->out.state = State::DESTROYED;
        n->in.signals.state = State::DESTROYED;
        n->out.signals.state = State::DESTROYED;

        //Init memory pool for this node (used to get memory for sample in send_message)
        p = (struct pool*) malloc(sizeof(struct pool));
        p->state = State::DESTROYED;
        p->queue.state = State::DESTROYED;
        //Allocate 1MB of pool memory for sent and received messages
        //200 kiByte = 204800
        //1 MiB = 1048576
        size_t blocksize=SAMPLE_LENGTH(meta.size()); //sizeof(struct sample) + meta.size()* sizeof(double);
        IO->log_info("Villas_interface: Memory pool allocation for " + std::to_string(meta.size())+ " signals and blocksize=" + std::to_string(blocksize));
        ret = pool_init(p, 1024, blocksize, &(memory_heap));
        if (ret) {
            throw std::runtime_error("Villas_interface: pool_init failed for node "  + _name +
            " and node type " + std::string(type->name) + " with return value " + std::to_string(ret));
        }


        // init VILLASnode based on config from above
        IO->log_info("Villas_interface: node_init");
        ret = node_init(n, type);
        if (ret) {
            throw std::runtime_error("Villas_interface: node_init failed for node "  + _name +
            " and node type " + std::string(type->name) + " with return value " + std::to_string(ret));
        }

        //parse configuration of node
        IO->log_info("Villas_interface: parsing node configuration");

        n->name = strdup(_name.c_str());
        n->in.enabled = 1;
        n->out.enabled = 1;
        n->out.vectorize = 1; //only one sample at a time
        n->in.vectorize = 1;
        IO->log_info("Villas_interface: parsing config for " + std::string(type->name));
        if(std::string(type->name) == "mqtt") {
            //configure the node for mqtt

            //set mqtt parameters
            auto *m = (mqtt *) n->_vd;

            //set broker
            m->host = strdup(_config->type_config.mqtt_conf->broker.c_str()) ;

            //set publish topic (if any)
            if(!_config->type_config.mqtt_conf->publish.empty()){
                m->publish = strdup(_config->type_config.mqtt_conf->publish.c_str());
            } else{
                m->publish = nullptr;
            }
            //set subscribe topic (if any)
            if(!_config->type_config.mqtt_conf->subscribe.empty()){
                m->subscribe = strdup(_config->type_config.mqtt_conf->subscribe.c_str());
            } else{
                m->subscribe = nullptr;
            }

            m->username = nullptr;
            m->password = nullptr;
            m->port = _config->type_config.mqtt_conf->port;
            m->retain = _config->type_config.mqtt_conf->retain;
            m->keepalive = _config->type_config.mqtt_conf->keepalive;
            m->qos = _config->type_config.mqtt_conf->qos;
            //SSL config params
            m->ssl.enabled = _config->type_config.mqtt_conf->ssl_enabled;
            m->ssl.insecure = _config->type_config.mqtt_conf->ssl_insecure;
            m->ssl.cert_reqs = _config->type_config.mqtt_conf->ssl_cert_reqs;
            m->ssl.tls_version = strdup(_config->type_config.mqtt_conf->ssl_tls_version.c_str());
            m->ssl.cafile = strdup(_config->type_config.mqtt_conf->ssl_cafile.c_str());
            m->ssl.capath = strdup(_config->type_config.mqtt_conf->ssl_capath.c_str());
            m->ssl.certfile = strdup(_config->type_config.mqtt_conf->ssl_certfile.c_str());
            m->ssl.ciphers = strdup(_config->type_config.mqtt_conf->ssl_ciphers.c_str());
            m->ssl.keyfile = strdup(_config->type_config.mqtt_conf->ssl_keyfile.c_str());
            m->queue.queue.state = State::DESTROYED;
            m->pool.state = State::DESTROYED;

            //lookup the format type for this mqtt node
            m->formatter = villas::node::FormatFactory::make(_config->format_name);
        }
        else if (std::string(type->name) == "nanomsg") {
            //configure the node for nanomsg

            // set nanomsg parameters
            auto *m = (nanomsg *) n->_vd;

            ret = vlist_init(&m->out.endpoints);
            if (ret) {
                throw std::runtime_error("Villas_interface: vlist_init of out.endpoints failed for node "  + _name +
                " with return value " + std::to_string(ret));
            }
            ret = vlist_init(&m->in.endpoints);
            if (ret) {
                throw std::runtime_error("Villas_interface: vlist_init of in.endpoints failed for node "  + _name +
                " with return value " + std::to_string(ret));
            }

            if(!_config->type_config.nanomsg_conf->in_endpoints.empty()) {
                for(const auto& endpoint : _config->type_config.nanomsg_conf->in_endpoints){
                    IO->log_info("PUSH IN ENDPOINT: " + endpoint);
                    vlist_push(&m->in.endpoints, &strdup(endpoint.c_str())[0]);
                }
            }
            if(!_config->type_config.nanomsg_conf->out_endpoints.empty()) {
                for(const auto& endpoint : _config->type_config.nanomsg_conf->out_endpoints){
                    IO->log_info("PUSH OUT ENDPOINT: " + endpoint);
                    vlist_push(&m->out.endpoints, &strdup(endpoint.c_str())[0]);
                }
            }
            m->formatter = villas::node::FormatFactory::make(_config->format_name);
        } else {
            //IO->log_info("Villas_interface: Error: node type " + std::string(type->name) + " not supported by DistAIX.");
            throw std::runtime_error("Villas_interface: Error in node " + _name + ": node type "
                    + std::string(type->name) + " not supported.");
        }


        // generate list of signals for villas node based on meta information of message type
        // configures behavior-specific input and output signals
        // this does the job of "node_direction_parse(...)"
        IO->log_info("Villas_interface: configure signals");
        for(auto & i : meta){
            SignalType sig_type;

            // map data type to VILLAS signal data types
            if(i.type == VILLAS_DATA_TYPE_INT64){
                sig_type = SignalType::INTEGER;
            }
            else if(i.type == VILLAS_DATA_TYPE_DOUBLE){
                sig_type = SignalType::FLOAT;
            }
            else if(i.type == VILLAS_DATA_TYPE_BOOLEAN){
                sig_type = SignalType::BOOLEAN;
            }
            else if(i.type == VILLAS_DATA_TYPE_COMPLEX){
                sig_type = SignalType::COMPLEX;
            } else {
                sig_type = SignalType::INVALID;
            }

            // create signals
            struct signal * sig_in = signal_create(i.name.c_str(), i.unit.c_str(), sig_type);
            struct signal * sig_out = signal_create(i.name.c_str(), i.unit.c_str(), sig_type);

            // save signals in Villas node signal lists
            vlist_push(&(n->in.signals), sig_in);
            vlist_push(&(n->out.signals), sig_out);
        }


        // set state to parsed to be able to continue with this node
        n->state = State::PARSED;
        // set in and out node directions to PARSED
        n->in.state = State::PARSED;
        n->out.state = State::PARSED;

        //check node
        IO->log_info("Villas_interface: node_check");
        ret = node_check(n);
        if (ret) {
            throw std::runtime_error("Villas_interface: node_check failed for node "  + _name +
            " and node type " + std::string(type->name) + " with return value " + std::to_string(ret));
        }

        //prepare node
        IO->log_info("Villas_interface: node_prepare");
        ret = node_prepare(n);
        if (ret) {
            throw std::runtime_error("Villas_interface: node_prepare failed for node "  + _name +
            " and node type " + std::string(type->name) + " with return value " + std::to_string(ret));
        }
    } else{
        IO->log_info("This instance of Villas_interface is used without node");
    }

#else
    //Warn the user and throw exception
    IO->log_info("ERROR: Your agent behavior is trying to instantiate a Villas_interface, but DistAIX is compiled without VILLASnode support.\n"
                 "This will very likely cause a simulation failure!\n"
                 "Pass option -villas to build_and_install.sh script to compile DistAIX with VILLASnode support");
    std::cerr << "ERROR: Your agent behavior is trying to instantiate a Villas_interface, but DistAIX is compiled without VILLASnode support." << std::endl;
    std::cerr << "This will very likely cause a simulation failure!" << std::endl;
    std::cerr << "Pass option -villas to build_and_install.sh script to compile DistAIX with VILLASnode support" << std::endl;
    throw std::runtime_error("Villas_interface: Trying to create instance of VILLAS_interface, but DistAIX is compiled without libvillas. Check log files for more infos. I'm stopping here.");
#endif
}


/*! \brief Destructor of Villas_interface class
 *
 * */
Villas_interface::~Villas_interface() {
#ifdef WITH_VILLAS
    if(with_node) {
        int ret;
        ret = pool_destroy(p);
        if (ret){
            IO->log_info("Villas_interface::Destructor pool_destroy failed for node "  + std::string(n->name) +
            " and node type " + std::string(type->name) + " with return value " + std::to_string(ret));
        }
    }
#endif
}


/*! \brief Start a Villas_interface
 *
 * */
int Villas_interface::start() {
#ifdef WITH_VILLAS
    int ret =0; //for return values of villas functions
    IO->log_info("Villas_interface::start: starting villas node " + std::string(n->name));
    if(with_node) {
        ret = node_start(n);
        if (ret) {
            IO->log_info("Villas_interface::start: node_start failed for node "  + std::string(n->name) +
            " and node type " + std::string(type->name) + " ; ret="+std::to_string(ret));
        }

    }
    else{
        IO->log_info("Villas_interface::start: Cannot start() villas node because this instance"
                     " of Villas_interface is used without node");
    }

    return ret;
#else
    return -1;
#endif
}


/*! \brief Stop a Villas_interface
 *
 * */
int Villas_interface::stop() {
#ifdef WITH_VILLAS
    int ret =0; //for return values of villas functions
    IO->log_info("Villas_interface::stop: stopping villas node " + std::string(n->name));
    if(with_node) {
        ret = node_stop(n);
        if (ret) {
            IO->log_info("Villas_interface::stop: node_stop failed for node "  + std::string(n->name) +
            " and node type " + std::string(type->name));
        }
    }
    else{
        IO->log_info("Villas_interface::stop: Cannot stop() villas node because this instance"
                     " of Villas_interface is used without node");
    }

    return ret;
#else
    return -1;
#endif
}

/*! \brief Destroy a Villas_interface
 *
 * */
int Villas_interface::destroy() {
#ifdef WITH_VILLAS
    int ret =0; //for return values of villas functions
    IO->log_info("Villas_interface::destroy: destroying villas node " + std::string(n->name));
    if(with_node) {
        ret = node_destroy(n);
        if (ret) {
            IO->log_info("Villas_interface::destroy: node_destroy failed for node "  + std::string(n->name) +
            " and node type " + std::string(type->name));
        }
    }
    else{
        IO->log_info("Villas_interface::destroy: Cannot destroy() villas node because this instance "
                     "of Villas_interface is used without node" );
    }
    return ret;
#else
    return -1;
#endif
}

/*! \brief Send a message via a Villas_interface
 *
 * */
void Villas_interface::send_message(Villas_message &msg) {
#ifdef WITH_VILLAS
    if(with_node) {

        //int ret = 0;
        int allocated;

        struct sample * new_sample[n->out.vectorize];
        allocated = sample_alloc_many(p, new_sample, (int) n->out.vectorize);

        //fill sample
        IO->log_info("Villas_interface::send_message: sending message of length: " + std::to_string(msg.length));
        new_sample[0]->length = msg.length;
        new_sample[0]->sequence = number_of_sent_messages;
        new_sample[0]->signals = &(n->out.signals);
        for(unsigned int k = 0; k< msg.length; k++){
            if(meta[k].type == VILLAS_DATA_TYPE_INT64){
                //IO->log_info("Villas_interface::send_message: Filling int " +
                //    std::to_string(msg.data[k].i) + " at data[" + std::to_string(k) + "]" );
                new_sample[0]->data[k].i = msg.data[k].i;
            }
            else if(meta[k].type == VILLAS_DATA_TYPE_DOUBLE){
                //IO->log_info("Villas_interface::send_message: Filling double " +
                //    std::to_string(msg.data[k].f) + " at data[" + std::to_string(k) + "]" );
                new_sample[0]->data[k].f = msg.data[k].f;
            }
            else if(meta[k].type == VILLAS_DATA_TYPE_BOOLEAN){
                //IO->log_info("Villas_interface::send_message: Filling boolean " +
                //             std::to_string(msg.data[k].b) + " at data[" + std::to_string(k) + "]" );
                new_sample[0]->data[k].b = msg.data[k].b;
            }
            else if (meta[k].type == VILLAS_DATA_TYPE_COMPLEX){
                //IO->log_info("Villas_interface::send_message: Filling complex " + "[" + 
                //              std::to_string(msg.data[k].c.real()) + "," + std::to_string(msg.data[k].c.imag()) + 
                //              "]" + " at data[" + std::to_string(k) + "]" );
                new_sample[0]->data[k].z = msg.data[k].z;
            }
        }

        new_sample[0]->ts.origin.tv_sec = (long int) msg.time_sec;
        double nano_sec = (msg.time_sec- (double) new_sample[0]->ts.origin.tv_sec) * 1000000000;
        new_sample[0]->ts.origin.tv_nsec = (long int) (nano_sec);

        //set flags of sample
        new_sample[0]->flags = 0;
        new_sample[0]->flags |= (unsigned int) SampleFlags::HAS_DATA;

        // send sample
        int release=allocated;
        int sent = node_write(n, new_sample, n->out.vectorize);
        IO->log_info("Villas_interface::send_message: node_write(...) completed, sent " + std::to_string(sent) + " samples");
        if (sent < 0) {
            IO->log_info("Villas_interface::send_message: Failed to sent sample, reason=" + std::to_string(sent));
        }
        number_of_sent_messages++;
        sample_decref_many(new_sample, release);
        sample_free_many(new_sample, (int) n->out.vectorize);


    }
    else{
        IO->log_info("Villas_interface::send_message: Cannot use send() for villas node"
                     " because this instance of Villas_interface is used without node");
    }

    //IO->log_info("Villas_interface::send_message: Finished!");
#endif

}

void Villas_interface::allocate_and_receive(int number_of_messages, std::list<Villas_message> &messages) {
#ifdef WITH_VILLAS
    if(number_of_messages > 0) {
        int allocated;
        int release_number_of_messages = number_of_messages;
        struct sample *new_sample[number_of_messages];
        allocated = sample_alloc_many(p, new_sample, number_of_messages);
        int release = allocated;

        //IO->log_info("Allocated " + std::to_string(number_of_messages) + " samples");

        // As nanomsg has no built-in queue, it is emulated by reading the socket for multiple times and
        // seting the number_of_messages based on node_read()'s actual return values.
        if (std::string(type->name) == "nanomsg"){
            struct sample *temp_sample[1]; // Temporary sample
            int allocated_temp = sample_alloc_many(p, temp_sample, 1);

            int received = 0;
            unsigned int index = 0;
            
            for (int i = 0; i < number_of_messages; ++i){

                int recv = node_read(n, temp_sample, 1);
                if (recv > 0){
                    sample_copy(new_sample[index], temp_sample[0]);
                    received++;  
                    index++;
                    // IO->log_info("temp_sample->data = "+ std::to_string(temp_sample[0]->data[0].z.real()) + ","
                    //             + std::to_string(temp_sample[0]->data[0].z.imag()) + "]" + ",");
                }
            }
            if (received > 0){
                number_of_messages = received;
                IO->log_info("Villas_interface::receive_messages: node_read(...) completed, received "
                        + std::to_string(received) + " samples");
            }
            else { // Skip error output as nanomsg polling is based on reading empty socket
                number_of_messages = 0;
            }

            // Free allocated memory for temporary sample
            sample_decref_many(temp_sample, allocated_temp);
            sample_free_many(temp_sample, 1);
        } 
        else { 
            //receive sample
            int recv = node_read(n, new_sample, number_of_messages);
            IO->log_info("Villas_interface::receive_messages: node_read(...) completed, received "
                        + std::to_string(recv) + " samples");

            if (recv < 0) {
                IO->log_info("Villas_interface::receive_messages: Failed to receive samples,"
                            " reason=" + std::to_string(recv));
            }
        }
        for (int x = 0; x < number_of_messages; x++) {
            //print received samples
            std::string output;
            output +=
                    "Villas_interface::receive_messages: Received the following sample of length " +
                    std::to_string(new_sample[x]->length) + ":\t";
            Villas_message new_msg;
            for (unsigned int s = 0; s < new_sample[x]->length; s++) {
                Data_element new_data;
                if (meta[s].type == VILLAS_DATA_TYPE_INT64) {
                    new_data.i = new_sample[x]->data[s].i;
                    new_msg.add_element(new_data, VILLAS_DATA_TYPE_INT64);
                    output += std::to_string(new_sample[x]->data[s].i) + ",";
                }
                else if (meta[s].type == VILLAS_DATA_TYPE_DOUBLE) {
                    new_data.f = new_sample[x]->data[s].f;
                    new_msg.add_element(new_data, VILLAS_DATA_TYPE_DOUBLE);
                    output += std::to_string(new_sample[x]->data[s].f) + ",";
                }
                else if (meta[s].type == VILLAS_DATA_TYPE_BOOLEAN) {
                    new_data.b = new_sample[x]->data[s].b;
                    new_msg.add_element(new_data, VILLAS_DATA_TYPE_BOOLEAN);
                    output += std::to_string(new_sample[x]->data[s].b) + ",";
                }
                else if (meta[s].type == VILLAS_DATA_TYPE_COMPLEX) {
                    new_data.z = new_sample[x]->data[s].z;
                    new_msg.add_element(new_data, VILLAS_DATA_TYPE_COMPLEX);
                    output += "[" + std::to_string(new_sample[x]->data[s].z.real()) + ","
                                + std::to_string(new_sample[x]->data[s].z.imag()) + "]" + ",";
                }

            }
            IO->log_info(output);

            //set time
            new_msg.time_sec = (double) new_sample[x]->ts.origin.tv_sec +
                            ((double) new_sample[x]->ts.origin.tv_nsec / 1000000000.0);

            messages.push_back(new_msg);
        }

        //IO->log_info("sample_put_many");
        sample_decref_many(new_sample, release);
        sample_free_many(new_sample, release_number_of_messages);
    }
#endif
}

/*! \brief Receive a message via a Villas_interface
 *
 * */
void Villas_interface::receive_messages(std::list<Villas_message> &messages) {
#ifdef WITH_VILLAS
    if(with_node) {
        
        //receive all messages which are in incoming queue of villas node
        messages.clear();

        int number_of_messages;
        // Check which villas node type is used and create corresponding struct
        if(std::string(type->name) == "mqtt") {
            auto * m = (struct mqtt*) n->_vd;

            //determine number of messages in the queue
            unsigned long queue_tail = m->queue.queue.tail;
            unsigned long queue_head = m->queue.queue.head;
            number_of_messages =  (int) (queue_tail - queue_head);
            //IO->log_info("Villas_interface::receive_messages: " + std::to_string(number_of_messages)
            //            + " samples are in the queue");

            // call allocate and receive for number_of_messages samples
	        if(number_of_messages > 0){
                allocate_and_receive(number_of_messages, messages);
	        }
        }
        else if(std::string(type->name) == "nanomsg") {
            allocate_and_receive(1000, messages); // 128kB Memory / 8 Byte per message = 16000 messages
        }
    } 
    else { 
        IO->log_info("Villas_interface::receive_messages: Cannot use receive() for villas node"
                     " because this instance of Villas_interface is used without node");
    }
        

    //IO->log_info("Villas_interface::receive_messages: Finished receiving of "
    //              + std::to_string(messages.size()) + " messages.");
#endif
}

/*! \brief Method to start a node type; should only be called once per process
 *
 * */
int Villas_interface::start_node_type() {
#ifdef WITH_VILLAS
    int ret;

    //init memory subsystem in each process
    ret = memory_init(0);
    if(ret){
        IO->log_info("Villas_interface::start_node_type: memory_init failed for type " + std::string(type->name));
    }

    //start node type
    IO->log_info("Villas_interface::start_node_type: Starting villas node type " + std::string(type->name));
    ret = node_type_start(type, nullptr);
    if(ret){
        IO->log_info("Villas_interface::start_node_type: node_type_start failed for type " + std::string(type->name));
    }

    return ret;
#else
    return -1;
#endif
}


/*! \brief Method to stop a node type; should only be called once per process
 *
 * */
int Villas_interface::stop_node_type() {
#ifdef WITH_VILLAS
    int ret;
    //stop node type
    IO->log_info("Villas_interface::stop_node_type: Stopping villas node type " + std::string(type->name));
    ret = node_type_stop(type);
    if(ret){
        IO->log_info("Villas_interface::stop_node_type: node_type_stop failed for type " + std::string(type->name));
    }

    return ret;
#else
    return -1;
#endif
}

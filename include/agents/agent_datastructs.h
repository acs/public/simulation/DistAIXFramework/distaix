/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DATASTRUCTS
#define DATASTRUCTS

#include "model/config.h"
#include "model/model_creator.h"
#include "behavior/agent_message.h"
#include <list>


/*This file contains all data struct definitions for models used in the simulation*/

struct Prosumer_data{
    int type;
    std::string subtype;
    double S_r = 0.0;
    double pf_min = 0.8;
    double P_nom;
    int profile1_id;
    int profile2_id;
    double profile_scale;
    double profile_scale_ww;

    double C_el;
    double SOC_el;
    double E_el;

    double C_th;
    double SOC_th;
    double E_th;
    double f_el;


    double Q_r;
    double N;

    double P_gen;
    double P_dem;
    double Q_dem;
    double P_th_dem;

    bool connected = false; //based on profile
    double t_connected = 0.0;  /*!<time until EV gets disconnected from grid*/

    double f_el_sec;
    double P_sec;
    double sec_heater_P_gen_el = 0.0;

    double v_re = 0.0;
    double v_im = 0.0;
    double i_re = 0.0;
    double i_im = 0.0;

    double P_max = 0.0;
    double P_optimal = 0.0;
    double P_min = 0.0;
    double Q_max = 0.0;
    double Q_optimal = 0.0;
    double Q_min = 0.0;
    double P_ctrl = 0.0;
    double Q_ctrl = 0.0;
    double n_ctrl = 0.0;
    double P = 0.0;
    double Q = 0.0;
    double pf = 0.0;

    double v_setpoint = 1.0;
    double v_meas = 1.0;

    double Vnom = 400.0; /*!< Nominal voltage level*/

    unsigned int msg_received = 0;
    unsigned int msg_sent = 0;
    unsigned int number_of_solves = 0;

    unsigned int swarm_size_p = 0;
    unsigned int swarm_size_q = 0;

    int node_id=0;
    bool ict_connected = true;
};


struct Substation_data {
    int N = 2; //number of switch states
    double range = 5;  // range of adjustable ratio [%]
    int n_ctrl = 0; //control value for switching state [0,N]
    int n = 0;       // current switching state

    double Vnom1 = 20000.0;
    double Vnom2 = 400.0;

    double P_loss = 0.0;
    double Q_loss = 0.0;

    double v1_setpoint = 1.0;
    double v2_setpoint = 1.0;

    double v_meas = 1.0;
    double v_min = 1.0;
    double v_mean = 1.0;
    double v_median = 1.0;
    double v_max = 1.0;

    unsigned int msg_received = 0;
    unsigned int msg_sent = 0;
    unsigned int number_of_solves = 0;

    unsigned int swarm_size_p = 0;
    unsigned int swarm_size_q = 0;

    int node_id=0;

    double i1_im = 0.0; //input, current upper side
    double i1_re = 0.0; //input, current upper side
    double v1_im = 0.0; //input, voltage upper side
    double v1_re = 0.0; //input, voltage upper side
    double i2_im = 0.0; //output, current lower side
    double i2_re = 0.0; //output, current lower side
    double v2_im = 0.0; //output, voltage lower side
    double v2_re = 230.0;   //output, voltage lower side

    double R = 0;
    double Z = 0;
    double X = 0;

    double S_r=0.0;

    double ratio = 0;

    double t_last_action = 0;
    bool ict_connected = true;
};

struct Secondary_heater_data{
    bool enabled = false;
    int type = -1;
    double P_nom = 0.0;
	double f_el = 0.0;

    double P_th_dem = 0;
    double P_th = 0;
    double P_gen_el = 0;

    double v_re = 0;
    double v_im = 0;
    double i_im = 0;
    double i_re = 0;
};


struct PiLine_data{
    double I1 = 0.0;        /*!< current measurement: Pin 1*/
    double I2 = 0.0;        /*!< current measurement: Pin 2*/
    double i1_re = 0.0;     /*!< Input value: real part of current i1 [A] */
    double i1_im = 0.0;     /*!< Input value: imaginary part of current i1 [A] */
    double i2_re = 0.0;     /*!< Output value: real part of current i2 [A] */
    double i2_im = 0.0;     /*!< Output value: imaginary part of current i2 [A] */

    double V1 = 0.0;        /*!< voltage measurement: Pin 1 */
    double V2 = 0.0;        /*!< voltage measurement: Pin 2 */
    double v1_im = 0.0;     /*!< Input value: real part of voltage v [V] */
    double v1_re = 0.0;     /*!< Input value: imaginary part of voltage v [V] */
    double v2_im = 0.0;     /*!< Output value: real part of voltage v [V] */
    double v2_re = 0.0;     /*!< Output value: imaginary part of voltage v [V] */

    double B = 0.0;
    double C = 0.0;
    double G = 0.0;
    double L = 0.0;
    double R = 0.00412;
    double X = 0.0016;

    double S1 = 0.0;
    double S2 = 0.0;
    double Slosses = 0.0;
    double Srel = 0.0;
    double Srel_emergency = 0.0;

    double system_fnom = 50.0;
    double system_omega = system_fnom*2*3.14159;

    double length = 0.02;   //parameter
    double Sr = 1.0;    //parameter
    double Sr_emergency = 1.0;
};


/*! \brief Data contained in a Slack*/
struct Slack_data{
    double i_im = 0.0;  //input
    double i_re = 0.0;  //input

    double v_im = 0.0;  //output
    double v_re = 0.0;  //output

    double Vnom = 20000.0;  //parameter
    double phiV = 0.0;  //parameter

    double P = 0.0; //calculated
    double Q = 0.0; //calculated

    double P_min = -1000000000;
    double P_optimal = 0;
    double P_max = 1000000000;
    double Q_min = -1000000000;
    double Q_optimal = 0;
    double Q_max = 1000000000;

    bool Q_target = false;
    bool P_target = false;

    double Q_corr = 0.0;
    double P_corr = 0.0;

    unsigned int msg_received = 0;
    unsigned int msg_sent = 0;
    unsigned int number_of_iterations = 0;

    std::string IP_address_port = "133.444.555.666:5000";
    bool ict_connected = true;
};


/*! \brief Data contained in a Node*/
struct Node_data{
    double i_im = 0.0;
    double i_re = 0.0;

    double v_im = 0.0;
    double v_re = 0.0;

    double v_norm = 1.0;

    double Vnom = 400.0; //!< nominal voltage of the node

    int transformer_id = -1; //ID of trafo under which this node is located

    unsigned int number_of_iterations = 0; //<! counts the number of iterations until convergence of a grid calculation
    bool ict_connected = true;
};

struct ElVertex_s{
    int vID;
    int vtype;
    int trafo_id;
};

struct DF_data{
    unsigned int msg_received = 0;
    unsigned int msg_sent = 0;
    std::vector<std::list<std::tuple<int,int,int>>> components_at_nodes; /*!< first: id of component, second: type of component, third: trafo id*/
    std::vector<bool> ict_connectivity_of_nodes; //<! saves ICT connectivity of nodes
    Model_creator * mc; //<! Used to determine hop distance between agents
};

struct MR_data{
    unsigned int msg_transmitted = 0;
    unsigned int msg_new = 0;
    unsigned int msg_pending = 0;
    unsigned int msg_to_other_MR = 0;
    unsigned int msg_dropped = 0;
    int number_of_nodes = 0;
    std::vector<Agent_message> messages_for_db;

    bool use_commdata;
    double *latency_matrix;
    double *per_matrix;
    //std::map<int, int> agents_to_nodes_map;
    std::vector<std::list<std::tuple<int,int,int>>> components_at_nodes; /*!< first: id of component, second: type of component, third: trafo id*/
    unsigned int *from_to_matrix;
    int *from_to_matrix_max;
    int *from_to_matrix_step;

    unsigned int *from_to_matrix_extra1;
    int *from_to_matrix_max_extra1;
    int *from_to_matrix_step_extra1;

    unsigned int *from_to_matrix_extra2;
    int *from_to_matrix_max_extra2;
    int *from_to_matrix_step_extra2;

    unsigned int *from_to_DF;
    int *from_to_DF_max;
    int *from_to_DF_step;

    int number_of_steps;
};

#endif //DATASTRUCTS

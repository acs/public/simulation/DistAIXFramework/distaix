/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef AGENT_IO_H
#define AGENT_IO_H

#include "model/config.h"
#include "model/io_object.h"
#include "model/time_measurement.h"
#include <repast_hpc/AgentId.h>
#include <fstream>

class AgentIO: public IO_object{
public:
    AgentIO(repast::AgentId &_id, int &_behavior_type, struct data_props _d_props);
    ~AgentIO() override = default;

    void init_logging();
    void save_meta(void *_model_data);
    void save_result(void *_model_data, bool first_step, Time_measurement* tm_csv, Time_measurement* tm_db, Time_measurement* tm_db_serialize, Time_measurement* tm_db_add);

private:
    repast::AgentId   id; //!< RepastHPC Agent ID [id, start rank, type, current rank]
    int behavior_type; //!< Behavior of the agents, 0 for reference behavior (non-intelligent), 1 for swarm behavior (intelligent)

    void set_result_file_header() override; /*This method sets the header of the csv log file based on the agent type*/
    void set_result_types(); //!< This method adds all result types of an agent to the database depending on the agent type
    void save_result_csv_prosumer(void *_model_data); //!< Write prosumer data to csv result stream
    void save_result_csv(void *_model_data); //!< This method saves results in the csv result file
    void save_result_db(void *_model_data, bool first_step, Time_measurement* tm_db_serialize, Time_measurement* tm_db_add); //!< This method saves results in the database
};

#endif //AGENT_IO_H

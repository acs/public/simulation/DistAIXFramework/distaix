/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DIRECTORYFACILITATOR_AGENT
#define DIRECTORYFACILITATOR_AGENT


#include "agents/agent.h"
#include "model/model_creator.h"

/*! \brief Class for Directory Facilitator agents
 *
 * This is a class representing directory facilitators.
 * */
class Directoryfacilitator_agent : public Agent{
public:
    Directoryfacilitator_agent(repast::AgentId &id, bool is_local);
    Directoryfacilitator_agent(repast::AgentId &id,
            bool is_local,
            double &step_size,
            int &_behavior_type,
            int &_model_type,
            std::string &_subtype,
            struct data_props _d_props,
            villas_node_config *_villas_config
            );
    ~Directoryfacilitator_agent() override;

    void set_model_creator(Model_creator * _mc);
    void set_components_at_nodes(std::vector<std::list<std::tuple<int,int,int>>> &_components_at_nodes);
    void set_ict_connectivity_of_nodes(std::vector<bool> &_ict_connectivity_of_nodes);

    void step() override;


private:
    DF_data model_data; //!< Model data specific for the directory facilitator model
};

#endif //DIRECTORYFACILITATOR_AGENT

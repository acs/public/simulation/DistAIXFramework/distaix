/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef AGENT_PACKAGE
#define AGENT_PACKAGE

#include <boost/serialization/complex.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/list.hpp>
#include "agents/agent.h"

/*! \brief Serializable Agent Package */
struct AgentPackage {

public:
    int    id; //!< ID of the agent contained in the package
    int    rank; //!< rank of the agent contained in the package
    int    type; //!< type of the agent contained in the package
    int    currentRank; //!< current rank of the agent contained in the package
    std::complex<double> current; //!< current of the agent contained in the package
    std::complex<double> voltage; //!< voltage of the agent contained in the package
    int next_action_expected; //!< next action expected in FBS algorithm of the agent contained in the package
    bool convergence; //!< convergence state for the FBS algorithm of the agent contained in the package
    std::list<Agent_message> msg_queue; //!< outgoing mesage queue of the Message Router agent contained in the package


    /* Constructors */
    AgentPackage(); // For serialization
    AgentPackage(int _id, int _rank, int _type, int _currentRank, std::complex<double> _current, std::complex<double> _voltage,  int _next_action_expected, bool _convergence);
    AgentPackage(int _id, int _rank, int _type, int _currentRank, std::list<Agent_message> _msg_queue);

    /*! \brief Seralization of data required for archive packaging (overwriting based on template class Archive)
     * \param ar [in,out] Reference to archive
     * \param version [in] parameter required for compatibility with boost::serialization library
     * */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & id;
        ar & rank;
        ar & type;
        ar & currentRank;
        if(type == TYPE_MR_INT) {
            ar & msg_queue;
        }
        else{
            ar & current;
            ar & voltage;
            ar & next_action_expected;
            ar & convergence;
        }

    }

};


/*! \brief Agent Package Provider */
class AgentPackageProvider {

private:
    repast::SharedContext<Agent>* agents; //!< RepastHPC Shared Context containing the agents of this process

public:

    AgentPackageProvider(repast::SharedContext<Agent>* agentPtr);

    void providePackage(Agent * agent, std::vector<AgentPackage>& out);

    void provideContent(repast::AgentRequest req, std::vector<AgentPackage>& out);

};



/*! \brief Agent Package Receiver */
class AgentPackageReceiver {

private:
    repast::SharedContext<Agent>* agents; //!< RepastHPC Shared Context containing the agents of this process

public:

    AgentPackageReceiver(repast::SharedContext<Agent>* agentPtr);

    Agent * createAgent(AgentPackage package);

    void updateAgent(AgentPackage package);

};


#endif //AGENT_PACKAGE

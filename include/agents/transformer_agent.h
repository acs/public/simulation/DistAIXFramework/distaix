/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef TRANSFORMER_AGENT_H
#define TRANSFORMER_AGENT_H

#include "agents/agent.h"
#include "component/transformer.h"

/*! \brief Class for Transformer agents
 *
 * This is a class representing transformer agents.
 * */
class Transformer_agent : public Agent {


public:
    Transformer_agent(repast::AgentId &id, bool is_local);

    Transformer_agent(repast::AgentId &id,
            bool is_local,
            double &step_size,
            int &node_id,
            int &_behavior_type,
            int &_model_type,
            std::string &_subtype,
            struct data_props _d_props,
            villas_node_config *_villas_config,
            double _Vnom1,
            double _Vnom2,
            double R,
            double X,
            double _Sr,
            int _N,
            int _range,
            bool _ict_connected
            );

    ~Transformer_agent() override;

    void step() override;

    void calculate() override;

    bool do_backward_sweep(
            repast::SharedNetwork <Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent>> *agent_network_elec) override;

    uint do_forward_sweep(
            repast::SharedNetwork <Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent>> *agent_network_elec) override;

private:
    Substation_data substation_data; //!< Model data specific for all substations
    Transformer* transformer_model;
};

#endif //TRANSFORMER_AGENT_H

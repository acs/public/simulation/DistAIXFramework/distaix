/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef PROSUMER_AGENT
#define PROSUMER_AGENT

#include "agents/agent.h"
#include "component/prosumer.h"

/*! \brief Class for Prosumer agents
 * */
class Prosumer_agent : public Agent {
public:
    Prosumer_agent(repast::AgentId &id, bool is_local);
    Prosumer_agent(repast::AgentId &id,
            bool is_local,
            double &step_size,
            int &_behavior_type,
            int &_model_type,
            Profile* profiles,
            std::string &interpolation_type,
            struct data_props _d_props,
            villas_node_config *_villas_config,
            Prosumer_data * _prosumer_data
            );
    ~Prosumer_agent() override;

    void step() override;
    void calculate() override;

private:
    Prosumer_data* prosumer_data; //!< Model data specific for all prosumers
    Prosumer *prosumer_model;

    void update_input_data() override;
};

#endif //PROSUMER_AGENT

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef NODE_AGENT
#define NODE_AGENT

#include "agents/agent.h"

/*! \brief Class for Node agents
 *
 * This is a class representing node agents.
 * */
class Node_agent : public Agent {

public:
    Node_agent(repast::AgentId &id, bool is_local);
    Node_agent(repast::AgentId &id,
            bool is_local,
            double &step_size,
            double _Vnom,
            int &_behavior_type,
            int &_model_type,
            std::string &_subtype,
            struct data_props _d_props,
            villas_node_config *_villas_config,
            int _ifht_id,
            bool _ict_connected
            );
    ~Node_agent() override;


    void step() override;

    bool do_backward_sweep(repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) override;
    uint do_forward_sweep(repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) override;


private:
    Node_data model_data; //!< Model data specific for a node
    int ifht_id; //!< ID of IFHT data
};

#endif //NODE_AGENT

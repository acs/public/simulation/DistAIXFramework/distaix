/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef AGENT
#define AGENT

#include <boost/filesystem.hpp>
#include <boost/multi_array.hpp>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedNetwork.h"
#include <fstream>
#include "model/config.h"
#include "edges/edge-content-manager.h"
#include "behavior/agent_message.h"
#include "agents/agent_datastructs.h"
#include "agents/agent_io.h"
#include "behavior/agent_behavior.h"

#include "profile.h"


/*! \brief Top-level agent class
 *
 * This class contains all members and methods that are common to all agents
*/
class Agent{

	
private:

protected:
    Agent_behavior *behavior; //!< Object of Agent_behavior class; defines the behavior of the agent

    // time measurements for result saving procedure (pointers to time measurements of model class)
    Time_measurement * tm_csv;
    Time_measurement * tm_db;
    Time_measurement * tm_db_serialize;
    Time_measurement * tm_db_add;

public:
    /*Variables that describe properties of the agent*/
    repast::AgentId   id_;          //!< RepastHPC Agent ID [id, start rank, type, current rank]
    bool is_local_instance;         //!< True if the agent is a local agent,
                                    //!< false if it is a non-local agent
    double t_start;                 //!<  Starting time of the simulation in sec, first step to simulate
    double t_step;                  //!< Step size of one simulation step in seconds
    double t_next;                  //!< Next simulation time step to simulate in seconds
    int behavior_type;              //!< Behavior type of agents (0 = reference behavior, 1 = swarm intelligence
    int model_type;                 //!< 0 for steady state, 1 for dynamic phasor
    std::string subtype;            //!< Subtype of the agent
    bool first_step;                //!< marks the first run of step, so constant values could be added properly

    /*Variables related to IO*/
    AgentIO *IO;                    //!< Object of AgentIO class used to handle all IO related methods

    /*Variables used in forward-backward sweep algorithm*/
    std::complex<double> current;         //!< current flowing out of previous node and into line segment
    std::complex<double> voltage;         //!< potential (voltage) of this agent
    std::complex<double> voltage_prev;    //!< potential (voltage) from previous iteration in
                                          //!< forward-backward sweep; used to determine convergence
    int next_action_expected;             //!< 1 if a forward iteration is expected next,
                                          //!< 0 if a backward iteration is expected next
                                          //!< and, 2 if a convergence check is expected next
    bool convergence;                     //!< Boolean value to indicate convergence of FBS method
    std::vector<Agent*> next_nodes_elec;  //!< vector saving the electrically next nodes (required in FBS)
    std::vector<Agent*> prev_nodes_elec;  //!< vector saving the electrically previous nodes (required in FBS)
    std::vector<Agent*> components;       //!< vector saving the electrically connected componentes (required in FBS)

    /*Variables related to messaging*/
    std::list<Agent_message> outgoing_messages;     //!< list of outgoing messages
    std::list<Agent_message> incoming_messages;     //!< queue of incoming messages
    unsigned int msg_received;                      //!< counting the number of received messages
    unsigned int msg_sent;                          //!< counting the number of sent messages

    Agent(repast::AgentId &id, bool is_local);
    Agent(repast::AgentId &id,
            bool is_local,
            double &step_size,
            int &_behavior_type,
            int &_model_type,
            std::string &_subtype,
            struct data_props _d_props);
    //constructors for agent packages
    Agent(repast::AgentId &id, bool is_local, std::list<Agent_message> _msg_queue);
    Agent(repast::AgentId &id, bool is_local, std::complex<double> _current,
          std::complex<double> _voltage, int _next_action_expected, bool _convergence);
	
    virtual ~Agent();
	
    /* Setter required to update agent copies*/
    void set(int currentRank, std::complex<double> _current, std::complex<double> _voltage,
             int _next_action_expected, bool _convergence);
    void set(int currentRank, std::list<Agent_message> _msg_queue);

    /* Methods to return the RepastHPC agent ID */
    virtual repast::AgentId& getId(){                   return id_;    }
    virtual const repast::AgentId& getId() const {      return id_;    }

    /*Methods used during initialization*/
    virtual void save_adjacent_agents(repast::SharedNetwork<Agent, Edge<Agent>,
            Edge_content<Agent>, Edge_content_manager<Agent> >* agent_network_elec);
    int init_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                       std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                       boost::multi_array<int, 2> *components_file,
                       boost::multi_array<std::string, 1> *subtypes_file,
                       boost::multi_array<int, 2> *el_grid_file);/*initialization of behavior*/

    /*Methods used during execution of forward backward sweep algorithm*/
    std::complex<double> get_current();
    std::complex<double> get_voltage();
    int get_next_action_expected();
    bool get_convergence();
    virtual bool do_backward_sweep(repast::SharedNetwork<Agent, Edge<Agent>,
            Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec);
    virtual uint do_forward_sweep(repast::SharedNetwork<Agent, Edge<Agent>,
            Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec);
    virtual void calculate(); // virtual function, to be re-implemented in all derived agent classes

    /*Methods related to messaging and execution of agent intelligence*/
    virtual void process_incoming_messages();
    virtual void update_input_data();
    void receive_message(Agent_message &msg);
    Agent_message get_next_outgoing_message();
    bool outgoing_is_empty();
    std::list<Agent_message> get_outgoing_queue();

    /*Method to disconnect villas node*/
    int villas_interface_disconnect();

    /* Method to advance a simulation step */
    virtual void step(); // virtual function, to be re-implemented in all derived agent classes

    /*Method to set pointers to time measurements*/
    void set_tm(Time_measurement *_tm_csv, Time_measurement * _tm_db, Time_measurement * tm_db_serialize, Time_measurement * tm_db_add);

    //Exit function for agent
    void do_exit(int code);
	
};

#endif

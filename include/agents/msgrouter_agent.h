/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef MSGROUTER_AGENT
#define MSGROUTER_AGENT

#include "agents/agent.h"
#include "boost/random.hpp"
#include "boost/generator_iterator.hpp"


/*! \brief Class for Message Router agents
 *
 * This is a class representing message routers.
 * */
class Messagerouter_agent : public Agent{
public:
    Messagerouter_agent(repast::AgentId &id, bool is_local);
    Messagerouter_agent(repast::AgentId &id,
            bool is_local,
            double &step_size,
            int &_behavior_type,
            int &_model_type,
            std::string &_subtype,
            struct data_props _d_props,
            villas_node_config *_villas_config,
            bool &_use_commdata
            );
    ~Messagerouter_agent () override;

    void route_outgoing_agent_messages_own(repast::SharedNetwork<Agent, Edge<Agent>,
            Edge_content<Agent>, Edge_content_manager<Agent> >* agent_network_comm);
    void route_outgoing_agent_messages_other(repast::SharedNetwork<Agent, Edge<Agent>,
            Edge_content<Agent>, Edge_content_manager<Agent> >* agent_network_comm);

    void set_comm_data(double *_latency_matrix, double *_per_matrix);
    void set_latency(double *_latency_matrix);
    void init_count_matrix(int _number_of_nodes);
    void set_components_at_nodes(std::vector<std::list<std::tuple<int,int,int>>> &_components_at_nodes);
    void set_agent_rank_relation(int * _agent_rank_relation);

    void step() override;

private:
    std::list<Agent_message> pending_messages; //!< Queue of pending agent messages
    boost::mt19937 *mt_gen; //!< For production of pseudo randomness in package drops
    boost::uniform_int<> *drop_probability; //!< For production of pseudo randomness in package drops
    boost::variate_generator<boost::mt19937, boost::uniform_int<> >
            *drop_probability_generator; //!< For production of pseudo randomness in package drops

    MR_data model_data; //!< Model data specific for message router

    void send_message(Agent_message msg);
    void add_to_pending(Agent_message msg);
    bool message_dropped(Agent_message &msg);
    double message_delay(Agent_message &msg);
    void process_pending_messages(repast::SharedNetwork<Agent, Edge<Agent>,
            Edge_content<Agent>, Edge_content_manager<Agent> >* agent_network_comm);

    int get_node(int _component_id);
    void count_msg(Agent_message &msg);
    void log_comm_matrices();


};

#endif //MSGROUTER_AGENT

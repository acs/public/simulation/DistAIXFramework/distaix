/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef SLACK_AGENT_H
#define SLACK_AGENT_H

#include "agents/agent.h"

/*! \brief Class for Slack agents
 *
 * This is a class representing slack agents.
 * */
class Slack_agent : public Agent {

public:
    Slack_agent(repast::AgentId &id, bool is_local);
    Slack_agent(repast::AgentId &id,
            bool is_local,
            double &step_size,
            double _Vnom,
            int &_behavior_type,
            int &_model_type,
            std::string &_subtype,
            struct data_props _d_props,
            villas_node_config *_villas_config,
            bool _ict_connected,
            bool _realtime = false
            );
    Slack_agent(repast::AgentId &id,
            bool is_local,
            double &step_size,
            double _Vnom,
            double _Vre,
            double _Vim,
            int &_behavior_type,
            int &_model_type,
            std::string &_subtype,
            struct data_props _d_props,
            villas_node_config *_villas_config,
            bool _ict_connected,
            bool _realtime = false
            );
    ~Slack_agent() override;

    void step() override;
    void calculate() override;

    bool do_backward_sweep(repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) override;
    uint do_forward_sweep(repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>, Edge_content_manager<Agent> > *agent_network_elec) override;

private:
    Slack_data model_data; //!< Model data specific for slack
    Slack_data rollback_state; //!< Rollback information of the model; used in FBS

    bool get_new_vals; //!< Boolean needed for dpsim_cosim behavior
    bool realtime;      //!< Boolean needed for realtime cosimulation
    
    void solve();
    void solve_steady_state();
    void solve_dynamic_phasor();    
    void save_model_state();
    void restore_model_state();

};

#endif //SLACK_AGENT_H

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DPSIM_COSIM_MESSAGE
#define DPSIM_COSIM_MESSAGE

#include <complex>

#include "villas_interface/villas_message.h"



class Dpsim_cosim_msg : public Villas_message {
public:

    Dpsim_cosim_msg();
    Dpsim_cosim_msg(Villas_message &msg);
    ~Dpsim_cosim_msg() override = default;

    std::complex<float> * value;

    static std::vector<Meta_infos> dpsim_cosim_message_meta;
    static bool meta_initialized;

    void set_pointers();
    static void init_meta();

    std::string get_message_output();

private:
    void init_message(std::complex<float> _value);
};

#endif // DPSIM_COSIM_MESSAGE
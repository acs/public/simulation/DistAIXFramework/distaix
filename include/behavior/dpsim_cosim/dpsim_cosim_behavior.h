/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DPSIM_COSIM_BEHAVIOR
#define DPSIM_COSIM_BEHAVIOR

#include "behavior/agent_behavior.h"
#include "dpsim_cosim_message.h"

//forwad declarations
struct villas_node_config;

/*! \brief abstract base class for nanomsg test behavior
 * */
class Dpsim_cosim_behavior : public Agent_behavior {
public:
    Dpsim_cosim_behavior(int _id, int _type, int _rank, std::string _subtype, double& step_size, struct data_props _d_props,
        villas_node_config * _villas_config);
    virtual ~Dpsim_cosim_behavior() override;

protected:
    /* message processing */
    void process_incoming_messages();
    
    virtual void process_dpsim_cosim_msg(Dpsim_cosim_msg &msg) = 0;

    /* message sending functions */
    void send_villas_msg(Villas_message &msg);

    struct villas_node_config * vnconfig;

    /* endpoint creation */
    void create_endpoints(villas_node_config * _vnconfig, std::string agent_id);

    unsigned int numOfSentMessages = 0;
    unsigned int numOfReceivedMessages = 0;
};

#endif //DPSIM_COSIM_BEHAVIOR
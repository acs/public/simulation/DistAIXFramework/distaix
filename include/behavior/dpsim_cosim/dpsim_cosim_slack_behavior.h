/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DPSIM_COSIM_SLACK_BEHAVIOR
#define DPSIM_COSIM_SLACK_BEHAVIOR

#include "behavior/dpsim_cosim/dpsim_cosim_behavior.h"

/*! \brief Class for nanomsg test slack behavior
 * */
class Dpsim_cosim_slack_behavior : public Dpsim_cosim_behavior {
public:
    Dpsim_cosim_slack_behavior(int _id, int _type, int _rank, std::string _subtype, double& _step_size, struct data_props _d_props,
        villas_node_config * _villas_config, bool _realtime, bool _sync);
    ~Dpsim_cosim_slack_behavior()= default;

    int initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                   std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                   boost::multi_array<int, 2> *components_file,
                                   boost::multi_array<std::string, 1> *subtypes_file,
                                   boost::multi_array<int, 2> *el_grid_file) override;
    void execute_agent_behavior() override;
    void report_to_superior_grid(Dpsim_cosim_msg message, std::list<Villas_message> *incoming_villas_messages);
    

private:
    void receive_msg();
    void receive_message(std::list<Villas_message> *incoming_villas_messages);
    void synchronize_with_remote();
    void process_dpsim_cosim_msg(Dpsim_cosim_msg &msg) override;
   
    int busy_receive_msg(std::list<Villas_message> *incoming_villas_messages);
    void send_msg(Dpsim_cosim_msg message);

    int sender;
    double step_size;

    bool first_step = true;
    bool realtime;
    bool sync;

    int timeout = 5000; // timeout for busy waiting in milliseconds

};

#endif // DPSIM_COSIM_SLACK_BEHAVIOR
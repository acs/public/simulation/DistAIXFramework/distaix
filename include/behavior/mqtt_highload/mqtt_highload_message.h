/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef MQTT_HIGHLOAD_MESSAGE
#define MQTT_HIGHLOAD_MESSAGE

#include "villas_interface/villas_message.h"
/*! \brief Struct for storing mqtt message data
 * */

#define MQTT_HIGHLOAD_MESSAGE_LENGTH 10



class Mqtt_highload_msg : public Villas_message {
public:

    Mqtt_highload_msg();
    Mqtt_highload_msg(Villas_message &msg);
    Mqtt_highload_msg( int _performative, int _sender, int _receiver);
    ~Mqtt_highload_msg() override = default;

    //pointers to elements
    int64_t * performative;      //!< type of communicative act
    int64_t * sender;                     //!< identity of the sender
    int64_t * receiver;                   //!< identity of the receiver
    int64_t * measurement_type_1;           //!< type of measurement
    double * measurement_value_1;       //!< value of measurement
    int64_t * measurement_type_2;           //!< type of measurement
    double * measurement_value_2;       //!< value of measurement
    int64_t * measurement_type_3;           //!< type of measurement
    double * measurement_value_3;       //!< value of measurement

    static std::vector<Meta_infos> mqtt_highload_msg_meta;
    static bool meta_initialized;

    void set_pointers();
    static void init_meta();

    std::string get_message_output();

private:
    void init_message(int _performative, int _sender, int _receiver, 
    int _measurement_type_1, double _measurement_value_1, 
    int _measurement_type_2, double _measurement_value_2, 
    int _measurement_type_3, double _measurement_value_3);


};

#endif //MQTT_HIGHLOAD_MESSAGE
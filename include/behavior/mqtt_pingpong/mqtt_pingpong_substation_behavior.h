/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DISTAIX_MQTT_PINGPONG_SUBSTATION_BEHAVIOR_H
#define DISTAIX_MQTT_PINGPONG_SUBSTATION_BEHAVIOR_H

#include "behavior/mqtt_pingpong/mqtt_pingpong_behavior.h"

/*! \brief Class for MQTT pingpong substation behavior
 * */
class Mqtt_pingpong_substation_behavior : public Mqtt_pingpong_behavior {
public:
    Mqtt_pingpong_substation_behavior(int _id, int _type, int _rank, std::string _subtype, double& step_size, struct data_props _d_props,
                                 villas_node_config * _villas_config, Substation_data * _substation_data);
    ~Mqtt_pingpong_substation_behavior()= default;

    void execute_agent_behavior() override;

protected:
    void process_mqtt_pingpong_msg(Mqtt_pingpong_msg &msg) override;

private:
    void apply_control_values();
    std::vector<std::tuple<int, double, bool>> voltage_measurements;

    Substation_data * substation_data;  //!< pointer to equipment specific datastruct
};

#endif //DISTAIX_MQTT_PINGPONG_SUBSTATION_BEHAVIOR_H

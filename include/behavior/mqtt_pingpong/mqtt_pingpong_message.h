/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DISTAIX_MQTT_PINGPONG_MESSAGE_H
#define DISTAIX_MQTT_PINGPONG_MESSAGE_H


#include "villas_interface/villas_message.h"
/*! \brief Struct for storing ensure test data
 * */

#define MQTT_PINGPONG_MESSAGE_LENGTH 6



class Mqtt_pingpong_msg : public Villas_message {
public:

    Mqtt_pingpong_msg();

    Mqtt_pingpong_msg(Villas_message &msg);

    Mqtt_pingpong_msg(int _performative, int _agent_id);

    ~Mqtt_pingpong_msg() override = default;

    //pointers to elements
    int64_t *performative;      //!< type of communicative act
    int64_t *agent_id;                     //!< identity of the agent
    double *P_ctrl;           //!< setpoint for active power
    double *Q_ctrl;       //!< setpoint for reactive power
    int64_t *n_ctrl;        //!< set point for tap position
    double *v_meas;     //!< voltage measurement value

    static std::vector<Meta_infos> mqtt_pingpong_msg_meta;
    static bool meta_initialized;

    void set_pointers();

    static void init_meta();

    std::string get_message_output();

private:
    void init_message(int _performative, int _agent_id, double _P_ctrl,
                      double _Q_ctrl, int _n_ctrl, double _v_meas);
};
#endif //DISTAIX_MQTT_PINGPONG_MESSAGE_H

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DISTAIX_ENSURE_SLACK_BEHAVIOR_H
#define DISTAIX_ENSURE_SLACK_BEHAVIOR_H

#include "behavior/ensure/ensure_behavior.h"
#include "behavior/ensure/ensure_knowledge.h"

/*! \brief Class for ENSURE slack behavior
 * */
class Ensure_slack_behavior : public Ensure_behavior {
public:
    Ensure_slack_behavior(int _id, int _type, int _rank, std::string _subtype, double& step_size,
                    struct data_props _d_props, villas_node_config * _villas_config, Slack_data * _slack_data);
    ~Ensure_slack_behavior()=default;

    void execute_agent_behavior() override;

protected:
    void process_ensure_msg(Ensure_msg &msg) override;

private:
    void send_measurement_message();
    Slack_data * slack_data;  //!< pointer to slack specific datastruct
    Ensure_knowledge knowledge;
};


#endif //DISTAIX_ENSURE_SLACK_BEHAVIOR_H

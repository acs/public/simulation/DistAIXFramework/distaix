/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DISTAIX_ENSURE_MESSAGE_H
#define DISTAIX_ENSURE_MESSAGE_H


#include "villas_interface/villas_message.h"
/*! \brief Struct for storing ensure test data
 * */

#define ENSURE_MESSAGE_LENGTH 6



class Ensure_msg : public Villas_message {
public:

    Ensure_msg();

    Ensure_msg(Villas_message &msg);

    Ensure_msg(int _performative, int _agent_id);

    ~Ensure_msg() override = default;

    //pointers to elements
    int64_t *performative;      //!< type of communicative act
    int64_t *agent_id;                     //!< identity of the agent
    double *P;           //!< setpoint for active power
    double *Q;       //!< setpoint for reactive power
    int64_t *n;        //!< set point for tap position
    double *v;     //!< voltage measurement value
    double *v2;     //!< second voltage measurement (for transformer)
    double *SOC;     //!< SOC measurement
    bool *connected; //!< connection status
    int64_t *t_connected; //!< time until disconnection in seconds

    static std::vector<Meta_infos> ensure_msg_meta;
    static bool meta_initialized;

    void set_pointers();

    static void init_meta();

    std::string get_message_output();

private:
    void init_message(int _performative, int _agent_id, double _P,
                      double _Q, int _n, double _v, double _v2, double _SOC, bool _conn, int _t_conn);
};
#endif //DISTAIX_ENSURE_MESSAGE_H

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DISTAIX_ENSURE_BEHAVIOR_H
#define DISTAIX_ENSURE_BEHAVIOR_H

#include "behavior/agent_behavior.h"
#include "behavior/ensure/ensure_message.h"

//forward declarations
struct villas_node_config;

/*! \brief abstract base class for reference ENSURE behavior
 * */
class Ensure_behavior : public Agent_behavior {
public:
    Ensure_behavior(int _id, int _type, int _rank, std::string _subtype, double& step_size, struct data_props _d_props,
                      villas_node_config * _villas_config);
    virtual ~Ensure_behavior() override;

protected:
    int initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                   std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                   boost::multi_array<int, 2> *components_file,
                                   boost::multi_array<std::string, 1> *subtypes_file,
                                   boost::multi_array<int, 2> *el_grid_file) override;

    /* message processing */
    void handshake();
    void process_incoming_messages();
    virtual void process_ensure_msg(Ensure_msg &msg) = 0;

    /* message sending functions */
    void send_villas_msg(Villas_message &msg);

    struct villas_node_config * vnconfig;
};

#endif //DISTAIX_ENSURE_BEHAVIOR_H

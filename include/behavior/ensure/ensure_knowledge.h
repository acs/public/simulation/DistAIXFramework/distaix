/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef ENSURE_KNOWLEDGE
#define ENSURE_KNOWLEDGE

#include "behavior/behavior_io.h"

/*! \brief Class representing knowledge of an agent 
 *
 * This class stores measurements performed by an agent and provides functions to extract desired
 * information
 * */
class Ensure_knowledge
{
private:
    int id;                     //!< ID of agent
    BehaviorIO *IO;             //!< output module for logging
    double SOC_meas_last;       //!< last transmitted SOC measurement
    double P_meas_last;         //!< last transmitted P measurement
    double Q_meas_last;         //!< last transmitted Q measurement
    double Pth_meas_last;       //!< last transmitted P_th measurement
    bool connected_last;        //!< last transmitted connection status
    double t_connected_last;    //!< last transmitted connection time
    double v_meas_last;         //!< last transmitted voltage measurement
    double v2_meas_last;         //!< last transmitted voltage measurement
    double t_last_sent;         //!< last time a message with measurements has been sent
    double P_ctrl_last;         //!< last received P set point
    double Q_ctrl_last;         //!< last received Q set point
    int n_ctrl_last;            //!< last received n set point
    double t_last_recv;         //!< last time a message with set point has been reveived

public:
    Ensure_knowledge();
    ~Ensure_knowledge() = default;

    void init(int &_id, BehaviorIO* _io);

    void set_SOC_meas(double soc);
    void set_PQ_meas(double p, double q);
    void set_Pth_meas(double pth);
    void set_conn(bool conn);
    void set_t_conn(double t_conn);
    void set_v_meas(double v_meas);
    void set_v2_meas(double v_meas);
    void set_PQ_ctrl(double p, double q);
    void set_n_ctrl(int n);
    void set_t_sent(double t);
    void set_t_recv(double t);

    double get_SOC_meas();
    void get_PQ_meas(double &p, double &q);
    double get_Pth_meas();
    bool get_conn();
    double get_t_conn();
    double get_v_meas();
    double get_v2_meas();
    void get_PQ_ctrl(double &p, double &q);
    int get_n_ctrl();
    double get_t_sent();
    double get_t_recv();

    void log_sensor_knowledge();
};

#endif // ENSURE_KNOWLEDGE

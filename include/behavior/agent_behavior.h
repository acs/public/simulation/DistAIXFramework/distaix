/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef AGENT_BEHAVIOR
#define AGENT_BEHAVIOR

#include "model/config.h"
#include "behavior/behavior_io.h"
#include "agents/agent_datastructs.h"
#include "behavior/agent_message.h"
#include "villas_interface/villas_message.h"



//forward declaration
struct villas_node_config;
class Villas_interface;

#undef V //added because otherwise model_data.V cannot be accessed (V is defined in villas/utils.h)

/*! \brief Abstract base class for agent behavior
 *
 * The class represents the behavior of an agent. In every time step function execute_agent_behavior
 * is executed. Therefore, the user has to implement it in child classes. Other public functions are
 * required as interface for the model class for initialization and message handling.
 * */
class Agent_behavior {

public:
    Agent_behavior(int _id, int _type, std::string _subtype, double& step_size, struct data_props _d_props);
    virtual ~Agent_behavior();

    virtual int initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                           std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                           boost::multi_array<int, 2> *components_file,
                                           boost::multi_array<std::string, 1> *subtypes_file,
                                           boost::multi_array<int, 2> *el_grid_file) = 0;
    virtual void execute_agent_behavior() = 0;

    void update_t(double _t_next);

    /* interaction with message router */
    void receive_message(Agent_message &msg);
    Agent_message get_next_outgoing_message();
    bool outgoing_is_empty();
    std::list<Agent_message> get_outgoing_queue();
    void clear_outgoing();
    unsigned int get_msg_received();
    unsigned int get_msg_sent();

    /*Methods related to villas interface*/
    int init_villas_interface(villas_node_config *_villas_config, std::vector<Meta_infos> &meta);
    int destroy_villas_interface();

protected:
    /* message processing */
    void send_message(Agent_message &msg);
    
    int id;                                     //!< Agent ID
    int type;                                   //!< agent type
    std::string subtype;                        //!< agent subtype
    BehaviorIO *IO;
    double t_start;                             //!< Starting time of the simulation in sec,
                                                // first step to simulate
    double t_next;                              //!< Next simulation time step to simulate in seconds
    double t_step;                              //!< Step size of one simulation step in seconds
    std::list<Agent_message> outgoing_messages; //!< message queue for outgoing messages
    std::list<Agent_message> incoming_messages; //!< message queue for incoming messages
    unsigned int msg_received;                  //!< number of received messages
    unsigned int msg_sent;                      //!< number of sent messages

    Villas_interface * villas_interface;          //!< villas interface that can be used in agent behavior
};

#endif

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef BEHAVIOR_IO_H
#define BEHAVIOR_IO_H

#include <iostream>
#include "model/config.h"
#include "model/io_object.h"
#include <fstream>

/*! \brief IO class for local logging of agent behavior
 *
 * A log file is created, filled and flushed after each time step. The log file is properly closed
 * at the end of the simulation.
 * */
class BehaviorIO : public IO_object{
public:
    BehaviorIO(int &_id, struct data_props _d_props);
    ~BehaviorIO() override = default;

    void init_logging();

private:
    int   id;                           //!< Agent ID
};

#endif //BEHAVIOR_IO_H

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef AGENT_MESSAGE
#define AGENT_MESSAGE

#include <repast_hpc/AgentId.h>
#include "model/config.h"

/* defines for message performatives based on FIPA */
#define FIPA_PERF_ACCEPT_PROPOSAL    1       /* The action of accepting a previously submitted proposal to perform an action */
#define FIPA_PERF_AGREE              2       /* The action of agreeing to perform some action, possibly in the future */
#define FIPA_PERF_CANCEL             3       /* The action of one agent informing another agent that the first agent no longer has the intention that the second agent perform some action */
#define FIPA_PERF_CALL_FOR_PROPOSAL  4       /* The action of calling for proposals to perform a given action */
#define FIPA_PERF_CONFIRM            5       /* The sender informs the receiver that a given proposition is true, where the receiver is known to be uncertain about the proposition */
#define FIPA_PERF_DISCONFIRM         6       /* The sender informs the receiver that a given proposition is false, where the receiver is known to believe, or believe it likely that, the proposition is true */
#define FIPA_PERF_FAILURE            7       /* The action of telling another agent that an action was attempted but the attempt failed */
#define FIPA_PERF_INFORM             8       /* The sender informs the receiver that a given proposition is true */
#define FIPA_PERF_INFORM_IF          9       /* A macro action for the agent of the action to inform the recipient whether or not a proposition is true */
#define FIPA_PERF_INFORM_REF         10      /* A macro action for sender to inform the receiver the object which corresponds to a descriptor, for example, a name */
#define FIPA_PERF_NOT_UNDERSTOOD     11      /* The sender of the act (for example, i) informs the receiver (for example, j) that it perceived that j performed some action, but that i did not understand what j just did. A particular common case is that i tells j that i did not understand the message that j has just sent to i */
#define FIPA_PERF_PROPAGATE          12      /* The sender intends that the receiver treat the embedded message as sent directly to the receiver, and wants the receiver to identify the agents denoted by the given descriptor and send the received propagate message to them */
#define FIPA_PERF_PROPOSE            13      /* The action of submitting a proposal to perform a certain action, given certain preconditions */
#define FIPA_PERF_PROXY              14      /* The  sender wants the receiver to select target agents denoted by a given description  and  to send an embedded message to them */
#define FIPA_PERF_QUERY_IF           15      /* The action of asking another agent whether or not a given proposition is true */
#define FIPA_PERF_QUERY_REF          16      /* The action of asking another agent for the object referred to by a referential expression */
#define FIPA_PERF_REFUSE             17      /* The action of refusing to perform a given action, and explaining the reason for the refusal */
#define FIPA_PERF_REJECT_PROPOSAL    18      /* The action of rejecting a proposal to perform some action during a negotiation */
#define FIPA_PERF_REQUEST            19      /* The sender requests the receiver to perform some action. One important class of uses of the request act is to request the receiver to perform another communicative act. */
#define FIPA_PERF_REQUEST_WHEN       20      /* The sender wants the receiver to perform some action when some given proposition becomes true */
#define FIPA_PERF_REQUEST_WHENEVER   21      /* The sender wants the receiver to perform some action as soon as some proposition becomes true and thereafter each time the proposition becomes true again */
#define FIPA_PERF_SUBSCRIBE          22      /* The act of requesting a persistent intention to notify the sender of the value of a reference, and to notify again whenever the object identified by the reference changes */
#define CUST_PERF_UNSUBSCRIBE        23      /* The act of withdrawing a former made subscribe; not in FIPA standards, custom performative */

/* defines for communication protocols based on FIPA */
#define FIPA_PROT_REQUEST               1
#define FIPA_PROT_QUERY                 2
#define FIPA_PROT_REQUEST_WHEN          3
#define FIPA_PROT_CONTRACT_NET          4
#define FIPA_PROT_ITERATED_CONTRACT_NET 5
#define FIPA_PROT_ENGLISH_AUCTION       6
#define FIPA_PROT_DUTCH_AUCTION         7
#define FIPA_PROT_BROKERING             8
#define FIPA_PROT_RECRUITING            9
#define FIPA_PROT_SUBSCRIBE             10
#define FIPA_PROT_PROPOSE               11

/*! \brief Class for Agent Messages*/
class Agent_message{
public:
    Agent_message();
    Agent_message(unsigned int _perf, int _sender, int _receiver, std::string _content, unsigned int _protocol);
    Agent_message(const Agent_message &msg);
    ~Agent_message() = default;

    /* message components based on FIPA specs*/
    unsigned int performative;      //!< type of communicative act
    int sender;         //!< identity of the sender
    int receiver;       //!< identity of the receiver

    //repast::AgentId reply_to;       //!< identity of agent to reply to with subsequent messages (not needed)
    std::string content;            //!< content of message
    //unsigned int language;          //!< language in which content is expressed (for interpretation of conten) (not needed)
    //unsigned int encoding;          //!< encoding of content language (not needed)
    //unsigned int ontology;          //!< used in conjunction with language parameter for interpretation (not needed)
    unsigned int protocol;          //!< interaction protocol
    //unsigned int conversation_id;   //!< identifier for ongoing sequence of communicative acts (not needed, but maybe useful)
    //std::string reply_with;         //!< expression that will be used in in_reply_to parameter (not needed)
    //std::string in_reply_to;        //!< expression that references an earlier message (not needed)
    //int reply_by;                   //!< latest allowed time to receive a reply (not needed, but maybe useful)
    double time_pending;              //!< Saves the time that this message was a pending message at the message router agent

    int get_receiver();
    double get_time_pending();
    void set_time_pending(double _delay);

    /* helper function for logging */
    std::string get_message_output();

    friend class boost::serialization::access;

    /* For archive packaging */
    template<class Archive>
    void serialize(Archive &ar, const unsigned int version){
        ar & sender;
        ar & receiver;
        ar & content;
        ar & protocol;
        ar & performative;
        ar & time_pending;
    }
};

#endif //AGENT_MESSAGE

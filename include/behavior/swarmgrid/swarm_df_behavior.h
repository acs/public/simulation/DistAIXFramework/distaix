/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef SWARM_DF_BEHAVIOR
#define SWARM_DF_BEHAVIOR

#include "behavior/swarmgrid/swarm_behavior.h"

/*! \brief Class for SwarmGrid directory facilitator behavior
 *
 * The DF answers to messages in the recruiting protocol and proposes new contact partners based on the 
 * agents' requests. For this purpose the DF has topological kowledge and is able to find all agents
 * within a certain distance from the sender
 * */
class Swarm_df_behavior : public Swarm_behavior {
public:
    Swarm_df_behavior(int _id, int _type, std::string _subtype, double& step_size, struct data_props _d_props,
        DF_data * _df_data, int _rank);
    ~Swarm_df_behavior() override;

    int initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                   std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                   boost::multi_array<int, 2> *components_file,
                                   boost::multi_array<std::string, 1> *subtypes_file,
                                   boost::multi_array<int, 2> *el_grid_file) override;
    void execute_agent_behavior() override;

protected:
    void process_contract_net_msg(swarm_contract_net_msg &msg) override;
    void process_subscribe_msg(swarm_subscribe_msg &msg) override;
    void process_query_msg(swarm_query_msg &msg) override;
    void process_recruiting_msg(swarm_recruiting_msg &msg) override;
    void process_request_msg(swarm_request_msg &msg) override;

private:
    void get_agents(int source_id, int node_source, std::string s_type, int _negtype, 
        unsigned int hops, std::vector<std::pair<int, unsigned int>> &agents);

    DF_data* df_data;
};

#endif

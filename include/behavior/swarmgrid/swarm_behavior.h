/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef SWARM_BEHAVIOR
#define SWARM_BEHAVIOR

#include "behavior/agent_behavior.h"
#include "behavior/swarmgrid/swarm_message.h"
#include "behavior/swarmgrid/swarm_config.h"

/*! \brief abstract super-class for SwarmGrid behavior
 *
 * This class implements functions common to all agents (Prosumer, Substation, Slack, DF) in the
 * swarm concept. These functions deal with common message handling (receiving and sending)
 * */
class Swarm_behavior : public Agent_behavior {
public:
    Swarm_behavior(int _id, int _type, std::string _subtype, double& step_size, struct data_props _d_props);
    virtual ~Swarm_behavior() override = default;

protected:
    /* message processing */
    void process_incoming_messages();
    virtual void process_contract_net_msg(swarm_contract_net_msg &msg) = 0;
    virtual void process_subscribe_msg(swarm_subscribe_msg &msg) = 0;
    virtual void process_query_msg(swarm_query_msg &msg) = 0;
    virtual void process_recruiting_msg(swarm_recruiting_msg &msg) = 0;
    virtual void process_request_msg(swarm_request_msg &msg) = 0;

    /* message sending functions */
    void send_contract_net_msg(swarm_contract_net_msg &msg);
    void send_subscribe_msg(swarm_subscribe_msg &msg);
    void send_recruiting_msg(swarm_recruiting_msg &msg);
    void send_query_msg(swarm_query_msg &msg);
    void send_request_msg(swarm_request_msg &msg);
};

#endif

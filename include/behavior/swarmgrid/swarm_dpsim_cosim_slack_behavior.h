/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/


#ifndef SWARM_DPSIM_COSIM_SLACK_BEHAVIOR
#define SWARM_DPSIM_COSIM_SLACK_BEHAVIOR

#include "behavior/swarmgrid/swarm_slack_behavior.h"
#include "behavior/dpsim_cosim/dpsim_cosim_message.h"


//forwad declarations
struct villas_node_config;

/*! \brief Class for dpsim cosim with swarm behavior
 * */

class Swarm_dpsim_cosim_slack_behavior : public Swarm_slack_behavior{
public:
    Swarm_dpsim_cosim_slack_behavior(int _id, int _type, int _rank, std::string _subtype, double& _step_size, struct data_props _d_props,
        Slack_data * _slack_data, villas_node_config * _villas_config, bool _realtime, bool _sync);

    ~Swarm_dpsim_cosim_slack_behavior();


    int initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                   std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                   boost::multi_array<int, 2> *components_file,
                                   boost::multi_array<std::string, 1> *subtypes_file,
                                   boost::multi_array<int, 2> *el_grid_file);
    void report_to_superior_grid(Dpsim_cosim_msg message, std::list<Villas_message> *incoming_villas_messages);
    

private:
    void create_endpoints(villas_node_config * _vnconfig, std::string agent_id);
    
    void receive_message(std::list<Villas_message> *incoming_villas_messages);
    void synchronize_with_remote();
   
    int busy_receive_msg(std::list<Villas_message> *incoming_villas_messages);
    void send_villas_msg(Dpsim_cosim_msg message);

    struct villas_node_config * vnconfig;

    int sender;
    double step_size;

    bool first_step = true;
    bool realtime;
    bool sync;

    int timeout = 5000; // timeout for busy waiting in milliseconds

    unsigned int numOfSentMessages = 0;
    unsigned int numOfReceivedMessages = 0;

};

#endif // DPSIM_COSIM_SLACK_BEHAVIOR
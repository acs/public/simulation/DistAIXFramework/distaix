/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

/* negotiation about P and Q */
#define NEGOTIATIONTYPE_P_INT 1
#define NEGOTIATIONTYPE_Q_INT 2

/* groups */
#define GROUPTYPE_INTERNAL_INT  1
#define GROUPTYPE_SWARM_INT     2
#define GROUPTYPE_SUBSTATION_INT    3
#define GROUPTYPE_SLACK_INT 4


#define QUERYTYPE_VOLTAGE    1
#define QUERYTYPE_CURRENT    2
#define REQUESTTYPE_SETPOINT    1

#define NEGOTIATION_THRESHOLD       10
#define NEGOTIATION_THRESHOLD_FACTOR    0.25
#define SOC_THRESHOLD_INTERNAL      0.05
#define SOC_THRESHOLD_SWARM         0.25
#define POWER_ACCURACY              0.001
#define MAX_SWARM_SIZE              20
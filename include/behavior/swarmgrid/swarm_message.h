/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef SWARM_MESSAGE
#define SWARM_MESSAGE

#include "behavior/swarmgrid/contract.h"

/*! \brief Struct for storing Contract Net message data
 * */
struct swarm_contract_net_msg {
    unsigned int performative;      //!< type of communicative act
    int sender;                     //!< identity of the sender
    int receiver;                   //!< identity of the receiver
    double time;                    //!< time when message is sent
    struct contract_ex power;       //!< power that is negotiated
    /* initialization */
    swarm_contract_net_msg() :performative(0), sender(), receiver(0), time(0){}
    swarm_contract_net_msg(unsigned int _performative, int _sender, int _receiver) :
        performative(_performative), sender(_sender), receiver(_receiver), time(0){}
};

/*! \brief Struct for storing Subscribe message data
 * */
struct swarm_subscribe_msg {
    unsigned int performative;      //!< type of communicative act
    int sender;                     //!< identity of the sender
    int receiver;                   //!< identity of the receiver
    struct service_ex service;      //!< power that is advertised
    unsigned int dist;              //!< distance of prosumers
    /* initialization */
    swarm_subscribe_msg() :performative(0), sender(), receiver(0), dist(0) {}
    swarm_subscribe_msg(unsigned int _performative, int _sender, int _receiver) :
        performative(_performative), sender(_sender), receiver(_receiver), dist(0) {}
};

/*! \brief Struct for storing Recruiting message data
 * */
struct swarm_recruiting_msg {
    unsigned int performative;                      //!< type of communicative act
    int sender;                                     //!< identity of the sender
    int receiver;                                   //!< identity of the receiver
    int node_sender;                                //!< if of the node the sender is attached to
    int base_type;                                  //!< consumer or producer
    int neg_type;                                   //!< negotiation type (active or reactive power)
    unsigned int hops;                                  //!< number of hops from node_sender
    std::vector<std::pair<int, unsigned int>> agents;   //!< vector of agents and their distance
    /* initialization */
    swarm_recruiting_msg() :performative(0), sender(), receiver(0), node_sender(0), base_type(0),
        neg_type(0), hops(0) {}
    swarm_recruiting_msg(unsigned int _performative, int _sender, int _receiver) :
        performative(_performative), sender(_sender), receiver(_receiver), node_sender(0),
        base_type(0),  neg_type(0), hops(0) {}
};

/*! \brief Struct for storing query message data
 * */
struct swarm_query_msg {
    unsigned int performative;      //!< type of communicative act
    int sender;                     //!< identity of the sender
    int receiver;                   //!< identity of the receiver
    int meas_type;                  //!< type of measurement (voltage, current)
    double time;                    //!< time when message is sent
    double meas_re;                 //!< real part of measurement
    double meas_im;                 //!< imaginary part of measurement
    /* initialization */
    swarm_query_msg() :performative(0), sender(), receiver(0), meas_type(0), time(0), meas_re(0),
        meas_im(0) {}
    swarm_query_msg(unsigned int _performative, int _sender, int _receiver) :
        performative(_performative), sender(_sender), receiver(_receiver), meas_type(0), time(0),
        meas_re(0), meas_im(0) {}
};

/*! \brief Struct for storing Request message data
 * */
struct swarm_request_msg {
    unsigned int performative;      //!< type of communicative act
    int sender;                     //!< identity of the sender
    int receiver;                   //!< identity of the receiver
    int request_type;               //!< type of request
    double request_value;           //!< value of request
    /* initialization */
    swarm_request_msg() :performative(0), sender(), receiver(0), request_type(0), request_value(0)
        {}
    swarm_request_msg(unsigned int _performative, int _sender, int _receiver) :
        performative(_performative), sender(_sender), receiver(_receiver), request_type(0),
        request_value(0) {}
};

#endif //SWARM_MESSAGE

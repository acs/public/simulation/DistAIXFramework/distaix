/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef CONTRACT
#define CONTRACT

#include <sstream>
#include <math.h>
#include <complex>
#include "model/config.h"
#include "behavior/swarmgrid/swarm_config.h"

/*! \brief Struct for storing information about a contract between a producer and a consumer
 * */
struct contract_ex {
    int neg_type;       //!< denotes if contract is about active or reactive power
    double needed;      //!< needed (non-flexible) power in W/var
    double optional;    //!< optional (flexible) power in W/var
    int producer;       //!< ID of producer
    int consumer;       //!< ID of consumer
    /* initialization */
    contract_ex() :neg_type(NEGOTIATIONTYPE_P_INT), needed(0), optional(0), producer(0), 
        consumer(0) {}
    contract_ex(int _neg_type, double _needed, double _optional, int _producer, int _consumer) 
        :neg_type(_neg_type), needed(_needed), optional(_optional), producer(_producer), 
        consumer(_consumer) {}
};

/*! \brief Struct for storing information about a producer service
 * */
struct service_ex {
    int svc_type;           //!< denotes if service is about active or reactive power
    double minimum;         //!< minimum power the producer wants to supply
    double optional_max;    //!< maximum power the producer supplies to cover optional power demand
    double needed_max;      //!< maximum power the producer supplies to cover needed power demand
    double t_last_update;   //!< time of last updtae
    /* initialization */
    service_ex() :svc_type(NEGOTIATIONTYPE_P_INT), minimum(0), optional_max(0), needed_max(0), 
        t_last_update(0) {}
    service_ex(int _svc_type, double _minimum, double _optional_max, double _needed_max, 
        double _t_last_update) :svc_type(_svc_type), minimum(_minimum), optional_max(_optional_max),
        needed_max(_needed_max), t_last_update(_t_last_update) {}
};

/*! \brief Class for contracts between producers and consumers
 *
 * This is a class representing a contract about active or reactive power that has been negotiated 
 * among a producer and a consumer agent.
 * */
class Contract
{

protected: 
    int producer_id;                        //!< agent ID of the producer
    int consumer_id;                        //!< agent ID of the consumer
    struct contract_ex contracted_power;    //!< power that is contracted
    double t_last_update;                   //!< time of last update
    
public:
    Contract();
    Contract(int _producer, int _consumer, struct contract_ex _contracted_power, double t);
    ~Contract() = default;

    /* getter */
    double get_needed_power();
    double get_optional_power();
    struct contract_ex get_power();
    int get_producer_id();
    int get_consumer_id();
    double get_t_last_update();

    void change_contract(struct contract_ex _contracted_power, double t);
    void set_contract_partners(int _producer, int _consumer);

    /* helper function for logging */
    std::string get_contract_output();
};

#endif // CONTRACT

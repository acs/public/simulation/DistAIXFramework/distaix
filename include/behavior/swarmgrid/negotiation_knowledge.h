/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef NEGOTIATION_KNOWLEDGE
#define NEGOTIATION_KNOWLEDGE

#include <sstream>
#include "behavior/behavior_io.h"
#include "behavior/swarmgrid/contract.h"
#include "behavior/swarmgrid/communication_knowledge.h"

/*! \brief Class representing negotiation knowledge of an agent 
 *
 * The class holds all contracts of either active or reactive power with either internal, swarm, 
 * substation or slack agents. Contracts which are still negotiated are also stored. Functions for
 * contract manipulation are provided. Additionally, all producer services are stored in this 
 * class, too. The sum of all contracts and services can be calculated.
 * */
class Negotiation_knowledge
{
private:
    int id;                                     //!< ID of agent
    BehaviorIO *IO;                             //!< output module for logging
    Communication_knowledge* knowledge_comm;    //!< pointer to agent's communication knowledge
    int negotiation_type;                       //!< type of power (active or reactive)
    int state;                                  //!< state of prosumer (producer or consumer)
    int group_type;                             //!< internal, swarm or substation

    /* lists of known agents */
    std::vector<std::pair<int, unsigned int>> producers;    //!< IDs of producer agents within the 
                                                            //!< same group and their distance
    std::vector<std::pair<int, unsigned int>> consumers;    //!< IDs of consumer agents within the 
                                                            //!< same group and their distance

    bool waiting_for_df;    //!< indicates whether or not agent is waiting for a reply from the DF

    /* list of services */
    std::vector<std::pair<int, struct service_ex>> services;    //!< services that were promoted by
                                                                //!< group producers

    /* list of contracts */
    std::vector<Contract> contracts;        //!< vector of all concluded contracts
    std::vector<Contract> open_contracts;   //!< vector of all open open contracts

    /* contracted power */
    struct contract_ex power;           //!< contracted power (sum of all concluded contracts)
    struct contract_ex open_power;      //!< open power (sum of all open contracts)

    /* helper */
    std::vector<Contract>::iterator get_it_contracts(int _producer_id, int _consumer_id);
    std::vector<Contract>::iterator get_it_open_contracts(int _producer_id, int _consumer_id);

public:
    Negotiation_knowledge();
    ~Negotiation_knowledge() = default;

    void init(int &_id, BehaviorIO* _io, Communication_knowledge* know_comm, int neg_type,
        int _group_type);

    /* contracts */
    int new_contract(int _producer_id, int _consumer_id, struct contract_ex _power, double t);
    int conclude_contract(int _producer_id, int _consumer_id, double t);
    int handle_contract_inform_msg(int _producer_id, int _consumer_id, struct contract_ex _power, 
        double t);
    int change_contract(int _producer_id, int _consumer_id, struct contract_ex _power, double t);
    int change_open_contract(int _producer_id, int _consumer_id, struct contract_ex _power, 
        double t);
    int delete_open_contract(int _producer_id, int _consumer_id);
    bool open_contract_exists(int _producer_id, int _consumer_id);
    std::vector<Contract> get_consumer_contracts();
    std::vector<Contract> get_producer_contracts();
    int get_negotiation_type();
    struct contract_ex get_open_contract(int _producer_id, int _consumer_id);
    struct contract_ex get_contracted_power(int _producer_id, int _consumer_id);

    /* finding producers */
    int get_suitable_producer(struct contract_ex& _power, double threshold);

	/* services */
    void new_producer_service(int _producer_id, struct service_ex _service);
    void update_producer_service(int _producer_id, struct service_ex _service);
    struct service_ex get_producer_service(int _producer_id);
    void get_older_producer_services(std::vector<std::pair<int, struct service_ex>>& older_services,
        double t);
    struct service_ex get_service_sum();

	/* swarm */
    bool is_producer(int _id);
    bool is_consumer(int _id);
    bool is_in_group(int _id);
    bool subscribed_to(int _id);
    void add_consumer(int _consumer_id, unsigned int dist, double t);
    void add_producer(int _consumer_id, unsigned int dist, double t);
    void delete_consumer(int _consumer_id);
    void delete_producers(unsigned int swarm_size, std::vector<int>& _producers);
    unsigned int get_effective_swarm_size(double t, double pmin);
	void get_consumers(std::vector<std::pair<int, unsigned int>>& _consumers);
    int get_group_type();

	/* DF */
    void contacted_df();
    void received_df_reply();
    bool is_waiting_for_df();

	/* power */
    struct contract_ex get_power();
    struct contract_ex get_open_power();

	/* state */
    int get_state();
    void set_state(int _state);

    /* helper function for logging */
    void log_negotiation_knowledge();
    std::string get_negotiation_header();
};

#endif // NEGOTIATION_KNOWLEDGE

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef SWARM_SLACK_BEHAVIOR
#define SWARM_SLACK_BEHAVIOR

#include "behavior/swarmgrid/swarm_negotiator_behavior.h"

/*! \brief Class for SwarmGrid slack behavior
 *
 * This behavior applies to only one agent in the system, the slack agent. It represents the
 * superior voltage level and can introduce its needs by participating in negotiations.
 * For this purpose the slack behavior is able to follow a set point for actve and reactive power.
 * */
class Swarm_slack_behavior : public Swarm_negotiator_behavior {
public:
    Swarm_slack_behavior(int _id, int _type, std::string _subtype, double& step_size, struct data_props _d_props,
        Slack_data * _slack_data);
    virtual ~Swarm_slack_behavior() override;

    int initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                   std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                   boost::multi_array<int, 2> *components_file,
                                   boost::multi_array<std::string, 1> *subtypes_file,
                                   boost::multi_array<int, 2> *el_grid_file) override;
    void execute_agent_behavior() override;

protected:
    /* input for contract negotiation */
    contract_ex get_power_lack(Negotiation_knowledge* know_neg) override;
    contract_ex get_consumer_power_exceed(Negotiation_knowledge* know_neg) override;
    contract_ex get_producer_power_exceed(Negotiation_knowledge* know_neg) override;
    service_ex get_new_service(Negotiation_knowledge* know_neg) override;

    /* helper functions */
    unsigned int get_swarm_size(int neg_type) override;
    void set_swarm_size(int neg_type, int swarm_size) override;
    double get_threshold(Negotiation_knowledge* know_neg) override;
    void change_setpoint(double setpoint) override;

private:
    /* slack specific behavior */
    void initial_actions();
    void determine_operation_range();
    void determine_state(int neg_type);
    void determine_power_correction();

    Slack_data * slack_data;  //!< pointer to prosumer specific datastruct
};

#endif

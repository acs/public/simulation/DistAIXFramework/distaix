/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef SWARM_PROSUMER_BEHAVIOR
#define SWARM_PROSUMER_BEHAVIOR

#include "behavior/swarmgrid/swarm_negotiator_behavior.h"

/*! \brief Class for SwarmGrid prosumer behavior (battery, biofuel, chp, ev, hp, load, pv, wec)
 *
 * Prosumers determine the input for the contract negotiation based on a single components that is
 * attached to a grid node. Their swarms form dynamically within the same voltage level based on the
 * situation. The class also implements a function to calculate the operation range of a prosumer
 * allowing to implement an abstract prosumer behavior based on that range instead of a behavior
 * specific to each component type
 * */
class Swarm_prosumer_behavior : public Swarm_negotiator_behavior {
public:
    Swarm_prosumer_behavior(int _id, int _type, std::string _subtype, double& step_size, struct data_props _d_props,
        Prosumer_data * _prosumer_data);
    virtual ~Swarm_prosumer_behavior() override;

    int initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                   std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                   boost::multi_array<int, 2> *components_file,
                                   boost::multi_array<std::string, 1> *subtypes_file,
                                   boost::multi_array<int, 2> *el_grid_file) override;
    void execute_agent_behavior() override;

protected:
    /* input for contract negotiation */
    contract_ex get_power_lack(Negotiation_knowledge* know_neg) override;
    contract_ex get_consumer_power_exceed(Negotiation_knowledge* know_neg) override;
    contract_ex get_producer_power_exceed(Negotiation_knowledge* know_neg) override;
    service_ex get_new_service(Negotiation_knowledge* know_neg) override;

    /* helper functions */
    unsigned int get_swarm_size(int neg_type) override;
    void set_swarm_size(int neg_type, int swarm_size) override;
    double get_threshold(Negotiation_knowledge* know_neg) override;
    void change_setpoint(double setpoint) override;

private:
    /* prosumer specific behavior */
    void apply_control_values();
    void initial_actions();
    void determine_nominal_voltage();
    void determine_operation_range();
    void determine_operation_range_load();
    void determine_operation_range_renewable(double v_norm, double v_old);
    void determine_operation_range_battery(double v_norm, double v_old);
    void determine_operation_range_ev(double v_norm, double v_old);
    void determine_operation_range_hp(double v_norm, double v_old);
    void determine_operation_range_chp(double v_norm, double v_old);
    void determine_operation_range_bio(double v_norm, double v_old);
    void determine_operation_range_compensator(double v_norm, double v_old);
    void determine_q_operation_range(double v_norm, double v_old);
    void determine_state(int neg_type);
    void determine_swarm_size(int neg_type);
    double get_q_available(double p);
    void save_voltage_measurement();

    Prosumer_data * prosumer_data;  //!< pointer to prosumer specific datastruct
};

#endif

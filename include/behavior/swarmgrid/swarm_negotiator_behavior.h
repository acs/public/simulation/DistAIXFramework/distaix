/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef SWARM_NEGOTIATOR_BEHAVIOR
#define SWARM_NEGOTIATOR_BEHAVIOR

#include "behavior/swarmgrid/swarm_behavior.h"
#include "behavior/swarmgrid/communication_knowledge.h"
#include "behavior/swarmgrid/negotiation_knowledge.h"
#include "behavior/swarmgrid/sensor_knowledge.h"

/*! \brief abstract super-class for SwarmGrid behavior
*
* This class implements functions common to all agents (Prosumer, Substation, Slack)
* Agents negotiate the amount of active and reactive power they consume/produce. For this
* purpose they interact with each other using the Subscribe and the Contract-Net protocol. Contracts
* are stored in objects of the Negotiation_knowledge class distinguishing internal, swarm,
* substation and slack contract partners as well as active and reactive power.
* The class contains functions for following all communication protocols, for contract negotiation,
* and swarm forming. Some functions need to be implemented in sub-classes.
* */
class Swarm_negotiator_behavior : public Swarm_behavior {
public:
    Swarm_negotiator_behavior(int _id, int _type, std::string _subtype, double& step_size, struct data_props _d_props);
    virtual ~Swarm_negotiator_behavior() override = default;

protected:
    /* message processing */
    void process_contract_net_msg(swarm_contract_net_msg &msg) override;
    void process_subscribe_msg(swarm_subscribe_msg &msg) override;
    void process_query_msg(swarm_query_msg &msg) override;
    void process_recruiting_msg(swarm_recruiting_msg &msg) override;
    void process_request_msg(swarm_request_msg &msg) override;

    /* input for contract negotiation */
    virtual contract_ex get_power_lack(Negotiation_knowledge* know_neg) = 0;
    virtual contract_ex get_consumer_power_exceed(Negotiation_knowledge* know_neg) = 0;
    virtual contract_ex get_producer_power_exceed(Negotiation_knowledge* know_neg) = 0;
    virtual service_ex get_new_service(Negotiation_knowledge* know_neg) = 0;

    /* contract negotiation */
    void cover_power_demand(Negotiation_knowledge* know_neg);
    void release_contracted_power(Negotiation_knowledge* know_neg, int state, double threshold);
    void update_services(Negotiation_knowledge* know_neg);
    bool decide_cfp(int _consumer_id, struct contract_ex& _power);
    bool decide_proposal(int _producer_id, struct contract_ex& _power);

    /* helper functions */
    Negotiation_knowledge* get_affected_negotiation_knowledge(int _agent_id, int neg_type);
    void change_state(int neg_type, int state);
    struct contract_ex get_power_sum(int neg_type, int group_type);
    struct contract_ex get_open_power_sum(int neg_type, int group_type);
    struct service_ex get_service_sum(int neg_type, int group_type);
    virtual unsigned int get_swarm_size(int neg_type) = 0;
    virtual void set_swarm_size(int neg_type, int swarm_size) = 0;
    virtual double get_threshold(Negotiation_knowledge* know_neg) = 0;
    virtual void change_setpoint(double setpoint) = 0;

    Communication_knowledge knowledge_comm;             //!< communication knowledge (vectors of
                                                        //!< internal and swarm agents, DF, Trafo
                                                        //!< and Slack agent)
    Negotiation_knowledge knowledge_neg_internal_p;     //!< Negotiation knowledge for internal
                                                        //!< active power
    Negotiation_knowledge knowledge_neg_internal_q;     //!< Negotiation knowledge for internal
                                                        //!< reactive power
    Negotiation_knowledge knowledge_neg_swarm_p;        //!< Negotiation knowledge for swarm active
                                                        //!< power
    Negotiation_knowledge knowledge_neg_swarm_q;        //!< Negotiation knowledge for swarm
                                                        //!< reactive power
    Negotiation_knowledge knowledge_neg_substation_p;   //!< Negotiation knowledge for substation
                                                        //!< active power
    Negotiation_knowledge knowledge_neg_substation_q;   //!< Negotiation knowledge for substation
                                                        //!< reactive power
    Negotiation_knowledge knowledge_neg_slack_p;        //!< Negotiation knowledge for slack active
                                                        //!< power
    Negotiation_knowledge knowledge_neg_slack_q;        //!< Negotiation knowledge for slack
                                                        //!< reactive power
    Sensor_knowledge knowledge_sens;                    //!< Knowledge about current and voltage
                                                        //!< sensors
};

#endif

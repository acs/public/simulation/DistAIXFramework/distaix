/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef SENSOR_KNOWLEDGE
#define SENSOR_KNOWLEDGE

#include <sstream>
#include "behavior/behavior_io.h"
#include "model/config.h"
#include "behavior/swarmgrid/communication_knowledge.h"
#include "behavior/swarmgrid/swarm_config.h"
#include <list>
#include <complex>

/*! \brief Struct for storing information about a sensor measurement
 * */
struct meas_ex {
    int id;                     //!< ID of agent that performed measurement
    int type;                   //!< type of measurement (current, voltage)
    std::complex<double> meas;  //!< measurement value
    double t;                   //!< time point of measurement
    bool updated;               //!< indicator of updated value
    /* initialization */
    meas_ex() : id(0), type(0), meas(0,0), t(0), updated(true) {}
    meas_ex(int _id, int _type, std::complex<double> _meas, double _t, bool _updated) 
        : id(_id), type(_type), meas(_meas), t(_t), updated(_updated) {}
};

/*! \brief Class representing sensor knowledge of an agent 
 *
 * This class stores measurements performed by an agent and provides functions to extract desired
 * information
 * */
class Sensor_knowledge
{
private:
    int id;                                     //!< ID of agent
    BehaviorIO *IO;                             //!< output module for logging
    Communication_knowledge* knowledge_comm;    //!< pointer to communication knowledge of agent

    std::vector<struct meas_ex> voltage_meas;   //!< vector of voltage measurements
    
    double voltage_old;                         //!< normalized voltage at last time step
    unsigned int filter_size_v;                 //!< size of moving average filter for voltage
    std::list<struct meas_ex> voltage_samples;  //!< voltage measurements to be used by filter
    double v_re_sum;                            //!< real part of sum of all measurements
    double v_im_sum;                            //!< imaginary part of sum of all measurements

    unsigned int filter_size_p;                 //!< size of moving average filter for power
    std::list<double> p_loss_samples;           //!< active power loss measurements
    std::list<double> q_loss_samples;           //!< reactive power loss measurements
    double p_loss_sum;                          //!< sum of all active power loss measurements
    double q_loss_sum;                          //!< sum of all reactive power loss measurements

public:
    Sensor_knowledge();
    ~Sensor_knowledge() = default;

    void init(int &_id, BehaviorIO* _io, Communication_knowledge* know_comm, 
        double step_size);

    void add_voltage_sample(struct meas_ex v_sample);

    void update_voltage_meas(int agent_id, std::complex<double> voltage, double t);
    std::complex<double> get_voltage_meas(int agent_id);
    double get_old_voltage_meas();
    double get_absolute_voltage(int agent_id);
    double get_mean_voltage();
    double get_min_voltage();
    double get_max_voltage();
    std::vector<int> get_ids_voltage_meas();

    bool all_voltage_meas_updated();
    void reset_voltage_meas();

    void add_loss_sample(double p_loss, double q_loss);
    double get_p_loss();
    double get_q_loss();

    void log_sensor_knowledge();
};

#endif // SENSOR_KNOWLEDGE

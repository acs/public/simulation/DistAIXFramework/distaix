/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef COMMUNICATION_KNOWLEDGE
#define COMMUNICATION_KNOWLEDGE

#include <sstream>
#include <vector>
#include "behavior/behavior_io.h"
#include "model/config.h"

/*! \brief Class representing communication knowledge of an agent 
 *
 * This class stores internal and swarm communication partners as well as the superior substation
 * and the slack agent.
* */
class Communication_knowledge
{
private:
    int id;                                 //!< ID of agent holding that knowledge
    BehaviorIO *IO;                         //!< output module for logging

    /* lists of known agents */
    int df_agent;                           //!< ID of local DF agent
    int substation_agent;                   //!< ID of the agent managing the substation
    int slack_agent;                        //!< ID of the slack agent
    std::vector<int> internal_agents;       //!< IDs of component agents connected to the same node
                                            //!< with unknown type
    std::vector<int> neighborhood_agents;   //!< IDs of component agents connected to the 
                                            //!< neighboring nodes
    std::vector<std::pair<int, unsigned int>> swarm_agents; //!< IDs of component agent within the 
                                                            //!< same swarm with unknown type and 
                                                            //!< their distance [hops]

public:
    Communication_knowledge();
    ~Communication_knowledge() = default;

    void init(int &_id, BehaviorIO* _io);

    bool is_internal_agent(int _id);
    bool is_swarm_agent(int _id);

    bool add_internal(int id);
    bool add_neighbor(int id);
    bool add_swarmmember(int id, int type, unsigned int dist);

    /* getter */
    void get_internal_agents(std::vector<int>& _internal_agents);
    void get_swarm_agents(std::vector<std::pair<int, unsigned int>>& _swarm_agents);
    unsigned int get_distance(int _agent_id);

    int get_substation_agent();
    int get_slack_agent();
    int get_df_agent();

    /* helper function for logging */
    void log_communication_knowledge();
};

#endif // COMMUNICATION_KNOWLEDGE

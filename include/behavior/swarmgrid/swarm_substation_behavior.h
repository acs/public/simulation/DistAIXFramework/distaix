/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef SWARM_SUBSTATION_BEHAVIOR
#define SWARM_SUBSTATION_BEHAVIOR

#include "behavior/swarmgrid/swarm_negotiator_behavior.h"

/*! \brief Class for SwarmGrid substation behavior
 *
 * The substation is responsible for controlling the tap changer and for contract negotiations among
 * different voltage levels. The tap changer is controlled by considering voltage at the primary
 * node as well as voltages in the grid. For this purpose voltage values of agents in the grid are
 * periodically queried.
 * Regarding the contract negotiation the substation represents the LV components in the MV grid and
 * vice versa. The total amount of contracted power in both grid has to be zero at all time (since
 * substation does not produce or consume power)
 * */
class Swarm_substation_behavior : public Swarm_negotiator_behavior {
public:
    Swarm_substation_behavior(int _id, int _type, std::string _subtype, double& step_size, struct data_props _d_props,
        Substation_data * _substation_data);
    virtual ~Swarm_substation_behavior() override;

    int initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                   std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                   boost::multi_array<int, 2> *components_file,
                                   boost::multi_array<std::string, 1> *subtypes_file,
                                   boost::multi_array<int, 2> *el_grid_file) override;
    void execute_agent_behavior() override;

protected:
    /* input for contract negotiation */
    contract_ex get_power_lack(Negotiation_knowledge* know_neg) override;
    contract_ex get_consumer_power_exceed(Negotiation_knowledge* know_neg) override;
    contract_ex get_producer_power_exceed(Negotiation_knowledge* know_neg) override;
    service_ex get_new_service(Negotiation_knowledge* know_neg) override;

    /* helper functions */
    unsigned int get_swarm_size(int neg_type) override;
    void set_swarm_size(int neg_type, int swarm_size) override;
    double get_threshold(Negotiation_knowledge* know_neg) override;
    void change_setpoint(double setpoint) override;

private:
    /* substation specific behavior */
    void apply_control_values();
    void initial_actions();
    void determine_state(int neg_type);
    void determine_swarm_size(int neg_type);
    //void determine_grid_losses();
    //void determine_grid_state();
    void query_voltage_meas();
    void save_voltage_measurement();
    void request_setpoint_change();
    
    Substation_data * substation_data;    //!< pointer to substation specific datastruct
};

#endif

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef REFIC_SUBSTATION_BEHAVIOR
#define REFIC_SUBSTATION_BEHAVIOR

#include "behavior/agent_behavior.h"

/*! \brief Class for reference substation behavior with internal communication
 *
 * The substaion only controls the tap changer in the reference scenario. This is done 
 * considering only the voltage at the secondary node.
 * */
class Refic_substation_behavior : public Agent_behavior {
public:
    Refic_substation_behavior(int _id, int _type, std::string _subtype, double& step_size, struct data_props _d_props,
        Substation_data * _substation_data);
    ~Refic_substation_behavior() override;

    int initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                   std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                   boost::multi_array<int, 2> *components_file,
                                   boost::multi_array<std::string, 1> *subtypes_file,
                                   boost::multi_array<int, 2> *el_grid_file) override;
    void execute_agent_behavior() override;

private:
    void apply_control_values();

    Substation_data * substation_data;  //!< pointer to equipment specific datastruct
};

#endif

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef REFIC_PROSUMER_BEHAVIOR
#define REFIC_PROSUMER_BEHAVIOR

#include "behavior/agent_behavior.h"

/*! \brief Class for reference prosumer behavior (battery, biofuel, chp, ev, hp, load, pv, wec) with internal communication
 *
 * Prosumers in the reference scenario behave according to simple rules. Reactive power is controlled regarding German grid codes.
 * */
class Refic_prosumer_behavior : public Agent_behavior {
public:
    Refic_prosumer_behavior(int _id, int _type, std::string _subtype, double& step_size, struct data_props _d_props,
        Prosumer_data * _prosumer_data);
    ~Refic_prosumer_behavior() override;

    int initialize_agent_behavior(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes,
                                   std::vector<std::list<std::tuple<int,int,int>>> * connected_nodes,
                                   boost::multi_array<int, 2> *components_file,
                                   boost::multi_array<std::string, 1> *subtypes_file,
                                   boost::multi_array<int, 2> *el_grid_file) override;
    void execute_agent_behavior() override;

private:
    void process_incoming_messages();
    void query_internal_agents();
    void determine_operation_range();
    void q_v();
    void cosphi_p();
    void apply_control_values();
    double get_q_available(double p);

    std::vector<std::pair<int, double>> internal_agents; //!< id and power of internal agents
    double t_last_query; //!< last time internal agents were querried

    Prosumer_data * prosumer_data;  //!< pointer to prosumer specific datastruct
};

#endif

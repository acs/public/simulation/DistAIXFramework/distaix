/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef MQTT_REF_SUBSTATION_BEHAVIOR
#define MQTT_REF_SUBSTATION_BEHAVIOR

#include "behavior/mqtt_reference/mqtt_ref_behavior.h"

/*! \brief Class for mqtt reference substation behavior
 * */
class Mqtt_ref_substation_behavior : public Mqtt_ref_behavior {
public:
    Mqtt_ref_substation_behavior(int _id, int _type, int _rank, std::string _subtype, double& step_size, struct data_props _d_props,
        villas_node_config * _villas_config, Substation_data * _substation_data);
    ~Mqtt_ref_substation_behavior()= default;
    void execute_agent_behavior() override;

protected:
    void process_mqtt_ref_msg(Mqtt_ref_msg &msg) override;
    void set_connection_params(std::vector<std::list<std::tuple<int,int,int>>> * components_at_nodes) override;

private:
    void apply_control_values();
    std::vector<std::tuple<int, double, bool>> voltage_measurements;

    Substation_data * substation_data;  //!< pointer to equipment specific datastruct
};

#endif //MQTT_REF_SUBSTATION_BEHAVIOR

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef MQTT_MESSAGE
#define MQTT_MESSAGE

#include "villas_interface/villas_message.h"
/*! \brief Struct for storing mqtt message data
 * */

#define MQTT_REF_MESSAGE_LENGTH 6



class Mqtt_ref_msg : public Villas_message {
public:

    Mqtt_ref_msg();
    Mqtt_ref_msg(Villas_message &msg);
    Mqtt_ref_msg( int _performative, int _sender, int _receiver);
    ~Mqtt_ref_msg() override = default;

    //pointers to elements
    int64_t * performative;      //!< type of communicative act
    int64_t * sender;                     //!< identity of the sender
    int64_t * receiver;                   //!< identity of the receiver
    int64_t * measurement_type;           //!< type of measurement
    double * measurement_value;       //!< value of measurement

    static std::vector<Meta_infos> mqtt_ref_msg_meta;
    static bool meta_initialized;

    void set_pointers();
    static void init_meta();

    std::string get_message_output();

private:
    void init_message(int _performative, int _sender, int _receiver, int _measurement_type, double _measurement_value);


};

#endif //MQTT_MESSAGE
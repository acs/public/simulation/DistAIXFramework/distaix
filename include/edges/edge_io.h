/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DISTAIX_EDGE_IO_H
#define DISTAIX_EDGE_IO_H

#include "model/io_object.h"
#include "model/time_measurement.h"

class EdgeIO : public IO_object{

public:
    EdgeIO(std::string &_id, struct data_props _d_props);
    ~EdgeIO() override = default;

    void init_logging();
    void set_result_file_header() override;

    void save_result(void *_model_data, bool first_step, Time_measurement *_tm_csv, Time_measurement * _tm_db, Time_measurement * _tm_db_serialize, Time_measurement * _tm_db_add);


private:
    std::string id;

    void set_result_types();
    void save_meta(void *_model_data);
    void save_result_csv(void *_model_data);
    void save_result_db(void *_model_data, bool first_step, Time_measurement * _tm_db_serialize, Time_measurement * _tm_db_add);

};

#endif //DISTAIX_EDGE_IO_H

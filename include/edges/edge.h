/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DISTAIX_EDGE
#define DISTAIX_EDGE

#include "repast_hpc/Edge.h"
#include "edges/edge_io.h"

/*! \brief Class used to model edges in RepastHPC SharedNetworks*/
template<typename V>
class Edge : public repast::RepastEdge<V> {

public:

    /*Constructors required for compatibility with RepastHPC library (addEdge(...) methods)*/
    Edge(V *source, V *target) : repast::RepastEdge<V>(source, target) {}
    Edge(V *source, V *target, double weight) : repast::RepastEdge<V>(source, target, weight)  { }
    Edge(boost::shared_ptr<V> source, boost::shared_ptr<V> target) : repast::RepastEdge<V>(source, target) { }
    Edge(boost::shared_ptr<V> source, boost::shared_ptr<V> target, double weight) : repast::RepastEdge<V>(source, target, weight) { }

    /*constructor for use in DistAIX*/
    Edge(
            V *source,
            V *target,
            bool _is_non_local,
            std::string _id,
            double _step_size
            ) : repast::RepastEdge<V>(source, target) {
        first_step=true;
        IO = nullptr;
        is_non_local = _is_non_local;
        id = _id;
        step_size = _step_size;
    }

    virtual ~Edge(){};

    /* Actions */
    virtual void calculate(double _voltage_input_real, double _voltage_input_imag, double _current_input_real, double _current_input_imag) {
    }; // virtual function, to be re-implemented in cable classes

    /*Getters*/
    std::string getId(){return id;};

    //Exit function for edge
    void do_exit(int code) {
        std::cerr << "Edge: "<< id << ": Error. Aborting!" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, code);
    }

    virtual std::complex<double> get_voltage_output(){return std::complex<double>(0,0);};
    virtual std::complex<double> get_leakage_current(){ return std::complex<double>(0, 0); };

    std::string id; /*!< name of the edge*/
    bool is_non_local; /*!< If true this edge connects a local and a non-local agent, if false this edge connects 2 local agents*/

    EdgeIO * IO; /*!< IO object for logging and result saving of edge*/

    //Timing variables
    double t_start; /*!< Starting time of the simulation in sec, first step to simulate */
    double t_step; /*!< Step size of one simulation step in seconds */
    double t_next; /*!< Next simulation time step to simulate in seconds */
    double step_size; /*!< size of one simulation step in seconds*/

    bool first_step; //marks the first run of step, so constant values could be added properly
};
#endif //DISTAIX_EDGE

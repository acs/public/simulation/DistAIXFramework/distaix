/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef EDGE_CONTENT
#define EDGE_CONTENT

#include "edges/edge.h"

/*! \brief Class used to model edge contents in SharedNetworks*/
template <typename V>
struct Edge_content : public repast::RepastEdgeContent<V>{

    friend class boost::serialization::access;

public:

    Edge_content() {}
    Edge_content(Edge<V>* edge) : repast::RepastEdgeContent<V>(edge) {}

    template<class Archive>
    void serialize(Archive& ar, const unsigned int version){
        repast::RepastEdgeContent<V>::serialize(ar, version);
    }


};


#endif //EDGE_CONTENT

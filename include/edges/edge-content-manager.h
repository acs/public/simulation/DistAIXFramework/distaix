/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef EDGE_CONTENT_MANAGER
#define EDGE_CONTENT_MANAGER

#include "edges/edge-content.h"

/*! \brief Class used to model edge content managers in SharedNetworks*/
template <typename V>
class Edge_content_manager{
public:
    Edge_content_manager() {}
    ~Edge_content_manager() {}
    Edge<V>* createEdge(Edge_content<V> & content, repast::Context<V>* context){
        return new Edge<V>(context->getAgent(content.source), context->getAgent(content.target), content.weight);
    }
    Edge_content<V>* provideEdgeContent(Edge<V>* edge){
        return new Edge_content<V>(edge);
    }
};
#endif //EDGE_CONTENT_MANAGER

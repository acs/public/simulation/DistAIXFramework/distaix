/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef CABLE_H
#define CABLE_H

#include "edges/edge.h"
#include "agents/agent.h"
#include "agents/agent_datastructs.h"
#include "component/transmission.h"

/*! \brief Class used to model cables in electrical network*/
class Cable : public Edge<Agent>{
private:
    int model_type; /*!< 0 for steady state, 1 for dynamic phasor*/
    std::string subtype;
    bool no_losses; /*!< 1 if this is a cable without losses, i.e. without FMU*/

    PiLine_data model_data;
    Transmission* line_model;

    int number_of_iterations;
    void set_cable_type(float _r, float _x, float _b, float _g, float length, float _long_term_rate, float _emergency_rate);

public:
    Cable(
            Agent *source,
            Agent *target,
            bool _is_non_local,
            std::string _id,
            double _step_size,
            int _model_type,
            std::string &_subtype,
            struct data_props _d_props,
            float _r,
            float _x,
            float _b,
            float _g,
            float length,
            float _long_term_rate,
            float _emergency_rate
            );

    ~Cable() override;

    /*Getters*/
    std::complex<double> get_voltage_output() override;
    std::complex<double> get_leakage_current() override;

    /* Actions */
    void calculate(double _voltage_input_real, double _voltage_input_imag, double _current_input_real, double _current_input_imag)  override;
    void step(Time_measurement *_tm_csv, Time_measurement * _tm_db, Time_measurement * tm_db_serialize, Time_measurement * tm_db_add);
};

#endif //CABLE_H

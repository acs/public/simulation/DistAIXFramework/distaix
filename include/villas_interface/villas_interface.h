/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef VILLAS_INTERFACE_H
#define VILLAS_INTERFACE_H

#include <string>
#include <list>
#include "villas_interface/villas_message.h"

//forward declarations
struct vnode;
struct vnode_type;
struct pool;

class IO_object;

#define DEFAULT_NUMBER_OF_SAMPLES 1
#define DEFAULT_LENGTH_OF_SAMPLE 64

//copied from villas utils.h
/** Get nearest up-rounded power of 2 */
#define MY_LOG2_CEIL(x)	(1 << (my_log2i((x) - 1) + 1))

/** Get log2 of long long integers */
static inline int my_log2i(long long x) {
    if (x == 0)
        return 1;

    return sizeof(x) * 8 - __builtin_clzll(x) - 1;
}


/*! \brief Struct that holds general info about the villas node type MQTT */
struct mqtt_data{
    std::string broker;
    int port;
    int retain;
    int keepalive;
    std::string publish;
    std::string subscribe;
    int qos;

    int ssl_enabled;
    int ssl_insecure;
    int ssl_cert_reqs;
    std::string ssl_tls_version;
    std::string ssl_cafile;
    std::string ssl_capath;
    std::string ssl_certfile;
    std::string ssl_ciphers;
    std::string ssl_keyfile;

};

/*! \brief Struct that holds general info about the villas node type nanomsg */
struct nanomsg_data{
    std::list<std::string> in_endpoints;
    std::list<std::string> out_endpoints;
};

/*! \brief Struct that holds general info about the villas node */
struct villas_node_config{
    std::string type_name;
    std::string format_name;
    std::string loglevel;
    bool with_node;
    double stop_at; // used to synchronize end of simulation in cosimulation
    union node_type_config{
        mqtt_data *mqtt_conf;
        nanomsg_data *nanomsg_conf; 
    } type_config;
};


class Villas_interface{

public:
    Villas_interface(villas_node_config *_config, IO_object *IO_obj, std::string _name, std::vector<Meta_infos> &_meta);
    ~Villas_interface();

    //methods to send/receive data via villas node
    void send_message(Villas_message &msg);
    void receive_messages(std::list<Villas_message> &messages);
    //methods to start/stop the villas node
    int start();
    int stop();
    //methods to init/destroy the villas node
    int destroy();

    //methods to control villas node type (used by model ONLY)
    int start_node_type();
    int stop_node_type();


private:
    /*VillasNode struct*/
    struct vnode *n;
    struct vnode_type *type;

    //Memory pool
    struct pool *p;

    // meta infos
    std::vector<Meta_infos> meta;

    //counting number of sent messages for sequence numbers
    unsigned int number_of_sent_messages;
    bool with_node;

    //for logging into agent log file
    IO_object *IO;

    void allocate_and_receive(int number_of_messages, std::list<Villas_message> &messages);

};

#endif //VILLAS_INTERFACE_H

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DISTAIX_VILLAS_MESSAGE_H
#define DISTAIX_VILLAS_MESSAGE_H

#include <string>
#include <vector>
#include <complex>

#define VILLAS_DATA_TYPE_DOUBLE 1
#define VILLAS_DATA_TYPE_INT64 2
#define VILLAS_DATA_TYPE_BOOLEAN 3
#define VILLAS_DATA_TYPE_COMPLEX 4

struct Meta_infos{
    std::string name;
    std::string unit;
    int type;
};

union Data_element{
    double f;
    int64_t i;
    bool b;
    std::complex<float> z;

    Data_element(){
    }
};

class Villas_message{

public:

    Villas_message();
    virtual ~Villas_message() = default;

    void add_element(Data_element _d, int _type);

    std::vector<Data_element> data;
    double time_sec; //!< simulation time when message is sent (has to be set manually before calling send_message())
    unsigned int length;

};

#endif //DISTAIX_VILLAS_MESSAGE_H

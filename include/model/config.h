/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#define MAX_CSV_AGENTS 100
#define MAX_STEPS_LOGGING 1000

/*definition of component type mappings*/
/*Loads*/
#define TYPE_LOAD_INT 0
#define TYPE_LOAD_STRING "Load"

#define TYPE_EV_INT 7
#define TYPE_EV_STRING "EV"

#define TYPE_COMPENSATOR_INT 8
#define TYPE_COMPENSATOR_STRING "Compensator"

#define TYPE_HP_INT 11
#define TYPE_HP_STRING "HP"

/*Generators*/
#define TYPE_PV_INT 1
#define TYPE_PV_STRING "PV"

#define TYPE_CHP_INT 6
#define TYPE_CHP_STRING "CHP"

#define TYPE_WEC_INT 9
#define TYPE_WEC_STRING "Wind"

#define TYPE_BIOFUEL_INT 17
#define TYPE_BIOFUEL_STRING "Biofuel"

/*Storages*/
#define TYPE_BATTERY_INT 2
#define TYPE_BATTERY_STRING "Storage"

/*Transformers*/
#define TYPE_TRANSFORMER_INT 3
#define TYPE_TRANSFORMER_STRING "Transformer"

/*Slack*/
#define TYPE_SLACK_INT 4
#define TYPE_SLACK_STRING "Slack"

/*Cable*/
#define TYPE_CABLE_INT 99

/* definition of base types */
/* Consumer (Load, EV, Storage, HP) */
#define BASETYPE_CONSUMER_INT 21

/* Producer (PV, WEC, CHP, Storage, BioFuel) */
#define BASETYPE_PRODUCER_INT 22

/* Utility (Transformer, Compensator) */
#define BASETYPE_UTILITY_INT 23

/* consumer and producer (Battery, Transformer) */
#define BASETYPE_PROSUMER_INT 24

/*Directory Facilitator*/
#define TYPE_DF_INT -2

/*Message Router*/
#define TYPE_MR_INT -3

/*Unknown*/
#define TYPE_UNKNOWN_INT -99

/*definition of node agent type*/
#define TYPE_NODE_INT 5
#define TYPE_NODE_STRING "Node"

/*Secondary ("peak load") heaters*/
#define TYPE_GASBOILER_INT 12
#define TYPE_GASBOILER_STRING "Gasboiler"

#define TYPE_HEATINGROD_INT 13
#define TYPE_HEATINGROD_STRING "Heatingrod"

#define TYPE_BIOMASS_INT 14
#define TYPE_BIOMASS_STRING "Biomass"

#define TYPE_ELECTRICKILN_INT 15
#define TYPE_ELECTRICKILN_STRING "ElectricKiln"

#define TYPE_GASICKILN_INT 16
#define TYPE_GASKILN_STRING "GasKiln"

/*define ID od slack*/
#define SLACK_ID 1

/*definition of states for forward and backward sweep and convergence check state*/
#define BACKWARD_SWEEP 0
#define FORWARD_SWEEP 1
#define CONVERGENCE_CHECK 2
#define DONE_NOTHING 0
#define DONE_FORWARD_SWEEP 1
#define DONE_CONVERGENCE_CHECK 2
#define ALREADY_FINISHED 3

/*default communication delay (seconds)*/
#define COMM_DELAY_DEFAULT 0.01

/*default package drop probability (1/100)*/
#define PACKAGE_DROP_PROB_DEFAULT 0.0

/*uncomment following if communication latency should be switched off at the beginning of the simulation*/
#define COMM_DELAY_START_OFF

/*if communication latency is switched off at the start of the simulation the following determines the time when it is switched on again*/
#define COMM_DELAY_TIME_ON 100.0

/*define epsilon for convergence determination*/
#define EPSILON 0.000001

/*definition of agent set names*/
#define SET_EL_CONNECTIONS "SET_el_connections"
#define SET_MESSAGE_ROUTERS "SET_message_routers"

/* defines for control behavior */
#define CTRL_NON_INTELLIGENT        0
#define CTRL_INTELLIGENT            1
#define CTRL_MQTT_TEST              2
#define CTRL_MQTT_PINGPONG          3
#define CTRL_ENSURE                 4
#define CTRL_MQTT_HIGHLOAD          5
#define CTRL_DPSIM_COSIM_REF        6
#define CTRL_DPSIM_COSIM_SWARMGRIDX 7
#define CTRL_INTCOMM            8

/*defines for model types*/
#define MODEL_TYPE_STEADY_STATE 0
#define MODEL_TYPE_DYNAMIC_PHASOR 1

/*set the number of steps which are ignored  at the beginning of a simulation
 * for the logging of communication effort (from_to_matrix and from_to_DF results)*/
#define NUMBER_OF_IGNORED_STEPS 100

/*set a period of time (in s) for which an extra logging of number of messages and max number of messages
 * shall be done*/
#define START_EXTRA1_LOGGING_SEC 100.0
#define END_EXTRA1_LOGGING_SEC 160.0

#define START_EXTRA2_LOGGING_SEC 160.0
#define END_EXTRA2_LOGGING_SEC 300.0

// profile files
#define FILE_PROFILE_LOAD   "load.csv"
#define FILE_PROFILE_PV     "pv.csv"
#define FILE_PROFILE_WEC    "wec.csv"
#define FILE_PROFILE_EV     "ev.csv"
#define FILE_PROFILE_TH     "heating.csv"
#define FILE_PROFILE_BIO    "bio.csv"

/*Database log levels*/
#define LOG_LEVEL_VERBOSE "verbose"
#define LOG_LEVEL_MINIMAL "minimal"
#define LOG_LEVEL_NORMAL "normal"

/*Scenario file dimension limits*/
#define COMPONENTS_MAX_ATTRIBUTE_COLUMNS 10
#define COMPONENTS_MAX_COLUMNS 14
#define ELGRID_MAX_ATTRIBUTE_COLUMNS 7
#define ELGRID_MAX_COLUMNS 10
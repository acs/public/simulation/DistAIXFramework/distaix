/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef MODEL_IO_H
#define MODEL_IO_H

#include <boost/multi_array.hpp>
#include <repast_hpc/Properties.h>
#include "time_measurement.h"
#include "io_object.h"
#include "profile.h"

class ModelIO : public IO_object{
public:
    ModelIO(repast::Properties* _props, int _rank, int _world_size, std::string _dir_profiles, struct data_props _d_props);
    ~ModelIO() override = default;

    void init_logging();
    void init_db();
    void addMetaEntry(int rank, std::string metatype, std::string key, std::string value);
    void flush_db();
    void print_model_configuration();

    void log_results_for_tick();
    void reset_time_measurements();
    void set_result_file_header() override;

    void finish_io() override;
    void finish_time_measurements(unsigned long  &stop_at);

    //general file reading
    void read_file_mpi(char* path, std::vector<std::string> &line_vector, int &n_columns, int &n_rows, const MPI_Comm &communicator);

    //profile reading
    void read_profiles(Profile *comp_profiles);
    bool read_profile_file(char* path, double* &t, std::vector<double*> &values, std::vector<std::string> &headers, unsigned int &num_values);

    //scenario file reading
    bool read_components_file(char* path, boost::multi_array<int, 2> &values, boost::multi_array<std::string, 1> &subtype, boost::multi_array<double, 2> &components_attributes); //read in scenario file components
    bool read_elgrid_file(char* path, boost::multi_array<int, 2> &values, boost::multi_array<std::string, 1> &subtypes, boost::multi_array<float, 2> &edge_attributes);//read in scenario file el_grid
    bool read_comm_data_file(char* path, double * values);

    int map_component_type(std::string &type);

    //time measurement methods
    void start_time_measurement(Time_measurement* tm);
    void stop_time_measurement(Time_measurement* tm);
    long long int get_time_measurement(Time_measurement* tm);

    /*counting number of FBS sweeps in model*/
    void reset_number_of_fbs_sweeps();
    void reset_loop_counters();
    void increment_number_of_fbs_sweeps();
    void add_to_forward_loop_counter(unsigned int n);
    void add_to_backward_loop_counter(unsigned int n);
    unsigned int get_number_of_fbs_sweeps();

    /*logging time values*/
    Time_measurement* tm; /*!< Time measurement for complete simulation time */
    Time_measurement* tm_sync_el; /*!< Time measurement for synchronize() function called for el connections*/
    Time_measurement* tm_sync_mr; /*!< Time measurement for synchronize() function called for MR agents*/
    Time_measurement* tm_tick; /*!< Time measurement for duration of one simulation tick*/
    Time_measurement* tm_rt_tick; /*!< Time measurement for duration of one simulation tick*/
    Time_measurement* tm_step; /*!< Time measurement for step() function */
    Time_measurement* tm_fb_sweep; /*!< Time measurement for forward_backward_sweep() function */
    Time_measurement* tm_msg_proc_internal;/*!< Time measurement for delivering process internal messages */
    Time_measurement* tm_msg_proc_external;/*!< Time measurement for delivering messages from other processes */
    Time_measurement* tm_behaviors; /*!< Time measurement for computation of agent behaviors */
    Time_measurement* tm_calculate_components;/*!< Time measurement for calculate_components() function */
    Time_measurement* tm_backward_sweep;/*!< Time measurement for do_backward_sweep() function */
    Time_measurement* tm_forward_sweep; /*!< Time measurement for do_forward_sweep() function */
    Time_measurement* tm_init; /*!< Time measurement for init() function */
    Time_measurement* tm_distribute_agents; /*!< Time measurement for agent distribution */
    Time_measurement* tm_init_MR; /*!< Time measurement for MR Agent init */
    Time_measurement* tm_comm_partners_init; /*!< Time measurement for communication partners init*/

    /* Time measurements for agent behaviors */
    Time_measurement* tm_load;
    Time_measurement* tm_ev;
    Time_measurement* tm_pv;
    Time_measurement* tm_hp;
    Time_measurement* tm_chp;
    Time_measurement* tm_wec;
    Time_measurement* tm_bio;
    Time_measurement* tm_bat;
    Time_measurement* tm_comp;
    Time_measurement* tm_substation;
    Time_measurement* tm_slack;
    Time_measurement* tm_df;

    /* Time measurements for MPI broadcast operations during FBS */
    Time_measurement* tm_bcast_FBS;
    Time_measurement* tm_bcast_forward_sweep;
    Time_measurement* tm_bcast_backward_sweep;

    /*Time measurements for result saving*/
    Time_measurement * tm_csv;
    Time_measurement * tm_db;
    Time_measurement * tm_db_serialize;
    Time_measurement * tm_db_add;

    unsigned int number_of_fbs_sweeps;        //!< counting the number of forward-backward sweeps
                                              //!< in each iteration
    unsigned int forward_loop_counter; //!< counting number of iterations through forward sweep loop
    unsigned int backward_loop_counter; //!< counting number of iterations through backward sweep loop


private:
    void log_result_csv(std::string result_line);

    int simId;
    repast::Properties* props; /*!< RepastHPC properties file */

    int rank; /*!< Internal process number of the MPI process (also called the "rank") */
    int world_size; /*!< Number of processes in the simulation */
    std::string profile_dir; /*!< Directory containing profile time series files*/

};

#endif //MODEL_IO_H

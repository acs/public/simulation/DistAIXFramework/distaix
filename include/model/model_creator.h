/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef MODEL_CREATOR
#define MODEL_CREATOR

#include <sys/time.h>
#include <boost/multi_array.hpp>
#include <repast_hpc/SharedContext.h>

// Class Model currently needs to be forward declared to use the agent_creator function
class Model;
class ModelIO;
class Agent;

/*! \brief This class distributes agents to processes and creates agents in their corresponding process
 *
 * Based on the distribution method, set in model.props respective scheduling plans are created
 * by the create_model() method. This is done in each process, due to the necessity that every
 * process has to fill the _agent_rank_relation array for later agent requests.
 *
 * For each agent that is scheduled, the process checks whether this agent belongs to it or is
 * scheduled into another process. Agents that are scheduled to the own process are treated as
 * local agents and are created using the agent_creator-function of the Model class. Afterwards,
 * created agents are pushed into the vector agents_scheduling which represents the actual
 * scheduling plan of the process!
 *
 * There is only one container agents_scheduling representing the scheduling plan which can be
 * accessed forward or backward during the forward-backward sweeping.
 * */
class Model_creator {
public:
    Model_creator(boost::multi_array<int, 2> *_scenario_file_components_content,
                    boost::multi_array<std::string, 1> *_scenario_file_components_subtypes,
                    boost::multi_array<double, 2> *_scenario_file_components_attributes,
                    boost::multi_array<int, 2> *_scenario_file_el_grid_content,
                    int _world_size, std::vector<int> &_transfomers,
                    std::string &_distribution_method, std::vector<Agent *> *_agents_scheduling,
                    int _rank, repast::SharedContext<Agent> *_context, ModelIO *_IO, Model *_model);
    ~Model_creator();

    void preprocessing();
    void create_model(int *_agent_rank_relation,
                      std::vector<std::list<std::tuple<int, int, int>>> &components_at_nodes,
                      std::vector<bool> &ict_connectivity_at_nodes);
    unsigned int get_agent_count();
    unsigned int get_number_of_nodes();
    int get_highest_filled_rank();
    // Prototype for hop distance calculation
    unsigned int hop_distance(int id1, int id2);

private:

    int* agent_workitem_relation;               //!< saves which workitem is created in which rank
    std::vector<int> *path_forknodes;           //!< saves for each node the path of fork nodes
                                                //!< that are visited between slack and this node
    std::vector<int> *path_workitems;           //!< saves for each node the path of workitems
                                                //!< that are visited between slack and this node

    // Variables needed for shared memory
    MPI_Comm mc_comm;                            //!< MPI communicator used for shared memory
    int mc_worldsize;                            //!< wordsize of the split communicator
    int mc_rank;                                 //!< rank of this process in the split communicator
    void * adjacent_nodes_base_pointer;          //!< base pointer for shared memory window saving
                                                 //!< the adjacent nodes array
    MPI_Win shared_mem_window_adjacent_nodes;    //!< MPI shared memory window for saving
                                                 //!< the adjacent nodes array
    int * number_of_adjacent_nodes;              //!< array saving the number of adjacent nodes per node
    void * scheduled_nodes_base_pointer;         //!< base pointer for shared memory window saving
                                                 //!< the scheduled nodes array
    MPI_Win shared_mem_window_scheduled_nodes;   //!< MPI shared memory window for saving
                                                 //!< the scheduled nodes array
    unsigned int * node_agents_scheduled;        //!< array saving the scheduled nodes

    std::vector<std::list<int>> adjacent_nodes; //!< saves a list of adjacent node IDs per node

    ModelIO *IO; //!< Pointer to ModelIO object of the Model class object that holds the instance of this class
    Model *model; //!< Pointer to the Model class object that holds the instance of this class

    boost::multi_array<int, 2>
            *scenario_file_components_content;     //!< Content of component scenario file
    boost::multi_array<std::string, 1>
            *scenario_file_components_subtypes;    //!< Subtypes of component scenario file
    boost::multi_array<double, 2>
            *scenario_file_components_attributes;  //!< Attributes of components in scenario file
    boost::multi_array<int, 2>
            *scenario_file_el_grid_content;        //!< Content of electrical grid scenario file

    std::string distribution_method;        //!< Selected agent distribution method
    int world_size;                         //!< Number of processes in the simulation
    int rank;                               //!< Rank (ID) of the process that hold this instance
    int highest_filled_rank;                //!< Highest rank of a process that has created agents
    unsigned int edges_per_rank;            //!< Number of edges treated by each rank in
                                            //!< method determine_number_of_adjacent_nodes
    int recommended_number_of_processes;    //!< Number of leafs in initial el. topology is
                                            //!< recommended number of processes

    unsigned int number_of_components;      //!< Number of component agents (e.g., loads, PV, ...)
                                            //!< owned by the MPI process
    unsigned int number_of_nodes;           //!< Number of node agents owned by the MPI process
    unsigned int agent_count;               //!< Number of created agents
    unsigned long number_of_workitems;      //!< Number of determined workitems
    int current_workitem;	                //!< Current workitem that is looked at during
                                            //!< distribution of node agents

    std::vector<int> transformers;          //!< Vector of all transformer IDs
    std::vector<int> leaf_nodes;            //!< Vector of all leaf node IDs
    std::vector<int> fork_nodes;            //!< Vector of all fork node IDs
    std::vector<int> node_agents;           //!< Vector of node agent IDs
    std::vector<Agent *> *agents_scheduling;//!< Scheduling for all agents in this process
                                            //!< (used in Model class later on)

    unsigned int* node_depth;       //!< Distance of all nodes to the root node (slack)
    unsigned int* hop_depth;        //!< Hop distance of all nodes to the root node (slack),
                                    //!< a lossless cable counts as 0 hops

    std::list<int> *workitems;           //!< list of all workitems
    std::vector<int> *child_workitems;   //!< list of all child workitems per workitem

    repast::SharedContext<Agent> *context; //!< SharedContext to which agents are added
                                           //!< (object from Model class)

    struct timeval distribute_agents_time_start;   //!< Start value for agent distribution time measurement
    struct timeval distribute_agents_time_end;     //!< End value for agent distribution time measurement

    void create_node_agent(int node_id);

    // Agent distribution functions
    void evenly_distribution(int * _agent_rank_relation);
    void workitem_distribution(int *_agent_rank_relation);

    // Private methods needed for preprocessing of the grid topology
    void allocate_shared_memory();
    void determine_workitems();
    void determine_leaf_nodes();
    void determine_fork_nodes();
    void determine_adjacent_nodes();
    void determine_number_of_adjacent_nodes();
    int topology_plausibility_check();

    // Methods used in agent distribution
    bool all_nodes_scheduled();
    unsigned int number_of_nodes_scheduled();
    void depth_search_hop(int v, unsigned int previous_depth, unsigned int previous_hop_depth,
                          int previous_workitem, bool lossless_cable,
                          std::vector<int> seen_forknodes, std::vector<int> seen_workitems,
                          bool visited_nodes[]);

    // Exit function of Model_creator
    void do_exit(int code);



};

#endif //MODEL_CREATOR
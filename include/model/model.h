/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef MODEL
#define MODEL

#include <boost/mpi.hpp>
#include <boost/progress.hpp>
#include <boost/multi_array.hpp>

#include <repast_hpc/Schedule.h>
#include <repast_hpc/Properties.h>
#include <repast_hpc/SharedContext.h>
#include <repast_hpc/SharedNetwork.h>
#include <repast_hpc/AgentId.h>
#include <repast_hpc/RepastProcess.h>

#include "profile.h"

#include "agents/agent.h"
#include "agents/agent-package.h"
#include "edges/cable.h"
#include "agents/directoryfacilitator_agent.h"
#include "agents/msgrouter_agent.h"
#include "model_io.h"
#include "model_creator.h"

//Forward declarations
class Villas_interface;
struct villas_node_config;

class Model_creator;

/*! \brief class describing the model owned by every MPI process
 *
 * The Model class defines the model that is used by every MPI process.
 * It handles agent creation, agent linking, step execution, synchronization, and overall method scheduling.
 * */
class Model{


public:
	Model(std::string &propsFile, int argc, char** argv, boost::mpi::communicator* comm);
	virtual ~Model();

	/*public methods used during model initialization*/
	void init();
    void initSchedule(repast::ScheduleRunner& runner);
    virtual void agent_creator(repast::AgentId &id, std::string &_subtype, int &node_id,
                               double * param, int profile_id1, int profile_id2, double Vnom, bool _ict_connected);

    /*public methods used during model execution*/
	void synchronize(const std::string agent_set);
    void step();
    void forward_backward_sweep();
    void process_agent_messages();

protected:

    /*Variables that describe properties of the model*/
    unsigned long stop_at;                  //!< number of RepastHPC ticks to simulate
    double step_size;                       //!< size of a simulation time step in seconds
    bool use_commdata;                      //!< true if files for latency and PER values are used,
                                            //!< false otherwise
    int model_type;                         //!< 0 for steady state, 1 for dynamic phasor
    repast::Properties* props;              //!< RepastHPC properties file
    repast::SharedContext<Agent> context;   //!< context of the Model class contains all agents
                                            //!< and projections of the model
    std::vector<boost::shared_ptr<
            Cable>> cables;                 //!< vector saving pointers to all cables in this rank
    std::vector<boost::shared_ptr<
            Edge<Agent>>> MR_edges;         //!< vector saving all edges to message routers in other ranks
    int rank;                               //!< Internal process number of the MPI process
                                            //!< (also called the "rank")
    int world_size;                         //!< Number of processes in the simulation
    unsigned int number_of_components;      //!< Number of component agents (e.g., loads, PV, ...)
                                            //!< owned by the MPI process
    unsigned int number_of_nodes;           //!< Number of node agents owned by the MPI process
    int behavior_type;               //!< determines the behavior of the agents,
                                            //!< 0=non-intelligent, 1=swarm intelligence
    bool realtime;                          //!< if true, simulation time step is real time

    /*Variables related to IO of the model*/
    struct data_props d_props;                   //!< Contains all properties with respect to logging and result saving
    ModelIO *IO;                                 //!< Object of ModelIO class respsonsible for
                                                 //!< all IO operations of the model
    std::string loglevel;                        //!< log level of the database and csv files,
                                                 //!< can be 'minimal', 'normal', or 'verbose'
    std::string scenario_file_components;        //!< Path to scenario file containing components
    std::string scenario_file_el_grid;           //!< Path to scenario file containing electrical grid
    std::string scenario_file_latency;           //!< Path to scenario file containing latency information (in sec)
    std::string scenario_dir;                    //!< Path to scenario file directory
    std::string profile_dir;                     //!< Path to profile time series files directory
    std::string interpolation_type;              //!< Type of profile interpolation to be used
                                                 //!< throughout the simulation, can be 'linear' or 'hold'
    boost::multi_array<int, 2>
            scenario_file_components_content;    //!< Content of component scenario file
    boost::multi_array<std::string, 1>
            scenario_file_components_subtypes;   //!< Subtypes of component scenario file
    boost::multi_array<int, 2>
            scenario_file_el_grid_content;       //!< Content of electrical grid scenario file
    boost::multi_array<std::string, 1>
            scenario_file_el_grid_subtypes;      //!< Subtypes of electrical grid scenario file
    boost::multi_array<float, 2>
            scenario_file_edge_attributes;       //!< Attributes of electrical edges
    boost::multi_array<double, 2>
            scenario_file_components_attributes; //!< Attributes of components
    Profile component_profiles;

    /*Variables related to agent creation*/
    Model_creator *creator;     //!< Object of class responsible for agent distribution and creation
    int highest_filled_rank;              //!< ID of the highest rank, that has scheduled node agents
    std::string distribution_method;      //!< Determines which node agent distribution method should be used
    std::vector<int> agent_logging;         //!< IDs of agents that should be logged
    std::string log_folder;                 //!< Folder name for log files
    bool log_agents;                        //!< Enable logging of agents
    bool log_ranks;                         //!< Enable logging of ranks
    bool log_cables;                        //!< Enable logging of cables
    bool mr_logging;                        //!< indicates if message routers should be logged
    bool df_logging;                        //!< indicates if dfs should be logged
    std::string results_folder;              //!< Folder name for csv result files
    std::vector<int> agent_results;         //!< IDs of agents for which results should be saved in csv files
    bool results_csv_agents;                //!< Enable result csv saving of agents
    bool results_csv_ranks;                 //!< Enable result csv saving of ranks
    bool results_csv_cables;                //!< Enable result csv saving of cables
    bool results_db_agents;                //!< Enable result db saving of agents
    bool results_db_ranks;                 //!< Enable result db saving of ranks
    bool results_db_cables;                //!< Enable result db saving of cables
    bool mr_results;                        //!< indicates if results of message routers should be saved in csv files
    bool df_results;                        //!< indicates if results of dfs should be saved in csv files

    /*Variables related to sending and receiving agent updates*/
    AgentPackageProvider* provider;                                 //!< Provider of agent packages
    AgentPackageReceiver* receiver;                                 //!< Receiver of agent packages
    Edge_content_manager<Agent> edgeContentManager;                 //!< Content manager for edges
    repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>,
            Edge_content_manager<Agent> >* agent_network_elec;      //!< RepastHPC SharedNetwork
                                                                    //!< for electrical grid among agents
    repast::SharedNetwork<Agent, Edge<Agent>, Edge_content<Agent>,
            Edge_content_manager<Agent> >* agent_network_MR;      //!< RepastHPC SharedNetwork for
                                                                    //!< communication network among agents


    /*progress bar*/
    boost::progress_display *progress_p; //!< Fancy progress bar of boost library

    /*Variables that contain agent selections*/
    std::vector<Agent *> local_agents;         //!< Vector of agents that belong to this process
    std::vector<Agent *> component_agents;     //!< Vector of component agents that belong to this process
    std::vector<Agent *> node_agents;          //!< Vector of node agents that belong to this process
    std::vector<std::list<std::tuple<
            int,int,int>>> components_at_nodes;//!< saving connected components for each node,
                                               //!< tuple: 1. ID, 2. type, 3. trafo ID
    std::vector<std::list<std::tuple<
            int,int,int>>> connected_nodes;    //!< saving connected nodes for each node,
                                               //!< tuple: 1. ID, 2. type, 3. trafo ID
    std::vector<bool> ict_connectivity_of_nodes; //!< saving ICT connectivity of all nodes
    std::vector<Agent *> agents_scheduling;    //!< Vector in which agents are ordered for scheduling
    std::vector<int> transformers;             //!< Vector of all transformer IDs in the scenario

    /*Directory Facilitator and Message Router agents*/
    Directoryfacilitator_agent *DF_agent;     //!< Directory Facilitator agent of this process
    Messagerouter_agent * MR_agent;           //!< Message Router agent of this process

    /*for agent_rank_relation*/
    MPI_Win shared_mem_window_agent_rank_relation;     //!< MPI shared memory window for saving
                                                       //!< the agent-rank-relation array
    void * agent_rank_relation_base_pointer;           //!< base pointer for shared memory window saving
                                                       //!< the agent-rank-relation array
    MPI_Comm agent_rank_relation_comm;                 //!< MPI communicator for shared memory window
                                                       //!< saving the agent-rank-relation array
    int agent_rank_relation_worldsize;                 //!< number of processes in the communicator
                                                       //!< of shared memory window saving the
                                                       //!< agent-rank-relation array
    int agent_rank_relation_rank;                      //!< rank of this process inthe communicator
                                                       //!< for shared memory window saving the
                                                       //!< agent-rank-relation array
    int* agent_rank_relation;                          //!< Pointer to array that contains which
                                                       //!< agent is created in which rank

    /*for MR agent*/
    MPI_Win shared_mem_window_latency;         //!< MPI shared memory window for saving
                                               //!< the latency matrix
    void * latency_base_pointer;               //!< base pointer for shared memory window saving
                                               //!< the latency matrix
    MPI_Comm MR_comm;                          //!< MPI communicator for shared memory window
                                               //!< saving the latency matrix
    int MR_worldsize;                          //!< number of processes in the communicator of
                                               //!< shared memory window saving the latency matrix
    int MR_rank;                               //!< rank of this process inthe communicator for
                                               //!< shared memory window saving the latency matrix
    double *latency_matrix;                    //!< Pointer to array that contains latency matrix

    /* MPI communicator for FBS*/
    MPI_Comm filled_ranks; //<! communication for all ranks that contain agents (omitting ranks that do not contain agents)


    Villas_interface *interface; //!< Interface to manage villas node type
    villas_node_config * villas_config_agents;
    villas_node_config * villas_config_model;
    villas_node_config * villas_config_node_disabled;


    /*protected methods used during model initialization*/
    void read_model_props();
    void read_villas_config();
    void read_scenario_files();
    virtual void read_profiles();
    virtual void create_agents();
    void create_MR_DF_agents();
    void init_elec_network();
    void init_comm_network();
    void init_agent_behaviors();
    void agent_selections();
    void set_MR_input();
    void synchronize_el_connections();

    /*protected methods used during model execution*/
    void start_tick_time_measurement();
    void calculate_components();
    void do_backward_sweep();
    void do_forward_sweep();
    void synchronize_schedulers();
    void do_exit(int code);

};

#endif

/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef DISTAIX_IO_OBJECT_H
#define DISTAIX_IO_OBJECT_H

#include <fstream>
#include <string>
#include "repast_hpc/AgentId.h"
#include "repast_hpc/AgentRequest.h"
#ifdef USE_DB
#include "DBConnector.h"
#endif


struct log_data_props{
    bool logging; //!< if true, log file for this agent is created, if false not
    std::string path_log_file; //!<  Path to message log file of the agent, output in runlog/agents/agent_X.log
};

struct result_data_props{
    bool db_enabled; //!< if true db is enabled, if false not
    bool db_results; //!< if true, results are saved in DB, if false not
    bool csv_results; //!< if true, results are saved in csv files, if false not
    std::string loglevel; //!< level for the database and csv files, can be minimal, normal or verbose
    std::string path_result_file; //!<  Path to result log file of the agent, output in results/agents/agent_X.csv

#ifdef USE_DB
    DBConnector* dbconn; //!< Pointer to DBconnector object enabling the communication with the database system
#else
    void * dbconn; //!< for compatibility in non-db mode only, is nullptr at all times
#endif

};

struct data_props{
    struct log_data_props log;
    struct result_data_props result;
};

class IO_object{

public:
    IO_object(struct data_props _d_props);
    virtual ~IO_object()=default;

    void init_logfile();
    void init_resultfile();
    virtual void set_result_file_header();


    void log_info(std::string log);
    void update_t(double _t_next);
    void update_file_paths(std::string &_path_log, std::string &_path_result);

    void flush_log();
    void flush_result();

    virtual void finish_io();

    std::string id2str(repast::AgentId &_id);
    std::string req2str(repast::AgentRequest &_req);

    /* Logging and result streams */
    std::ofstream log_stream; /*!< Log stream for logging messages on rank level. Output in runlogs/rankX.log */
    std::ofstream result_stream; /*!< Result stream for time measurements on rank level. Output in results/rankX.csv */

    double t_next;                      //!<  The time step that is currently simulated
    struct data_props d_props;          //!< Contains logging and result file properties
};

#endif //DISTAIX_IO_OBJECT_H

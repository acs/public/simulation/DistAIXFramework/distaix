/**
 * This file is part of DistAIX
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *********************************************************************************/

#ifndef TIME_MEASUREMENT_H
#define TIME_MEASUREMENT_H

#include <string>
#include <chrono>

class Time_measurement{

private:
  std::chrono::high_resolution_clock::time_point start_t;
  std::chrono::high_resolution_clock::time_point stop_t;
  long long duration;
  std::string measurementType;
  int id;

public:
  Time_measurement(std::string _measurementType, int _id);
  ~Time_measurement() = default;
  void reset();
  void start();
  void stop();
  std::string get_time_measurement_type();
  int getId();
  long long get_duration();
};


#endif

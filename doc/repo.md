# Repository Structure

## `bin`  - Binary directory
This folder contains several run scrips and the simulator binary file after build.

## `build` - Build directory
This folder contains a script to build and install the simulator using ``cmake`` and a script to clean up the build files.

## `doc` - Documentation directory
This folder contains a Doxyfile to create html documentation of the source code.

## `examples` - Example scenarios for DistAIX
This folder contains a few example grids and a set of profiles to use DistAIX.

## `include` - Include directory
This folder contains all header files.

## `libs` - Library directory
This folder contains libraries used by DistAIX.

### `dbconnector`
The interface library for using the simulator with a PostgreSQL and a Cassandra database.
The library is built as part of DistAIX CMake build system automatically when cmake is invoked.

### `fbs-components`
A library containing electrical component models for the fortward-backward sweep solution methos used in DistAIX.
The library is used as a submodule and needs to be initialized with `git submodule --init update /distaix/libs/fbs-components`.
The library is built automatically by invoking the CMake build system of DistAIX.

### `repasthpc`
The RepastHPC library provides the agent-based modeling and simulation framework required for DistAIX.
It is used as a submodule here which needs to be initialized by `git submodule --init update /distaix/libs/repasthpc`.
RepastHPC is compiled automatically via the CMake build system of DistAIX.
It requires an MPI distribution on your system and several boost libraries.
We do not use the CMake file that is shipped with RepastHPC github repository, but have created a custom one that only compiles the required RepastHPC library.

### `villasnode`
The VILLASnode library is used as a submodule by DistAIX.
Some agent behaviors may require interconnection of agents with other infrastructures.
VILLASnode provides interfaces for this purpose.
It is compiled automatically by the CMake build system of DistAIX.
The submodule has to be initialized with `git submodule --init update ./distaix/libs/villasnode`. (from the top level of the repo)

## `props` - Configuring a DistAIX simulation
This folder contains model's properties file to configure a simulation.

## `proto` - Data serialization
The [Google Protocol Buffer](https://developers.google.com/protocol-buffers/docs/cpptutorial) files that are used to serialize and deserialize the results that are sent to the database.
Proto files are automatically converted to cpp and header files during the Makefile generation of CMake.
DistAIX uses the prosumer_agent.proto file to serialize the data of prosumer agents. In the backend of the web application, for each seperate agent type (Load, PV, HP, etc) there is one individual proto file used.

## `src` - Source folder
This folder contains all source files.
# Configuration Parameters
A simulaton is configured with the [model.props](https://git.rwth-aachen.de/acs/research/swarmgrid/distaix/blob/master/props/model.props) file contained in the props folder.

## General simulation properties

|Parameter                  | Possible values                      | Description                                             |
|---------------------------|--------------------------------------|---------------------------------------------------------|
|dir.scenario               |folder path as string                 |relative path (from ``bin`` directory) to folder containing scenario files|
|dir.profiles               |folder path as string                 |relative path (from ``bin`` directory) to folder containing profile time series files|
|file.scenario.components   |file name as string                   |name of the file that contains the nodes and components of the scenario as well as their parameters|
|file.scenario.elgrid       |file name as string                   |name of the file that contains all electrical connections between nodes and components as well as the properties of these connections|
|file.scenario.latency      |file name as string                   |name of the file that contains a node to node matrix of the communication latencies in seconds. This file is only needed if use.commdata=1|
|use.commdata               |0 or 1                                |Set to 1 if latency input file shall be used to model individual communication link properties. If set to 0, default values for latency and PER are used for all links|
|stop.at                    |integer > 0                           |Number of simulation steps to execute|
|step.size                  |double                                |Size of a simulation step in seconds|
|profile.interpolation_type |hold or linear                        |set to linear for linear profile interpolation; set to hold to hold last profile value until next value; for EV profiles hold is always used|
|distribution_method        |"evenly" or "workitem"                |Set the agent distribution method. Select "evenly" for a topology-independent distribution and "workitem" for an agent-distribution depending on the topology|
|ctrl.type                  |0 or 1                                |set to 1 if agents shall apply their intelligent behavior, set to 0 if default behavior without agent coordination is desired|
|model.type                 |0 or 1                                |set to 1 to simulate with dynamic phasor models, set to 0 to simulate with steady state models|
|realtime                   |0 or 1                                |set to 1 to simulate in real time (soft real time); in this case DistAIX will extend the duration of a simulation time step to step.size seconds; set to 0 otherwise|

## Result saving properties
|Parameter                  | Possible values                      | Description                                             |
|---------------------------|--------------------------------------|---------------------------------------------------------|
|results.loglevel           |"normal", "verbose" or "minimal"      |Set the result logging level for DB and CSV files. Select "normal" for logging of PQ data, "verbose" for additional logging of swarm specifics and "minimal" for logging of node data only|
|results.folder             |folder name as string                 |Name of the folder for csv results. Make sure you have write access to the specified folder|
|results.db.ranks           |0 or 1                                |Set to 1 if time measurements of processes shall be saved to DB, 0 otherwise|
|results.db.agents          |0 or 1                                |Set to 1 if agent results shall be saved to DB, 0 otherwise
|results.db.cables          |0 or 1                                |Set to 1 if cable results shall be saved to DB, 0 otherwise
|results.csv.ranks          |0 or 1                                |set to 1 if rank results shall be saved to csv files|
|results.csv.agents         |0 or 1                                |set to 1 if agent results shall be saved to csv files|
|results.csv.cables         |0 or 1                                |set to 1 if cable results shall be saved to csv files |
|results.ids                |comma separated list of agent IDs     |save results only for specified agents; insert comma separated list of agent ids or 0 for all|
|results.df                 |0 or 1                                |set to 1 if directory facilitator results shall be saved to csv files |
|results.mr                 |0 or 1                                |set to 1 if message router results shall be saved to csv files |

## Logging properties
|Parameter                  | Possible values                      | Description                                             |
|---------------------------|--------------------------------------|---------------------------------------------------------|
|log.folder                 |folder name as string                 |Name of the folder for log files. Make sure you have write access to the specified folder|
|log.ranks                  |0 or 1                                |Set to 1 if log files of processes shall be saved, 0 otherwise|
|log.agents                 |0 or 1                                |Set to 1 if log files of agents and agent behaviors shall be saved, 0 otherwise|
|log.cables                 |0 or 1                                |Set to 1 if log files of cables shall be saved, 0 otherwise|
|log.ids                    |comma separated list of agent IDs     |save log files only for specified agents; insert comma separated list of agent ids or 0 for all|
|log.df                     |0 or 1                                |Set to 1 log file for directory facilitator agents shall be saved, 0 otherwise|
|log.mr                     |0 or 1                                |Set to 1 log file for message router agents shall be saved, 0 otherwise|

## Database properties
|Parameter                  | Possible values                      | Description                                             |
|---------------------------|--------------------------------------|---------------------------------------------------------|
|db.host                    |x.x.x.x (x unsigned integer)          |IP adress of the PostgreSQL database|
|db.port                    |unsigend integer                      |Port of the PostgreSQL database (default: 5432)|
|db.user                    |username as string                    |Username of the PostgreSQL database (default: postgres)|
|db.pass                    |password as string                    |Password of the PostgreSQL database (default: postgres)|
|db.dbname                  |name as string                        |Name of the PostgreSQL database (default: swarmgrid)|
|cassandra.hosts            |comma separated list of IP addresses  |IP addresses of all Cassandra DB hosts to be used|
|cassandra.iothreads        |Integer > 0                           |Set number of IO threads used by Cassandra|

## VILLASnode properties
|Parameter                  | Possible values                      | Description                                             |
|---------------------------|--------------------------------------|---------------------------------------------------------|
|villas.loglevel            |off, error, warning, info, debug      |Specify the log level of the VILLASnode library|
|villas.nodetype             |String for villas node type         |Specify the node type of the VILLASnode (mqtt and nanomsg supported) |
|villas.format              |String for villas format type         |Specify the format type that is used by the villas interface, e.g. "json". "villas.human" or "csv". Check the docs of villas to get a complete list of available formats|
|villas.mqtt.broker         |IP address of MQTT broker             |IP adress of MQTT broker, you can use "localhost" if the broker is running on the same machine as DistAIX|
|villas.mqtt.port           |Integer of port number                |Port number of the MQTT broker|
|villas.mqtt.qos            |Integer  (0,1 or 2)                   |Quality of Service level of MQTT|
|villas.mqtt.retain         |0 or 1                                |Retain flag passed to MQTT|
|villas.mqtt.ssl_enabled    |0 or 1                                |Set to 1 if ssl shall be used for authentication at MQTT broker; 0 otherwise|
|villas.mqtt.keepalive      |Integer >0                            |Interval of seconds in which inactive MQTT connections send a ping to keep connection alive|
|villas.mqtt.user           |String                                |User name to authenticate an MQTT client at the broker|
|villas.mqtt.password       |String                                |Password to authenticate an MQTT client at the broker|
|villas.mqtt.subscribe      |String                                |Default subscribe topic; if nothing else is specified in an agent behavior, this topic is used to subscribe|
|villas.mqtt.publish        |String                                |Default publish topic; if nothing else is specified in an agent behavior, this topic is used to publish|

# System Preparation

In order to work with DistAIX, you need to prepare your system according to the following infos.
It is recommended to have a look at the [Dockerfile](../Dockerfile) file for information on all required packages and libraries in a Ubuntu 18.04 system.
Note that this Dockerfile installs all dependencies including the ones for DBconnector, FBS-components, VILLASnode and RepastHPC.

## Quick Start: Use Docker
If you want to get started with DistAIX in a quick an easy way, we recommend using the [Dockerfile](../Dockerfile) provided in the repository to build a Docker container image that has all dependencies of DistAIX installed.
To build the image, invoke the following Docker command in the top level folder of the repository on the command line of your system:

```bash
$ docker build -f Dockerfile -t distaix:ubuntu .
```

In this command, `docker:ubuntu` is the tag of the container image.
To run an interactive container based on the image call:

```bash
$ docker run --rm -it distaix:ubuntu
```

By using the `--rm` option of docker, the container will be removed upon exit.
Omit this option if you want the container to persist on your system.
From within the container, you can compile and run DistAIX according to the provided instructions.
Note that the Docker solution is a valid option for getting started with DistAIX, but not for performance critical simulation runs.
Also, a connection to a result database system does not work out of the box using a Docker container.

## Package dependencies

### DBConnector
DistAIX uses the DBConnector library (as DistAIX this is an in-house development of the Institute for Automation of Complex Power Systems), to store simulation results in a database system consisting of PostgreSQL and Cassandra databases.
The library is included as a git submodule in DistAIX and compiled automatically by CMake if the right options are selected (see [here](install.md)).
If you plan to use DistAIX with the database functionality, please check the documentation [here](https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/dbconnector).
DBConnector has the following dependencies:
* [Cassandra CPP driver](https://github.com/datastax/cpp-driver.git), version 2.10.0 on Ubuntu 18.04
* PostgreSQL, packages `postgresql postgresql-contrib postgresql-server-dev-10` on Ubuntu 18.04

### FBS-components
DistAIX uses the FBS-components library (as DistAIX this is an in-house development of the Institute for Automation of Complex Power Systems) to model and solve electrical components such as load, generators, heat pumps, transformers, and lines.
The library is included as a git submodule in DistAIX and compiled automatically by CMake.
It has the following dependency:
* [GNU Scientific Library (GSL)](https://www.gnu.org/software/gsl/) and headers


### Google Protocol Buffers
DistAIX uses Google Protocol Buffers to serialize data before sending it to the database.
Hence it is only required if DistAIX is used with database.
The software can be obtained here:
* [Protocol Buffers Repository](https://github.com/protocolbuffers/protobuf), branch 3.3.x

Please follow the instructions provided in the repository linked above for details about build and install.


### RepastHPC
The agent-based modeling and simulation framework RepastHPC is used by DistAIX to parallelize the execution of the model.
RepastHPC itself is integrated as a git submodule in the `libs` folder.
The library has the following dependencies:
* MPI distribution (e.g. MPICH or Open MPI)
* Boost Libraries
    - system
    - filesystem
    - serialization
    - mpi 
* NetCDF libraries for C and C++ and headers (-dev packages!)
* Curl library and headers (not only the binary!)  

**Imortant remark for Boost mpi**: If you install this library via the package manager of your system, make sure it links to the correct MPI library that is used by your system. If your package manager does not support the installation of Boost MPI for your MPI distribution, you have to download, compile and install this library manually. In that case it is advisable to also install the other required Boost libraries manually for the same version of Boost to avoid incompatibilities.

### VILLASnode
The VILLASnode library is integrated as a git submodule into the DistAIX repo and compiled automatically by CMake if the right options are set (see [here](install.md)).
VILLASnode has several dependencies to other libraries.
Please check the [documentation of VILLASnode](https://villas.fein-aachen.org/doc/node-installation.html) for more information (Section "Prerequisites").
It depends on your use case of VILLASnode, which of the optional dependencies you need to install on your system.  

## HPC Cluster

If you plan to execute DistAIX on a high performance computing cluster using multiple computing nodes, please make sure that your MPI distribution is properly installed and accessible for your cluster.
The boost-mpi library has to link against the MPI distribution that you are using.
Depending on your setup, it can be neccessary to compile boost-mpi from source.
In that case, it is recommended to also compile the other required boost components (serialization, system, and filesystem) from source to avoid mismatches.

Also DistAIX itself has to be installed in such a way that it is accessible for all nodes in your cluster.
As the cluster setup can be highly specific for different hardware equipment, operating systems, and MPI distributions, no detailed instructions are provided here.
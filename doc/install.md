## DistAIX Compilation and Installation
DistAIX uses a build system based on CMake (>= version 3.6). 
To compile DistAIX, execute ``./build_and_install.sh`` in the ``build`` folder.
You can pass the following two options to this script:
* `-db`: enables database support; if this option is NOT specified, DistAIX cannot be used with the database
* `-villas`: enables VILLASnode interface support; if this option is NOT specified, DistAIX cannot be used with behaviors that use VILLASnode

The executable ``distaix`` is automatically installed to the ``bin`` folder.
If an error occurs, run the ``./cleanup.sh`` script and then again ``./build_and_install.sh`` in the build folder as first troubleshooting.
CMake will let you know if some required dependencies (see also [here](preparation.md)) are missing on your system.
In that case, install the missing packages and then proceed with the compilation of DistAIX.

On the first build attempt, CMake will issue the compile of the protocolbuffers libraries. This may take some time.
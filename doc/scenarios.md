# Scenario Structure

DistAIX uses its own scenario format, consisting of two comma-seperated-values (CSV) files. The `components.csv` holds all components that are simulated, whereas the `el_grid.csv` defines the topology of the simulated grid, i.e. the interconnection between components.

## components.csv

There are four different kinds of components, each with its own set of parameters, that can be specified.
Each components is represented as one row in the csv-file. The corresponding parameters are arranged in columns from left to right, seperated with a ", " (comma).

### Slack (columns: 3)

| Column | Parameter | Type     | Description                              |
| ------ | --------- | -------- | ---------------------------------------- |
| 1      | "1"       | `int`    | Slack has fixed identifier "1"           |
| 2      | "Slack"   | `string` | Slack has fixed type "Slack"             |
| 3      | V_nom     | `int`    | Nominal voltage of the slack agent  in V |

### Transformer (columns: 12)

| Column | Parameter     | Type     | Description                                 |
| ------ | ------------- | -------- | ------------------------------------------- |
| 1      | ID            | `int`    | Numeric identifier of the transformer agent |
| 2      | "Transformer" | `string` | Transformer has fixed type "Transformer"    |
| 3      | "0"           | `int`    | _Unused_                                    |
| 4      | "0"           | `int`    | _Unused_                                    |
| 5      | "0"           | `int`    | _Unused_                                    |
| 6      | Vnom1         | `double` | Nominal voltage of primary terminal in V    |
| 7      | Vnom2         | `double` | Nominal voltage of secondary terminal in V  |
| 8      | R             | `double` | Resistance in Ohm                           |
| 9      | X             | `double` | Reactance in in Ohm                         |
| 10     | Sr            | `double` | Rated apparent power in Watt                |
| 11     | N             | `int`    | Number of tap levels                        |
| 12     | range         | `int`    | Range of adjustable ratio in percent        |

### Node (columns: 6)

| Column | Parameter        | Type     | Description                                                                                     |
| ------ | ---------------- | -------- | ----------------------------------------------------------------------------------------------- |
| 1      | ID               | `int`    | Numeric identifier of the node agent                                                            |
| 2      | "Node"           | `string` | Node agent has fixed type "Node"                                                                |
| 3      | Transfomer ID    | `int`    | Numeric identifier of the transformer agent at the top of the subgrid to which the node belongs |
| 4      | "0"              | `int`    | _Unused_                                                                                        |
| 5      | "0"              | `int`    | _Unused_                                                                                        |
| 6      | Vnom             | `double` | Nominal voltage of the node in V                                                                |
| 7      | "0"              | `int`    | _Unused_                                                                                        |
| 8      | ICT Connectivity | `int`  | Indicates if node is connected to the communication network (`1` = connected / `0` = disconnected)|


### Prosumer (columns: 8)

| Column | Parameter      | Type     | Description                                                                    |
| ------ | -------------- | -------- | ------------------------------------------------------------------------------ |
| 1      | ID             | `int`    | Numeric identifier of the prosumer agent                                       |
| 2      | Type           | `string` | Type of the prosumer agent, see [types](#Types) for further information        |
| 3      | Subtype        | `string` | Subtype of the defined type, see [subtypes](#Subtypes) for further information |
| 4      | profile1 ID    | `int`    | Identifier of profile1, e.g. P profile of loads                                |
| 5      | profile2 ID    | `int`    | Identifier of profile2, e.g. Q profile of loads                                |
| 6      | Scaling factor | `double` | Scaling factor of profile1 and profile2 (only one factor for both profiles)    |
| 7      | S_r            | `double` | Rated apparent power of the prosumer in Watt                                   |
| 8      | pf             | `double` | Power factor of the component                                                  |

#### Types

Each prosumer can have one of the following types:
- Biofuel
- CHP (Combined Heat and Power)
- Compensator
- EV (Electric Vehicle)
- HP (Heatpump)
- Load
- PV (Photovoltaik)
- Storage
- Wind (Wind energy converter)

#### Subtypes

For each prosumer type, there exist predefined subtypes that can be used to create simulation scenarios.
The up-to-date subtypes can be found in the repository of the [DistAIX Scenario Generator](https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/scenariogenerator/-/tree/master/subtypes), a supplementary tool of the DistAIX Framework.

#### Profiles

Prosumers can have values that vary over time, e.g. loads can have changing real and reactive powers. This variation is defined in profiles. In these profiles, the first column defines points in time, while the other columns define the values of the changing variables for the existing subtypes. Example profiles for different prosumers can be found [here](https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/distaix/-/tree/master/examples/profiles).



## el_grid.csv

This file defines the topology of the simulated grid. Each connection, i.e. each line, is represented by one row. The corresponding paramters are arranged in columns from left to right, seperated with a ", " (comma).

### (columns: 10)

| Column | Parameter    | Type     | Description                                                                                                                                                      |
| ------ | ------------ | -------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1      | ID1          | `int`    | ID of the first component the line connects                                                                                                                      |
| 2      | ID2          | `string` | ID of the second component the line connects                                                                                                                     |
| 3      | Subtype      | `string` | _Unused_: This parameter is not used in the simulation but it can be useful to distinguish between different cable subtypes during configuration of the scenario |
| 4      | Length       | `float`  | Length of the line in km                                                                                                                                         |
| 5      | R            | `float`  | Resistance of the line in Ohm/km                                                                                                                                 |
| 6      | X            | `float`  | Reactance of the line in Ohm/km                                                                                                                                  |
| 7      | B            | `float`  | Magnetic flux density of the line in 1/(Ohm * km)                                                                                                                |
| 8      | G            | `float`  | Conductance of the line in 1/(Ohm * km)                                                                                                                          |
| 9      | Sr           | `float`  | Rated long-term apparent power of the line in Watt                                                                                                               |
| 10     | Sr_emergency | `float`  | Rated short-term apparent power of the line in Watt                                                                                                              |
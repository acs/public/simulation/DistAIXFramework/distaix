## Publications related to DistAIX

### Conference Papers
[S. Kolen, T. Isermann, S. Dähling and A. Monti, "Swarm behavior for distribution grid control," 2017 IEEE PES Innovative Smart Grid Technologies Conference Europe (ISGT-Europe), Torino, 2017, pp. 1-6.](https://doi.org/10.1109/ISGTEurope.2017.8260160)

### Journal Papers
[S. Kolen, S. Dähling, T. Isermann, and A. Monti, “Enabling the Analysis of Emergent Behavior in Future Electrical Distribution Systems Using Agent-Based Modeling and Simulation,” Complexity, vol. 2018, Article ID 3469325, 16 pages, 2018.](https://doi.org/10.1155/2018/3469325)

[S. Dähling, S. Kolen, and A. Monti, "Swarm-based automation of electrical power distribution and transmission system support", IET Cyber-Physical Systems: Theory and Applications, 2018](http://digital-library.theiet.org/content/journals/10.1049/iet-cps.2018.5001)

[S. Happ, S. Dähling, and A. Monti, "Scalable assessment method for agent-based control in cyber-physical distribution grids", IET Cyber-Physical Systems: Theory and Applications, 2020](https://doi.org/10.1049/iet-cps.2019.0096)
# Execution Guide
There are different execution scripts available in the `bin` folder.

## Execution on single compute node
Execute ``./run.sh -n 4`` to run a simulation with 4 processes.
The 4 can be replaced by the desired number of processes.
If the script is executed without option ``-n``, 4 processes are the default.

## Execution on multiple compute nodes

### Execution on multiple nodes in general
To execute DistAIX on multiple computing nodes, `mpiexec` requires the specification of hosts through the `-hosts` parameter.
You can specify multiple hosts, for example, in the following way: 

`mpiexec -n 24 -hosts=xxx.xxx.xxx.xxx,yyy.yyy.yyy.yyy ./distaix ../props/config.props ../props/model.props`

Here, `xxx.xxx.xxx.xxx` and `yyy.yyy.yyy.yyy` are IP adresses of two computing nodes of which one is the IP address (or the host name) of the node from which you start the simulation. 
Host names can also be used, depending on your network configuration.
Only the `-hosts` parameter is added compared to the execution in the `run.sh` script.
Please ensure that your MPI distribution is able to connect between all the hosts you are using.

### Execution in the SwarmGrid cluster of ACS
To run a simulation in the cluster of swarm{0,1,2,3} nodes you can use  the script `run_cluster.sh`.
Execute ``./run_cluster.sh -n 24 -h 0,1`` to run a simulation with 24 processes on hosts swarm0 and swarm1.
This script has to be started on one of the specified computed nodes.
Automatic ssh-key authentication (i.e. without password typing required) between the compute nodes has to be established before execution.
Processes are distributed in blocks to the nodes, i.e., in the example above, processes 0 to 11 go to swarm0 and and 12 - 23 to swarm1.
The MPI distribution determines automatically which communication medium (usually shared memory, Infiniband or Ethernet) is the fastest for each pair of processes that has to communicate during the simulation.

Additionally, this script accepts the option ``-i`` if IP over Infiniband (IPoIB) shall be used for inter node communication.

## Example output
```
######################################################################################
##################################### MODEL CONFIG ###################################
######################################################################################
Number of processes		=	2
Scenario folder:		=	../examples/grids/27_nodes/
Components file:		=	components.csv
El. grid file			=	el_grid.csv
Profile folder:			=	../examples/profiles/
Use latency matrix:		=	0
Interpolation:			=	linear
Number of simulation steps:	=	100
Size of simulation step in s:	=	3600
Agent distribution method:	=	workitem
Behavior type			=	1
Model type			=	0
Real time			=	0
############################# RESULT SAVING CONFIG ###################################
Result folder:			=	results
Loglevel:			=	normal
Rank reuslts csv:		=	0
Rank reuslts DB:		=	0
Agent results csv:		=	0
Agent results DB:		=	0
Cable results csv:		=	0
Cable results DB:		=	0
Save DF agent:			=	0
Save MR agent:			=	0
################################ LOGGING CONFIG ######################################
Log folder:			=	runlog
Rank logging:			=	1
Agent logging:			=	1
Agents to log:			=	2, 55
Cable logging:			=	0
Log DF agent:			=	0
Log MR agent:			=	0
######################################################################################
### Initializing model in all MPI processes...
 * Read profile data...
Done.
 * Creating agents in MPI processes...
---> Determine workitems
---> Start workitem distribution
     Number of leaf nodes: 3
     Number of workitems: 4
---> Create component agents
Done.
 * Initializing electrical network in all MPI processes...
---> creating electrical connections of local agents
---> requesting non local agents
---> creating electrical connections to non-local agents
Done.
 * Initializing communication network in all MPI processes... 
Done.
 * Initializing agent behaviors... 
Done.
### Model initialization finished in all MPI processes.

0%   10   20   30   40   50   60   70   80   90   100%
|----|----|----|----|----|----|----|----|----|----|
***************************************************


```
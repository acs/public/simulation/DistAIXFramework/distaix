#include "msgrouter.pb.h"
#include <iostream>

int serialize(char** bytes, int count);
void deserialize(char* bytes, int size);

int main(){
    char* bytes;
    int l = serialize(&bytes, 5);
    deserialize(bytes, l);
}

int serialize(char** bytes, int count){
    MsgRouter m;
    for(;count>0;count--){
        MsgRouter_Msg* message = m.add_messages();
        message->set_value(0.1234*count);
    }
    int size = m.ByteSizeLong();
    void* p = malloc(size);
    m.SerializeToArray(p,size);
    (*bytes) = (char *) p;
    return size;
}

void deserialize(char* bytes, int size){
    MsgRouter m;
    m.ParseFromArray(bytes, size);
    for(int i=0;i<m.messages_size();i++ ){
        MsgRouter_Msg msg = m.messages(i);
        std::cout << "Value " << msg.value() << std::endl;
    }
}
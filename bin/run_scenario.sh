#############################################################################
#
# This file is part of DistAIX
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

#! /bin/sh

# script runs distaix using run_cluster.sh script
# for different configurations 
# in the current folder and props in the props folder in the project root
#

cat ../props/model.props

sed "s,stop\.at.*$,stop.at=1000,g" -i ../props/model.props

sed "s,file\.scenario\.components.*$,file.scenario.components=../scenarios/scenario_1x_lv_rural_components.csv,g"   -i ../props/model.props
sed "s,file\.scenario\.elgrid.*$,file.scenario.elgrid=../scenarios/scenario_1x_lv_rural_grid.csv,g"   -i ../props/model.props

# scenario 1x490 agents:
./run_cluster.sh -n 1 -h 0 > sim_1x_1proc_1000steps.log
cp -r results results_1x_1proc_1000steps

./run_cluster.sh -n 2 -h 0 > sim_1x_2proc_1000steps.log
cp -r results results_1x_2proc_1000steps

./run_cluster.sh -n 4 -h 0 > sim_1x_4proc_1000steps.log
cp -r results results_1x_4proc_1000steps

./run_cluster.sh -n 8 -h 0 > sim_1x_8proc_1000steps.log
cp -r results results_1x_8proc_1000steps

./run_cluster.sh -n 16 -h 0 > sim_1x_16proc_1000steps.log
cp -r results results_1x_16proc_1000steps

./run_cluster.sh -n 32 -h 0 > sim_1x_32proc_1000steps.log
cp -r results results_1x_32proc_1000steps

# scenario 5x490 agents
sed "s,file\.scenario\.components.*$,file.scenario.components=../scenarios/scenario_5x_lv_rural_components.csv,g"   -i ../props/model.props
sed "s,file\.scenario\.elgrid.*$,file.scenario.elgrid=../scenarios/scenario_5x_lv_rural_grid.csv,g"   -i ../props/model.props
# scenario 1x490 agents:
./run_cluster.sh -n 1 -h 0 > sim_5x_1proc_1000steps.log
cp -r results results_5x_1proc_1000steps

./run_cluster.sh -n 2 -h 0 > sim_5x_2proc_1000steps.log
cp -r results results_5x_2proc_1000steps

./run_cluster.sh -n 4 -h 0 > sim_5x_4proc_1000steps.log
cp -r results results_5x_4proc_1000steps

./run_cluster.sh -n 5 -h 0 > sim_5x_5proc_1000steps.log
cp -r results results_5x_5proc_1000steps

./run_cluster.sh -n 6 -h 0 > sim_5x_6proc_1000steps.log
cp -r results results_5x_6proc_1000steps

./run_cluster.sh -n 10 -h 0 > sim_5x_10proc_1000steps.log
cp -r results results_5x_10proc_1000steps

./run_cluster.sh -n 20 -h 0 > sim_5x_20proc_1000steps.log
cp -r results results_5x_20proc_1000steps

./run_cluster.sh -n 40 -h 0,1 -i > sim_5x_40proc_1000steps.log
cp -r results results_5x_40proc_1000steps

# scenario 10x490 agents
sed "s,file\.scenario\.components.*$,file.scenario.components=../scenarios/scenario_10x_lv_rural_components.csv,g"   -i ../props/model.props
sed "s,file\.scenario\.elgrid.*$,file.scenario.elgrid=../scenarios/scenario_10x_lv_rural_grid.csv,g"   -i ../props/model.props

./run_cluster.sh -n 1 -h 0 > sim_10x_1proc_1000steps.log
cp -r results results_10x_1proc_1000steps

./run_cluster.sh -n 2 -h 0 > sim_10x_2proc_1000steps.log
cp -r results results_10x_2proc_1000steps

./run_cluster.sh -n 5 -h 0 > sim_10x_5proc_1000steps.log
cp -r results results_10x_5proc_1000steps

./run_cluster.sh -n 9 -h 0 > sim_10x_9proc_1000steps.log
cp -r results results_10x_9proc_1000steps

./run_cluster.sh -n 10 -h 0 > sim_10x_10proc_1000steps.log
cp -r results results_10x_10proc_1000steps

./run_cluster.sh -n 11 -h 0 > sim_10x_11proc_1000steps.log
cp -r results results_10x_11proc_1000steps

./run_cluster.sh -n 16 -h 0 > sim_10x_16proc_1000steps.log
cp -r results results_10x_16proc_1000steps

./run_cluster.sh -n 32 -h 0 > sim_10x_32proc_1000steps.log
cp -r results results_10x_32proc_1000steps

./run_cluster.sh -n 40 -h 0,1 -i > sim_10x_40proc_1000steps.log
cp -r results results_10x_40proc_1000steps

# scenario 20x490 agents
sed "s,file\.scenario\.components.*$,file.scenario.components=../scenarios/scenario_20x_lv_rural_components.csv,g"   -i ../props/model.props
sed "s,file\.scenario\.elgrid.*$,file.scenario.elgrid=../scenarios/scenario_20x_lv_rural_grid.csv,g"   -i ../props/model.props

./run_cluster.sh -n 1 -h 0 > sim_20x_1proc_1000steps.log
cp -r results results_20x_1proc_1000steps

./run_cluster.sh -n 2 -h 0 > sim_20x_2proc_1000steps.log
cp -r results results_20x_2proc_1000steps

./run_cluster.sh -n 4 -h 0 > sim_20x_4proc_1000steps.log
cp -r results results_20x_4proc_1000steps

./run_cluster.sh -n 5 -h 0 > sim_20x_5proc_1000steps.log
cp -r results results_20x_5proc_1000steps

./run_cluster.sh -n 10 -h 0 > sim_20x_10proc_1000steps.log
cp -r results results_20x_10proc_1000steps

./run_cluster.sh -n 19 -h 0 > sim_20x_19proc_1000steps.log
cp -r results results_20x_19proc_1000steps

./run_cluster.sh -n 20 -h 0 > sim_20x_20proc_1000steps.log
cp -r results results_20x_20proc_1000steps

./run_cluster.sh -n 21 -h 0 > sim_20x_21proc_1000steps.log
cp -r results results_20x_21proc_1000steps

./run_cluster.sh -n 32 -h 0 > sim_20x_32proc_1000steps.log
cp -r results results_20x_32proc_1000steps

./run_cluster.sh -n 40 -h 0,1 -i > sim_20x_40proc_1000steps.log
cp -r results results_20x_40proc_1000steps

./run_cluster.sh -n 80 -h 0,1,2 -i > sim_20x_80proc_1000steps.log
cp -r results results_20x_80proc_1000steps

# scenario 50x490 agents
sed "s,file\.scenario\.components.*$,file.scenario.components=../scenarios/scenario_50x_lv_rural_components.csv,g"   -i ../props/model.props
sed "s,file\.scenario\.elgrid.*$,file.scenario.elgrid=../scenarios/scenario_50x_lv_rural_grid.csv,g"   -i ../props/model.props

./run_cluster.sh -n 1 -h 0 > sim_50x_1proc_1000steps.log
cp -r results results_50x_1proc_1000steps

./run_cluster.sh -n 2 -h 0 > sim_50x_2proc_1000steps.log
cp -r results results_50x_2proc_1000steps

./run_cluster.sh -n 4 -h 0 > sim_50x_4proc_1000steps.log
cp -r results results_50x_4proc_1000steps

./run_cluster.sh -n 5 -h 0 > sim_50x_5proc_1000steps.log
cp -r results results_50x_5proc_1000steps

./run_cluster.sh -n 10 -h 0 > sim_50x_10proc_1000steps.log
cp -r results results_50x_10proc_1000steps

./run_cluster.sh -n 25 -h 0 > sim_50x_25proc_1000steps.log
cp -r results results_50x_25proc_1000steps

./run_cluster.sh -n 32 -h 0 > sim_50x_32proc_1000steps.log
cp -r results results_50x_32proc_1000steps

./run_cluster.sh -n 49 -h 0,1 -i > sim_50x_49proc_1000steps.log
cp -r results results_50x_49proc_1000steps

./run_cluster.sh -n 50 -h 0,1 -i > sim_50x_50proc_1000steps.log
cp -r results results_50x_50proc_1000steps

./run_cluster.sh -n 51 -h 0,1 -i > sim_50x_51proc_1000steps.log
cp -r results results_50x_51proc_1000steps

./run_cluster.sh -n 100 -h 0,1,2 -i > sim_50x_100proc_1000steps.log
cp -r results results_50x_100proc_1000steps

# scenario 100x490 agents
sed "s,file\.scenario\.components.*$,file.scenario.components=../scenarios/scenario_100x_lv_rural_components.csv,g"   -i ../props/model.props
sed "s,file\.scenario\.elgrid.*$,file.scenario.elgrid=../scenarios/scenario_100x_lv_rural_grid.csv,g"   -i ../props/model.props

./run_cluster.sh -n 1 -h 0 > sim_100x_1proc_1000steps.log
cp -r results results_100x_1proc_1000steps

./run_cluster.sh -n 2 -h 0 > sim_100x_2proc_1000steps.log
cp -r results results_100x_2proc_1000steps

./run_cluster.sh -n 4 -h 0 > sim_100x_4proc_1000steps.log
cp -r results results_100x_4proc_1000steps

./run_cluster.sh -n 5 -h 0 > sim_100x_5proc_1000steps.log
cp -r results results_100x_5proc_1000steps

./run_cluster.sh -n 10 -h 0 > sim_100x_10proc_1000steps.log
cp -r results results_100x_10proc_1000steps

./run_cluster.sh -n 20 -h 0 > sim_100x_20proc_1000steps.log
cp -r results results_100x_20proc_1000steps

./run_cluster.sh -n 25 -h 0 > sim_100x_25proc_1000steps.log
cp -r results results_100x_25proc_1000steps

./run_cluster.sh -n 50 -h 0,1 -i > sim_100x_50proc_1000steps.log
cp -r results results_100x_50proc_1000steps

./run_cluster.sh -n 99 -h 0,1,2 -i > sim_100x_99proc_1000steps.log
cp -r results results_100x_99proc_1000steps

./run_cluster.sh -n 100 -h 0,1,2 -i > sim_100x_100proc_1000steps.log
cp -r results results_100x_100proc_1000steps

./run_cluster.sh -n 101 -h 0,1,2 -i > sim_100x_101proc_1000steps.log
cp -r results results_100x_101proc_1000steps
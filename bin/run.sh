#############################################################################
#
# This file is part of DistAIX
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

#! /bin/sh

# script runs distaix with binary
# in the current folder and props in the props folder in the project root
#
# Its possible to specify how many processes should be
# used with the -n [COUNT] option
#
# Default count of processes are 4, specified by the
# PROCESS_N variable under this paragraph.

PROCESS_N=4
PROPERTY_FILE="../props/model.props"
CONFIG_FILE="../props/config.props"
cd "${0%/*}"

while getopts n:c:p:d: opt
do
	case $opt in
		n) PROCESS_N=$OPTARG;;
        c) CONFIG_FILE=$OPTARG;;
        p) PROPERTY_FILE=$OPTARG;;
        d) PROPERTY_DIRECTORY=$OPTARG
	esac
done

pwd

if [ -z ${PROPERTY_DIRECTORY+x} ]; then

    # No directory was specified, run single simulation for PROPERTY_FILE

    echo "################### START #######################"
    start_time=`date +%s%N`
    mpiexec -n $PROCESS_N ./distaix $CONFIG_FILE $PROPERTY_FILE 2> error
    end_time=`date +%s%N`
    echo "execution time was `expr $end_time - $start_time` ns."
    echo "######################## END ########################"
else
    # Directory including multiple property files was specified by PROPERTY_DIRECTORY,
    # run simulations for all of them

    echo "###### Start simulation of Series ######"
    echo "## Properties Directory: $PROPERTY_DIRECTORY "
    echo "## Identified following property files..."

    for prop_file in "$PROPERTY_DIRECTORY"/*
    do
        echo "## - $prop_file"
    done

    sleep 5

    for PROP_FILE in "$PROPERTY_DIRECTORY"/*
    do
        echo "################### START #######################"
        start_time=`date +%s%N`
        mpiexec -n $PROCESS_N ./distaix $CONFIG_FILE $PROP_FILE 2> error
        end_time=`date +%s%N`
        echo "execution time was `expr $end_time - $start_time` ns."
        echo "######################## END ########################"
    done

fi

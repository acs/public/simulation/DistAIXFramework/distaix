#############################################################################
#
# This file is part of DistAIX
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

#! /bin/sh

# script runs distaix using run_cluster.sh script
# for different configurations 
# in the current folder and props in the props folder in the project root
#

cat ../props/model.props

sed "s,stop\.at.*$,stop.at=1000,g" -i ../props/model.props

# scenario 10x490 agents
sed "s,file\.scenario\.components.*$,file.scenario.components=../scenarios/scenario_10x_lv_rural_components.csv,g"   -i ../props/model.props
sed "s,file\.scenario\.elgrid.*$,file.scenario.elgrid=../scenarios/scenario_10x_lv_rural_grid.csv,g"   -i ../props/model.props

./run_cluster.sh -n 1 -h 0 > sim_10x_1proc_1000steps.log
cp -r results results_10x_1proc_1000steps

./run_cluster.sh -n 2 -h 0 > sim_10x_2proc_1000steps.log
cp -r results results_10x_2proc_1000steps

./run_cluster.sh -n 3 -h 0 > sim_10x_3proc_1000steps.log
cp -r results results_10x_3proc_1000steps

./run_cluster.sh -n 4 -h 0 > sim_10x_4proc_1000steps.log
cp -r results results_10x_4proc_1000steps

./run_cluster.sh -n 5 -h 0 > sim_10x_5proc_1000steps.log
cp -r results results_10x_5proc_1000steps

./run_cluster.sh -n 6 -h 0 > sim_10x_6proc_1000steps.log
cp -r results results_10x_6proc_1000steps

./run_cluster.sh -n 7 -h 0 > sim_10x_7proc_1000steps.log
cp -r results results_10x_7proc_1000steps

./run_cluster.sh -n 8 -h 0 > sim_10x_8proc_1000steps.log
cp -r results results_10x_8proc_1000steps

./run_cluster.sh -n 9 -h 0 > sim_10x_9proc_1000steps.log
cp -r results results_10x_9proc_1000steps

./run_cluster.sh -n 10 -h 0 > sim_10x_10proc_1000steps.log
cp -r results results_10x_10proc_1000steps

./run_cluster.sh -n 11 -h 0 > sim_10x_11proc_1000steps.log
cp -r results results_10x_11proc_1000steps

./run_cluster.sh -n 12 -h 0 > sim_10x_12proc_1000steps.log
cp -r results results_10x_12proc_1000steps

./run_cluster.sh -n 13 -h 0 > sim_10x_13proc_1000steps.log
cp -r results results_10x_13proc_1000steps

./run_cluster.sh -n 14 -h 0 > sim_10x_14proc_1000steps.log
cp -r results results_10x_14proc_1000steps

./run_cluster.sh -n 15 -h 0 > sim_10x_15proc_1000steps.log
cp -r results results_10x_15proc_1000steps

./run_cluster.sh -n 16 -h 0 > sim_10x_16proc_1000steps.log
cp -r results results_10x_16proc_1000steps

./run_cluster.sh -n 17 -h 0 > sim_10x_17proc_1000steps.log
cp -r results results_10x_17proc_1000steps

./run_cluster.sh -n 18 -h 0 > sim_10x_18proc_1000steps.log
cp -r results results_10x_18proc_1000steps

./run_cluster.sh -n 19 -h 0 > sim_10x_19proc_1000steps.log
cp -r results results_10x_19proc_1000steps

./run_cluster.sh -n 20 -h 0 > sim_10x_20proc_1000steps.log
cp -r results results_10x_20proc_1000steps

./run_cluster.sh -n 21 -h 0 > sim_10x_21proc_1000steps.log
cp -r results results_10x_21proc_1000steps

./run_cluster.sh -n 22 -h 0 > sim_10x_22proc_1000steps.log
cp -r results results_10x_22proc_1000steps

./run_cluster.sh -n 23 -h 0 > sim_10x_23proc_1000steps.log
cp -r results results_10x_23proc_1000steps

./run_cluster.sh -n 24 -h 0 > sim_10x_24proc_1000steps.log
cp -r results results_10x_24proc_1000steps

./run_cluster.sh -n 25 -h 0 > sim_10x_25proc_1000steps.log
cp -r results results_10x_25proc_1000steps

./run_cluster.sh -n 26 -h 0 > sim_10x_26proc_1000steps.log
cp -r results results_10x_26proc_1000steps

./run_cluster.sh -n 27 -h 0 > sim_10x_27proc_1000steps.log
cp -r results results_10x_27proc_1000steps

./run_cluster.sh -n 28 -h 0 > sim_10x_28proc_1000steps.log
cp -r results results_10x_28proc_1000steps

./run_cluster.sh -n 29 -h 0 > sim_10x_29proc_1000steps.log
cp -r results results_10x_29proc_1000steps

./run_cluster.sh -n 30 -h 0 > sim_10x_30proc_1000steps.log
cp -r results results_10x_30proc_1000steps

./run_cluster.sh -n 31 -h 0 > sim_10x_31proc_1000steps.log
cp -r results results_10x_31proc_1000steps

./run_cluster.sh -n 32 -h 0 > sim_10x_32proc_1000steps.log
cp -r results results_10x_32proc_1000steps

./run_cluster.sh -n 33 -h 0 > sim_10x_33proc_1000steps.log
cp -r results results_10x_33proc_1000steps

./run_cluster.sh -n 34 -h 0 > sim_10x_34proc_1000steps.log
cp -r results results_10x_34proc_1000steps

./run_cluster.sh -n 35 -h 0 > sim_10x_35proc_1000steps.log
cp -r results results_10x_35proc_1000steps

./run_cluster.sh -n 36 -h 0 > sim_10x_36proc_1000steps.log
cp -r results results_10x_36proc_1000steps

./run_cluster.sh -n 37 -h 0 > sim_10x_37proc_1000steps.log
cp -r results results_10x_37proc_1000steps

./run_cluster.sh -n 38 -h 0 > sim_10x_38proc_1000steps.log
cp -r results results_10x_38proc_1000steps

./run_cluster.sh -n 39 -h 0 > sim_10x_39proc_1000steps.log
cp -r results results_10x_39proc_1000steps

./run_cluster.sh -n 40 -h 0 > sim_10x_40proc_1000steps.log
cp -r results results_10x_40proc_1000steps

./run_cluster.sh -n 41 -h 0 > sim_10x_41proc_1000steps.log
cp -r results results_10x_41proc_1000steps

./run_cluster.sh -n 42 -h 0 > sim_10x_42proc_1000steps.log
cp -r results results_10x_42proc_1000steps

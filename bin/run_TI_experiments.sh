#############################################################################
#
# This file is part of DistAIX
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

#!/bin/bash

# script runs distaix with binary
# in the current folder and props in the props folder in the project root
SWARM=1
WORLDSIZE=24

STEPSIZE="step.size=0.01"
STEPS="stop.at=40000"

SCENARIO_DIR="dir.scenario=/global/work/share/swarmgrid/usecase/IFHT_UW_677_v1/"
PROFILES="dir.profiles=/global/work/share/swarmgrid/usecase/IFHT_UW_677_v1/h3853_3858/"

COMPONENTS_FILE="file.scenario.components=h3853_3858/components_initSOC.csv"
ELGRID_FILE="file.scenario.elgrid=el_grid.csv"

LATENCY_FILE_FO="file.scenario.latency=commdata/exp_latency_FO_NEW_UW_677.csv"
LATENCY_FILE_HSPA="file.scenario.latency=commdata/exp_latency_HSPA_NEW_UW_677.csv"
LATENCY_FILE_COST_MIN_PLC128="file.scenario.latency=commdata/exp_latency_cost_min_PLC128_UW_677.csv"
LATENCY_FILE_COST_MIN_PLC500="file.scenario.latency=commdata/exp_latency_cost_min_PLC500_UW_677.csv"

COMM_ON="ctrl.type=1"

DB_ON="db.use=1"
DB_OFF="db.use=0"
DB_LOGLEVEL="db.loglevel=normal"

CSV_OFF="csv.use.ranks=0 csv.use.agents=0 csv.use.edges=0"

INTERPOLATION_LINEAR="profile.interpolation_type=linear"
INTERPOLATION_HOLD="profile.interpolation_type=hold"

USE_COMM_DATA="use.commdata=1"

DIST_METHOD="distribution_method=workitem"
MODEL_TYPE="model.type=0"

timestamp=`date +%Y%m%d-%H%M%S`

ARG_LIST_DB_ON="${SCENARIO_DIR} ${COMPONENTS_FILE} ${ELGRID_FILE} ${PROFILES} ${STEPSIZE} ${STEPS} ${DB_ON} ${DB_LOGLEVEL} ${CSV_OFF} ${INTERPOLATION_LINEAR} ${DIST_METHOD} ${MODEL_TYPE} ${USE_COMM_DATA} ${COMM_ON}"
ARG_LIST_DB_OFF="${SCENARIO_DIR} ${COMPONENTS_FILE} ${ELGRID_FILE} ${PROFILES} ${STEPSIZE} ${STEPS} ${DB_OFF} ${DB_LOGLEVEL} ${CSV_OFF} ${INTERPOLATION_LINEAR} ${DIST_METHOD} ${MODEL_TYPE} ${USE_COMM_DATA} ${COMM_ON}"

#echo "Run experiment for HSPA"
#mkdir results
#mkdir results/agents/
#./run_cluster.sh -n${WORLDSIZE} -h${SWARM} ${LATENCY_FILE_HSPA} ${ARG_LIST_DB_OFF} > "${timestamp}"_sim_HSPA.log
#
##save communication results in seperate folder
#mv MR_commdata "${timestamp}"_MR_commdata_HSPA
##save results in seperate folder
#mv results "${timestamp}"_results_HSPA
#
#echo "Run experiment for FO"
#mkdir results
#mkdir results/agents/
#./run_cluster.sh -n${WORLDSIZE} -h${SWARM} ${LATENCY_FILE_FO} ${ARG_LIST_DB_OFF} > "${timestamp}"_sim_FO.log
#
##save communication results in seperate folder
#mv MR_commdata "${timestamp}"_MR_commdata_FO
##save results in seperate folder
#mv results "${timestamp}"_results_FO



echo "Run experiment for cost min PLC 128 kBit/s"
mkdir results
mkdir results/agents/
./run_cluster.sh -n${WORLDSIZE} -h${SWARM} ${LATENCY_FILE_COST_MIN_PLC128} ${ARG_LIST_DB_OFF} > "${timestamp}"_sim_cost_min_PLC128.log

#save communication matrices in separate folder
mv MR_commdata "${timestamp}"_MR_commdata_cost_min_PLC128
#save results in seperate folder
mv results "${timestamp}"_results_cost_min_PLC128

echo "Run experiment for cost min PLC 500 kBit/s"
mkdir results
mkdir results/agents/
./run_cluster.sh -n${WORLDSIZE} -h${SWARM} ${LATENCY_FILE_COST_MIN_PLC500} ${ARG_LIST_DB_OFF} > "${timestamp}"_sim_cost_min_PLC500.log

#save communication matrices in separate folder
mv MR_commdata "${timestamp}"_MR_commdata_cost_min_PLC500
#save results in seperate folder
mv results "${timestamp}"_results_cost_min_PLC500

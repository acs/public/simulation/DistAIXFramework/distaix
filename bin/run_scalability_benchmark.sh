#############################################################################
#
# This file is part of DistAIX
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

#!/bin/bash

# script runs distaix with binary
# in the current folder and props in the props folder in the project root
#
# Its purpose is to benchmark the scalability of distaix based on the 177 LV rural grid
# The used number of processes and the execution time in [ns] will be logged in csv files
#
# Options:
# -comm: benchmark with agent communication enabled (Message Routers are used); as many processes as feeders in the grid
# -nocomm: benchmark without agent communication (Message Routers are NOT used); as many processes as feeders in the grid
# -lvcomm: benchmark with agent communication enabled (Message Routers are used); as many processes as LV grids in the grid
# -lvnocomm: benchmark without agent communication (Message Routers are NOT used); as many processes as LV grids in the grid
#

MAX_PROCESS_N=4
PROCESS_COUNTER=1

STEPSIZE="step.size=1"
STEPS="stop.at=1000"

PROFILES="dir.profiles=../profiles/paper/scalability/"

DIR_1x="dir.scenario=../scenarios/1x_lv_rural/"
DIR_2x="dir.scenario=../scenarios/scalability/2x_lv_rural/"
DIR_5x="dir.scenario=../scenarios/scalability/5x_lv_rural/"
DIR_10x="dir.scenario=../scenarios/scalability/10x_lv_rural/"
DIR_15x="dir.scenario=../scenarios/scalability/15x_lv_rural/"
DIR_20x="dir.scenario=../scenarios/scalability/20x_lv_rural/"
DIR_25x="dir.scenario=../scenarios/scalability/25x_lv_rural/"
DIR_30x="dir.scenario=../scenarios/scalability/30x_lv_rural/"
DIR_35x="dir.scenario=../scenarios/scalability/35x_lv_rural/"
DIR_40x="dir.scenario=../scenarios/scalability/40x_lv_rural/"
DIR_45x="dir.scenario=../scenarios/scalability/45x_lv_rural/"
DIR_50x="dir.scenario=../scenarios/scalability/50x_lv_rural/"
DIR_100x="dir.scenario=../scenarios/scalability/100x_lv_rural/"
DIR_250x="dir.scenario=../scenarios/scalability/250x_lv_rural/"
DIR_500x="dir.scenario=../scenarios/scalability/500x_lv_rural/"
DIR_750x="dir.scenario=../scenarios/scalability/750x_lv_rural/"
DIR_1000x="dir.scenario=../scenarios/scalability/1000x_lv_rural/"

COMM_ON="ctrl.type=1"
COMM_OFF="ctrl.type=0"

DB_OFF="db.use=0"
CSV_OFF="csv.use.ranks=0 csv.use.agents=0 csv.use.edges=0"

INTERPOLATION="profile.interpolation_type=linear"
NO_COMM_DATA="use.commdata=0"

DIST_METHOD="distribution_method=workitem"
MODEL_TYPE="model.type=0"

ARG_LIST="${STEPSIZE} ${STEPS} ${PROFILES} ${DB_OFF} ${CSV_OFF} ${INTERPOLATION} ${DIST_METHOD} ${MODEL_TYPE} ${NO_COMM_DATA}"


FILENAME_NO_COMM=scalability_benchmark_no_comm.csv
FILENAME_WITH_COMM=scalability_benchmark_with_comm.csv
FILENAME_NO_COMM_FEWPROCS=scalability_benchmark_no_comm_few_procs.csv
FILENAME_WITH_COMM_FEWPROCS=scalability_benchmark_with_comm_few_procs.csv
timestamp=`date +%Y%m%d-%H%M%S`
FILENAME_NO_COMM="$timestamp"_"$FILENAME_NO_COMM"
FILENAME_WITH_COMM="$timestamp"_"$FILENAME_WITH_COMM"
FILENAME_NO_COMM_FEWPROCS="$timestamp"_"$FILENAME_NO_COMM_FEWPROCS"
FILENAME_WITH_COMM_FEWPROCS="$timestamp"_"$FILENAME_WITH_COMM_FEWPROCS"


for ARG in "$@"
do

    if [ "$ARG" = "-comm" ]; then
        echo "########## Executing benchmark with agent communication and #processes=#feeder...."
        echo "################### Saving results in:" ${FILENAME_WITH_COMM}
        echo 'scenario size factor,runtime in ns' >> ${FILENAME_WITH_COMM}

        #benchmark with communication between agents (Message Routers are used, swarm intelligence is enabled)

        #1x_lv_rural (4 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n4 -h0 ${COMM_ON} ${DIR_1x} ${ARG_LIST}
        end=`date +%s%N`
        echo '1,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #2x_lv_rural (8 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n8 -h0 ${COMM_ON} ${DIR_2x} ${ARG_LIST}
        end=`date +%s%N`
        echo '2,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #5x_lv_rural (20 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n20 -h0 ${COMM_ON} ${DIR_5x} ${ARG_LIST}
        end=`date +%s%N`
        echo '5,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #10x_lv_rural (40 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n40 -h0,1 ${COMM_ON} ${DIR_10x} ${ARG_LIST}
        end=`date +%s%N`
        echo '10,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #15x_lv_rural (60 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n60 -h0,1,2 ${COMM_ON} ${DIR_15x} ${ARG_LIST}
        end=`date +%s%N`
        echo '15,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #20x_lv_rural (80 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n80 -h0,1,2,3 ${COMM_ON} ${DIR_20x} ${ARG_LIST}
        end=`date +%s%N`
        echo '20,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #25x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_ON} ${DIR_25x} ${ARG_LIST}
        end=`date +%s%N`
        echo '25,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #30x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_ON} ${DIR_30x} ${ARG_LIST}
        end=`date +%s%N`
        echo '30,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #35x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_ON} ${DIR_35x} ${ARG_LIST}
        end=`date +%s%N`
        echo '35,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #40x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_ON} ${DIR_40x} ${ARG_LIST}
        end=`date +%s%N`
        echo '40,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #45x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_ON} ${DIR_45x} ${ARG_LIST}
        end=`date +%s%N`
        echo '45,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #50x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_ON} ${DIR_50x} ${ARG_LIST}
        end=`date +%s%N`
        echo '50,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #100x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_ON} ${DIR_100x} ${ARG_LIST}
        end=`date +%s%N`
        echo '100,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #250x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_ON} ${DIR_250x} ${ARG_LIST}
        end=`date +%s%N`
        echo '250,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #500x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_ON} ${DIR_500x} ${ARG_LIST}
        end=`date +%s%N`
        echo '500,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #750x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_ON} ${DIR_750x} ${ARG_LIST}
        end=`date +%s%N`
        echo '750,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}

        #1000x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_ON} ${DIR_1000x} ${ARG_LIST}
        end=`date +%s%N`
        echo '1000,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM}
        echo "########## DONE."

    elif [ "$ARG" = "-nocomm" ]; then

        echo "########## Executing benchmark without agent communication and #processes=#feeder...."
        echo "################### Saving results in:" ${FILENAME_NO_COMM}
        echo 'scenario size factor,runtime in ns' >> ${FILENAME_NO_COMM}
        #benchmark for no communication between agents (Message Routers not used, just electrical part is simulated)

        #1x_lv_rural (4 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n4 -h0 ${COMM_OFF} ${DIR_1x} ${ARG_LIST}
        end=`date +%s%N`
        echo '1,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #2x_lv_rural (8 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n8 -h0 ${COMM_OFF} ${DIR_2x} ${ARG_LIST}
        end=`date +%s%N`
        echo '2,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #5x_lv_rural (20 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n20 -h0 ${COMM_OFF} ${DIR_5x} ${ARG_LIST}
        end=`date +%s%N`
        echo '5,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #10x_lv_rural (40 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n40 -h0,1 ${COMM_OFF} ${DIR_10x} ${ARG_LIST}
        end=`date +%s%N`
        echo '10,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #15x_lv_rural (60 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n60 -h0,1,2 ${COMM_OFF} ${DIR_15x} ${ARG_LIST}
        end=`date +%s%N`
        echo '15,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #20x_lv_rural (80 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n80 -h0,1,2,3 ${COMM_OFF} ${DIR_20x} ${ARG_LIST}
        end=`date +%s%N`
        echo '20,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #25x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_OFF} ${DIR_25x} ${ARG_LIST}
        end=`date +%s%N`
        echo '25,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #30x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_OFF} ${DIR_30x} ${ARG_LIST}
        end=`date +%s%N`
        echo '30,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #35x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_OFF} ${DIR_35x} ${ARG_LIST}
        end=`date +%s%N`
        echo '35,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #40x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_OFF} ${DIR_40x} ${ARG_LIST}
        end=`date +%s%N`
        echo '40,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #45x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_OFF} ${DIR_45x} ${ARG_LIST}
        end=`date +%s%N`
        echo '45,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #50x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_OFF} ${DIR_50x} ${ARG_LIST}
        end=`date +%s%N`
        echo '50,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #100x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_OFF} ${DIR_100x} ${ARG_LIST}
        end=`date +%s%N`
        echo '100,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #250x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_OFF} ${DIR_250x} ${ARG_LIST}
        end=`date +%s%N`
        echo '250,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #500x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_OFF} ${DIR_500x} ${ARG_LIST}
        end=`date +%s%N`
        echo '500,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #750x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_OFF} ${DIR_750x} ${ARG_LIST}
        end=`date +%s%N`
        echo '750,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}

        #1000x_lv_rural (96 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n96 -h0,1,2,3 ${COMM_OFF} ${DIR_1000x} ${ARG_LIST}
        end=`date +%s%N`
        echo '1000,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM}
        echo "########## DONE."

    elif [ "$ARG" = "-lvcomm" ]; then

        echo "########## Executing benchmark with agent communication and #processes=#LV grids...."
        echo "################### Saving results in:" ${FILENAME_WITH_COMM_FEWPROCS}
        echo 'scenario size factor,runtime in ns' >> ${FILENAME_WITH_COMM_FEWPROCS}
        #benchmark with communication between agents (Message Routers are used, swarm intelligence is enabled)

        #1x_lv_rural (1 process)
        start=`date +%s%N`
        ./run_cluster.sh -n1 -h0 ${COMM_ON} ${DIR_1x} ${ARG_LIST}
        end=`date +%s%N`
        echo '1,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM_FEWPROCS}

        #2x_lv_rural (2 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n2 -h0 ${COMM_ON} ${DIR_2x} ${ARG_LIST}
        end=`date +%s%N`
        echo '2,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM_FEWPROCS}

        #5x_lv_rural (5 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n5 -h0 ${COMM_ON} ${DIR_5x} ${ARG_LIST}
        end=`date +%s%N`
        echo '5,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM_FEWPROCS}

        #10x_lv_rural (10 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n10 -h0 ${COMM_ON} ${DIR_10x} ${ARG_LIST}
        end=`date +%s%N`
        echo '10,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM_FEWPROCS}

        #15x_lv_rural (15 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n15 -h0 ${COMM_ON} ${DIR_15x} ${ARG_LIST}
        end=`date +%s%N`
        echo '15,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM_FEWPROCS}

        #20x_lv_rural (20 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n20 -h0 ${COMM_ON} ${DIR_20x} ${ARG_LIST}
        end=`date +%s%N`
        echo '20,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM_FEWPROCS}

        #25x_lv_rural (25 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n25 -h0,1 ${COMM_ON} ${DIR_25x} ${ARG_LIST}
        end=`date +%s%N`
        echo '25,'`expr ${end} - ${start}` >> ${FILENAME_WITH_COMM_FEWPROCS}
        echo "########## DONE."

    elif [ "$ARG" = "-lvnocomm" ]; then

        echo "########## Executing benchmark without agent communication and #processes=#LV grids...."
        echo "################### Saving results in:" ${FILENAME_NO_COMM_FEWPROCS}
        echo 'scenario size factor,runtime in ns' >> ${FILENAME_NO_COMM_FEWPROCS}
        #benchmark for no communication between agents (Message Routers not used, just electrical part is simulated)

        #1x_lv_rural (1 process)
        start=`date +%s%N`
        ./run_cluster.sh -n1 -h0 ${COMM_OFF} ${DIR_1x} ${ARG_LIST}
        end=`date +%s%N`
        echo '1,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM_FEWPROCS}

        #2x_lv_rural (2 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n2 -h0 ${COMM_OFF} ${DIR_2x} ${ARG_LIST}
        end=`date +%s%N`
        echo '2,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM_FEWPROCS}

        #5x_lv_rural (5 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n5 -h0 ${COMM_OFF} ${DIR_5x} ${ARG_LIST}
        end=`date +%s%N`
        echo '5,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM_FEWPROCS}

        #10x_lv_rural (10 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n10 -h0 ${COMM_OFF} ${DIR_10x} ${ARG_LIST}
        end=`date +%s%N`
        echo '10,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM_FEWPROCS}

        #15x_lv_rural (15 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n15 -h0 ${COMM_OFF} ${DIR_15x} ${ARG_LIST}
        end=`date +%s%N`
        echo '15,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM_FEWPROCS}

        #20x_lv_rural (20 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n20 -h0 ${COMM_OFF} ${DIR_20x} ${ARG_LIST}
        end=`date +%s%N`
        echo '20,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM_FEWPROCS}

        #25x_lv_rural (25 processes)
        start=`date +%s%N`
        ./run_cluster.sh -n25 -h0,1 ${COMM_OFF} ${DIR_25x} ${ARG_LIST}
        end=`date +%s%N`
        echo '25,'`expr ${end} - ${start}` >> ${FILENAME_NO_COMM_FEWPROCS}
        echo "########## DONE."
    else
        echo "Unknown option: " "$ARG" " --> Exit!"
    fi

done






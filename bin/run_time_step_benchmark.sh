#############################################################################
#
# This file is part of DistAIX
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

#!/bin/bash

# script runs the time step benchmark for distaix
#
# Options:
# -f <number>: maximum number of feeders
# -l <number>: maximum feeder length (in nodes)
# -lvcomm: benchmark with agent communication enabled (Message Routers are used); as many processes as LV grids in the grid
# -lvnocomm: benchmark without agent communication (Message Routers are NOT used); as many processes as LV grids in the grid
###################

#default values for script options
MAX_FEEDERS=5
MAX_FEEDER_LENGTH=20
SWARM=3
#SCENARIO_DIR_START="/global/work/share/swarmgrid/timestepbench/v1/2018-06-11_13-28-10_L"
#SCENARIO_DIR_START="/global/work/share/swarmgrid/timestepbench/v2/2018-06-12_09-52-59_L"
#SCENARIO_DIR_START="/global/work/share/swarmgrid/timestepbench/v3/2018-06-12_14-57-19_L"
#SCENARIO_DIR_START="/global/work/share/swarmgrid/timestepbench/v4/2018-06-12_16-43-01_L"
SCENARIO_DIR_START="/global/work/share/swarmgrid/timestepbench/v5/2018-06-12_17-44-40_L"
SCENARIO_DIR_MIDDLE="_F"

#counter for number of arguments passed to the script
ARG_COUNTER=0

#DistAIX configuration
STEPSIZE="step.size=1"
STEPS="stop.at=1000"

COMM_ON="ctrl.type=1"
COMM_OFF="ctrl.type=0"

DB_OFF="db.use=0"
CSV_OFF="csv.use.ranks=1 csv.use.agents=0 csv.use.edges=0"

INTERPOLATION="profile.interpolation_type=hold"
NO_COMM_DATA="use.commdata=0"

DIST_METHOD="distribution_method=workitem"
MODEL_TYPE="model.type=0"
LOG_RANKS="db.use.ranks=0"

#TODO create profile files for SHF loads with steps and ramp
PROFILES="dir.profiles=../../distaix-scenarios/profiles/timestepbench/step/"

# create argument list for distAIX
ARG_LIST="${STEPSIZE} ${STEPS} ${PROFILES} ${DB_OFF} ${CSV_OFF} ${INTERPOLATION} ${DIST_METHOD} ${MODEL_TYPE} ${NO_COMM_DATA} ${COMM_OFF} ${LOG_RANKS}"

#file in which overall simulation time measurements are saved
timestamp=`date +%Y%m%d-%H%M%S`
FILENAME_SIM_TIME_RESULTS="$timestamp"_"sim_time_results.csv"
echo "length of feeders,number of feeders,sim time in ns" >> ${FILENAME_SIM_TIME_RESULTS}


# function to simulate a model
function simulate(){
    scenario=$1
    length=$2
    feeders=$3
    file=$4
    time=$5
    textlength=$6
    textfeeders=$7

    scenarioarg="dir.scenario=""${scenario}"
    echo "Simulating " "${scenario}" "..."
    start=`date +%s%N`
    #run simulation using another script
    if [ "${feeders}" -lt "25" ]    
    then
        ./run_cluster.sh -n${feeders} -h${SWARM} ${scenarioarg} ${ARG_LIST}
    else
        ./run_cluster.sh -n24 -h${SWARM} ${scenarioarg} ${ARG_LIST}
    fi
    end=`date +%s%N`
    echo "${length}","${feeders}",`expr ${end} - ${start}` >> ${file}

    #save result data
    resdir=${time}_L${textlength}_F${textfeeders}
    mkdir ${resdir}
    mv ./results ${resdir}
}


#read command line arguments
while getopts f:l: opt
do
	case ${opt} in
		f)
			MAX_FEEDERS=$OPTARG
			ARG_COUNTER=$((ARG_COUNTER+1))
			;;
		l)
			MAX_FEEDER_LENGTH=$OPTARG
			ARG_COUNTER=$((ARG_COUNTER+1))
			;;
	esac
done

echo "Max feeders: " ${MAX_FEEDERS}
echo "Max feeder length: " ${MAX_FEEDER_LENGTH}

#create array of scenario folders and simulate each scenario that is found
SCENARIO_DIRS=
COUNTER=0
for length in `seq 1 ${MAX_FEEDER_LENGTH}`;
do
    for feeders in `seq 1 ${MAX_FEEDERS}`;
    do
        #add leading zeros to match with folder names where necessary
        if [ "${length}" -lt  "10" ]
        then
            textlength="00""${length}"
        elif [ "${length}" -lt  "100" ]
        then
            textlength="0""${length}"
        else
            textlength="${length}"
        fi

        if [ "${feeders}" -lt  "10" ]
        then
            textfeeders="00""${feeders}"
        elif [ "${feeders}" -lt  "100" ]
        then
            textfeeders="0""${feeders}"
        else
            textfeeders="${feeders}"
        fi

        NEW_DIR="${SCENARIO_DIR_START}${textlength}${SCENARIO_DIR_MIDDLE}${textfeeders}""/"

        #test if new directory exists
        if [ -d "${NEW_DIR}" ]
        then
            SCENARIO_DIRS="${SCENARIO_DIRS} ${NEW_DIR}"
            COUNTER=$((COUNTER+1))
            echo "Added scenario directory: " "${NEW_DIR}"
            simulate ${NEW_DIR} ${length} ${feeders} ${FILENAME_SIM_TIME_RESULTS} ${timestamp} ${textlength} ${textfeeders}
        else
            #ignore if directory does not exist
            echo "Ignoring because not a directory: " "${NEW_DIR}"
        fi

    done
done

echo "The following " "${COUNTER}" " scenarios have been simulated:"
echo ${SCENARIO_DIRS}

echo "Done."

#############################################################################
#
# This file is part of DistAIX
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

#! /bin/sh

# script runs distaix with binary
# in the current folder and props in the props folder in the project root
#
# Its possible to specify how many processes should be 
# used with the -n [COUNT] option
#
# Default count of processes are 4, specified by the 
# PROCESS_N variable under this paragraph.

PROCESS_N=4

##
#
# Host configuration
#
##

#SWARM0_ETH=137.226.133.156
#SWARM1_ETH=137.226.133.157
#SWARM2_ETH=137.226.133.158
#SWARM3_ETH=137.226.133.159

#SWARM0_INF=10.0.0.20
#SWARM1_INF=10.0.0.21
#SWARM2_INF=10.0.0.22
#SWARM3_INF=10.0.0.23

SWARM0=swarm0
SWARM1=swarm1
SWARM2=swarm2
SWARM3=swarm3

#USE_INF=false

cd "${0%/*}"

set_hosts() {
	HOSTS=""
	#echo "$HOSTS_ARG" | awk -F "," '{print NF-1}'
	NUMBER_OF_COMMAS="$(echo "$HOSTS_ARG" | awk -F "," '{print NF-1}')"
	NUMBER_OF_HOSTS=$(( 1+NUMBER_OF_COMMAS ))
	#echo "$HOSTS_ARG"
	#echo "$NUMBER_OF_HOSTS"
	RANKS_PER_HOST=$(( PROCESS_N/NUMBER_OF_HOSTS ))
	REST_RANKS=$(( PROCESS_N%NUMBER_OF_HOSTS ))

	#echo "$RANKS_PER_HOST"
	#echo "$REST_RANKS"

    if [[ $NUMBER_OF_HOSTS == 1 ]]
    then
        case $HOSTS_ARG in
            *0*)
                HOSTS+="$SWARM0,"
                ;;&
            *1*)
                HOSTS+="$SWARM1,"
                ;;&
            *2*)
                HOSTS+="$SWARM2,"
                ;;&
            *3*)
                HOSTS+="$SWARM3,"
                ;;&
        esac
    else
        COUNTER=0
        case $HOSTS_ARG in
            *0*)
                if [[ $COUNTER == 0 ]]
                then
                    RANKS=$(( RANKS_PER_HOST+REST_RANKS ))
                    HOSTS+="$SWARM0:$RANKS,"
                else
                    HOSTS+="$SWARM0:$RANKS_PER_HOST,"
                fi
                #HOSTS+="$SWARM0,"
                (( COUNTER++ ))
                ;;&
            *1*)
                if [[ $COUNTER == 0 ]]
                then
                    RANKS=$(( RANKS_PER_HOST + REST_RANKS ))
                    HOSTS+="$SWARM1:$RANKS,"
                else
                    HOSTS+="$SWARM1:$RANKS_PER_HOST,"
                fi

                #HOSTS+="$SWARM1,"
                (( COUNTER++ ))
                ;;&
            *2*)
                if [[ $COUNTER == 0 ]]
                then
                    RANKS=$(( RANKS_PER_HOST + REST_RANKS ))
                    HOSTS+="$SWARM2:$RANKS,"
                else
                    HOSTS+="$SWARM2:$RANKS_PER_HOST,"
                fi
                #HOSTS+="$SWARM2,"
                (( COUNTER++ ))
                ;;&
            *3*)
                if [[ $COUNTER == 0 ]]
                then
                    RANKS=$(( RANKS_PER_HOST + REST_RANKS ))
                    HOSTS+="$SWARM3:$RANKS,"
                else
                    HOSTS+="$SWARM3:$RANKS_PER_HOST,"
                fi
                #HOSTS+="$SWARM3,"
                (( COUNTER++ ))
                ;;&
        esac
	fi
}

#set_eth(){
#	SWARM0=$SWARM0_ETH
#	SWARM1=$SWARM1_ETH
#	SWARM2=$SWARM2_ETH
#	SWARM3=$SWARM3_ETH
#}

#set_inf(){
#	SWARM0=$SWARM0_INF
#	SWARM1=$SWARM1_INF
#	SWARM2=$SWARM2_INF
#	SWARM3=$SWARM3_INF
#}


echo "################### START #######################"

ARG_COUNTER=0

while getopts n:h:i opt
do
	case $opt in
		n) 
			PROCESS_N=$OPTARG
			ARG_COUNTER=$((ARG_COUNTER+1))
			;;
		h) 
			HOSTS_ARG=$OPTARG
			ARG_COUNTER=$((ARG_COUNTER+1))
			;;
		#i)
		#	USE_INF=true
		#	ARG_COUNTER=$((ARG_COUNTER+1))
		#	;;
	esac
done

#if [[ $USE_INF == true ]]
#then
#	set_inf
#else
#	set_eth
#fi


if [[ $HOSTS_ARG == "" ]]
then
	HOSTS=127.0.0.1
else
	set_hosts
fi


set -x
start_time=`date +%s%N`
#export PSP_DEBUG=1
mpiexec -n $PROCESS_N -hosts=$HOSTS ./distaix ../props/config.props ../props/model.props 2> error
#mpiexec -n $PROCESS_N -hosts=$HOSTS ./distaix ../props/config.props ../props/model.props ${@:$((ARG_COUNTER+1))} 2> error
end_time=`date +%s%N`
echo execution time was `expr $end_time - $start_time` ns.
echo "######################## END ########################"

#############################################################################
# DistAIX Ubuntu 18.04 Dockerfile
#
# This Dockerfile builds an image based on Ubuntu 18.04 which contains all dependencies
# to build DistAIX
# However, DistAIX itself it not part of the image.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

FROM ubuntu:18.04

ARG GIT_REV=unknown
ARG GIT_BRANCH=unknown
ARG VERSION=unknown
ARG VARIANT=unknown

ENV DEBIAN_FRONTEND=noninteractive

# Toolchain
RUN apt-get update && apt-get install -y \
	make libc6-dev dpkg-dev git wget unzip \
	vim autoconf automake libtool \
	curl gcc-8 g++-8 cmake\
	pkg-config openssl

# Redirect gcc to gcc-8
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 700 --slave /usr/bin/g++ g++ /usr/bin/g++-7
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 800 --slave /usr/bin/g++ g++ /usr/bin/g++-8
RUN update-alternatives --set gcc /usr/bin/gcc-8 

# Dependencies of RepastHPC
RUN apt-get update && apt-get install -y \
	mpich libcurl4-gnutls-dev \
	libnetcdf-dev libnetcdf-c++4-1 \
	libnetcdf-c++4 libnetcdf-cxx-legacy-dev libnetcdf-c++4-dev

# Install Boost (Dependency of RepastHPC and DistAIX)
RUN cd /tmp && mkdir boost && cd boost && \
    wget https://sourceforge.net/projects/boost/files/boost/1.69.0/boost_1_69_0.tar.gz && \
    tar xf boost_1_69_0.tar.gz && \
    cd boost_1_69_0 && \
    ./bootstrap.sh --with-libraries=mpi,system,filesystem,serialization && \
    echo "using mpi ;" >> user-config.jam && \
    ./b2 --prefix=/usr --user-config=user-config.jam install || true && \
    ldconfig && \
    rm -rf /tmp/*

# Install Protobuf 3.3.2 (Dependency of DistAIX)
RUN cd /tmp && mkdir protobuf && cd protobuf && \
    git clone https://github.com/protocolbuffers/protobuf . && \
    git checkout 3.3.x && \
    git submodule update --init --recursive && \
    ./autogen.sh && \
    ./configure && \
    make -j$(nproc) && make install && \
    ldconfig && \
    rm -rf /tmp/*

# Dependencies of DBconnector Library
RUN apt-get update && apt-get install -y \
    libuv1-dev libssl-dev libpq-dev \
    postgresql postgresql-contrib postgresql-server-dev-10

# Install Cassandra cpp driver (Dependency of DBConnector Library)
RUN cd /tmp && mkdir cass && cd cass && \
    git clone https://github.com/datastax/cpp-driver.git . && \
    git checkout 2.10.0 && \
    mkdir build && cd build && \
    cmake .. && make -j$(nproc) && make install && \
    ldconfig && \
    rm -rf /tmp/*

# Dependencies of FBScomponent Library
RUN apt-get update && apt-get install -y \
    libgsl-dev

# Dependencies of VILLASnode Library
RUN apt-get update && apt-get install -y \
    libconfig-dev libnl-3-dev libnl-route-3-dev \
    libmosquitto-dev uuid-dev libuuid1 \
    libjansson-dev

# Install libwebsockets version 3.0 (VILLASnode Dependency)
RUN cd /tmp && mkdir libwebsockets && cd libwebsockets && \
    git clone https://github.com/warmcat/libwebsockets . && \
    git checkout v3.0-stable && \
    mkdir build && cd build && \
    cmake .. && make -j$(nproc) && make install && \
    ldconfig && \
    rm -rf /tmp/*

# Install nanomsg version 1.1.5 (VILLASnode and DistAIX dependency)
RUN cd /tmp && mkdir nanomsg && cd nanomsg && \
    wget -q https://github.com/nanomsg/nanomsg/archive/1.1.5.tar.gz && \
    tar -xzf 1.1.5.tar.gz && \
    cd nanomsg-1.1.5 && mkdir build && cd build && \
    cmake .. && cmake --build . --target install && \
    ldconfig && \
    rm -rf /tmp/*

# Build & Install fmtlib
RUN cd /tmp && \
	git clone --recursive https://github.com/fmtlib/fmt.git && \
	mkdir -p fmt/build && cd fmt/build && \
	git checkout 5.3.0 && \
	cmake -DBUILD_SHARED_LIBS=1 .. && make -j$(nproc) install && \
	rm -rf /tmp/*

# Build & Install spdlog
RUN cd /tmp && \
	git clone --recursive https://github.com/gabime/spdlog.git && \
	mkdir -p spdlog/build && cd spdlog/build && \
	git checkout v1.6.0 && \
	cmake -DSPDLOG_BUILD_SHARED=ON -DCMAKE_BUILD_TYPE=Release -DSPDLOG_FMT_EXTERNAL=1 -DSPDLOG_BUILD_BENCH=OFF .. && make -j$(nproc) install && \
	rm -rf /tmp/*

# set library environment variables
ENV LD_LIBRARY_PATH="/usr/local/lib:/usr/local/lib/x86_64-linux-gnu:/usr/local/lib64:/usr/lib"
ENV LIBRARY_PATH="/usr/local/lib:/usr/local/lib/x86_64-linux-gnu:/usr/local/lib64:/usr/lib"
ENV PATH="usr/local/bin:${PATH}"

WORKDIR /distaix
ENTRYPOINT bash

LABEL \
	org.label-schema.schema-version="1.0" \
	org.label-schema.name="DistAIX" \
	org.label-schema.license="GPL-3.0" \
	org.label-schema.vcs-ref="$GIT_REV" \
	org.label-schema.vcs-branch="$GIT_BRANCH" \
	org.label-schema.version="$VERSION" \
	org.label-schema.variant="$VARIANT" \
	org.label-schema.vendor="Institute for Automation of Complex Power Systems, RWTH Aachen University" \
	org.label-schema.description="An image containing all build-time dependencies for DistAIX based on Ubuntu" \
	org.label-schema.vcs-url="https://git.rwth-aachen.de/acs/public/simulation/DistAIXFramework/distaix" \
